package utils;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import game.board.PlanetModelI;

/**
 * Tests the Maploader
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class MapLoaderTest {
	MapLoader mapLoader = new MapLoader();

	// Test Planets
	/**
	 * Prints all loaded Planets 
	 * @throws Exception
	 */
	@Test
	public void testPlanets() throws Exception {
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapLoaderTest.txt"));
		ArrayList<PlanetModelI> planets = mapLoader.getPlanets();
		System.out.println("\n-PLANETS-");
		for (PlanetModelI p : planets) {
			System.out.println(p.getId() + "," + p.getColor() + "," + p.getPos()[0] + "," + p.getPos()[1]);
		}
	}

	// Test Map
	/**
	 * prints loaded map
	 * @throws Exception
	 */
	@Test
	public void testMap() throws Exception {
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapLoaderTest.txt"));
		ArrayList<ArrayList<Integer>> map = mapLoader.getNeighbors();
		System.out.println("--LoadedMap--");
		for (ArrayList<Integer> i : map) {
			System.out.println(i.toString());
		}

	}

	/**
	 * prints loaded undeads
	 * @throws Exception
	 */
	@Test
	public void testMapUndead() throws Exception {
		System.out.println("===========================================");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapLoaderUndead.txt"));
		ArrayList<PlanetModelI> planets = mapLoader.getPlanets();
		for (PlanetModelI p : planets) {
			System.out.println(
					p.getUndeads(0) + "," + p.getUndeads(1) + "," + p.getUndeads(2) + "," + p.getUndeads(3));
		}

	}

	// Test Exceptions
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testMAPMDException() throws Exception { // Multiple ID
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPMD2");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPMD.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPWCException() throws Exception { // Wrong Color
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPWCc");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPWC.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPXDException() throws Exception { // Wrong X Dimension
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPXD3");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPXD.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPYDException() throws Exception { // Wrong Y Dimension
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPYD3");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPYD.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPMSException() throws Exception { // Multiple StartPlanets
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPMS3");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPMS.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPNSException() throws Exception { // No StartPlanet
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPNS");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPNS.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPLPException() throws Exception { // No enough planets from each color
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPLP");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPLP.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPPIException() throws Exception { // parse int
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPPI");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPPI.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPFTException() throws Exception { // FromTo
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPFT");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPFT.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPPOException() throws Exception { // wrong file path
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPPO");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapExceptionMAPPO.txt"));
		fail("no Excpetion :(");
	}

	@Test
	public void testMAPFPException() throws Exception { // wrong file path
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPFP");
		mapLoader.loadMap(System.class.getResourceAsStream("sfefdsfsdfg"));
		fail("no Excpetion :(");
	}
	

	@Test
	public void testMAPNEException() throws Exception {
		thrown.expect(Exception.class);
		thrown.expectMessage("MAPNE");
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/UndeadTest.txt"));
		fail("no Excpetion :(");
	}

}
