package utils;

import java.io.File;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import game.GameModel;
import game.GameModelI;
import game.GameStatusModel;
import game.GameStatusModelI;
import start.ConfigModelI;
import start.ConfigModelStub;

/**
 * Tests the Save and Load Game function
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class SaveLoaderTest {
	private ConfigModelI cM;
	SaveLoader g = new SaveLoader();

	/**
	 * sets tests variables
	 */
	@Before
	public void before() {
		MapLoader mL = new MapLoader();

		try {
			mL.loadMap(System.class.getResourceAsStream("/maps/MapStandard.txt"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		cM = new ConfigModelStub(mL.getPlanets(), mL.getNeighbors());
	}
	
	/**
	 * loads a game 
	 * @throws Exception
	 */
	@Test
	public void testGameLoad() throws Exception {
		File savedGame = new File("src/test/java/utils/test.ser");
		g.load(savedGame);
	}
	
	/**
	 * saves a game
	 * @throws Exception
	 */
	@Test
	public void testGameSave() throws Exception{
		GameModelI gameModel =  new GameModel(cM.getPlayers());
		GameStatusModelI status = new GameStatusModel(42, 21);
		g.save("src/test/java/utils/test.ser", gameModel, status, cM);
	}

	/**
	 * saves a map with null, needed for a test below 
	 * @throws Exception
	 */
	@Test
	public void nullTest() throws Exception {
		g.save("src/test/java/utils/nullTest.ser",null, null, null);
	}
	
	//TEST EXCEPTIONS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void gLFilePath() throws Exception {
		thrown.expect(Exception.class);
		thrown.expectMessage("SLGFP");
		File savedGame = new File("qwertz");
		g.load(savedGame);
	}
	
	@Test
	public void gLNull() throws Exception {
		thrown.expect(Exception.class);
		thrown.expectMessage("SLGNL");
		File savedGame = new File("src/test/java/utils/nullTest.ser");
		g.load(savedGame);
	}
	
	@Test
	public void gLCorrupted() throws Exception {
		thrown.expect(Exception.class);
		thrown.expectMessage("SLGCF");
		File savedGame = new File("src/test/java/utils/corrupted.ser");
		g.load(savedGame);
	}
}
