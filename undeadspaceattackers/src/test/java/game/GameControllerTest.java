package game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import cards.Card;
import cards.CardController;
import cards.CardControllerI;
import cards.CardModel;
import cards.PlayerCard;
import game.board.BoardControllerI;
import game.board.BoardControllerStub;
import game.board.PlanetModelI;
import start.ConfigModelI;
import start.ConfigModelStub;
import utils.MapLoader;


/**
 * 	Tests the GameController
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class GameControllerTest {
	
	ConfigModelI cM;
	GameControllerI controller;
	GameModel model;
	BoardControllerI boardController;
	CardControllerI cardController;
	GameStatusModelI status;
	PlanetModelI p0, p1, p2;
	
	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		MapLoader mL = new MapLoader();

		try {
			mL.loadMap(System.class.getResourceAsStream("/maps/MapStandard.txt"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		cM = new ConfigModelStub(mL.getPlanets(), mL.getNeighbors());
		model = new GameModel(cM.getPlayers());
		boardController =  new BoardControllerStub(cM.getBoardModel());
		status = new GameStatusModel(2,10);
		cardController = new CardController(new CardModel(mL.getPlanets(), 4, cM.getPlayers()));
		controller =  new GameController(cM, model, status, cardController);
		controller.setBoardController(boardController);

		controller.initiateGame();
		p0 = boardController.getPlanet(0);
		p1 = boardController.getPlanet(1);
		p2 = boardController.getPlanet(2);
	}

	/**
	 * Tests the initiate Game Method
	 * game will be initiate twice in test, so the counters are * 2
	 */
	@Test
	public void initiateGame(){
		System.out.println("initateGame");

		controller.initiateGame();
		assertEquals(18,controller.getCardController().getDiscardPile().size());
		int i = 0;
		for(Iterator<PlanetModelI> iterator=cM.getBoardModel().getPlanets(); iterator.hasNext();){
			PlanetModelI p = iterator.next();
			if(p.hasUndead()) {
				System.out.println(p.getUndeads(1));
				i++;
			}
		}
		assertEquals(18, i);
	}
	
	@Test //TODO: make this complete new
	public void testNextTurn() {
//		System.out.println("NextTurn");
//		for(int i = 0; i < 20; i++) {
//			System.out.println("Player " + model.getCurrentPlayerId() + " | LA:" + model.getCurrentPlayer().getLeftActions());
//			controller.cardThrown(new PlayerCard(1,1,1), model.getCurrentPlayerId());
//			controller.endAction();
//		}
	}
	
	/**
	 *  tests addUndead method
	 */
	@Test
	public void addUndead() {
		for(int i=0; i<4; i++) {
			p0.removeAllUndead(i);
			p1.removeAllUndead(i);
		}
		controller.addUndead(0,0, true);
		controller.addUndead(0,0, true);
		controller.addUndead(0,0, true);
		assertEquals(0,p1.getUndeads(0));
		controller.addUndead(0,0, true);
		assertEquals(1,p1.getUndeads(0)); //neighbour planet1 gets undead because of conquest 
	}
	
	/**
	 * tests EndTurn
	 */
	@Test
	public void endTurnTest() {
		controller.addUndead(1, 0, true);;
		p1.setAbomination(true);
		p1.setPlayer(0, true);
		controller.endTurn();
	}
	
	/**
	 * tests MassAttack 
	 */
	@Test
	public void undeadMassAttackTest() {
		status.increaseUndeadStrengthMarker();
		status.increaseUndeadStrengthMarker();
		controller.undeadMassAttack();
	}
	
	/**
	 * tests flip player with no left action and no actions left, should 
	 */
	@Test
	public void flipPlayerCardTestNoAction() {
		model.getCurrentPlayer().decreaseLeftActions();
		model.getCurrentPlayer().decreaseLeftActions();
		model.getCurrentPlayer().decreaseLeftActions();
		model.getCurrentPlayer().decreaseLeftActions();
		assertNull(cardController.getTopPlayerCard());
		controller.flipPlayerCard();
		assertNotNull(cardController.getTopPlayerCard());
	}
	/**
	 * tests flip player card with left actions, should reduce one action
	 */
	@Test
	public void flipPlayerCardTestLeftActions() {
		model.getLastUsedCard().clear();
		model.getCurrentPlayer().resetLeftActions();
		assertEquals(5, model.getCurrentPlayer().getLeftActions());
		controller.flipPlayerCard();
		assertEquals(4, model.getCurrentPlayer().getLeftActions());
	}
	/**
	 * flips player card with left actions, card should be returned	 
	 */
	@Test
	public void flipPlayerCardTestLeftCards() {
		Card card = new PlayerCard(1,1,1);
		controller.cardThrown(card, 0);
		controller.flipPlayerCard();
		assertTrue(model.getCurrentPlayer().getHand().contains(card));
	}
	
	/**
	 *  takes player card
	 */
	@Test
	public void takePlayerCardTest() {
		status.resetCardCounters();
		cardController.flipTopPlayerCard();
		controller.takePlayerCard();
	}
	
	/**
	 * flips undead card
	 */
	@Test
	public void flipUndeadCardTest() {
		status.incPlayerCardCounter();
		status.incPlayerCardCounter();
		controller.flipUndeadCard();
	}
	
	//------------ ACTION METHODS TESTS ------------------//
	
	/**
	 * tests if base was build
	 */
	@Test
	public void buildBaseTest() {
		model.getCurrentPlayer().setPlanetId(2);
		controller.cardThrown(new PlayerCard(2,1,1), model.getCurrentPlayerId());
		controller.buildBase(2); 
		assertTrue(p2.hasBase());
	}
	
	
	/**
	 *  tests if player was movedbyspaceship
	 */
	@Test
	public void moveBySpaceShipTest() {
		p1.setBlocked(false);
		p0.setBlocked(false);
		model.getCurrentPlayer().setPlanetId(0);
		controller.moveSpaceship(0, 0, 1);
		assertEquals(1, model.getCurrentPlayer().getPlanetId());
	}

	/**
	 * tets if player was warped to base
	 */
	@Test
	public void warpToBaseTest() {
		model.getCurrentPlayer().setPlanetId(1);
		p1.setBase(true);
		p1.setBlocked(false);
		p2.setBase(true);
		p2.setBlocked(false);
		assertEquals(1, model.getCurrentPlayer().getPlanetId());
		controller.moveSpaceship(0, 1, 2);
		assertEquals(2, model.getCurrentPlayer().getPlanetId());
	}
	
	/**
	 * tets if player was warped to planet
	 */
	@Test
	public void warpToPlanetTest() {
		p0.setBlocked(false);
		boardController.getPlanet(3).setBlocked(false);
		
		model.getCurrentPlayer().setPlanetId(0);
		controller.cardThrown(new PlayerCard(3, 3, 3), model.getCurrentPlayerId());
		assertEquals(0, model.getCurrentPlayer().getPlanetId());
		controller.moveSpaceship(0, 0, 3);
		assertEquals(3, model.getCurrentPlayer().getPlanetId());
	}
	
	/**
	 * tests if card was shared
	 */
	@Test
	public void shareKnowledgeTest(){
		model.getPlayer(0).setPlanetId(1);
		model.getPlayer(1).setPlanetId(1);
		Card card = new PlayerCard(1,1,1);
		controller.shareKnowledge(card, 0, 1);
		model.getPlayer(1).getHand().contains(card);
	}
	
	/**
	 * tests if kill undead kills 1 undead
	 */
	@Test
	public void killUndeadTest() { //normal killing
		for(int i=0; i<4; i++) {
			p0.removeAllUndead(i);
		}
		model.setCurrentPlayer(1); //Warpmaster
		model.getCurrentPlayer().setPlanetId(0);
		controller.addUndead(0, 0, true);
		controller.addUndead(0, 0, true);
		assertEquals(2, p0.getUndeads(0));
		controller.killUndead(0, 0);
		assertEquals(1, p0.getUndeads(0));
	}
	
	/**
	 * tests if weapon kills all undeads
	 */
	@Test
	public void killUndeadWeaponTest() { //weapon developed
		status.developWeapon(0);
		model.setCurrentPlayer(1); //Warpmaster
		model.getCurrentPlayer().setPlanetId(0);
		
		controller.addUndead(0, 0, true);
		controller.addUndead(0, 0, true);
		controller.addUndead(0, 0, true);
		assertEquals(3, p0.getUndeads(0));
		controller.killUndead(0, 0);
		assertEquals(0, p0.getUndeads(0));
	}
	
	/**
	 * tests if exorcist kills all undeads
	 */
	@Test
	public void killUndeadExorcistTest() { //exorcist kills all
		model.setCurrentPlayer(0); //Exorcist
		model.getCurrentPlayer().setPlanetId(0);
		
		controller.addUndead(0, 0, true);
		controller.addUndead(0, 0, true);
		controller.addUndead(0, 0, true);
		assertEquals(3, p0.getUndeads(0));
		controller.killUndead(0, 0);
		assertEquals(0, p0.getUndeads(0));
	
	}
	
	/**
	 *  tests if dark column was removed
	 */
	@Test
	public void removeDarkColumnTest() {
		int dk = status.getDarkColumnPlanets().get(0);
		PlanetModelI p = boardController.getPlanet(dk);
		assertTrue(p.hasDarkColumn());
		model.getCurrentPlayer().setPlanetId(dk);
		controller.cardThrown(new PlayerCard(31,p.getColor(),1), model.getCurrentPlayerId());
		controller.cardThrown(new PlayerCard(33,p.getColor(),1), model.getCurrentPlayerId());
		controller.removeDarkColumn(dk);
		assertFalse(p.hasDarkColumn());
	}
	
	/**
	 * tests if weapon was developed
	 */
	@Test
	public void developWeaponTest() {
		model.getCurrentPlayer().setPlanetId(0);
		p0.setBase(true);
		for(int i = 0; i < 5; i++) {
			controller.cardThrown(new PlayerCard(i,0,0), model.getCurrentPlayerId());
		}
		controller.developWeapon(0);
		assertTrue(status.getDevelopedWeapons()[0]);
	} 
	
	/**
	 * tests if improved weapon was developed and kills properly 2 undeads
	 */
	@Test
	public void buyImprovedWeaponsTest() {
		model.getCurrentPlayer().setPlanetId(0);
		p0.setBase(true);
		controller.addUndead(0,0, true);
		controller.addUndead(0,0, true);
		controller.cardThrown(new PlayerCard(1,2,0), model.getCurrentPlayerId());
		controller.buyImprovedWeapon();
		assertEquals(3,model.getCurrentPlayer().getImprovedWeaponLeft());
		controller.killUndead(0, 0);
		assertEquals(0,p0.getUndeads(0));
	}
	
	/**
	 * tests if resource was mined
	 */
	@Test
	public void mineResourceTest() {
		model.getCurrentPlayer().setPlanetId(2);
		assertFalse(model.getCurrentPlayer().getResource(0));
		p2.setResource(0);
		assertTrue(	p2.hasResource(0));
		controller.mineResource(0, 2);
		assertTrue(model.getCurrentPlayer().getResource(0));
	}

	/**
	 * tests if healer was recruited
	 */
	@Test
	public void recruiteHealer() {
		model.getCurrentPlayer().setResource(0);		
		model.getCurrentPlayer().setPlanetId(0);
		p0.setBase(true);
		assertFalse(p0.hasHealer(0));
		controller.recruiteHealer(0);
		assertTrue(p0.hasHealer(0));
	}
	
	/**
	 * tests if plage was healed
	 */
	@Test
	public void healPlaque() {
		model.getCurrentPlayer().setPlanetId(0);
		model.getCurrentPlayer().setPlague(0);
		p0.setBase(true);
		p0.setHealer(0);
		assertTrue(model.getCurrentPlayer().hasPlague(0));
		controller.healPlague(0);
		assertFalse(model.getCurrentPlayer().hasPlague(0));
	}
	
	
	
	//----------------------------------------------------------------------//	
}
