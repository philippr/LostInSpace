package game.board;

import java.util.ArrayList;

import game.board.BoardControllerI;
import game.board.BoardModelI;
import game.board.PlanetModelI;

public class BoardControllerStub implements BoardControllerI {
	
	private BoardModelI model;
	
	
	public BoardControllerStub(BoardModelI boardModel) {
		
		model = boardModel;
	}

	@Override
	public ArrayList<Integer> getNeighbours(int planetId) {
		return model.getNeighbors(planetId);
	}

	@Override
	public PlanetModelI getPlanet(int planetId) {
		return model.getPlanet(planetId);
	}

	@Override
	public int getStartPlanet() {
		return 1;
	}

	@Override
	public void connectAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectUndead(UndeadView undead, UndeadGrid parent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBoardView(BoardView boardView) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBoardModel(BoardModelI boardModel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMovedAndDraged() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scroll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragReleased() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerSpaceShips() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void planetViews() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showMessage(String s) {
		// TODO Auto-generated method stub
		
	}

}
