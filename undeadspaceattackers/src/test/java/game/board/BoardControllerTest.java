package game.board;

import java.util.ArrayList;

import org.junit.Before;

import game.GameControllerStub;
import game.antagonist.Abomination;
import utils.MapLoader;


public class BoardControllerTest { //TODO RENAME TO ABOMINATION TEST
	
	BoardControllerI bC;
	GameControllerStub gC;
	ArrayList<Integer> abominations = new ArrayList<>();
	ArrayList<ArrayList<Integer>> map;
	ArrayList<PlanetModelI> planets = new ArrayList<>();
	Abomination abomination;
	
	@Before
	public void before() {
		MapLoader mL = new MapLoader();

		try {
			mL.loadMap(System.class.getResourceAsStream("/maps/MapStandard.txt"));
			planets= mL.getPlanets();
			map = mL.getNeighbors();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		gC = new GameControllerStub();
		bC = new BoardController(gC, null, null, null);
		BoardModelStub bM = new BoardModelStub(planets, map);
		bC.setBoardModel(bM);
		abominations.add(0);
		planets.get(0).setAbomination(true);
		
		abomination = new Abomination(bC);
		
		
	}

}
