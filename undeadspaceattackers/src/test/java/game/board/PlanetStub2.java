package game.board;

public class PlanetStub2 extends PlanetStub {
	int id = 2;
	
	@Override
	public int getId() {
		return id;
	}
	@Override
	public boolean hasPlayers() {
		return false;
	}


}
