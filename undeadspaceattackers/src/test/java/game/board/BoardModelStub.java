package game.board;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;


@SuppressWarnings("serial")
public class BoardModelStub implements BoardModelI, Serializable {
	ArrayList<PlanetModelI> planets;
	ArrayList<ArrayList<Integer>> map;
	
	
	public BoardModelStub(ArrayList<PlanetModelI> planets, ArrayList<ArrayList<Integer>> map) {
		this.map = map;
		this.planets = planets;
	}
	
	@Override
	public ArrayList<Integer> getNeighbors(int planetId) {
		return map.get(planetId);
	}

	@Override
	public void setNeighbors(ArrayList<ArrayList<Integer>> map) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PlanetModelI getPlanet(int planetId) {
		return planets.get(planetId);
	}

	@Override
	public int getStartPlanet() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setStartPlanet(int startPlanet) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setPlanets(ArrayList<PlanetModelI> planets) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	public ArrayList<PlanetModelI> getPlanetsList() {
		return planets;
	}
	
	
	@Override
	public Iterator<PlanetModelI> getPlanets() {
		return planets.iterator();
	}

	@Override
	public int getOffsetX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getOffsetY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getnPlanets() {
		// TODO Auto-generated method stub
		return planets.size();
	}

}
