package game.board;

import java.util.Observer;

public class PlanetStub implements game.board.PlanetModelI {
	public int[] undeads = {0,1,0,0};
	public boolean[] players = {true,true,true,true};
	private int id;
	private boolean[] ressource = {false,false,false,false};
	private boolean[] healer = {false,false,false,false};

	private boolean hasAbomination = false;

	private boolean hasBase = false;
	private boolean isBlocked = false;
	private boolean hasDarkColumn = false;
	private boolean showName = false;
	
	
	@Override
	public int getId() {
		return id;
	}


	@Override
	public int getUndeads(int colorId) {
		return undeads[colorId];
	}

	@Override
	public boolean[] getPlayers() {
		return players;
	}

	@Override
	public void setUndeads(int[] undeads) {
		this.undeads = undeads;
	}

	@Override
	public void addUndead(int color, boolean notify) {
		undeads[color]++;
	}

	@Override
	public void removeUndead(int color, boolean propagate) {
		undeads[color]--;
	}

	@Override
	public void removeAllUndead(int color) {
		undeads[color] = 0;
	}
	
	@Override
	public boolean hasUndead() {
		for (int i = 0; i < 4; i++) {
			if (undeads[i] > 0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setPlayer(int playerId, boolean isPlayer) {
		this.players[playerId] = isPlayer;
	}

	

	@Override
	public boolean hasHealer(int colorId) {
		return healer[colorId];
	}

	@Override
	public void setHealer(int colorId) {
		this.healer[colorId] = true;
	}

	@Override
	public void setResource(int resourceColor) {
		this.ressource[resourceColor] = true;
	}

	@Override
	public boolean hasResource(int colorId) {
		return ressource[colorId];
	}

	@Override
	public boolean hasPlayers() {
		return true;
	}

	@Override
	public boolean hasPlayer(int playerId) {
		return players[playerId];
	}

	@Override
	public void setAbomination(boolean has) {
		hasAbomination = has;
	}

	@Override
	public void setBase(boolean has) {
		hasBase = has;	
	}

	@Override
	public void setBlocked(boolean is) {
		isBlocked = is;		
	}

	@Override
	public void setDarkColumn(boolean has) {
		hasDarkColumn = has;
	}

	@Override
	public void showName(boolean show) {
		showName = show;
		
	}

	@Override
	public boolean hasAbomination() {
		return hasAbomination;
	}

	@Override
	public boolean hasBase() {
		return hasBase;
	}

	@Override
	public boolean isBlocked() {
		return isBlocked;
	}

	@Override
	public boolean hasDarkColumn() {
		return hasDarkColumn;
	}

	@Override
	public boolean showName() {
		return showName;
	}


	@Override
	public int getColor() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int[] getPos() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int getNUndeads() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void setColor(int colorId) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void removeRandomUndead() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addObserverX(Observer planetView) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}
}
