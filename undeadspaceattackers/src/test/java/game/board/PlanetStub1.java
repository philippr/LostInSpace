package game.board;

public class PlanetStub1 extends PlanetStub {
	
	@Override
	public int getId() {
		return 1;
	}
	
	@Override
	public int getColor() {
		return 1;
	}
}
