package game;

import cards.Card;
import cards.CardControllerI;
import game.board.BoardControllerI;
import game.board.UndeadView;

public class GameControllerStub implements GameControllerI {

	@Override
	public void setCardController(CardControllerI cardController) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CardControllerI getCardController() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setBoardController(BoardControllerI boardControllerI) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initiateGame() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean cardThrown(Card card, int playerId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void flipPlayerCard() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void takePlayerCard() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void flipUndeadCard() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void takeUndeadCard() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void undeadMassAttack() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endGame(boolean isWon) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endTurn() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void endAction(boolean decreaseAction) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean mayDragUndead() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean buildBase(int planetId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean moveSpaceship(int actionPlayerId, int startPlanetId, int endPlanetId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean shareKnowledge(Card card, int givePlayerId, int getPlayerId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean killUndead(int planetId, int undeadColor) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeDarkColumn(int planetId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean developWeapon(int weaponColor) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean buyImprovedWeapon() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mineResource(int colorId, int planetId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean recruiteHealer(int colorId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean healPlague(int colorId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean moveUndead(int colorId, int sPlanetId, int ePlanetId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void saveGame(String path) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleException(Exception e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exorcistKills(int planetId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectUndead(UndeadView uV) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addUndead(int planetId, int colorId, boolean silent) {
		// TODO Auto-generated method stub
		
	}

}
