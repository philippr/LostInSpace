package game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import cards.CardControllerStub;
import game.antagonist.Abomination;
import game.board.BoardControllerI;
import game.board.BoardControllerStub;
import game.board.PlanetModelI;
import start.ConfigModelI;
import start.ConfigModelStub;
import utils.MapLoader;

/**
 * Tests Abomination
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class AbominationTest {
	
	ConfigModelI cM;
	GameControllerI gameController;
	GameModelI model;
	BoardControllerI boardController;
	GameStatusModelI status;
	ArrayList<Integer> abominations = new ArrayList<>();
	ArrayList<ArrayList<Integer>> neighbors;
	ArrayList<PlanetModelI> planets = new ArrayList<>();
	Abomination abomination;

	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		MapLoader mL = new MapLoader();

		try {
			mL.loadMap(System.class.getResourceAsStream("/maps/MapStandard.txt"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		planets= mL.getPlanets();
		neighbors = mL.getNeighbors();
		cM = new ConfigModelStub(mL.getPlanets(), mL.getNeighbors());
		model = new GameModel(cM.getPlayers());
		status = new GameStatusModel(2,10);
		boardController = new BoardControllerStub(cM.getBoardModel());
		gameController =  new GameController(cM, model, status, new CardControllerStub());
		gameController.setBoardController(boardController);

		abominations.add(0);
		planets.get(0).setAbomination(true);
		
		abomination = new Abomination(abominations, boardController);
	}
	
	/**
	 * tests abomination
	 */
	@Test
	public void abominationTest() {

		PlanetModelI p = boardController.getPlanet(2);
		p.setPlayer(0, true);
		model.getPlayer(0).setPlanetId(2);

		PlanetModelI aP = boardController.getPlanet(3);
		aP.setAbomination(true);
		abomination.addAbomination(3);
		abomination.moveNew();
		abomination.moveAllAbomination();

		System.out.println(abomination.getAbominations().get(0));
		assertTrue(boardController.getPlanet(2).hasAbomination()); //TODO fails sometime if the abominations decides to take a different way
	}
	

	@Test
	public void testMoveAllAbomination() { //Abomination on Planet 0; Player on 1
		planets.get(1).setPlayer(1, true);
		abomination.moveNew();
		abomination.moveAllAbomination();
		assertEquals(1, (int) abominations.get(0));
		assertTrue(planets.get(1).hasAbomination());
	}
	
	@Test
	public void testMoveAboHasPlayer() { //Abomination on Planet 0; Player on 0
		planets.get(0).setPlayer(0, true);
		abomination.moveAllAbomination();
		assertEquals(0, (int) abominations.get(0));
		assertTrue(planets.get(0).hasAbomination());
	}
	
}
