package start;

import java.io.Serializable;
import java.util.ArrayList;

import cards.CardModel;
import cards.CardModelI;
import game.GameStatusModel;
import game.GameStatusModelI;
import game.board.BoardModelI;
import game.board.BoardModelStub;
import game.board.PlanetModelI;
import player.PlayerI;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;
import player.stubs.PlayerStubWarpmaster;
import player.stubs.PlayerStubWeapontrader;

/**
 *  ConfigModel Stub
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class ConfigModelStub implements ConfigModelI, Serializable {
	BoardModelI boardModel;
	
	public ConfigModelStub(ArrayList<PlanetModelI> planets, ArrayList<ArrayList<Integer>> map) {
		boardModel =  new BoardModelStub(planets, map);
	}
	
	@Override
	public int getnMassAttackCards() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public boolean isEvilAI() {
		return true;
	}
	
	
	@Override
	public boolean isDarkColumns() {
		return true;
	}

	@Override
	public boolean isAbomination() {
		return true;
	}
	
	@Override
	public boolean isImprovedWeapons() {
		return true;
	}
	
	@Override
	public ArrayList<PlayerI> getPlayers() {
		ArrayList<PlayerI> players =  new ArrayList<PlayerI>(4);
		players.add(new PlayerStubExorcist());
		players.add(new PlayerStubWarpmaster());
		players.add(new PlayerStubWeapontrader());
		players.add(new PlayerStubLieutenant());
//		players.add(new PlayerStubUntoaster());
		return  players;
	}
	@Override
	public void setPlayers(ArrayList<PlayerI> players) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int getnPlayers() {
		return 4;
	}
	@Override
	public void setnPlayers(int nPlayers) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public BoardModelI getBoardModel() {
		return boardModel;
	}
	@Override
	public void setBoardModel(BoardModelI boardModel) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public GameStatusModelI getGameStatusModel() {
		return new GameStatusModel(1,7); 
	}
	@Override
	public void setGameStatusModel(GameStatusModelI gameStatusModel) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public boolean isPlague() {
		return true;
	}

	@Override
	public int[] getRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRoles(int[] roles) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public ArrayList<PlayerI> getAllPlayers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CardModel getCardModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSettingsNo(int newNumber, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFlags(int index, boolean flags) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCardModel(CardModelI cardModel) {
		// TODO Auto-generated method stub
		
	}

}
