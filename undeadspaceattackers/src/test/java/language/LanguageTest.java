package language;

import org.junit.Before;
import org.junit.Test;

/**
 * Runs the language class, just checking if no exception is thrown
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class LanguageTest {

	Language l;
	
	@Before
	public void before() {
		l = Language.getInstance();
	}
	
	@Test
	public void testGetInstance() {
		Language.getInstance();
	}

	@Test
	public void testLoadDictMainMenu() {
		l.loadDictMainMenu();
	}

	@Test
	public void testLoadDictSelection() {
		l.loadDictSelection();
	}

	@Test
	public void testLoadDictSettings() {
		l.loadDictSettings();
	}

	@Test
	public void testLoadDictInfoView() {
		l.loadDictInfoView();
	}

	@Test
	public void testLoadDictEndView() {
		l.loadDictEndView();
	}

	@Test
	public void testLoadDictExceptions() {
		l.loadDictExceptions();
	}

	@Test
	public void testTranslate() {
		l.translate(1);
	}

	@Test
	public void testGetText() {
		l.getText("ROLLA");
	}

	@Test
	public void testGetLanguage() {
		l.getLanguage();
	}

}
