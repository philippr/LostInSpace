package mapGenerator;

import java.net.URL;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import game.board.BoardModelI;
import game.board.BoardModelStub;
import game.board.PlanetModelI;
import utils.MapLoader;

public class GeneratorControllerTest {
	BoardModelI boardModel;
	MapLoader mapLoader;

	@SuppressWarnings("unused")
	@Before
	public void before() throws Exception {
		ArrayList<ArrayList<Integer>> map = new ArrayList<>(4);
		for (int i = 0; i < 4; i++) {
			map.add(new ArrayList<>());
			map.get(i).add(i + 1);
		}
		URL url = getClass().getResource("/maps/testMaps/MapLoaderTest.txt");
		mapLoader = new MapLoader();
		mapLoader.loadMap(System.class.getResourceAsStream("/maps/testMaps/MapLoaderTest.txt"));
		ArrayList<PlanetModelI> planets = mapLoader.getPlanets();

		for (PlanetModelI planet : planets) {
			for (int i = 0; i < 4; i++) {
				for(int j = 0; j<planet.getId() % 4; j++)
				planet.addUndead(i, true);
			}
		}

		boardModel = new BoardModelStub(planets, map);
		boardModel.setStartPlanet(0);
	}

	@SuppressWarnings("unused")
	@Test
	public void testWriteMap() throws Exception {
		GeneratorModel model = new GeneratorModel();
		GeneratorController gen = new GeneratorController(null, model, null);
		//TODO
//		URL url = getClass().getResource("/maps/testMaps/testWrite.txt");
//		mapLoader.loadMap(url.getPath());
//	
	}

}
