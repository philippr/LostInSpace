package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import cards.PlayerCard;
import cards.UndeadCard;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;
import player.stubs.PlayerStubWarpmaster;

/**
 * Tests RemoveDarkColumn Action in Role
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class RoleRemoveDarkColumnTest {
	PlayerStubExorcist exorcist;
	PlayerStubLieutenant lieutenant;
	PlayerStubWarpmaster warpmaster;
	PlanetModelI p1;
	PlanetModelI p2;
	ArrayList<Card> lastUsedCard;
	
	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();
		warpmaster = new PlayerStubWarpmaster();
		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
		lastUsedCard = new ArrayList<Card>();
	}
	
	/**
	 * tests remove darkcolumn, needs action, same planet, 2 cards thrown, same cardColor
	 * @throws Exception
	 */
	@Test
	public void removeDarkColumnTest() throws Exception {
		lastUsedCard.add(new PlayerCard(1,1,1));
		lastUsedCard.add(new PlayerCard(2,1,1));
		assertTrue(exorcist.getRole().removeDarkColumn(exorcist, p1, lastUsedCard));
	}

	//EXCEPTIONSTESTS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test 
	public void rdkROLLA() throws Exception { //ROLLA
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().removeDarkColumn(exorcist, p1, lastUsedCard);
	}
	
	@Test 
	public void rdkROLPL() throws Exception { //ROLPL
		exorcist.resetLeftActions();
		exorcist.setPlanetId(3);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().removeDarkColumn(exorcist, p1, lastUsedCard);
	}

	@Test 
	public void rdkROLNC() throws Exception { //ROLNC
		exorcist.resetLeftActions();
		lastUsedCard.add(new PlayerCard(1,1,1));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		exorcist.getRole().removeDarkColumn(exorcist, p1, lastUsedCard);
	}

	@Test 
	public void rdkROLCT() throws Exception { //ROLCT
		exorcist.resetLeftActions();
		lastUsedCard.add(new UndeadCard(1,1,1));
		lastUsedCard.add(new UndeadCard(1,1,1));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		exorcist.getRole().removeDarkColumn(exorcist, p1, lastUsedCard);
	}
	
	@Test 
	public void rdkROLCC() throws Exception { //ROLCC
		exorcist.resetLeftActions();
		lastUsedCard.add(new PlayerCard(1,1,1));
		lastUsedCard.add(new PlayerCard(1,3,1));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCC");
		exorcist.getRole().removeDarkColumn(exorcist, p1, lastUsedCard);
	}
}
