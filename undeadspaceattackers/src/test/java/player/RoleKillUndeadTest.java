package player;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import game.board.PlanetStub1;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubWarpmaster;

/**
 *	Tests KillUndead Action in Role and Exorcists 
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class RoleKillUndeadTest {
	PlayerStubWarpmaster warpmaster;
	PlayerStubExorcist exorcist;
	PlanetStub1 planet1;
	
	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		warpmaster = new PlayerStubWarpmaster();
		exorcist = new PlayerStubExorcist();
		planet1 = new PlanetStub1();
		warpmaster.setPlanetId(1);
	}
	
	/**
	 * tests killundead with normal player, needs left action, same planet
	 * @throws Exception
	 */
	@Test
	public void killUndeadTest() throws Exception {
		assertTrue(warpmaster.getRole().killUndead(warpmaster, planet1, 1));		
	}
	
	/**
	 * tets killundead with exorcist, same conditions
	 * @throws Exception
	 */
	@Test
	public void killUndeadTestExorcist() throws Exception {
		assertTrue(exorcist.getRole().killUndead(exorcist, planet1, 1));		
	}
	
	//EXCEPTIONTESTS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test 
	public void killUndeadROLLA() throws Exception { //ROLLA
		warpmaster.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		killUndeadTest();
	}	
	
	@Test 
	public void exorcistROLLA() throws Exception { //ROLLA
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		killUndeadTestExorcist();
	}
	
	@Test
	public void killlUndeadROLPL() throws Exception { //ROLPL
		warpmaster.setPlanetId(42);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		killUndeadTest();		
	}
	
	@Test
	public void exorcistROLPL() throws Exception { //ROLPL
		exorcist.setPlanetId(42);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		killUndeadTestExorcist();
	}
	
	@Test
	public void killlUndeadROLUC() throws Exception { //ROLUC
		planet1.undeads[1] =  0;		
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLUC");
		killUndeadTest();		
	}
	
	@Test
	public void exorcistROLUC() throws Exception { //ROLUC
		planet1.undeads[1] =  0;		
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLUC");
		killUndeadTestExorcist();
	}
	
}
