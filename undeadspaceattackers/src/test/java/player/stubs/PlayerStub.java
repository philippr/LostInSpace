package player.stubs;

import java.io.Serializable;
import java.util.Observer;

import cards.Card;
import player.PlayerI;
import player.Role;
import utils.Hand;


/**
 *	general player stub
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class PlayerStub implements PlayerI, Serializable {

	private int leftActions = 4;
	private int planetId;
	private int improvedWeaponLeft;
	private Hand playerHand = new Hand();
	private boolean[] resource = {false,false,false,false};
	private boolean[] plague = {false,false,false,false};
	
	/* (non-Javadoc)
	 * @see player.PlayerI#getRole()
	 */
	@Override
	public Role getRole() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#getPlanetId()
	 */
	@Override
	public int getPlanetId() {
		return planetId;
	}
	
	/* (non-Javadoc)
	 * @see player.PlayerI#getLeftActions()
	 */
	@Override
	public int getLeftActions() {
		return leftActions;
	}

	/**
	 * sets the left actions
	 * @param leftActions
	 */
	public void setLeftActions(int leftActions) {
		this.leftActions  = leftActions;
	}
	
	/* (non-Javadoc)
	 * @see player.PlayerI#decreaseLeftActions()
	 */
	@Override
	public void decreaseLeftActions() {
		leftActions--;
	}
	
	/* (non-Javadoc)
	 * @see player.PlayerI#resetLeftActions()
	 */
	@Override
	public void resetLeftActions() {
		leftActions = 5;
	}


	/* (non-Javadoc)
	 * @see player.PlayerI#setPlanetId(int)
	 */
	@Override
	public void setPlanetId(int id) {
		planetId = id;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#setImprovedWeapon()
	 */
	@Override
	public void setImprovedWeapon() {
		improvedWeaponLeft = 3;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#getImprovedWeaponLeft()
	 */
	@Override
	public int getImprovedWeaponLeft() {
		return improvedWeaponLeft;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#getColorId()
	 */
	@Override
	public int getColorId() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#setResource(int)
	 */
	@Override
	public void setResource(int resource) {
		this.resource [resource] = true;
	}




	/* (non-Javadoc)
	 * @see player.PlayerI#getPlague(int)
	 */
	@Override
	public boolean hasPlague(int colorId) {
		return plague[colorId];
	}


	/* (non-Javadoc)
	 * @see player.PlayerI#setPlague(int)
	 */
	@Override
	public void setPlague(int colorId) {
		this.plague[colorId] = true;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#removePlague(int)
	 */
	@Override
	public void removePlague(int colorId) {
		this.plague[colorId] = false;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#getResource(int)
	 */
	@Override
	public boolean getResource(int colorId) {
		return resource[colorId];
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#removeResource(int)
	 */
	@Override
	public void removeResource(int colorId) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#useImprovedWeapon()
	 */
	@Override
	public void useImprovedWeapon() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#removeCard(cards.Card)
	 */
	@Override
	public void removeCard(Card card) {
		// TODO Auto-generated method stub
		
	}



	/* (non-Javadoc)
	 * @see player.PlayerI#addObserverX(java.util.Observer)
	 */
	@Override
	public void addObserverX(Observer playerPanel) {
		// TODO Auto-generated method stub
		
	}


	/* (non-Javadoc)
	 * @see player.PlayerI#killPlayer()
	 */
	@Override
	public void killPlayer() {
		// TODO Auto-generated method stub
		
	}
	
	/* (non-Javadoc)
	 * @see player.PlayerI#getHand()
	 */
	public Hand getHand() {
		return playerHand;
	}
	
	/* (non-Javadoc)
	 * @see player.PlayerI#setHand(java.util.LinkedList)
	 */
	public void setHand(Hand playerHand) {
		this.playerHand  = playerHand;
	}
	
	/* (non-Javadoc)
	 * @see player.PlayerI#addCard(cards.Card)
	 */
	public void addCard(Card card) {
		this.playerHand.add(card);
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#getId()
	 */
	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setId(int playerId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getLastPlanetId() {
		// TODO Auto-generated method stub
		return 0;
	}

	

}
