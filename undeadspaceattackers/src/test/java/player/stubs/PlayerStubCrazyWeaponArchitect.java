package player.stubs;

import player.CrazyWeaponArchitect;
import player.Role;

/**
 * PlayerStub with Role CrazyWeaponArchitect
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class PlayerStubCrazyWeaponArchitect extends PlayerStub {

	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getRole()
	 */
	@Override
	public Role getRole() {
		return new CrazyWeaponArchitect();
	}
	
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getId()
	 */
	@Override
	public int getId() {
		return 0;
	}
}
