package player.stubs;

import player.Role;
import player.Warpmaster;

/**
 * PlayerStub with Role Warpmaster 
 *  @author Philipp, Fabian, Julia, Ronja

 */
@SuppressWarnings("serial")
public class PlayerStubWarpmaster extends PlayerStub{
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getRole()
	 */
	@Override
	public Role getRole() {
		return new Warpmaster();
	}	
	
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getId()
	 */
	@Override
	public int getId() {
		return 1;
	}
}
