package player.stubs;

import player.Lieutenant;
import player.Role;

/**
 * PlayerStub with Role Lieutenant
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class PlayerStubLieutenant extends PlayerStub{

	private int leftActions = 5;

	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getRole()
	 */
	@Override
	public Role getRole() {
		return new Lieutenant();
	}	
	
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getLeftActions()
	 */
	@Override
	public int getLeftActions() {
		return leftActions;
	}

	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#setLeftActions(int)
	 */
	public void setLeftActions(int leftActions) {
		this.leftActions  = leftActions;
	}
	
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#decreaseLeftActions()
	 */
	@Override
	public void decreaseLeftActions() {
		leftActions--;
	}
	
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getId()
	 */
	@Override
	public int getId() {
		return 3;
	}



	
	

}
