package player.stubs;

import player.Exorcist;
import player.Role;

/**
 * PlayerStub with Role Exorcist
  *  @author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public class PlayerStubExorcist extends PlayerStub {
	
	int planetId = 1;

	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getPlanetId()
	 */
	public int getPlanetId() {
		return planetId;
	}

	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#setPlanetId(int)
	 */
	public void setPlanetId(int planetId) {
		this.planetId = planetId;
	}

	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getRole()
	 */
	@Override
	public Role getRole() {
		return new Exorcist();
	}
	
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getId()
	 */
	@Override
	public int getId() {
		return 0;
	}
}
