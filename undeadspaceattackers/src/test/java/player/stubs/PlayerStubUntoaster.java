package player.stubs;

import player.Role;
import player.Untoaster;

/**
 *  PlayerStub with Role Untoaster 
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class PlayerStubUntoaster extends PlayerStub {

	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getRole()
	 */
	@Override
	public Role getRole() {
		return new Untoaster();
	}
}