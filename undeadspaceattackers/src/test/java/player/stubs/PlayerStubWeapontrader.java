package player.stubs;

import player.Role;
import player.Weapontrader;

/**
 * PlayerStub with Role Weapontrader
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class PlayerStubWeapontrader extends PlayerStub  {
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getRole()
	 */
	@Override
	public Role getRole() {
		return new Weapontrader();
	}
	/* (non-Javadoc)
	 * @see player.stubs.PlayerStub#getId()
	 */
	@Override
	public int getId() {
		return 2;
	}
}
