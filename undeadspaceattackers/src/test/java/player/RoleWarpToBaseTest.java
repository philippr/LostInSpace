package player;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import game.board.PlanetStub1;
import game.board.PlanetStub2;
import game.board.PlanetStub3;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubWarpmaster;


/**
 * Tests WarpToBase Action in Role and Warpmaster
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class RoleWarpToBaseTest {

	PlayerStubExorcist exorcist;
	PlayerStubWarpmaster warpmaster;
	PlanetStub1 p1;
	PlanetStub2 p2;
	PlanetStub3 p3;

	/**
	 * sets stub variables
	 */
	@Before
	public void before(){
		exorcist = new PlayerStubExorcist();
		warpmaster = new PlayerStubWarpmaster();
		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
		p3 = new PlanetStub3();

		p2.setBase(true);
		p3.setBase(true);
	}

	/**
	 * Tests WarpToBase with normal player, needs base, left action, not blocked,  
	 * @throws Exception
	 */
	@Test
	public void WarpToBaseTest() throws Exception {
		exorcist.setPlanetId(2);
		assertTrue(exorcist.getRole().warpToBase(exorcist, exorcist, p2, p3));	
	}
	
	/**
	 * tests WarpToBase with Warpmaster, can warp any player
	 * @throws Exception
	 */
	@Test
	public void WarpToBaseWarpmaster() throws Exception { //Warpmaster Test
		exorcist.setPlanetId(2);
		assertTrue(warpmaster.getRole().warpToBase(warpmaster, exorcist, p2, p3));	
	}

	//ExceptionTests

	@Rule
	public ExpectedException thrown = ExpectedException.none();


	@Test
	public void WarpToBaseROLPL() throws Exception { //Role wrong planet
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().warpToBase(exorcist, exorcist, p2, p3);
		fail("exception expectet");
	}
	
	@Test
	public void warpmasterROLPL() throws Exception { //Role wrong planet
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		warpmaster.getRole().warpToBase(warpmaster, exorcist, p2, p3);
		fail("exception expectet");
	}
	
	@Test
	public void WarpToBaseROLNB() throws Exception { //Role no base
		exorcist.resetLeftActions();
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNB");
		exorcist.getRole().warpToBase(exorcist, exorcist, p1, p2);
		fail("exception expectet");
	}
	
	@Test
	public void WarpToBaseROLLA() throws Exception { //Role no base
		exorcist.setLeftActions(0);
		p1.setBase(true);
		p1.setBase(true);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().warpToBase(exorcist, exorcist, p1, p2);
		fail("exception expectet");
	}
	
	@Test
	public void WarpToBaseTestROLBL() throws Exception {
		exorcist.setPlanetId(2);
		p3.setBlocked(true);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLBL");
		exorcist.getRole().warpToBase(exorcist, exorcist, p2, p3);	
		fail("exception expectet");
	}	
	
	@Test
	public void warpmasterROLBL() throws Exception {
		exorcist.setPlanetId(2);
		p3.setBlocked(true);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLBL");
		warpmaster.getRole().warpToBase(warpmaster, exorcist, p2, p3);	
		fail("exception expectet");
	}
	
	
	
	@Test
	public void warpmasterROLLA() throws Exception { //Warpmastertest: no left action
		warpmaster.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		warpmaster.getRole().warpToBase(warpmaster, exorcist, p1, p2);
		fail("expected exception");
	}
	
	@Test
	public void wrpmasterROLNB() throws Exception { //Warpmaster: Role no base
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNB");
		warpmaster.getRole().warpToBase(warpmaster, warpmaster, p1, p2);
		fail("exception expectet");
	}
}
