package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;

/**
 *  Tests recruite Healer Action in Role
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class RoleRecruiteHealerTest {
	PlayerStubExorcist exorcist;
	PlayerI lieutenant;
	PlanetModelI p1;
	PlanetModelI p2;
	ArrayList<Card> lastUsedCard;

	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();
		p1 = new PlanetStub1();
		p1.setBase(true);
		p2 = new PlanetStub2();
		p2.setBase(false);
	}	
	
	/**
	 * tests recruite Healer, needs base, same planet, mined resource
	 * @throws Exception
	 */
	@Test
	public void recruiteHealerTest() throws Exception {
		exorcist.resetLeftActions();
		exorcist.setResource(1);
		assertTrue(exorcist.getRole().recruiteHealer(exorcist,p1, 1));
	}

	//EXCEPTIONSTESTS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void recruiteHealerROLNB() throws Exception { //ROLNB no base
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNB");
		exorcist.getRole().recruiteHealer(exorcist, p2, 0);
		fail("expcetion expectet :(");
	}

	@Test
	public void recruiteHealerROLPL() throws Exception { //ROLPL wrong planet
		exorcist.resetLeftActions();
		exorcist.setPlanetId(42);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().recruiteHealer(exorcist, p1, 0);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void recruiteHealerROLRS() throws Exception { //ROLRS no resource on planet
		exorcist.resetLeftActions();
		p1.setResource(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLRS");
		exorcist.getRole().recruiteHealer(exorcist, p1, 3);
		fail("expcetion expectet :(");
	}
}
