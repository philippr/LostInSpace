package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import cards.FindWarptor;
import cards.PlayerCard;
import cards.UndeadCard;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import game.board.PlanetStub3;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubWarpmaster;

/**
 * Tests WarpToPlanet Action in Role and Warpmaster
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class RoleWarpToPlanet {
	PlayerStubExorcist exorcist;
	PlayerStubWarpmaster warpmaster;
	PlanetModelI p1;
	PlanetModelI p2;
	PlanetModelI p3;
	ArrayList<Card> lastUsedCard;

	/**
	 * sets stub variables
	 */
	@Before
	public void before(){
		exorcist = new PlayerStubExorcist();
		warpmaster = new PlayerStubWarpmaster();

		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
		p3 = new PlanetStub3();
		lastUsedCard = new ArrayList<Card>();
	}

	/**
	 * Tests warpToPlanet Action with normal player, needs card from planet, action, not blocked
	 * @throws Exception
	 */
	@Test
	public void WarpToPlanetTest() throws Exception {
		lastUsedCard.add(new PlayerCard(1,1,0));			//planetcard 1
		assertTrue(exorcist.getRole().warpToPlanet(exorcist, exorcist, p1, p2, lastUsedCard));	
		
	}
	
	/**
	 * tests warpToPlanet ACtion with normal player, the other way around
	 * @throws Exception
	 */
	@Test
	public void WarpToPlanetTest2() throws Exception {
		lastUsedCard.add(new PlayerCard(2,1,0));			//planetcard 2
		assertTrue(exorcist.getRole().warpToPlanet(exorcist, exorcist, p1, p2, lastUsedCard));	
	}
	
	/**
	 * Tests FindWarpTor ActionCard, can warp anyone to any planet
	 * @throws Exception
	 */
	@Test
	public void FindWarptorTest() throws Exception {
		lastUsedCard.add(new  FindWarptor());		
		assertTrue(exorcist.getRole().warpToPlanet(exorcist, warpmaster, p1, p2, lastUsedCard));	
	}
	
	/**
	 * Tests warpToPlanet with Warpmaster, can warp anyone
	 * @throws Exception
	 */
	@Test
	public void WarpToPlanetWarpmaster() throws Exception { //Warpmaster Test
		warpmaster.resetLeftActions();										
		lastUsedCard.add(new PlayerCard(3,1,0));						
		assertTrue(warpmaster.getRole().warpToPlanet(warpmaster, exorcist, p1, p3, lastUsedCard));	
	}
	
	/**
	 * tests warpToPlanet with Warpmaster, the other way around
	 * @throws Exception
	 */
	@Test
	public void WarpToPlanetWarpmaster2() throws Exception { //Warpmaster Planet to Planet
		exorcist.setPlanetId(1);							
		assertTrue(warpmaster.getRole().warpToPlanet(warpmaster, exorcist, p1, p3, lastUsedCard));	
	}

	//ExceptionTests

	@Rule
	public ExpectedException thrown = ExpectedException.none();


	@Test
	public void WarpToPlanetROLPL() throws Exception { //Role wrong planet
		lastUsedCard.add(new PlayerCard(42,1,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().warpToPlanet(exorcist, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}
	
	@Test
	public void warpmasterROLPL() throws Exception { //Role wrong planet
		lastUsedCard.add(new PlayerCard(42,1,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		warpmaster.getRole().warpToPlanet(warpmaster, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}
	
	
	@Test
	public void WarpToPlanetROLNC() throws Exception { //Number of cards
		lastUsedCard.add(new UndeadCard(0,0,0));
		lastUsedCard.add(new UndeadCard(0,0,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		exorcist.getRole().warpToPlanet(exorcist, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}
	
	@Test
	public void WarpToPlanetROLCT() throws Exception { //Role card type
		lastUsedCard.add(new UndeadCard(0,0,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		exorcist.getRole().warpToPlanet(exorcist, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}
	
	@Test
	public void WarpToPlanetROLLA() throws Exception { //Role no Planet
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().warpToPlanet(exorcist, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}
	
	@Test
	public void warpmasterROLLA() throws Exception { //Role no Planet
		warpmaster.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		warpmaster.getRole().warpToPlanet(warpmaster, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}

	@Test
	public void WarpToPlanetROLBL() throws Exception { //ROLBL
		lastUsedCard.add(new PlayerCard(1,1,0));			//planetcard 1
		p2.setBlocked(true);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLBL");
		exorcist.getRole().warpToPlanet(exorcist, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}

	@Test
	public void warpmasterROLBL() throws Exception { //ROLBL
		lastUsedCard.add(new PlayerCard(1,1,0));			//planetcard 1
		p2.setBlocked(true);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLBL");
		warpmaster.getRole().warpToPlanet(warpmaster, exorcist, p1, p2, lastUsedCard);
		fail("exception expectet");
	}
}
