package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;
import player.stubs.PlayerStubWarpmaster;

/**
 *  Tests moveBySpaceShip Action in Role and Warpmaster
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class RoleMoveBySpaceshipTest {
	PlayerStubExorcist exorcist;
	PlayerStubLieutenant lieutenant;
	PlayerStubWarpmaster warpmaster;
	PlanetModelI p1;
	PlanetModelI p2;
	ArrayList<Integer> neighbours;
	
	
	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		neighbours = new ArrayList<Integer>();
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();
		warpmaster = new PlayerStubWarpmaster();
		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
	}
	
	/**
	 * tests moveBySpaceship with normal Player, needs Action, startplanet must be same as players, endplanet must be neighbour
	 * @throws Exception
	 */
	@Test
	public void moveBySpaceshipTest() throws Exception {
		neighbours.add(2);
		assertTrue(exorcist.getRole().moveBySpaceShip(exorcist, exorcist, p1, p2, neighbours));
	}
	
	/**
	 * tests moveBySpaceship with normale Player, same condition, but can take any spaceship
	 * @throws Exception
	 */
	@Test
	public void moveBySpaceshipWarpmaster() throws Exception { //Warpmastertest
		neighbours.add(2);
		assertTrue(warpmaster.getRole().moveBySpaceShip(warpmaster, exorcist, p1, p2, neighbours));		
	}
	
	//EXCEPTIONSTETS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void moveBySpaceShipROLLA() throws Exception { //ROLLA no left action
		neighbours.add(1);
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().moveBySpaceShip(exorcist, exorcist, p1, p2 , neighbours);
		fail("expected exception");
	}
	
	@Test
	public void warpmasterROLLA() throws Exception { //Warpmastertest: no left action
		neighbours.add(1);
		warpmaster.setLeftActions(0);
				
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		warpmaster.getRole().moveBySpaceShip(warpmaster, exorcist, p1, p2, neighbours);
		fail("expected exception");
	}
	
	@Test
	public void moveBySpaceshipUnequalPlayerTest() throws Exception { //ROLLA different player (cPlayer;aPlayer)
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().moveBySpaceShip(exorcist, lieutenant, p1, p2 , neighbours);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void moveBySpaceShipROLNP() throws Exception {	//ROLNP not a neighbor planet
		neighbours.add(3);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNP");
		exorcist.getRole().moveBySpaceShip(exorcist, exorcist, p1, p2 , neighbours);
	}
	
	@Test
	public void moveBySpaceShipROLBL() throws Exception {	//ROLBL planet is conquered
		p2.setBlocked(true);
		neighbours.add(2);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLBL");
		exorcist.getRole().moveBySpaceShip(exorcist, exorcist, p1, p2 , neighbours);
	}
	
	@Test
	public void warpamsterROLBL() throws Exception {	//ROLBL planet is conquered
		p2.setBlocked(true);
		neighbours.add(2);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLBL");
		warpmaster.getRole().moveBySpaceShip(warpmaster, exorcist, p1, p2 , neighbours);
	}

	@Test
	public void warpmasterROLNP() throws Exception { //Warpmastertest: ROLNP not a neighbor planet 
		neighbours.add(3);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNP");
		warpmaster.getRole().moveBySpaceShip(warpmaster, warpmaster, p1, p2 ,neighbours);
	}

}
