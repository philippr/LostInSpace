package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import cards.PlayerCard;
import cards.UndeadCard;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;

/**
 *	Tests the Ruleset of Action Buy improved Weapon in Role
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class RoleBuyImprovedWeaponTest {
	PlayerI exorcist;
	PlayerI lieutenant;
	PlanetModelI p1;
	PlanetModelI p2;
	ArrayList<Card> lastUsedCard;

	/**
	 * sets the stub variables
	 */
	@Before
	public void prepareBaseTest() {
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();

		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
		p1.setBase(true);
		lastUsedCard = new ArrayList<Card>();
	}	
	
	/**
	 * tests buy improved weapon, needs on thrown card and a Base
	 * @throws Exception
	 */
	@Test
	public void buyImprovedWeaponTest() throws Exception {
		lastUsedCard.add(new PlayerCard(0,1,1));
		assertTrue(exorcist.getRole().buyImprovedWeapon(exorcist, p1, lastUsedCard));
	}
	
	//EXCEPTIONSTESTS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test 
	public void imprWeaponROLPL() throws Exception { //ROLPL
		exorcist.setPlanetId(432);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().buyImprovedWeapon(exorcist, p1, lastUsedCard);
	}
	
	@Test 
	public void imprWeaponROLNB() throws Exception { //ROLPL
		lastUsedCard.add(new PlayerCard(0,2,1));
		exorcist.setPlanetId(2);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNB");
		exorcist.getRole().buyImprovedWeapon(exorcist, p2, lastUsedCard);
	}
	
	@Test 
	public void imprWeaponROLNC() throws Exception { //ROLNC
		lastUsedCard.add(new PlayerCard(0,1,1));
		lastUsedCard.add(new PlayerCard(0,1,1));
		lastUsedCard.add(new PlayerCard(0,1,1));

		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		exorcist.getRole().buyImprovedWeapon(exorcist, p1, lastUsedCard);
	}
		
	@Test 
	public void imprWeaponROLCT() throws Exception { //ROLCT
		lastUsedCard.add(new UndeadCard(0,1,1));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		exorcist.getRole().buyImprovedWeapon(exorcist, p1, lastUsedCard);
	}


	@Test 
	public void imprWeaponROLCC() throws Exception { //ROLCC
		lastUsedCard.add(new PlayerCard(0,3,1));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCC");
		exorcist.getRole().buyImprovedWeapon(exorcist, p1, lastUsedCard);
	}
}
