package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;

/**
 *  Tests Mine Resource Action in Role
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class RoleMineResourceTest {
	PlayerStubExorcist exorcist;
	PlayerI lieutenant;
	PlanetModelI p1;
	PlanetModelI p2;
	ArrayList<Card> lastUsedCard;

	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();
		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
	}	
	
	/**
	 * tests mine resource action in Role, needs resource on planet, left action, same planet
	 * @throws Exception
	 */
	@Test
	public void mineResourceTest() throws Exception {
		exorcist.resetLeftActions();
		p1.setResource(1);
		assertTrue(exorcist.getRole().mineResource(exorcist,p1, 1));
		
	}

	//EXCEPTIONTESTS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void mineResourceROLLA() throws Exception { //ROLLA no left action
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().mineResource(exorcist, p1, 0);
		fail("expcetion expectet :(");
	}

	@Test
	public void mineResourceROLPL() throws Exception { //ROLPL wrong planet
		exorcist.resetLeftActions();
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().mineResource(exorcist, p2, 0);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void mineResourceROLRS() throws Exception { //ROLRS no resource on planet
		exorcist.resetLeftActions();
		p1.setResource(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLRS");
		exorcist.getRole().mineResource(exorcist, p1, 3);
		fail("expcetion expectet :(");
	}
}
