package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import cards.FindBase;
import cards.PlayerCard;
import cards.ProtectionShield;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubConstructor;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;

/**
 *  Tests the Ruleset of the action Build Base in Role and Constructor
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class RoleBuildBaseTest {
	PlayerStubExorcist exorcist;
	PlayerI lieutenant;
	PlayerStubConstructor constructor;
	PlanetModelI p1;
	PlanetModelI p2;
	ArrayList<Card> lastUsedCard;

	/**
	 * sets the stubs needed for the test
	 */
	@Before
	public void prepareBaseTest() {
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();
		constructor = new PlayerStubConstructor();
		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
		p2.setBase(true);
		lastUsedCard = new ArrayList<Card>();
	}	
	
	/**
	 * Test with fitting PlayerCard 
	 * @throws Exception
	 * */
	@Test
	public void baseTestPlanetCard() throws Exception {
		exorcist.resetLeftActions();
		lastUsedCard.add(new PlayerCard(1,1,0));
		assertTrue(exorcist.getRole().buildBase(exorcist,p1, lastUsedCard));
	}

	/**
	 * Test with ActionCard FindBase (any Planet, no Action)
	 * @throws Exception
	 */
	@Test
	public void baseTestActionCard() throws Exception { 
		exorcist.resetLeftActions();
		lastUsedCard.add(new FindBase());
		assertTrue(exorcist.getRole().buildBase(exorcist,p1, lastUsedCard));
	}
	

	/**
	 * Test with Constructor & Actioncard 
	 * @throws Exception
	 */
	@Test
	public void constructorActionCard() throws Exception { 
		constructor.resetLeftActions();
		lastUsedCard.add(new FindBase());
		assertTrue(constructor.getRole().buildBase(constructor,p1, lastUsedCard));
	}

	
	/**
	 * Test with Constructor (no Card needed)
	 * @throws Exception
	 */
	@Test
	public void baseTestConstructor() throws Exception {
		constructor.resetLeftActions();
		assertTrue(constructor.getRole().buildBase(constructor,p1, lastUsedCard));
	}

	
	// EXCEPTION TESTS
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void buildBaseROLLA() throws Exception { //ROLLA no left action
		exorcist.setLeftActions(0);
		lastUsedCard.add(new PlayerCard(0,1,0));

		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().buildBase(exorcist,p1, lastUsedCard);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void constructorROLLA() throws Exception { //ROLLA no left action
		constructor.setLeftActions(0);
		lastUsedCard.add(new PlayerCard(0,1,0));

		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		constructor.getRole().buildBase(constructor,p1, lastUsedCard);
		fail("expcetion expectet :(");
	}

	@Test
	public void buildBaseROLNC() throws Exception{ //ROLNC wrong number cards
		exorcist.resetLeftActions();
		lastUsedCard.add(new PlayerCard(1,1,0));
		lastUsedCard.add(new PlayerCard(2,2,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		exorcist.getRole().buildBase(exorcist, p1, lastUsedCard);
		fail("expcetion expectet :(");
	}

	@Test
	public void buildBaseROLHB() throws Exception{ //ROLHB has base
		exorcist.resetLeftActions();
		exorcist.setPlanetId(2);
		lastUsedCard.add(new PlayerCard(2,1,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLHB");
		exorcist.getRole().buildBase(exorcist,p2, lastUsedCard);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void constructorROLHB() throws Exception{ //ROLHB has base
		constructor.resetLeftActions();
		constructor.setPlanetId(2);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLHB");
		constructor.getRole().buildBase(constructor,p2, lastUsedCard);
		fail("expcetion expectet :(");
	}


	@Test
	public void buildBaseROLPL() throws Exception{ //ROLPL: wrong planet given
		exorcist.resetLeftActions();
		lastUsedCard.add(new PlayerCard(0,1,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().buildBase(exorcist, p1, lastUsedCard);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void constructoreROLPL() throws Exception{ //ROLPL: wrong planet given
		constructor.resetLeftActions();
		constructor.setPlanetId(42);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		constructor.getRole().buildBase(constructor, p1, lastUsedCard);
		fail("expcetion expectet :(");
	}


	@Test
	public void buildBaseROLPL2() throws Exception{ //ROLPL: wrong planetcard
		exorcist.resetLeftActions();
		exorcist.setPlanetId(42);
		lastUsedCard.add(new PlayerCard(2,1,0));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().buildBase(exorcist, p1, lastUsedCard);
		fail("expcetion expectet :(");
	}


	@Test
	public void buildBaseROLCT() throws Exception{ //ROLCT: wrong card type
		exorcist.resetLeftActions();
		lastUsedCard.add(new ProtectionShield());
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		exorcist.getRole().buildBase(exorcist, p1, lastUsedCard);
		fail("expcetion expectet :(");
	}
}
