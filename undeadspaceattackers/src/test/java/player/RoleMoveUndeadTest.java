package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubUntoaster;

/**
 * Tests Move Undead in Role and Untoaster
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class RoleMoveUndeadTest {

	PlayerStubUntoaster untoaster = new PlayerStubUntoaster();
	PlayerStubExorcist exorcist = new PlayerStubExorcist();
	PlanetModelI sPlanet;
	PlanetModelI ePlanet;
	PlanetModelI p1 = new PlanetStub1();
	PlanetModelI p2 = new PlanetStub2();
	ArrayList<Integer> neighbours = new ArrayList<>();
	int[] undeads = {1,0,0,0};
	
	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		sPlanet = p1;
		p1.setUndeads(undeads);
		ePlanet = p2;		
		neighbours.add(2);
	}
	
	/**
	 * tests moveUndead Untoaster, planet must have undead, planet must be neighbour
	 * @throws Exception
	 */
	@Test
	public void moveUndead() throws Exception {
		assertTrue(untoaster.getRole().moveUndead(sPlanet, ePlanet, 0, neighbours));
	}

	/**
	 * Tests move Undead in Role, not allowed
	 * @throws Exception
	 */
	@Test
	public void moveUndeadRole() throws Exception {
		assertFalse(exorcist.getRole().moveUndead(sPlanet, ePlanet, 0, neighbours));
	}
	
	//EXCEPTIONSTESTS
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test 
	public void moveUndeadROLUC() throws Exception { //ROLUC UndeadColor
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLUC");
		undeads[0] = 0;
		moveUndead();
	}
	
	@Test 
	public void moveUndeadROLNP() throws Exception { //ROLNP not a neighbour planet
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNP");
		neighbours.clear();
		neighbours.add(2354);
		moveUndead();
	}
}