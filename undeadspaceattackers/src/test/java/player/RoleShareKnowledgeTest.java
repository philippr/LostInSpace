package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import cards.FindBase;
import cards.PlayerCard;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubCrazyWeaponArchitect;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubWarpmaster;

/**
 * Tests ShareKnowledge Action in Role and crazyWeaponArchitect
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class RoleShareKnowledgeTest {
	PlayerStubExorcist exorcist;
	PlayerI warpmaster;
	PlayerStubCrazyWeaponArchitect cwArchitect;
	PlanetModelI p1;
	PlanetModelI p2;
	Card card;
	ArrayList<Card> lastUsedCard;
	PlayerI givePlayer;
	PlayerI getPlayer;
	PlayerI cPlayer;
	
	/**
	 * sets stub variables
	 */
	@Before
	public void before(){
		exorcist = new PlayerStubExorcist();
		warpmaster = new PlayerStubWarpmaster();
		cwArchitect = new PlayerStubCrazyWeaponArchitect();
		
		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
		lastUsedCard = new ArrayList<Card>();
		exorcist.resetLeftActions();
		exorcist.setPlanetId(1);
		givePlayer = exorcist;
		cPlayer = exorcist;
		warpmaster.setPlanetId(1);
		getPlayer = warpmaster;
		cwArchitect.setPlanetId(1);
		
		card = new PlayerCard(1,1,0);
	}

	/**
	 * Tests shareKnowledge with normal player, needs action, tradersTurn, number of cards, card type, same planet
	 * @throws Exception
	 */
	@Test
	public void shareKnowledgeTest() throws Exception {	
		assertTrue(exorcist.getRole().shareKnowledge(cPlayer, givePlayer, getPlayer, lastUsedCard, card));//planetcard 0
	} 
	
	/**
	 * tests shareKnowledge with crazyweaponarchitect
	 * @throws Exception
	 */
	@Test
	public void shareKnowledgeTestCrazy() throws Exception { //Crazy Weapon architect
		assertTrue(cwArchitect.getRole().shareKnowledge(cPlayer, givePlayer, getPlayer, lastUsedCard, card));
	}
	
	/**
	 * ....., can share any card
	 * @throws Exception
	 */
	@Test
	public void shareKnowledgeTestCrazy2() throws Exception { //Crazy Weapon architect
		card =  new PlayerCard(5,1,1);
		givePlayer = cwArchitect;
		cPlayer = warpmaster;
		assertTrue(cwArchitect.getRole().shareKnowledge(cPlayer, givePlayer, getPlayer, lastUsedCard, card));
	}
	

	//EXCEPTIONTESTS
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void moveBySpaceShipROLLA() throws Exception { //ROLLA no left action
		exorcist.setLeftActions(0);				
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		shareKnowledgeTest();
		fail("expected exception");
	}
	
	@Test
	public void cwArchitectROLLA() throws Exception { //ROLLA no left action
		cwArchitect.setLeftActions(0);		
		cPlayer = cwArchitect;
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		shareKnowledgeTestCrazy(); 
		fail("expected exception");
	}
	


	@Test
	public void shareKnowledgeROLTT() throws Exception { //ROLTT traders turn needed
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLTT");
		cPlayer = cwArchitect;
		shareKnowledgeTest();
		}
	
	@Test
	public void cwArchitetctROLTT() throws Exception { //ROLTT traders turn needed
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLTT");
		givePlayer = exorcist;
		getPlayer = warpmaster;
		cPlayer = cwArchitect;
		shareKnowledgeTestCrazy(); 
	}	
	
	@Test
	public void shareKnowledgeROLNC() throws Exception { //ROLNC number cards
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		lastUsedCard.add(new PlayerCard(32,1,1));
		shareKnowledgeTest();
	}
	
	@Test
	public void cwArchitetctROLNC() throws Exception { //ROLNC number cards
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		lastUsedCard.add(new PlayerCard(32,1,1));
		shareKnowledgeTestCrazy(); 
	}
	
	@Test
	public void shareKnowledgeROLCT() throws Exception { //ROLCT wrong card type
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		card = new FindBase();
		shareKnowledgeTest();
	}
	
	@Test
	public void cwArchitectROLCT() throws Exception { //ROLCT wrong card type
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		card = new FindBase();
		shareKnowledgeTestCrazy(); 
	}
	
	@Test
	public void shareKnowledgeROLPL() throws Exception { //ROLPL wrong planet
		exorcist.setPlanetId(43);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		shareKnowledgeTest();
	}
	
	@Test
	public void cwArchitectROLPL() throws Exception { //ROLPL wrong planet
		warpmaster.setPlanetId(43);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		shareKnowledgeTestCrazy(); 
	}
	
	@Test
	public void shareKnowledgeROLPL2() throws Exception { //ROLPL wrong planet
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		card =  new PlayerCard(42,1,0);
		shareKnowledgeTest();
	}	
}
