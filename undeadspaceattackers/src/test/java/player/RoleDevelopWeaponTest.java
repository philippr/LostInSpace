package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import cards.FindBase;
import cards.PlayerCard;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;
import player.stubs.PlayerStubWarpmaster;
import player.stubs.PlayerStubWeapontrader;

/**
 * Tests the ACtion Develop Weapon in Role and Weapontrader
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class RoleDevelopWeaponTest {

	PlayerStubExorcist exorcist;
	PlayerStubLieutenant lieutenant;
	PlayerStubWarpmaster warpmaster;
	PlayerStubWeapontrader weapontrader;
	PlanetModelI p1;
	PlanetModelI p2;
	PlanetModelI planet;
	ArrayList<Card> lastUsedCard;
	PlayerI cPlayer;
	
	/**
	 * sets the stub variables
	 */
	@Before
	public void before() {
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();
		warpmaster = new PlayerStubWarpmaster();
		weapontrader = new PlayerStubWeapontrader();
		
		p1 = new PlanetStub1();
		p2 = new PlanetStub2();
		p2.setBase(true);
		
		lastUsedCard = new ArrayList<Card>();
		
		for(int i = 0; i < 5; i++) {		
			lastUsedCard.add(new PlayerCard(42,0,0));
		}
		cPlayer = exorcist;
		exorcist.setPlanetId(2);
		weapontrader.setPlanetId(2);
		planet = p2;

	}
	
	/**
	 * tests developWeapon with normal player, 5 cards of the same color as planet thrown, base exists, needs action
	 * @throws Exception
	 */
	@Test
	public void developWeaponTest()  throws Exception { //Develop Weapon test
		assertTrue(exorcist.getRole().developWeapon(cPlayer, planet, 0, lastUsedCard));
	}
	
	/**
	 * testes developweapon with weapontrader, 4 cards thrown, base exists
	 * @throws Exception
	 */
	@Test
	public void weaponTraderTest()  throws Exception { //Weapontrader Test
		lastUsedCard.remove(0);
		assertTrue(weapontrader.getRole().developWeapon(weapontrader, planet, 0, lastUsedCard));
	}
	
	
	//EXEPTIONSTESTS
	

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test 
	public void devWeapROLLA() throws Exception { //ROLLA
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		developWeaponTest();
	}
	
	@Test 
	public void traderROLLA() throws Exception { //ROLLA
		weapontrader.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		weaponTraderTest();
	}
	
	@Test 
	public void devWeapROLNB() throws Exception { //ROLNB
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNB");
		planet = p1;
		developWeaponTest();
	}

	@Test 
	public void traderROLNB() throws Exception { //ROLNB
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNB");
		planet = p1;
		weaponTraderTest();
	}	
	
	@Test 
	public void devWeapROLNC() throws Exception { //ROLNC
		lastUsedCard.remove(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		developWeaponTest();
	}
	
	@Test 
	public void traderROLNC() throws Exception { //ROLNC
		lastUsedCard.remove(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNC");
		weaponTraderTest();
	}
	
	
	@Test 
	public void devWeapROLCT() throws Exception { //ROLCT
		lastUsedCard.remove(0);
		lastUsedCard.add(new FindBase());
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		developWeaponTest();
	}		
	@Test 
	public void traderROLCT() throws Exception { //ROLCT
		lastUsedCard.remove(0);
		lastUsedCard.add(new FindBase());
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCT");
		weaponTraderTest();
	}	
	
	@Test 
	public void devWeapROLCC() throws Exception { //ROLCC
		lastUsedCard.remove(0);
		lastUsedCard.add(new PlayerCard(2,2,2));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCC");
		developWeaponTest();
	}

	
	@Test 
	public void traderLCC() throws Exception { //ROLCC
		lastUsedCard.remove(0);
		lastUsedCard.add(new PlayerCard(2,2,2));
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLCC");
		weaponTraderTest();
	}

	//TODO: Test weapontrader

	
	
	
	
	
	
}
