package player;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cards.Card;
import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;

/**
 * Tests the Heal Plague Action in Role
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class RoleHealPlagueTest {
	PlayerStubExorcist exorcist;
	PlayerI lieutenant;
	PlanetModelI p1;
	PlanetModelI p2;
	ArrayList<Card> lastUsedCard;

	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		exorcist = new PlayerStubExorcist();
		lieutenant = new PlayerStubLieutenant();
		p1 = new PlanetStub1();
		p1.setBase(true);
		p2 = new PlanetStub2();
		p2.setBase(false);
	}	
	
	/**
	 * tests heal plague action, needs healer, base, left action
	 * @throws Exception
	 */
	@Test
	public void healPlagueTest() throws Exception {
		exorcist.resetLeftActions();
		p1.setHealer(1);
		assertTrue(exorcist.getRole().healPlague(exorcist,p1, 1));
	}
	
	//EXCEPTIONSTESTS

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void healPlagueROLLA() throws Exception { //ROLLA no left action
		exorcist.setLeftActions(0);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLLA");
		exorcist.getRole().healPlague(exorcist, p1, 0);
		fail("expcetion expectet :(");
	}

	@Test
	public void healPlagueROLPL() throws Exception { //ROLPL wrong planet
		exorcist.resetLeftActions();
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLPL");
		exorcist.getRole().healPlague(exorcist, p2, 0);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void healPlagueROLNB() throws Exception { //ROLNB no base
		exorcist.resetLeftActions();
		exorcist.setPlanetId(2);
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLNB");
		exorcist.getRole().healPlague(exorcist, p2, 3);
		fail("expcetion expectet :(");
	}
	
	@Test
	public void healPlagueROLHL() throws Exception { //ROLHL no healer
		exorcist.resetLeftActions();
		thrown.expect(Exception.class);
		thrown.expectMessage("ROLHL");
		exorcist.getRole().healPlague(exorcist, p1, 3);
		fail("expcetion expectet :(");
	}
}
