package cards;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import game.board.PlanetModelI;
import game.board.PlanetStub1;
import game.board.PlanetStub2;
import game.board.PlanetStub3;
import player.PlayerI;
import player.stubs.PlayerStubExorcist;
import player.stubs.PlayerStubLieutenant;

/**
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class CardModelTest {

	ArrayList<PlanetModelI> planets;
	int nMassAttack;
	int nPlayers;
	CardModelI cardModel;
	ArrayList<PlayerI> players;
	
	/**
	 * sets stub variables
	 */
	@Before
	public void before() {
		nPlayers = 3;
		nMassAttack = 5;
		planets = new ArrayList<PlanetModelI>();

		for(int i = 0; i < 7; i++) {
			planets.add(new PlanetStub1());
			planets.add(new PlanetStub2());
			planets.add(new PlanetStub3());
		}
		
		players = new ArrayList<>();
		players.add(new PlayerStubExorcist());
		players.add(new PlayerStubLieutenant());
		
		cardModel = new CardModel(planets, nMassAttack, players);
	}

	/**
	 * tests creation of hands and piles  
	 */
	@Test
	public void testCardModel() {
		assertEquals(4,players.get(0).getHand().size());	
		assertEquals(4,players.get(1).getHand().size());
		assertEquals(21, cardModel.getUndeadPile().size()); //21 planets <=> 21 undeadcards
		assertEquals(21, cardModel.getPlayerPile().size()); // (21 playercards (nPlanets) + 3 ActionCards + 5 MassAttack) - (2*4 taken handcards)
		
	}
	
	/**
	 * tests the deck mixing
	 */
	@Test
	public void testMixDecks() {
		assertEquals(21, cardModel.getUndeadPile().size());
		assertEquals(0, cardModel.getDiscardPile().size());
		cardModel.getDiscardPile().add(new UndeadCard(1,1,1));
		cardModel.getDiscardPile().add(new UndeadCard(1,1,1));
		cardModel.getDiscardPile().add(new UndeadCard(1,1,1));
		assertEquals(3, cardModel.getDiscardPile().size());
		cardModel.mixDecks();
		assertEquals(24, cardModel.getUndeadPile().size());
	}

}
