package cards;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observer;

public class CardModelStub implements CardModelI {
	ArrayList<Card> playerDeck;
	ArrayList<UndeadCard> undeadDeck;
	ArrayList<UndeadCard> discardPile;
	LinkedList<Card>[] playerHands;

	
	@SuppressWarnings("unchecked")
	public CardModelStub() {
		int nPlanets = 20;
		
		playerDeck = new ArrayList<>(nPlanets + main.Constants.nActionCards);
		discardPile = new ArrayList<>(nPlanets);
		undeadDeck = new ArrayList<>(nPlanets);
		
		playerHands = new LinkedList[4];
	}


	@Override
	public void mixDecks() {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<UndeadCard> getUndeadPile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<UndeadCard> getDiscardPile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Card> getPlayerPile() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void addObserverX(Observer observer) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Card removePlayerCard(int i) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setTopPlayerCard(Card object) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public UndeadCard removeUndeadCard(int i) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public UndeadCard getTopUndeadCard() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setTopUndeadCard(UndeadCard removeUndeadCard) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Card getTopPlayerCard() {
		// TODO Auto-generated method stub
		return null;
	}
}
