package game;

import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class DragContainer {
	private Pane parentPane;
	private ImageView dragImage;
	private Pane dragPane;
	private int colorId;
	private int id;
	private String msg;
	private double rotate;
	private int parentId;
	private Object boxedObj;
	
	public DragContainer() {
		clear();
	}
	
	public void set(ImageView drag, String msg, Pane parentPane, int colorId) {
		this.dragImage = drag;
		this.msg = msg;
		this.parentPane = parentPane;
		this.colorId = colorId;
	}

	
	public void set(Pane drag, String msg, Pane parentPane, int colorId) {
		this.dragPane = drag;
		this.msg = msg;
		this.parentPane = parentPane;
		this.colorId = colorId;
	}
	

	
	/**
	 * Getter for the parentPane 
	 * @return parentPane
	 */
	public Pane getParentPane() {
		return parentPane;
	}
	/**
	 * Getter for dragImage
	 * @return dragImage 
	 */
	public ImageView getDragImage() {
		return dragImage;
	}
	/**
	 * Setter for dragPane
	 * @return dragImage
	 */
	public Pane getDragPane() {
		return dragPane;
	}
	/**
	 * Getter for colorId as an integer
	 * @return colorId
	 */
	public int getColorId() {
		return colorId;
	}
	/**
	 * Getter for msg
	 * @return msg
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * clear the variables to null
	 */
	public void clear() {
		msg = "";
		colorId = -1;
		parentPane = null;
		dragPane = null;
		dragImage = null;
		rotate = 0.0;
		parentId = -1;
		id = -1;
		boxedObj = null;
	}
		
	/**
	 * Getter for the oldY
	 * @return the oldY
	 */
	public double getRotate() {
		return rotate;
	}
	
	public void setRotate(double rotate) {
		this.rotate = rotate;
	}
	
	
	public int getParentId() {
		return parentId;
	}


	public void setParentId(int parentId) {
		this.parentId = parentId;

	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Object getBoxedObj() {
		return boxedObj;
	}


	public void setBoxedObj(Object boxed) {
		this.boxedObj = boxed;
	}
}
