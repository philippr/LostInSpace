package game.board;

import java.util.Observable;
import java.util.Observer;

import javafx.scene.image.ImageView;
import utils.ImageLoader;

/**
 * Undead view that is displaying an undead.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class UndeadView extends ImageView implements Observer{

	/**
	 * Color of undead.
	 */
	private final int colorId;
	
	/**
	 * Type of a planet (diffrent pictures for a planet of a certain color
	 * possible):
	 */
	private final int type;
	
	/**
	 * Grid with undeads and abominations.
	 */
	private UndeadGrid parentGrid;
	
	/**
	 * States whether a planet is clicked.
	 */
	private boolean picked = true;
	
	/**
	 * Loads a picture of a planet with a color and a type corresponding to the
	 * given parameters.
	 * 
	 * @param colorId
	 *            Color of planet.
	 * @param type
	 *            Type of planet.
	 * @param parent
	 *            Corresponding undead grid.
	 */
	UndeadView(int colorId, int type, UndeadGrid parent){
		super(ImageLoader.getInstance().get("/undeads/undead"+ colorId + "" + type +".png"));
		this.colorId = colorId;
		this.type = type;
		this.parentGrid = parent;
	}

	/**
	 * Getter for boolean if planet is picked.
	 * @return True if planet is picked.
	 */
	public boolean isPicked() {
		return picked;
	}

	/**
	 * Getter for undead parent grid.
	 * @return parentGrid
	 */
	public UndeadGrid getParentGrid() {
		return parentGrid;
	}

	/**
	 * Setter for parent undead grid.
	 * @param parent
	 */
	public void setParentGrid(UndeadGrid parent) {
		this.parentGrid = parent;
	}

	/**
	 * Getter for the colorID of the undead
	 * 
	 * @return the colorId as an Integer
	 */
	public int getColorId() {
		return colorId;
	}	
	
	/**
	 * Getter for the type of the undead
	 * 
	 * @return the type as an Integer
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * If an undead is clicked or picked in an action the undead view attribute
	 * picked is set true.
	 */
	public void pick(){
		picked = true;
	}
	
	
	
	@Override
	public void update(Observable model, Object msg) {
		if(msg.toString().equals("nxtPl")){
			picked = false;
		}
	}
}
