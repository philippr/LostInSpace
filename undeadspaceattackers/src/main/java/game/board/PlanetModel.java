package game.board;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * Notifications: 
 * addUnX: add undead of colorId X 
 * remUnX: remove undead of colorId X 
 * wipUnX: wipe out undeads of colorId X 
 * block: block planet 
 * setAb: set abomination 
 * remAb: remove abomination 
 * sBase: set base 
 * remDC: remove dark
 * column setDC: set dark column 
 * setHlX: set healer of colorId X 
 * setRs: set resource
 * sucid: called by the close() method to propagate that this object will not longer exist
 * redrw: redraw - simple update event
 *
 */

@SuppressWarnings("serial")
public class PlanetModel extends Observable implements PlanetModelI, Serializable {

	/**
	 * Every planet gets a unique planetId.
	 */
	private final int planetId;

	/**
	 * A planet has one of four different colors. 0 : yellow 1 : green 2 : blue
	 * 3 : red
	 */
	private int color;

	/**
	 * Type of a planet with a specific color. A planet can be blue, but there
	 * is more than one type of a colour
	 */
	private final int type;

	/**
	 * A planet has a position on the field. The position is given by an
	 * x-coordinate and a y-coordinate
	 */
	private int[] pos;

	/**
	 * Integer array that displays how many undeads from which species are on a
	 * planet. 0 : skeletons 1 : zombies 2 : mummies 3 : ghosts
	 */
	private final int[] undeads = new int[4];

	/**
	 * number of Undeads on planet. In the beginning there are no undeads on a
	 * planet.
	 */
	private int nUndeads = 0;

	/**
	 * Boolean array that states whether there are players on the planet. If
	 * there is a player on a planet the corresponding boolean is set true.
	 */
	private final boolean[] players = new boolean[4];

	/**
	 * Integer that states whether there is a resource on a planet. The default
	 * value is -1 and states that there is no resource on a planet. If there is
	 * a resource the value can be between 0 and 3. 0 : yellow 1 : green 2 :
	 * blue 3 : red
	 * 
	 */
	private int resource = -1;

	private boolean hasAbomination;
	private boolean hasBase;
	private boolean isBlocked;
	private boolean hasDarkColumn;
	private boolean showName;
	
	
	/**
	 * Flags for healer of four differen colors. 0 : yellow, 1 : green, 2 : blue,  3
	 * : red
	 */
	private final boolean[] healer = { false, false, false, false };
	
	/**
	 * Creates the Planet with the given parameters
	 * 
	 * @param id
	 *            the id of the planet
	 * @param color
	 *            the color(0-3)
	 * @param posx
	 *            the x position in the grid
	 * @param posy
	 *            the y position in the grid type the type(0-3) is random
	 * @param showName
	 *            set the showNamn flag; view should hide the planet name if false 
	 */
	public PlanetModel(int id, int color, int posx, int posy, boolean showName) {
		this.planetId = id;
		this.color = color;
		this.pos = new int[] { posx, posy };
		this.type = ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS);		
		hasAbomination = false;
		hasBase = false;
		isBlocked = false;
		hasDarkColumn = false;
		this.showName = showName;
		
		
	}

	// -----------------PLANET---------------//

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#getId()
	 */
	@Override
	public int getId() {
		return planetId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#getColor()
	 */
	@Override
	public int getColor() {
		return color;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#setColor(int)
	 */
	public void setColor(int colorId) {
		this.color = colorId;
		setChanged();
		notifyObservers("chPlC");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#getType()
	 */
	@Override
	public int getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#getPosX()
	 */
	@Override
	public int[] getPos() {
		return pos;
	}
	

	// -----------------PLAYERS---------------//

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#getPlayers()
	 */
	@Override
	public boolean[] getPlayers() {
		return players;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#hasPlayer()
	 */
	@Override
	public boolean hasPlayers() {
		for (boolean i : players) {
			if (i) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#setPlayer(int)
	 */
	@Override
	public void setPlayer(int playerId, boolean isPlayer) {
		this.players[playerId] = isPlayer;
	}


	// -----------------UNDEADS---------------//

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#getUndeads()
	 */
	@Override
	public int getUndeads(int colorId) {
		return undeads[colorId];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#getnUndeads()
	 */
	public int getNUndeads() {
		return nUndeads;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#setUndeads(int[])
	 */
	@Override
	public void setUndeads(int[] undeads) {
		for (int i = 0; i < 4; i++) {
			if (this.undeads[i] < undeads[i]) {
				for (int j = 0; j < undeads[i] - this.undeads[i]; j++) {
					addUndead(i, true);
				}
			} else if (this.undeads[i] > undeads[i]) {
				for (int j = 0; j < this.undeads[i] - undeads[i]; j++) {
					removeUndead(i, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#addUndead(int)
	 */
	@Override
	public void addUndead(int colorId, boolean notify) {
		if(undeads[colorId]<3){
			undeads[colorId]++;
			nUndeads++;
			if (notify) {
				setChanged();
				notifyObservers("addUn" + colorId);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#hasUndead()
	 */
	@Override
	public boolean hasUndead() {
		for (int i = 0; i < 4; i++) {
			if (undeads[i] > 0) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#removeUndead(int)
	 */


	public void removeUndead(int colorId, boolean propagate) {
		if (undeads[colorId] > 0) {
			undeads[colorId]--;
			nUndeads--;
			if(propagate){
				setChanged();
				notifyObservers("remUn" + colorId);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see board.PlanetI#removeAllUndead(int)
	 */
	@Override
	public void removeAllUndead(int colorId) {
		nUndeads -= this.undeads[colorId];
		undeads[colorId] = 0;
		setChanged();
		notifyObservers("wipUn" + colorId);
	}



	// -----------------PLAGUE---------------//

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#hasHealer(int)
	 */
	@Override
	public boolean hasHealer(int colorId) {
		return healer[colorId];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#setHealer(int)
	 */
	@Override
	public void setHealer(int colorId) {
		healer[colorId] = true;
		setChanged();
		notifyObservers("setHl" + colorId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#hasResource(int)
	 */
	@Override
	public boolean hasResource(int colorId) {
		return resource == colorId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#setResource(int)
	 */
	@Override
	public void setResource(int colorId) {
		resource = colorId;
		setChanged();
		notifyObservers("setRs" + colorId);
	}

	// -----------------ABDOMINATION---------------//


	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#removeRandomUndead()
	 */
	@Override
	public void removeRandomUndead() {
		int[] ud = new int[4];
		for (int i = 0; i < 4; i++) {
			if(i != 0) {
				ud[i]+=ud[i-1];
			}
			if (undeads[i] > 0) {
				ud[i] = undeads[i];
			}
		}
		int rand = ThreadLocalRandom.current().nextInt(0, ud[3]);
		
		for(int i =0; i<4 ; i++) {
			if(rand < ud[i]) {
				removeUndead(i, true);
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.board.PlanetModelI#addObserverX(java.util.Observer)
	 */
	@Override
	public void addObserverX(Observer planetView) {
		addObserver(planetView);

	}

	@Override
	public boolean hasPlayer(int playerId) {
		return players[playerId];
	}
	
	/**
	 * Notify observer if model is dead. Therefore view will remove planet from
	 * board.
	 */
	public void close() {
		setChanged();
		notifyObservers("sucid");
	}
	
	/**
	 * Notify observer if planet view need to be drawn again if infomation in
	 * model has changed.
	 */
	public void redraw() {
		setChanged();
		notifyObservers("redrw");
	}

	@Override
	public void setAbomination(boolean has) {
		hasAbomination = has;
		setChanged();
		if(has)
			notifyObservers("setAb");
		else
			notifyObservers("remAb");	
	}

	@Override
	public void setBase(boolean has) {
		hasBase = has;
		setChanged();
		notifyObservers("sBase");
		
	}

	@Override
	public void setBlocked(boolean is) {
		isBlocked = is;
		setChanged();
		notifyObservers("block");
		
	}

	@Override
	public void setDarkColumn(boolean has) {
		hasDarkColumn = has;
		setChanged();
		if(has)
			notifyObservers("setDC");
		else
			notifyObservers("remDC");
	}

	@Override
	public void showName(boolean show) {
		showName = show;
		
	}

	@Override
	public boolean hasAbomination() {
		return hasAbomination;
	}

	@Override
	public boolean hasBase() {
		return hasBase;
	}

	@Override
	public boolean isBlocked() {
		return isBlocked;
	}

	@Override
	public boolean hasDarkColumn() {
		return hasDarkColumn;
	}

	@Override
	public boolean showName() {
		return showName;
	}

}
