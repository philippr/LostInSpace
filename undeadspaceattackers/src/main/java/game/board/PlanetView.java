package game.board;

import java.util.Observable;
import java.util.Observer;

import game.Connector;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import utils.ImageLoader;

/**
 * View that displays a planet with all possible planettiles (undeads, base,
 * resources, healer ...).
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class PlanetView extends Pane implements Observer {

	/**
	 * Model contains information about what tiles are on a planet.
	 */
	private final PlanetModelI model;

	/**
	 * Image of planet. Can have four different colors and different types.
	 */
	private final ImageView planet;

	/**
	 * View that displays dark column.
	 */
	private ImageView darkColumn;

	/**
	 * Grid for all undead on planet.
	 */
	private final UndeadGrid undeads;

	/**
	 * Displays a base on a planet.
	 */
	private BaseView base;

	/**
	 * Displays a resource on a planet.
	 */
	private ImageView resource;

	/**
	 * Connects all tiles.
	 */
	private final Connector connector;

	/**
	 * Displays whether a planet is blocked.
	 */
	private ParallelTransition blockAnimation = null;

	/**
	 * Blocks are displayed by ones and zeros. This view is displaying zeros.
	 */
	private ImageView block0;

	/**
	 * Blocks are displayed by ones and zeros. This view is displaying ones.
	 */
	private ImageView block1;

	/**
	 * Displaying planets names.
	 */
	private Label planetName;
	
	/**
	 * The timeline for the dropshadow animation.
	 */
	private Timeline timeline;

	
	/**
	 * Creates view with all planettiles.
	 * 
	 * @param model
	 *            Model of planet with all information about planet.
	 * @param connector
	 *            Connector to connect elements from Planet view to controller
	 */
	public PlanetView(PlanetModelI model, Connector connector) {
		this.model = model;
		this.model.addObserverX(this);
		this.connector = connector;

		connector.connectPlanet(this);

		DoubleProperty fontSize = new SimpleDoubleProperty(10);

		planet = new ImageView(
				ImageLoader.getInstance().get("/planets/planet" + model.getColor() + model.getType() + ".png",
						main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));

		planet.fitHeightProperty().bind(this.heightProperty().multiply(main.Constants.PLANET_CELL_RATIO));
		planet.fitWidthProperty().bind(this.widthProperty().multiply(main.Constants.PLANET_CELL_RATIO));

		planet.translateXProperty().bind(this.widthProperty().subtract(planet.fitWidthProperty()).divide(2));
		planet.translateYProperty().bind(this.heightProperty().subtract(planet.fitHeightProperty()).divide(2));

		undeads = new UndeadGrid(planet, connector, model);
		this.model.addObserverX(undeads);

		planetName = new Label(Integer.toString(model.getId()));
		planetName.translateXProperty().bind((this.widthProperty().subtract(planetName.widthProperty())).divide(2));
		planetName.translateYProperty().bind(planet.fitHeightProperty().add(planet.translateYProperty()));
		fontSize.bind(planet.fitHeightProperty().divide(4));
		planetName.styleProperty()
				.bind(Bindings.concat("-fx-font-size: ", fontSize.asString(), ";", "-fx-text-fill: white;",
						"-fx-effect: dropshadow(three-pass-box, black ,", fontSize.divide(10).asString(), ",",
						fontSize.divide(10).asString(), ",0.0,0.0 );"));
		planetName.setVisible(model.showName());

		this.getChildren().addAll(planet, undeads, planetName);

		drawBase();
		for (int colorId = 0; colorId < 4; colorId++) {
			if (model.hasResource(colorId)) {
				drawResource(colorId);
			}
			if (model.hasHealer(colorId)) {
				base.drawHealer(colorId);
			}
		}
		if (model.hasDarkColumn()) {
			drawDarkColumn();
		}

		if (model.isBlocked()) {
			blockPlanet();
		}

		this.setPickOnBounds(false);
	}

	/**
	 * Create a base for the game.
	 */
	private void drawBase() {
		if (base == null && model.hasBase()) {
			base = new BaseView();
			base.prefHeightProperty().bind(this.heightProperty().multiply(main.Constants.BASE_RATIO));
			base.prefWidthProperty().bind(this.widthProperty().multiply(main.Constants.BASE_RATIO));

			base.translateXProperty().bind(this.widthProperty().subtract(base.prefWidthProperty()).divide(2));

			base.translateYProperty()
					.bind(planet.translateYProperty().subtract(base.prefHeightProperty().multiply(0.8)));
			this.model.addObserverX(base);
		}

		if (model.hasBase()) {
			if (!this.getChildren().contains(base)) {
				this.getChildren().add(base);
			}
		} else {
			this.getChildren().remove(base);
		}
	}

	/**
	 * create a resource for the game
	 * 
	 * @param colorId
	 *            of the resource
	 */
	private void drawResource(int colorId) {
		resource = new ImageView(ImageLoader.getInstance().get("/planetTiles/resource" + colorId + ".png"));

		resource.setRotate(90);

		resource.fitHeightProperty().bind(this.heightProperty().multiply(main.Constants.BASE_RATIO));
		resource.fitWidthProperty().bind(this.widthProperty().multiply(main.Constants.BASE_RATIO));

		resource.translateYProperty().bind(this.heightProperty().subtract(resource.fitHeightProperty()).divide(2));

		resource.translateXProperty().bind(planet.translateXProperty()
				.add(planet.fitWidthProperty().subtract(resource.fitWidthProperty().multiply(0.32))));
		this.getChildren().add(resource);
		connector.connectResource(resource, colorId, model.getId());
	}

	/**
	 * Planet View gets deleted if planet model is deleted. Needed in generator
	 * if a planet is removed from planet.
	 */
	private void close() {
		Pane parent = (Pane) this.getParent();
		if (parent != null) {
			parent.getChildren().remove(this);
		}

	}

	/**
	 * Displays a by evil AI blocked planet.
	 */
	private void blockPlanet() {
		if (model.isBlocked()) {
			if (blockAnimation == null) {
				block0 = new ImageView(ImageLoader.getInstance().get("/planetTiles/block0.png"));
				block0.fitHeightProperty().bind(this.heightProperty());
				block0.fitWidthProperty().bind(this.widthProperty());
				block1 = new ImageView(ImageLoader.getInstance().get("/planetTiles/block1.png"));
				block1.fitHeightProperty().bind(this.heightProperty());
				block1.fitWidthProperty().bind(this.widthProperty());

				block0.toFront();
				block1.toFront();

				FadeTransition fade0 = new FadeTransition(Duration.millis(3000), block0);
				fade0.setFromValue(1.0d);
				fade0.setToValue(0.0d);
				fade0.setCycleCount(2);
				fade0.setAutoReverse(true);

				FadeTransition fade1 = new FadeTransition(Duration.millis(3000), block1);
				fade1.setFromValue(0.0d);
				fade1.setToValue(1.0d);
				fade1.setCycleCount(2);
				fade1.setAutoReverse(true);

				blockAnimation = new ParallelTransition();
				blockAnimation.getChildren().addAll(fade0, fade1);
				blockAnimation.setCycleCount(Timeline.INDEFINITE);
			}
			this.getChildren().addAll(block0, block1);
			blockAnimation.play();

		} else if(blockAnimation != null){
			blockAnimation.stop();
			this.getChildren().removeAll(block0, block1);
		}

	}

	/**
	 * Changes color of a planet. Needed in generator to switch color of displayed planet
	 * in bar where a planet can be chosen to be added to a map. 
	 * @param colorId Color of new planet.
	 */
	private void changeColor(int colorId) {
		planet.setImage(ImageLoader.getInstance().get("/planets/planet" + colorId + model.getType() + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
	}

	/**
	 * Removes the darkColumn from planet. 
	 */
	private void removedarkColumn() {
		this.getChildren().remove(darkColumn);
		darkColumn = null;

	}

	/**
	 * Creates a darkColomn.
	 */
	private void drawDarkColumn() {
		darkColumn = new ImageView(ImageLoader.getInstance().get("/planetTiles/column.png"));

		darkColumn.setRotate(-90);

		darkColumn.fitHeightProperty().bind(this.heightProperty().multiply(main.Constants.BASE_RATIO));
		darkColumn.fitWidthProperty().bind(this.widthProperty().multiply(main.Constants.BASE_RATIO));

		darkColumn.translateYProperty().bind(this.heightProperty().subtract(darkColumn.fitHeightProperty()).divide(2));

		darkColumn.translateXProperty()
				.bind(planet.translateXProperty().subtract(darkColumn.fitWidthProperty().multiply(0.8)));

		connector.connectDarkColumn(darkColumn, model.getId());
		this.getChildren().add(darkColumn);
	}

	/**
	 * Getter for model.
	 * 
	 * @return Planet model.
	 */
	public PlanetModelI getModel() {
		return model;
	}

	/**
	 * Getter for color.
	 * 
	 * @return  Color of planet.
	 */
	public int getColor() {
		return model.getColor();
	}

	/**
	 * Getter for planet's id.
	 * @return Planet ID.
	 */
	public int getPlanetId() {
		return model.getId();
	}

	/**
	 * Moves the player's spaceship.
	 * 
	 * @param player
	 *            Player's spaceship.
	 */
	public void movePlayer(PlayerView player) {
		ImageView iV = player;
		player.setLastParent(this);
	
		if (!iV.fitHeightProperty().isBound()) {
			iV.fitHeightProperty().bind(this.heightProperty().multiply(main.Constants.PLAYER_VRATIO));
			iV.fitWidthProperty().bind(this.widthProperty().multiply(main.Constants.PLAYER_HRATIO));
		}
	
		iV.setTranslateX(0);
		iV.setTranslateY(0);
		iV.setScaleX(1);
		iV.setScaleY(1);
	
		switch (player.getColorId()) {
		case 0:
			iV.setRotate(45);
			break;
		case 1:
			iV.xProperty().bind(this.widthProperty().subtract(iV.fitWidthProperty()));
			iV.setRotate(135);
			break;
		case 2:
			iV.xProperty().bind(this.widthProperty().subtract(iV.fitWidthProperty()));
			iV.yProperty().bind(this.heightProperty().subtract(iV.fitHeightProperty()));
			iV.setRotate(225);
			break;
		case 3:
			iV.yProperty().bind(this.heightProperty().subtract(iV.fitHeightProperty()));
			iV.setRotate(315);
			break;
		}
		this.getChildren().add(player);
		player.setMouseTransparent(false);
	}
	
	/**
	 * Called if untoaster moves undead.
	 * @param dragImage Undead that is dragged.
	 * @param colorId Color of draged undead.
	 */
	public void moveUndead(UndeadView dragImage, int colorId) {
		undeads.moveUndeadView(dragImage, colorId);

	}

	@Override
	public void update(Observable model, Object msg) {
		switch (msg.toString().substring(0, 5)) {
		case "sBase": // set base
			drawBase();
			return;
		case "setDC": // set darkColumn
			drawDarkColumn();
			return;
		case "remDC": // remove darkColumn
			removedarkColumn();
			return;
		case "setRs": // set resource
			drawResource(Character.getNumericValue(msg.toString().charAt(5)));
			return;
		case "chPlC":// change planet color
			changeColor(((PlanetModel) model).getColor());
			return;
		case "block":// block planet
			blockPlanet();
			return;
		case "sucid":// block planet
			close();
			return;
		case "addUn":
			if(this.model.getUndeads(Character.getNumericValue(msg.toString().charAt(5))) == 3) {
				shadow();
			}
			return;	
		case "remUn":
			if(this.model.getUndeads(Character.getNumericValue(msg.toString().charAt(5))) != 2) { //TODO
				return;
			}
		case "wipUn":
			shadow();
			return;		
		}
	}

	/**
	 * Adds a dropshadow of the undeads color if 3 of its kind are on the planet.
	 */
	private void shadow() {
		int counter = 1;
		if(this.getEffect() == null) {			
			DropShadow shadow = new DropShadow();
			shadow.radiusProperty().bind(planet.fitWidthProperty().multiply(0.7));
			shadow.setSpread(0.5);
			shadow.setColor(Color.TRANSPARENT);
			planet.setEffect(shadow);
			timeline = new Timeline();
			timeline.setAutoReverse(false);
			timeline.setCycleCount(-1);
		}
		timeline.stop();
		timeline.getKeyFrames().clear();

		for(int i=0; i<4; i++) {
			if(model.getUndeads(i) == 3) {
				timeline.getKeyFrames().add(new KeyFrame(Duration.millis(1500*(2*counter-1)), new KeyValue(((DropShadow)planet.getEffect()).colorProperty(), main.Constants.UNDEAD_COLORS[i])));
				timeline.getKeyFrames().add(new KeyFrame(Duration.millis(1500*(2*counter)), new KeyValue(((DropShadow)planet.getEffect()).colorProperty(), Color.TRANSPARENT)));	
				counter++;
			}
		}
		if(!timeline.getKeyFrames().isEmpty()) {
			timeline.play();
		}	
	}
}
