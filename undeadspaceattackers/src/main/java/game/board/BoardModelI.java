package game.board;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public interface BoardModelI {

	/**
	 * Getter for the neighbors
	 * @return neighbors
	 */
	ArrayList<Integer> getNeighbors(int planetId);

	/**
	 * Setter for the neighbors
	 * @param neighbors
	 */
	void setNeighbors(ArrayList<ArrayList<Integer>> neighbors);



	/**
	 * Getter for an Iterator of the planets
	 * 
	 * @return an Iterator 
	 */
	Iterator<PlanetModelI> getPlanets();

	/**
	 * Setter for the planets
	 * @param planets
	 */
	void setPlanets(ArrayList<PlanetModelI> planets);
	


	/**
	 * Getter for the startPlanet
	 * @return startPlanet as an integer 
	 */
	int getStartPlanet();

	/**
	 * Setter for the startPlanet
	 * @param startPlanet
	 */
	void setStartPlanet(int startPlanet);

	/**
	 * Getter for the height
	 * @return height as an integer 
	 */
	public int getHeight();

	/**
	 * Getter for the width
	 * @return width as an integer 
	 */
	public int getWidth();

	
	/**
	 * Getter for a special planet
	 * 
	 * @param planetId
	 * @return the planet with the given planetId
	 */
	PlanetModelI getPlanet(int planetId);

	
	/**
	 * Getter for the horizontal offset of all planets.
	 * It's the distance between the position of the model and the view.
	 * 
	 * @return the difference as an Integer
	 */
	public int getOffsetX();
	
	
	/**
	 * Getter for the vertical offset of all planets.
	 * It's the distance between the position of the model and the view.
	 * 
	 * @return the difference as an Integer
	 */
	public int getOffsetY();
	
	/**
	 * Number of planets in map.
	 * @return number planets
	 */
	public int getnPlanets();

}