package game.board;

import java.util.Observer;

public interface PlanetModelI{
	/**
	 * Getter for the planetID
	 * 
	 * @return the planetID as an integer
	 */
	int getId();

	/**
	 * Getter for the planet color
	 * 
	 * @return the color as an integer
	 */
	int getColor();
	
	/**
	 * Getter for the planet type
	 * 
	 * @return the type as an integer
	 */
	int getType();
	
	/**
	 * Getter for the planet x position in the grid
	 * 
	 * @return the x(0) and y(1) coordinate as an integer
	 */
	int[] getPos();
	
	void setAbomination(boolean has);
	void setBase(boolean has);
	void setBlocked(boolean is);
	void setDarkColumn(boolean has);
	void showName(boolean show);
	
	boolean hasAbomination();
	boolean hasBase();
	boolean isBlocked();
	boolean hasDarkColumn();
	boolean showName();


	/**
	 * Getter for the undead number of a given color.
	 * 
	 * @param colorId 
	 * @return the y coordinate as an integer
	 */
	int getUndeads(int colorId);
	

	/**
	 * Return the total amount of undeads on the planet
	 * 
	 * @return the number of undeads on the planet
	 */
	int getNUndeads();
	
	
	/**
	 * Setter for the undead array.
	 * 
	 * e.g.
	 * On position i in the array is Number n.
	 * So n undeads of colorID i are on the planet.
	 * 
	 * @param undeads the array of the undeads
	 */
	void setUndeads(int[] undeads);

	/**
	 * Add one undead of color colorId to the planet.
	 * 
	 * @param colorId the Id of the undead
	 * @param notify if observer should be notified
	 */
	void addUndead(int colorId, boolean notify);
	


	
	/**
	 * Remove one undead of color colorId from the planet.
	 * 
	 * @param colorId the Id of the undead
	 * @param propagate true to notify
	 */
	void removeUndead(int colorId, boolean propagate);
	
	/**
	 * Remove all undeads of a color from the planet.
	 * 
	 * @param colorId the Id of the undead
	 */
	void removeAllUndead(int colorId);
	
	
	
	/**
	 * Getter for the player array.
	 * The ith position is true if palyer i is on the planet
	 * 
	 * @return a boolean[] with player flags
	 */
	boolean[] getPlayers();
	
	/**
	 * Returns true if any player is on the planet
	 * 
	 * @return a boolean
	 */
	boolean hasPlayers();

	/**
	 * Add a player to the planet
	 * 
	 * @param playerId the ID of the player
	 */
	void setPlayer(int playerId, boolean isPlayer);



	/**
	 * sets color of Planet 
	 * @param colorId color of planet
	 */
	public void setColor(int colorId) ;
	
	/**
	 * Checks whether a healer is on the planet.
	 * 
	 * @param colorId the colorId of the healer
	 * @return true if the healer is on the planet
	 */
	boolean hasHealer(int colorId);
	
	/**
	 * Place a healer on the planet
	 * 
	 * @param colorId the colorId of the healer
	 */
	void setHealer(int colorId);

	/**
	 * Checks whether the planet has a resource with given colorId. 
	 * 
	 * @param colorId the color of the resource
	 * @return true/false
	 */
	boolean hasResource(int colorId);

	/**
	 * Set the resource
	 * 
	 * @param colorId the color of the resource
	 */
	void setResource(int colorId);	

	
	/**
	 * Getter for undead
	 * @return true if there are undeads, false if there are not
	 */
	public boolean hasUndead();

	
	/**
	 * removes a random player, caused by abdomination
	 */
	void removeRandomUndead();


	/**
	 * adds an observer to planetView 
	 * @param planetView
	 */
	void addObserverX(Observer planetView);

	
	
	/**
	 * checks the planet for a player with a given playerId
	 * 
	 * @param playerId
	 * @return true if the player with the given iD is on the planet
	 */
	boolean hasPlayer(int playerId);

	
	/**
	 * called by the to propagate that this object will not longer exist
	 */
	public void close();
}