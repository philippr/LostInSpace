package game.board;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Displays all planets and their connection.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class BoardModel implements BoardModelI, Serializable {

	/**
	 * List of planets of neighbors.
	 */

	private ArrayList<ArrayList<Integer>> neighbors;

	/**
	 * The ArrayList of all Planet models.
	 */
	private ArrayList<PlanetModelI> planets;
	
	/**
	 * Planet where players start. Start planet has a base.
	 */
	private int startPlanet;
	
	/**
	 * Width of board.
	 */
	private int width;
	
	/**
	 * Height of board.
	 */
	private int height;
	
	/**
	 * Total number of planets.
	 */
	private int nPlanets;
	


	/**
	 * Instantiates attributes.
	 * @param neighbors Neighbors of all planets. 
	 * @param planets Neighbors of a planet
	 * @param startPlanet Start planet for players.
	 * @param width Width of board.
	 * @param height Height of board.
	 */


	public BoardModel(ArrayList<ArrayList<Integer>> neighbors, ArrayList<PlanetModelI> planets, int startPlanet, int width, int height) {
		this.neighbors = neighbors;
		this.planets = planets;
		this.startPlanet = startPlanet;
		this.width = width;
		this.height = height;
		this.nPlanets = planets.size();
		
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#getNeighbors()
	 */
	@Override
	public ArrayList<Integer> getNeighbors(int planetId) {
		return neighbors.get(planetId);
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#setNeighbors(java.util.ArrayList)
	 */
	@Override

	public void setNeighbors(ArrayList<ArrayList<Integer>> neighbor) {
		this.neighbors = neighbor;
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#getPlanets()
	 */
	@Override
	public Iterator<PlanetModelI> getPlanets() {
		return planets.iterator();
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#setPlanets(board.PlanetI[])
	 */
	@Override
	public void setPlanets(ArrayList<PlanetModelI> planets) {
		this.planets = planets;
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#getStartPlanet()
	 */
	@Override
	public int getStartPlanet() {
		return startPlanet;
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#setStartPlanet(int)
	 */
	@Override
	public void setStartPlanet(int startPlanet) {
		this.startPlanet = startPlanet;
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#setSmall(boolean)
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/* (non-Javadoc)
	 * @see board.BoardModelI#setSmall(boolean)
	 */
	@Override
	public int getHeight() {
		return height;
	}

	/*
	 * (non-Javadoc)
	 * @see game.board.BoardModelI#getPlanet(int)
	 */
	@Override
	public PlanetModelI getPlanet(int planetId) {
		return planets.get(planetId);
	}

	/*
	 * (non-Javadoc)
	 * @see game.board.BoardModelI#getOffsetX()
	 */
	@Override
	public int getOffsetX() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see game.board.BoardModelI#getOffsetY()
	 */
	@Override
	public int getOffsetY() {
		return 0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.board.BoardModelI#getnPlanets()
	 */
	@Override
	public int getnPlanets() {
		return nPlanets;
	}
}
