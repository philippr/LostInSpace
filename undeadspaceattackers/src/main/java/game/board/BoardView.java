package game.board;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import game.Connector;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import player.PlayerI;
import utils.RemovingLine;

/**
 * BoardView displays all the field with all planets, spaceships and connections.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class BoardView extends Pane implements  Observer{
	
	/**
	 * Pane where objects can be stuck on.
	 */
	private StackPane stack;
	
	/**
	 * Model representing information in board.
	 */
	private game.board.BoardModelI model;
	
	/**
	 * Gridpane representing the grid of the field.
	 */
	private GridPane field;
	
	/**
	 * Connections between planets.
	 */
	private Pane connections;
	
	/**
	 * List of all planet views.
	 */
	private ArrayList<PlanetView> views;
	
	/**
	 * Scale is used to resize.
	 */
	private double scale = 1;
	
	/**
	 * Used in resizing.
	 */
	private double[] scaleXY = {1.0, 1.0};
	
	/**
	 * Pane for moved object.
	 */
	private Pane movePane;
	
	/**
	 * List of players represented by their spaceships.
	 */
	private ArrayList<PlayerView> spaceships;
	
	/**
	 * Connector to connect views and controllers.
	 */
	private Connector connector;

	/**
	 * Creates a view displaying the hole board. This constructor is used if a saved game is loaded because
	 * does not need to know players.
	 * @param model Boardmodel with information about map.
	 * @param connector Connector to connect views and controllers.
	 * @param size Size of board.
	 */
	public BoardView(game.board.BoardModelI model, Connector connector , double[] size){
		this(model, null, connector, size);
	}

	
	
	/**
	 * Creates a view displaying the hole board. This constructor is used if a new game is created and players need to 
	 * be set on their start planet initially.
	 * @param boardModel Boardmodel with information about map.
	 * @param players Players who are playing the game.
	 * @param connector Connector to connect views and controllers.
	 * @param size Size of board.
	 */
	public BoardView(game.board.BoardModelI boardModel, ArrayList<PlayerI> players, Connector connector, double[] size) {
		stack = new StackPane();
		connections = new Pane();
		field = new GridPane();
		movePane = new Pane();
		spaceships = new ArrayList<>(4);
		views = new ArrayList<>();
		this.connector = connector;
		scale = 1;
		
		setModel(boardModel, players);

		movePane.setPrefHeight(10);
		movePane.setPrefWidth(10);
		movePane.setMouseTransparent(true);	
		
		field.setGridLinesVisible(false);
		
		field.setPadding(new Insets(5,5,5,5));

		this.getChildren().add(stack);

		stack.getChildren().add(connections);
		stack.getChildren().add(field);
		this.getChildren().add(movePane);

		this.widthProperty().addListener((obs, oldVal, newVal) -> {
			this.resize(obs, oldVal, newVal, true);
		});
		this.heightProperty().addListener((obs, oldVal, newVal) -> {
			this.resize(obs, oldVal, newVal, false);
		});

		changeMapRatio(size[0], size[1], true);
		stack.setPickOnBounds(false);
		movePane.setPickOnBounds(false);
		connections.setMouseTransparent(true);
	}


	/**
	 * Getter of the stack
	 * @return stack
	 */
	public StackPane getStack() {
		return stack;
	}



	/**
	 * Getter for the boardModel
	 * 
	 * @return model
	 */
	public game.board.BoardModelI getModel() {
		return model;
	}



	/**
	 * Getter for GridPane.
	 * @return Gridpane with field.
	 */
	public GridPane getField() {
		return field;
	}

	/**
	 * Getter for connections.
	 * @return connections
	 */
	public Pane getConnections() {
		return connections;
	}



	/**
	 * Getter for the views of the planets
	 * 
	 * @return views
	 */
	public ArrayList<PlanetView> getViews() {
		return views;
	}

	/**
	 * Getter for the spaceship
	 * 
	 * @return spaceship
	 */
	public ArrayList<PlayerView> getSpaceships() {
		return spaceships;
	}



	/**
	 * Getter for movePane.
	 * @return movePane
	 */
	public Pane getMovePane() {
		return movePane;
	}



	/**
	 * Getter for the scale of the board
	 * 
	 * @return scale
	 */
	public double getScale() {
		return scale;
	}



	/**
	 * Setter for new BoardModel. If a new map is created with the map generator
	 * the game model needs to be changed and set to new information.
	 * 
	 * @param model
	 *            new boardModel
	 * @param players
	 *            Players in game.
	 */
	public void setModel(BoardModelI model, ArrayList<PlayerI> players) {
		this.model = model;
		int mapHeight = model.getHeight()-1;
		int mapWidth = model.getWidth()-1;
	
		Group gridLinesSave = null;
		
		for(Node child: field.getChildren()){
			if(child instanceof Group) {
				gridLinesSave = (Group) child;
				break;
			}
		}
		field.getChildren().clear();
		if(gridLinesSave != null){
			field.getChildren().add(gridLinesSave);			
		}
		
		connections.getChildren().clear();	
	
		views.clear();
		
		ColumnConstraints column;
		RowConstraints row;
	
		field.getColumnConstraints().clear();
		field.getRowConstraints().clear();
		
		for (int i = 0; i < mapWidth+1 ; i++) {
			column = new ColumnConstraints();
			column.setPercentWidth(100.0 / mapWidth);
			field.getColumnConstraints().add(column);
		}
	
		for (int i = 0; i < mapHeight+1; i++) {
			row = new RowConstraints();
			row.setPercentHeight(100.0 / mapHeight);
			field.getRowConstraints().add(row);
		}
		
		
		for(Iterator<PlanetModelI> iterator=model.getPlanets(); iterator.hasNext();){
			PlanetModelI planet = iterator.next();
			addPlanet(planet);
	
		}
	
	
		if(players != null){
			for (PlayerI p : players) {
				PlayerView pV = new PlayerView(p.getId(), p.getColorId());
				spaceships.add(pV);
				p.addObserverX(spaceships.get(spaceships.size()-1));
				views.get(p.getPlanetId()).movePlayer(pV);
				
			}
		}
		drawConnections();
		changeMapRatio(getPrefWidth(), getPrefHeight(), true);
	}



	/**
	 * Setter for FieldBackground. Sets Field in given color.
	 * @param color
	 */
	public void setFieldBackground(String color){
		connections.setStyle("-fx-background-color:" + color +";");
	}



	/**
	 * Scale the board.
	 * @param event Scroll event that happened.
	 * @param factor Factor for scaling.
	 * @param restfactor Mathematical factor to ensure correct shift.
	 */
	public void scale(ScrollEvent event, double factor, double restfactor){
		double centerx = stack.getWidth()/2 + stack.getLayoutX();
		double centery = stack.getHeight()/2 + stack.getLayoutY();
		
		scale*=factor;
		stack.setScaleX(scale);
		stack.setScaleY(scale);

		movePane.setScaleX(scale);
		movePane.setScaleY(scale);

		stack.setLayoutX(stack.getLayoutX() + (event.getX()-centerx-stack.getTranslateX())*restfactor);
		stack.setLayoutY(stack.getLayoutY() + (event.getY()-centery-stack.getTranslateY())*restfactor);
		reposition();
	}
	
	/**
	 * Create connections of the planets.
	 */
	private void drawConnections() {
		PlanetModelI a;
		PlanetModelI b;
		
		for(Iterator<PlanetModelI> iterator=model.getPlanets(); iterator.hasNext();){
			a = iterator.next();
			
			for (int neighborId: model.getNeighbors(a.getId())) {
				b = model.getPlanet(neighborId);

				if (a.getId() > b.getId()) {
					continue;
				}
				connectPlanets(a, b);
			}
		}
	}

	/**
	 * Connects planets in BoarView if there exists a line between them.
	 * Connection does resize when window size changes.
	 * 
	 * @param planetA
	 *            Planet where connection starts.
	 * @param planetB
	 *            Planet where connection ends.
	 */
	public void connectPlanets(PlanetModelI planetA, PlanetModelI planetB){
		RemovingLine line = new RemovingLine(planetA, planetB);
		line.startXProperty().bind(
				views.get(planetA.getId()).layoutXProperty().add(views.get(planetA.getId()).widthProperty().divide(2.0)));
		line.endXProperty().bind(
				views.get(planetB.getId()).layoutXProperty().add(views.get(planetB.getId()).widthProperty().divide(2.0)));
		line.startYProperty().bind(
				views.get(planetA.getId()).layoutYProperty().add(views.get(planetA.getId()).heightProperty().divide(2.0)));
		line.endYProperty().bind(
				views.get(planetB.getId()).layoutYProperty().add(views.get(planetB.getId()).heightProperty().divide(2.0)));
		line.setStroke(Color.WHITE);
		line.strokeWidthProperty().bind(views.get(planetA.getId()).heightProperty().divide(15.0));

		connections.getChildren().add(line);
		connector.connectLine(line);
	}

	
	/**
	 * Change the map ratio and size. Needed initial or after adding/removing
	 * rows/columns.
	 * 
	 * @param width
	 *            width of the new map
	 * @param height
	 *            height of the new map
	 * @param initial
	 *            true if this is the initial ratio change
	 */
	public void changeMapRatio(double width, double height, boolean initial){
		this.setPrefWidth(width);
		this.setPrefHeight(height);
	
		double s = Math.min(width / model.getWidth(), height / model.getHeight());
		field.setPrefSize(s * model.getWidth(), s * model.getHeight());
		stack.setPrefSize(s * model.getWidth(), s * model.getHeight());
	
		if(initial){
			changePrefSize(width, height);
		}
		
		scaleXY[0] = width / stack.getPrefWidth();
		scaleXY[1] = height / stack.getPrefHeight();
		double scale = Math.min(scaleXY[0], scaleXY[1]);
		this.scale = scale;
		
		if(! initial){
			resize(null, width, width, true);
			resize(null, height, height, false);
		} else {
			stack.setTranslateX((width - stack.getPrefWidth() * scale) / 2);
			stack.setTranslateY((height - stack.getPrefHeight() * scale) / 2);
		}
	}

	/**
	 * Handels resizing.
	 * 
	 * @param obs
	 *            ObservableValue
	 * @param oldVal
	 *            old value
	 * @param newVal
	 *            new value
	 * @param horizontal
	 *            States whether resizing was horizontal (true) or vertical
	 *            (false).
	 */
	public void resize(ObservableValue<? extends Number> obs, Number oldVal, Number newVal, boolean horizontal) {
		if (stack.getWidth() == 0) {
			return;
		}
		
		scale/=Math.min(scaleXY[0], scaleXY[1]);
		
		if(horizontal){
			scaleXY[0] = scaleXY[0]/oldVal.doubleValue()*newVal.doubleValue();
		} else {
			scaleXY[1] = scaleXY[1]/oldVal.doubleValue()*newVal.doubleValue();
		}
		this.scale = scale * Math.min(scaleXY[0], scaleXY[1]);

		stack.setScaleX(scale);
		stack.setScaleY(scale);

		movePane.setScaleX(scale);
		movePane.setScaleY(scale);

		stack.setTranslateX((this.getWidth() - stack.getPrefWidth()) / 2);
		stack.setTranslateY((this.getHeight() - stack.getPrefHeight()) / 2);
	
		reposition();
	}

	/**
	 * Change the preferenced Size
	 * 
	 * @param width
	 *            Width of board.
	 * @param height
	 *            Height of board.
	 */
	public void changePrefSize(double width, double height) {
		if (field.getPrefHeight() < 0) {
			double s = Math.min(width / model.getWidth(), height / model.getHeight());
			field.setPrefSize(s * model.getWidth(), s * model.getHeight());
			stack.setPrefSize(s * model.getWidth(), s * model.getHeight());
		}
	}

	/**
	 * Reposition objects after scaling.
	 */
	public void reposition() {
		// the diff of stack-with(map grid) and game-width(board).
		double diffX = getWidth() - getStack().getWidth()*getScale();
    	double diffY = getHeight() - getStack().getHeight()*getScale();
    	
    	// value of retranslation
    	double transx;
    	double transy;
    	
    	// set the stack to the boarders if necessary
    	
    	if(diffX<0){ // gridX>boardX
    		transx = Math.min(Math.max(stack.getLayoutX(), diffX/2), -diffX/2);
    	} else {// gridX<boardX
        	transx = Math.min(Math.max(stack.getLayoutX(), -diffX/2), 
        			getWidth()-getStack().getWidth()*getScale()-diffX/2);
    	}
    	
    	if(diffY<0){// gridY>boardY
    		transy = Math.min(Math.max(stack.getLayoutY(), diffY/2), -diffY/2);
		} else {// gridY<boardY
			transy = Math.min(Math.max(stack.getLayoutY(), -diffY / 2),
					getHeight() - getStack().getHeight() * getScale() - diffY / 2);
		}
    	stack.setLayoutX(transx);
    	stack.setLayoutY(transy);
		
	}

	/**
	 * Adds a message box with an error message to the boardView.
	 * 
	 * @param msgBox
	 *            Box with error messages.
	 */
	public void addMsgBox(Pane msgBox) {
		this.getChildren().add(msgBox);
		msgBox.layoutXProperty().bind((this.widthProperty().subtract(msgBox.widthProperty())).divide(2));
		msgBox.toFront();
		StackPane.setAlignment(msgBox, Pos.TOP_CENTER);
	}



	/*
	 * (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable model, Object msg) {
		switch(msg.toString().substring(0, 5)){
		case "adPln":
			addPlanet(((BoardModelI) model).getPlanet(Integer.parseInt(msg.toString().substring(5))));
			return;
		case "conct":
			String[] iDs = msg.toString().substring(5).split("#");
			connectPlanets(this.model.getPlanet(Integer.parseInt(iDs[0])), this.model.getPlanet(Integer.parseInt(iDs[1])));
			return;
		}
		
	}


	/**
	 * Adds a planet to the board view.
	 * 
	 * @param planet
	 *            Planet that is added to the board.
	 */
	private void addPlanet(PlanetModelI planet) {
		PlanetView planetView = new PlanetView(planet, connector);
		views.add(planetView);
		GridPane.setConstraints(planetView, planet.getPos()[0]-model.getOffsetX(), planet.getPos()[1]-model.getOffsetY());
		field.getChildren().add(planetView);
	}
}
