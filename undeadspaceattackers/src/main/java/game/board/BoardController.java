package game.board;

import java.util.ArrayList;
import java.util.LinkedList;

import cards.Card;
import game.DragContainer;
import game.GameControllerI;
import game.cardinfo.PlayerPanel;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import utils.MessageBoxController;

/**
 * BoardController handles all events happening on board.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class BoardController implements BoardControllerI {

	/**
	 * View showing the board.
	 */
	private BoardView view;
	
	/**
	 * Model with information about board.
	 */
	private BoardModelI model;
	
	/**
	 * Last mouse x position in boardView.
	 */
	private double mousex;
	
	/**
	 * Last mouse y position in boardView.
	 */
	private double mousey;
	
	/**
	 * Drag container for the currently dragged object.
	 */
	private final DragContainer draged;
	
	/**
	 * Last mouse positions.
	 */
	private final LinkedList<double[]> posPath;
	
	/**
	 * GameController which handels events happening in the game.
	 */
	private final GameControllerI gameController;
	
	/**
	 * Pane that is moved.
	 */
	private final Pane movePane;
	
	/**
	 * Message box controller to add error messages.
	 */
	MessageBoxController msgBoxController;

	/**
	 * Instantiates attributes and activates messages.
	 * @param gameController Controller of game.
	 * @param dragContainer Container contains dragged object.
	 * @param movePane Pane that is moved.
	 */
	public BoardController(GameControllerI gameController, DragContainer dragContainer, Pane movePane, MessageBoxController msgBoxController) {
		posPath = new LinkedList<>();
		this.gameController = gameController;
		draged = dragContainer;
		this.movePane = movePane;
		this.msgBoxController = msgBoxController;
	}

	/* (non-Javadoc)
	 * @see game.board.BoardControllerI#setBoardView(game.board.BoardView)
	 */
	@Override
	public void setBoardView(BoardView boardView) {
		this.view = boardView;
	}
	
	/* (non-Javadoc)
	 * @see game.board.BoardControllerI#setBoardModel(game.board.BoardModelI)
	 */
	@Override
	public void setBoardModel(BoardModelI boardModel) {
		this.model = boardModel;
	}

	/* (non-Javadoc)
	 * @see game.board.BoardControllerI#connectAll()
	 */
	@Override
	public void connectAll() {
		
		// Important to place the movepane under the courser right after beginning the game
		// movepane: container for all items on the board (spaceship & undeads)
		view.setOnMouseEntered(event -> {
			view.getMovePane().setLayoutX(event.getSceneX() - view.getMovePane().getWidth() / 2);
			view.getMovePane().setLayoutY(event.getSceneY() - view.getMovePane().getHeight() / 2);
		});
		
		
		view.setOnMousePressed(event -> {
			mousex = event.getSceneX();
			mousey = event.getSceneY();
			if (draged.getMsg() == "") {
				draged.set(view.getStack(), "map", view, -1);
			}
		});

		// prevent getting stuck by using Tabulator to leave the window and focus another.
		mouseExited();
		mouseMovedAndDraged();
		

		scroll();
		mouseReleased();
		mouseDragReleased();

		playerSpaceShips();

		planetViews();

		
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.board.BoardControllerI#mouseExited()
	 */
	public void mouseExited () {
		view.setOnMouseExited(event -> {
			if (draged.getMsg() == "ship") {
				view.getMovePane().getChildren().remove(draged.getDragImage());
				view.getMovePane().setRotate(0);
				((PlanetView) draged.getParentPane()).movePlayer((PlayerView) draged.getDragImage());
			}

			if (draged.getMsg() == "map") {
				draged.clear();
			}

			if (draged.getMsg() == "undead") {
				view.getMovePane().getChildren().remove(draged.getDragImage());
				((UndeadView) draged.getDragImage()).getParentGrid().moveUndeadView((UndeadView) draged.getDragImage(),
						draged.getColorId());
			}
			draged.clear();

		});
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.board.BoardControllerI#mouseMovedAndDraged()
	 */
	@Override
	public void mouseMovedAndDraged() {

		view.setOnMouseMoved(event -> {
			view.getMovePane().setLayoutX(event.getSceneX() - view.getMovePane().getWidth() / 2);
			view.getMovePane().setLayoutY(event.getSceneY() - view.getMovePane().getHeight() / 2);
			event.consume();
		});

		view.setOnMouseDragged(event -> {
			if (draged.getMsg() == "map") {
				view.getStack().setLayoutX(view.getStack().getLayoutX() + (event.getSceneX() - mousex));
				view.getStack().setLayoutY(view.getStack().getLayoutY() + (event.getSceneY() - mousey));
				view.reposition();
				mousex = event.getSceneX();
				mousey = event.getSceneY();
			}

			view.getMovePane().setLayoutX(event.getSceneX() - view.getMovePane().getWidth() / 2);
			view.getMovePane().setLayoutY(event.getSceneY() - view.getMovePane().getHeight() / 2);
		});
	}


	/*
	 * (non-Javadoc)
	 * @see game.board.BoardControllerI#scroll()
	 */
	@Override
	public void scroll() {
		view.setOnScroll(event -> {
			if (event.getDeltaY() > 0) {
				if (view.getScale() > main.Constants.ZOOM_MIN) {
					view.scale(event, main.Constants.ZOOM_FACTOR, main.Constants.ZOOM_FACTOR_REST);

				}
			} else {
				if (view.getScale() < main.Constants.ZOOM_MAX) {
					view.scale(event, main.Constants.ZOOM_FACTOR_INVERSE, main.Constants.ZOOM_FACTOR_REST_INVERSE);
				}
			}
		});
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.board.BoardControllerI#mouseReleased()
	 */
	@Override
	public void mouseReleased() {
		view.setOnMouseReleased(event -> {

			if (draged.getMsg() == "map") {
				draged.clear();
			}

			// just listen to the primary button
			if (event.getButton() != MouseButton.PRIMARY) {
				return;
			}

			if (draged.getMsg() == "ship") {
				view.getMovePane().getChildren().remove(draged.getDragImage());
				view.getMovePane().setRotate(0);
				((PlanetView) draged.getParentPane()).movePlayer((PlayerView) draged.getDragImage());

			}

			if (draged.getMsg() == "undead") {
				view.getMovePane().getChildren().remove(draged.getDragImage());
				((UndeadView) draged.getDragImage()).getParentGrid().moveUndeadView((UndeadView) draged.getDragImage(),
						draged.getColorId());
			}
			draged.clear();
			event.consume();
		});
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.board.BoardControllerI#mouseDragReleased()
	 */
	public void mouseDragReleased() {
		view.setOnMouseDragReleased(event -> {
			// just listen to the primary button
			if (event.getButton() != MouseButton.PRIMARY) {
				return;
			}

			if (draged.getMsg() == "ship") {
				view.getMovePane().getChildren().remove(draged.getDragImage());
				((PlanetView) draged.getParentPane()).movePlayer((PlayerView) draged.getDragImage());

			}
			if (draged.getMsg() == "undead") {
				view.getMovePane().getChildren().remove(draged.getDragImage());
				((UndeadView) draged.getDragImage()).getParentGrid().moveUndeadView((UndeadView) draged.getDragImage(),
						draged.getColorId());
			}

			if (draged.getMsg() == "card") {
				if (gameController.cardThrown((Card) draged.getBoxedObj(), draged.getParentId())) {
					Pane parent = (Pane) draged.getDragPane().getParent();
					parent.getChildren().remove(draged.getDragPane());
				} else {
					((PlayerPanel) draged.getParentPane()).moveCard(draged.getDragPane());
				}
			}
			
			if(draged.getMsg() == "base"){
				movePane.getChildren().clear();
			}
			draged.clear();
			event.consume();

		});
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.board.BoardControllerI#playerSpaceShips()
	 */
	@Override
	public void playerSpaceShips() {
		for (PlayerView pV : view.getSpaceships()) {
			pV.setOnDragDetected(event -> {
				pV.startFullDrag();
				event.consume();
			});

			pV.setOnMousePressed(event -> {
				// just listen to the primary button
				if (event.getButton() != MouseButton.PRIMARY) {
					return;
				}

				draged.set(pV, "ship", (Pane) pV.getParent(), pV.getColorId());
				draged.setRotate(pV.getRotate());
				draged.setParentId(((PlayerView) pV).getLastParent().getPlanetId());
				draged.setId(pV.getPlayerId());

				double px = (event.getX() - pV.getX()) / pV.getFitWidth();
				double py = (event.getY() - pV.getY()) / pV.getFitHeight();

				pV.xProperty().unbind();
				pV.yProperty().unbind();
				pV.fitHeightProperty().unbind();
				pV.fitWidthProperty().unbind();
				pV.setX(0);
				pV.setY(0);
				pV.setRotate(0);

				posPath.clear();
				posPath.add(new double[] { event.getScreenX(), event.getScreenY() });

				draged.getParentPane().getChildren().remove(draged.getDragImage());
				view.getMovePane().getChildren().add(draged.getDragImage());

				pV.setTranslateX(view.getMovePane().getWidth() / 2 - pV.getFitWidth() / 2);
				pV.setTranslateY(view.getMovePane().getHeight() / 2 - pV.getFitHeight() / 2);
				pV.setTranslateX(pV.getTranslateX() - (px - 0.5) * pV.getFitWidth());
				pV.setTranslateY(pV.getTranslateY() - (py - 0.5) * pV.getFitHeight());

				view.getMovePane().setRotate(draged.getRotate());

				event.consume();
			});

			pV.setOnMouseDragged(event -> {
				posPath.add(new double[] { event.getScreenX(), event.getScreenY() });
				// rotate if the Path is long enough
				if (posPath.size() == 10) {
					double dx = posPath.getLast()[0] - posPath.getFirst()[0];
					double dy = posPath.getLast()[1] - posPath.getFirst()[1];

					if (dx == 0) {
						dx = Double.MIN_VALUE;
					}

					double a = Math.toDegrees(Math.atan(dy / dx));

					if (dx > 0 && dy > 0) {
						view.getMovePane().setRotate(a + 90);
					} else if (dx > 0 && dy <= 0) {
						view.getMovePane().setRotate(90 + a);
					} else if (dx <= 0 && dy < 0) {
						view.getMovePane().setRotate(a + 270);
					} else if (dx <= 0 && dy >= 0) {
						view.getMovePane().setRotate(-90 + a);
					}
					posPath.remove();
				}
			});
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.board.BoardControllerI#planetViews()
	 */
	@Override
	public void planetViews() {
		for (PlanetView target : view.getViews()) {
			target.setOnMouseDragReleased(event -> {

				// just listen to the primary button
				if (event.getButton() != MouseButton.PRIMARY) {
					return;
				}

				if (draged.getMsg() == "ship") {
					event.consume();
					if (gameController.moveSpaceship(draged.getId(), draged.getParentId(), target.getPlanetId())) {
						view.getMovePane().getChildren().remove(draged.getDragImage());
						target.movePlayer((PlayerView) draged.getDragImage());
					} else {
						view.getMovePane().getChildren().remove(draged.getDragImage());
						((PlanetView) draged.getParentPane()).movePlayer((PlayerView) draged.getDragImage());

					}
					draged.clear();
				}
				if (draged.getMsg() == "undead") {
					event.consume();
					if (gameController.moveUndead(draged.getColorId(), draged.getParentId(), target.getPlanetId())) {
						view.getMovePane().getChildren().remove(draged.getDragImage());
						target.moveUndead((UndeadView) draged.getDragImage(), draged.getColorId());
						((UndeadView) draged.getDragImage()).pick();
					} else {
						view.getMovePane().getChildren().remove(draged.getDragImage());
						((UndeadView) draged.getDragImage()).getParentGrid()
								.moveUndeadView((UndeadView) draged.getDragImage(), draged.getColorId());
					}
					draged.clear();
				}
				
				
				if(draged.getMsg() == "base"){
					gameController.buildBase(target.getPlanetId());
					movePane.getChildren().clear();
					draged.clear();
				}
				
			});
		}
	}
	
	/* (non-Javadoc)
	 * @see game.board.BoardControllerI#connectUndead(game.board.UndeadView, game.board.UndeadGrid)
	 */
	@Override
	public void connectUndead(UndeadView undead, UndeadGrid parent) {

		undead.setOnDragDetected(event -> {
			undead.startFullDrag();
			event.consume();
		});

		undead.setOnMousePressed(event -> {

			if (!gameController.mayDragUndead() || undead.isPicked()) {
				event.consume();
				return;
			}

			draged.set(undead, "undead", (Pane) parent, undead.getColorId());
			draged.setParentId(undead.getParentGrid().getPlanetId());

			double px = (event.getX() - undead.getX()) / undead.getFitWidth();
			double py = (event.getY() - undead.getY()) / undead.getFitHeight();

			undead.xProperty().unbind();
			undead.yProperty().unbind();

			undead.fitHeightProperty().unbind();
			undead.fitWidthProperty().unbind();
			undead.setX(0);
			undead.setY(0);
			undead.setRotate(0);

			posPath.clear();
			posPath.add(new double[] { event.getScreenX(), event.getScreenY() });
			parent.getChildren().remove((ImageView)undead);
			view.getMovePane().getChildren().add(draged.getDragImage());

			undead.setTranslateX(view.getMovePane().getWidth() / 2 - undead.getFitWidth() / 2);
			undead.setTranslateY(view.getMovePane().getHeight() / 2 - undead.getFitHeight() / 2);
			undead.setTranslateX(undead.getTranslateX() - (px - 0.5) * undead.getFitWidth());
			undead.setTranslateY(undead.getTranslateY() - (py - 0.5) * undead.getFitHeight());

			view.getMovePane().setRotate(draged.getRotate());

			event.consume();

		});
		undead.setOnMouseDragged(event -> {
		});
	}


	/* (non-Javadoc)
	 * @see game.board.BoardControllerI#getNeighbours(int)
	 */
	@Override
	public ArrayList<Integer> getNeighbours(int planetId) {
		return model.getNeighbors(planetId);

	}


	/* (non-Javadoc)
	 * @see game.board.BoardControllerI#getPlanet(int)
	 */
	@Override
	public PlanetModelI getPlanet(int planetId) {
		return model.getPlanet(planetId);
	}

	
	/* (non-Javadoc)
	 * @see game.board.BoardControllerI#getStartPlanet()
	 */
	@Override
	public int getStartPlanet() {
		return model.getStartPlanet();
	}
	
	/**
	 * Shows an error message if players does something wrong.
	 */
	public void showMessage(String s) {	
		msgBoxController.addMsg(s);
	}
}
