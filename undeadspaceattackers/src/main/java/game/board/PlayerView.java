package game.board;

import java.util.Observable;
import java.util.Observer;

import javafx.scene.image.ImageView;
import utils.ImageLoader;

/**
 * 
 * Player displays the player's spaceship in player's color.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class PlayerView extends ImageView implements Observer{

	/**
	 * Color of spaceship for player.
	 */
	private final int colorId;
	
	/**
	 * Player's id.
	 */
	private final int playerId;
	
	/**
	 * Last parent so player's spaceship can be moved.
	 */
	private PlanetView lastParent=null;

	
	/**
	 * Creates new playerView in shape of a spaceship for a certain player.
	 * @param playerId Id of the player the playerView is created for.
	 * @param colorId Color of the spaceship that is created.
	 */
	public PlayerView(int playerId, int colorId){
		super(ImageLoader.getInstance().get("/players/player"+colorId+".png"));
		this.colorId = colorId;
		this.playerId = playerId;
	}


	/**
	 * Getter for lastParent
	 * @return lastParent
	 */
	public PlanetView getLastParent() {
		return lastParent;
	}


	/**
	 * Setter for lastParent
	 * @param parent
	 */
	public void setLastParent(PlanetView parent) {
		this.lastParent = parent;
	}

	/**
	 * Getter for color's ID.
	 * @return ID as an integer.
	 */
	public int getColorId() {
		return colorId;
	}

	/**
	 * Getter for player's ID.
	 * @return player's ID.
	 */
	public int getPlayerId() {
		return playerId;
	}


	@Override
	public void update(Observable model, Object msg) {
		if(msg.toString().substring(0, 5).equals("killP")){ //kill player
			lastParent.getChildren().remove(this);
		}
	}
}
