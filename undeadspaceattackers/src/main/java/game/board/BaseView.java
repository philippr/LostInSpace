package game.board;


import java.util.Observable;
import java.util.Observer;

import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import utils.ImageLoader;

/**
 * View displaying a base.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class BaseView extends StackPane implements Observer{
	
	
	/**
	 * Creates a base view with image of a base.
	 */
	public BaseView(){
		ImageView iV = new ImageView(ImageLoader.getInstance().get("/planetTiles/base.png", main.Constants.PLANET_LOAD_SIZE/3, main.Constants.PLANET_LOAD_SIZE/3));
		iV.fitHeightProperty().bind(this.prefHeightProperty());
		iV.fitWidthProperty().bind(this.prefWidthProperty());
		this.getChildren().add(iV);
	}

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object msg) {
		if(msg.toString().substring(0, 5).equals("setHl")) {
			drawHealer(msg.toString().charAt(5));
		}
	}


	/**
	 * Creates healer displayed on a base.
	 * @param colorId Color of healer.
	 */
	public void drawHealer(int colorId) {
		ImageView iV = new ImageView(ImageLoader.getInstance().get("/planetTiles/healer" + colorId + ".png", main.Constants.PLANET_LOAD_SIZE/3, main.Constants.PLANET_LOAD_SIZE/3));
		iV.fitHeightProperty().bind(this.prefHeightProperty());
		iV.fitWidthProperty().bind(this.prefWidthProperty());
		this.getChildren().add(iV);
	}
}