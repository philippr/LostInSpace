package game.board;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ThreadLocalRandom;

import game.Connector;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import utils.ImageLoader;

/**
 * Undead grid shows arrangement of undeads on grid. 
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class UndeadGrid extends Pane implements Observer{
	
	/**
	 * List of views representing undeads.
	 */
	private final ArrayList< LinkedList<ImageView>> views;
	
	/***
	 * View representing a planet where undeads are arranged on.
	 */
	private final ImageView planet;
	
	/**
	 * Connector connects views to controllers.
	 */
	private final Connector connector;
	
	/**
	 * Planet model containing infomation about all planet tiles and which
	 * undeads are on a planet.
	 */
	private final PlanetModelI model;
	
	/**
	 * View for the abomination. Initially there are no abominations on a planet.
	 */
	private ImageView abomination = null;
	
	/**
	 * Array of size and translation of the undead ImageViews in relation to the
	 * planet size for 1-12 undeads.
	 * {size in relation to the planet size}, {position of the n_th undead in
	 *  relation to the planet position}
	 */
	private static final double[][][] pos = new double[][][] {
		{{0.707}, {0.1465, 0.1465}},
		{{0.5}, {0, 0.25}, {0.5, 0.25}},
		{{0.422}, {0.289, 0.078}, {0.078,0.5}, {0.5,0.5}},
		{{0.422}, {0.078,0.078}, {0.5,0.078}, {0.078,0.5}, {0.5,0.5}},
		{{0.3}, {0.11,0.11}, {0.59,0.11}, {0.35,0.35}, {0.11, 0.59}, {0.59, 0.59}},
		{{0.306}, {0.194, 0.041}, {0.194, 0.347}, {0.194, 0.635}, {0.5, 0.041}, {0.5, 0.347}, {0.5, 0.635}},
		{{0.306}, {0.194, 0.041}, {0.347, 0.347}, {0.194, 0.635}, {0.5, 0.041}, {0.041, 0.347}, {0.5, 0.635}, {0.653, 0.347}},
		{{0.274}, {0.089, 0.089}, {0.363, 0.089}, {0.637, 0.089}, {0.089, 0.363}, {0.637,0.363}, {0.089, 0.637,}, {0.363, 0.637}, {0.637,0.637}},
		{{0.274}, {0.089, 0.089}, {0.363, 0.089}, {0.637, 0.089}, {0.089, 0.363}, {0.363,0.363}, {0.637,0.363}, {0.089, 0.637,}, {0.363, 0.637}, {0.637,0.637}},
		{{0.2425}, {0.015, 0.265}, {0.015, 0.5}, {0.258, 0.265}, {0.258, 0.5}, {0.5, 0.265}, {0.5, 0.5}, {0.742, 0.265}, {0.742, 0.5}, {0.37875, 0.015}, {0.37875, 0.7425}},
		{{0.2425}, {0.015, 0.265}, {0.015, 0.5}, {0.258, 0.265}, {0.258, 0.5}, {0.5, 0.265}, {0.5, 0.5}, {0.742, 0.265}, {0.742, 0.5}, {0.37875, 0.015}, {0.2575, 0.7425}, {0.5, 0.7425}},
		{{0.2425}, {0.015, 0.265}, {0.015, 0.5}, {0.258, 0.265}, {0.258, 0.5}, {0.5, 0.265}, {0.5, 0.5}, {0.742, 0.265}, {0.742, 0.5}, {0.2575, 0.015}, {0.5, 0.015}, {0.2575, 0.7425}, {0.5, 0.7425}}                                           
	};
	

	/**
	 * Creates the view ArrayList and memorize the planet ImageView for the size
	 * and translate calculations.
	 * 
	 * @param planet
	 *            the ImageView of the Planet where the undeads attack.
	 */
	public UndeadGrid(ImageView planet, Connector connector, PlanetModelI model){
		this.planet = planet;
		this.model = model;
		this.connector = connector;
		views = new ArrayList<>(4);
		for(int i= 0; i<4;i++){
			views.add(new LinkedList<ImageView>());
		}
		
		if(model.hasAbomination()){
			addAbomination();
		}
		
		for(int colorId =0; colorId<4; colorId++){
			for(int n=0; n<model.getUndeads(colorId);n++){
				addUndead(model, colorId);
			}
		}
	}
	
	
	/**
	 * Method to add an images of an undaed race.
	 * 
	 * @param model
	 *            The planet model to get the total of all undeads
	 * @param colorId
	 *            The colorId of the added undead.
	 */
	private void addUndead(PlanetModelI model, int colorId) {
		UndeadView iV = new UndeadView(colorId, ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS), this);
		views.get(colorId).add(iV);
		connector.connectUndead(iV, model.getId(), colorId, this);
		this.getChildren().add(iV);
		draw();
	}
	
	/**
	 * Method to remove an images of an undaed race.
	 * 
	 * @param model
	 *            The planet model to get the total of all undeads
	 * @param colorId
	 *            The colorId of the killed undead.
	 */
	private void removeUndead(PlanetModelI model, int colorId) {
		if(model.getUndeads(colorId)<views.get(colorId).size()){
			ImageView rem = views.get(colorId).remove();
			this.getChildren().remove(rem);
			draw();	
		}
		
	}
	
	/**
	 * Method to remove all images of an undead race.
	 * 
	 * @param model
	 *            The planet model to get the total of all undeads
	 * @param colorId
	 *            The colorId of the wiped out undead race.
	 */
	private void wipeUndead(PlanetModelI model, int colorId) {
		while(!views.get(colorId).isEmpty()){
			ImageView rem = views.get(colorId).remove();
			this.getChildren().remove(rem);
		}
		draw();
	}
	
	
	/**
	 * This method dosn't really redraw the Images, but sets new positions.
	 * 
	 * @param n the Number of undeads in total
	 */
	void draw(){
		int n = model.getNUndeads()-1;
		int c=1;
		for(LinkedList<ImageView> undeadRace: views){
			for(ImageView undead: undeadRace){
				undead.fitHeightProperty().unbind();
				undead.fitWidthProperty().unbind();
				undead.xProperty().unbind();
				undead.yProperty().unbind();

				undead.fitHeightProperty().bind(planet.fitHeightProperty().multiply(pos[n][0][0]));
				undead.fitWidthProperty().bind(planet.fitWidthProperty().multiply(pos[n][0][0]));
				undead.xProperty().bind(planet.translateXProperty().add(planet.fitWidthProperty().multiply(pos[n][c][0])));
				undead.yProperty().bind(planet.translateYProperty().add(planet.fitWidthProperty().multiply(pos[n][c][1])));
				
				c++;
			}
		}
	}

	/**
	 * This method is called whenever the observed object is changed. An
	 * application calls an Observable object's notifyObservers method to have
	 * all the object's observers notified of the change.
	 * 
	 * This overwritten Method processes messages depending the undeads on a
	 * Planet.
	 * 
	 * @param model
	 *            here: the Planet
	 * @param msg
	 *            here: the the message+thecolorID of the undead
	 */
	@Override
	public void update(Observable model, Object msg) {

		switch (msg.toString().substring(0, 5)) {
		case "addUn": // add undead
			this.addUndead((PlanetModelI) model, Character.getNumericValue(msg.toString().charAt(5)));
			return;
		case "remUn": // remove undead
			this.removeUndead((PlanetModelI) model, Character.getNumericValue(msg.toString().charAt(5)));
			return;
		case "wipUn": //wipe out undeads
			this.wipeUndead((PlanetModelI) model, Character.getNumericValue(msg.toString().charAt(5)));
			return;
		case "setAb": //set abomination
			addAbomination();
			return;
		case "remAb": //remove abomination
			removeAbomination();
			return;
		case "redrw":// redraw planet
			draw();
			return;
		}
	}


	/**
	 * Adds an abdomination to a undead grid. Is added when planet is conquered
	 * or when an abomination goes to a neighbor planet.
	 */
	private void addAbomination() {
		if (abomination == null){
			abomination = new ImageView(ImageLoader.getInstance().get("/planetTiles/fleischkoloss_96.png"));
			abomination.fitHeightProperty().bind(planet.fitHeightProperty().multiply(0.9));
			abomination.fitWidthProperty().bind(planet.fitWidthProperty().multiply(0.9));
			abomination.xProperty().bind(planet.translateXProperty().add(planet.fitWidthProperty().multiply(0.05)));
			abomination.yProperty().bind(planet.translateYProperty().add(planet.fitWidthProperty().multiply(0.05)));
			abomination.toBack();
		}
		this.getChildren().add(abomination);
		abomination.toBack();
		draw();
	}
	
	/**
	 * Removes an abomination from a grid if abomination is killed or
	 * abomination goes to a neighbor planet.
	 */
	private void removeAbomination() {
		this.getChildren().remove(abomination);
		draw();
	}

	/**
	 * Moves an undead from one planet to another.
	 * @param view Undead which is moved.
	 * @param colorId Color of undead that is moved.
	 */
	public void moveUndeadView(UndeadView view, int colorId) {
		// don't add on a undead conquest -> maximal number of undeads of a color = 3
		view.getParentGrid().removeUndeadView(view);
		if(model.getUndeads(colorId)!=4){ 
			views.get(colorId).add(view);
			connector.connectUndead(view, model.getId(), colorId, this);
			this.getChildren().add(view);
			view.setParentGrid(this);
			view.setTranslateX(0);
			view.setTranslateY(0);
		}
		draw();
	}
	
	/**
	 * Removes undead view from current planet.
	 * @param view Undead that is removed.
	 */
	public void removeUndeadView(UndeadView view){
		views.get(view.getColorId()).remove(view);
		draw();
		
	}
	
	/**
	 * Getter for planet ID.
	 * @return Returns planet's ID.
	 */
	public int getPlanetId(){
		return model.getId();
	}
	
}
	
	
