package game.board;

import game.Connector;
import game.GameControllerI;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import utils.RemovingLine;

/**
 * Connects planettiles to their connectors. 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class PlanetConnecter implements Connector{
	
	/**
	 * GameController handles events on game (e.g. actions involving planet tiles).
	 */
	private final GameControllerI gameController;
	
	/**
	 * BoardController handels events on gameboard.
	 */
	private final BoardControllerI boardController;


	/**
	 * instantiates attributes.
	 * @param gameController Game event handler.
	 * @param boardController Board event handler.
	 */
	public PlanetConnecter(GameControllerI gameController, BoardControllerI boardController) {
		this.gameController = gameController;
		this.boardController = boardController;
	}
	
	/**
	 * Connects resources from view to game controller.
	 */
	public void connectResource(ImageView resource, int color, int planetId) {
		resource.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				
				gameController.mineResource(color, planetId); 
			}
		});
	}
	
	/**
	 * Connects dark columns from view to gameController.
	 */
	public void connectDarkColumn(ImageView darkColumn, int planetId) {
		darkColumn.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				gameController.removeDarkColumn(planetId);
			}
		});
	}
	
	/**
	 * Connects undead from view to gameController.
	 */
	public void connectUndead(UndeadView uV, int planetId, int colorId, UndeadGrid parent) {
		uV.setOnMouseClicked(event -> {
				gameController.killUndead(planetId, colorId);
		});
		boardController.connectUndead(uV, parent);
		gameController.connectUndead(uV);

	}

	@Override
	public void connectPlanet(PlanetView planetView) {		
	}

	@Override
	public void connectLine(RemovingLine line) {
	}
}
