package game.board;

import java.util.ArrayList;

public interface BoardControllerI {

	void setBoardView(BoardView boardView);

	void connectAll();

	/**
	 * Prevents errors if pane is exited while dragging an object.
	 */
	void mouseExited ();
	
	/**
	 * activates Drag and Drop.
	 */
	void mouseMovedAndDraged();
	
	/**
	 * Activates zoom.
	 */
	void scroll();
	
	/**
	 *  Handles event when mouse is released.
	 */
	void mouseReleased();
	
	/**
	 *  Handles event when mouse is released and an object was dragged.
	 */
	void mouseDragReleased();

	/**
	 * Handles drag and drop for Spaceships.
	 */
	void playerSpaceShips();

	/**
	 * Moves spaceship or undead or builds base.
	 */
	void planetViews();

	void connectUndead(UndeadView undead, UndeadGrid parent);

	/**
	 * Getter for the planets as neighbours 
	 * @param planetId
	 * @return model.getMap()[planetId]
	 */
	ArrayList<Integer> getNeighbours(int planetId);

	/**
	 * Getter for the planet 
	 * @param planetId
	 * @return planet
	 */
	PlanetModelI getPlanet(int planetId);

	/**
	 * Getter for the startPlanet 
	 * @return startPlanet
	 */
	int getStartPlanet();

	void setBoardModel(BoardModelI boardModel);

	
	public void showMessage(String s);
}