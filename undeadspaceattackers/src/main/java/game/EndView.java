package game;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import language.Language;
import start.Title;
import utils.ImageLoader;

public class EndView extends BorderPane {

	/**
	 * Message states whether players have won.
	 */
	private Title message;

	/**
	 * Button to go back to main menu.
	 */
	private final Button btnMenu;

	/**
	 * Button to quit the game.
	 */
	private final Button btnQuit;

	/**
	 * Constructor for EndView, creates different Texts in View depending on end
	 * of the game
	 * 
	 * @param win
	 *            States whether the normal player have won, true if they have
	 *            won.
	 * @param fifthPlayer
	 *            States whether if there is fifth Player, true if there is.
	 */
	public EndView(boolean win, boolean fifthPlayer) {

		// creates a text depending on how the game has ended and if untoaster
		// has played
		if (win == true) {
			message = new Title(Language.getInstance().createText("ENDW"));
		} else if (win == false && fifthPlayer == false) {
			message = new Title(Language.getInstance().createText("ENDLF"));
		} else if (win == false && fifthPlayer == true) {
			message = new Title(Language.getInstance().createText("ENDLT"));
		}

		message.getText().setId("endText");

		btnMenu = Language.getInstance().createButton("MM");
		btnQuit = Language.getInstance().createButton("QUG");
		btnQuit.setMinWidth(250);
		btnMenu.setMinWidth(250);

		VBox vbox = new VBox(btnMenu, btnQuit);
		vbox.setSpacing(20);
		vbox.setAlignment(Pos.CENTER);

		// image
		Image image = ImageLoader.getInstance().get("/start/logo.png");
		ImageView imageView = new ImageView(image);
		imageView.preserveRatioProperty().set(true);

		imageView.fitHeightProperty().bind(this.heightProperty().multiply(0.7));
		imageView.fitWidthProperty().bind(this.widthProperty().multiply(0.4));

		VBox right = new VBox(imageView);
		right.setAlignment(Pos.CENTER_RIGHT);
		right.getStyleClass().add("vbox");

		setRight(right);
		setTop(message);
		setCenter(vbox);

		new EndController(this);

	}

	/**
	 * Getter for Button to switch to main menu.
	 * 
	 * @return the btnMenu
	 */
	public Button getBtnMenu() {
		return btnMenu;
	}

	/**
	 * Getter for button to quit the game.
	 * 
	 * @return the btnQuit
	 */
	public Button getBtnQuit() {
		return btnQuit;
	}

}
