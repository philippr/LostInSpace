package game;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import start.MenuController;

/**
 * EndController handels events on EndView. It can change the view back to the
 * main menu or quits the game.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class EndController implements EventHandler<ActionEvent> {

	/**
	 * View where events are happening.
	 */
	EndView endView;

	/**
	 * Sets buttons of EndView on action.
	 * @param endView
	 */
	public EndController(EndView endView) {
		this.endView = endView;
		endView.getBtnQuit().setOnAction(this);
		endView.getBtnMenu().setOnAction(this);
	}

	/**
	 * Handles events on clicked buttons, switches view to main menu or
	 * quits game.
	 */
	public void handle(ActionEvent event) {
		if (event.getSource() == endView.getBtnQuit()) {
			Platform.exit();
			System.exit(0);
		}
		if (event.getSource() == endView.getBtnMenu()) {
			MenuController.switchContent();
		}
	}
}
