package game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ThreadLocalRandom;

import cards.Card;
import game.board.PlanetModelI;
import player.PlayerI;

/**
 * GameModel contains information about all players. Knows which one is current
 * player and which one is next. Knows whether untoaster is selected and has
 * information about the last used card.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 * 
 *         Notifications:
 * 
 *         nxtPl: next player decLA: decrement left actions
 *
 */
@SuppressWarnings("serial")
public class GameModel extends Observable implements GameModelI, Serializable {
	
	/**
	 * Id of the current player who is allowed to 
	 */
	private int currentPlayer;	
	
	/**
	 * HashMap contains all players and their Id.
	 */
	private final HashMap<Integer, PlayerI> players = new HashMap<>(9);
	
	/**
	 * Order of players. Information about who's turn it is and who's turn is next.
	 */
	private final ArrayList<Integer> order = new ArrayList<>(5);
	
	/**
	 * List of all last used cards of player. Cards a user has thrown away to make an action.
	 */
	private final ArrayList<Card> lastUsedCard = new ArrayList<>();
	
	/**
	 * Flag stating whether untoaster is activated.
	 */
	private final boolean untoaster;

	/**
	 * Fills attributes with information from given playerList.
	 * 
	 * @param playerList
	 *            List of all players playing the game (including untoaster if
	 *            chosen).
	 */
	public GameModel(ArrayList<PlayerI> playerList) {
		for(PlayerI player: playerList){
			players.put(player.getId(), player);
			order.add(player.getId());
		}
		untoaster = players.containsKey(-1);

		currentPlayer = 0; // startplayer?
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#getCurrentPlayerId()
	 */
	@Override
	public int getCurrentPlayerId() {
		return getCurrentPlayer().getId();
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#nextPlayer()
	 */
	@Override
	public void nextPlayer() {
		currentPlayer++;
		currentPlayer%= order.size();
		setChanged();
		notifyObservers("nxtPl");
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#setCurrentPlayer(int)
	 */
	@Override
	public void setCurrentPlayer(int id) {
		this.currentPlayer = id;
		setChanged();
		notifyObservers("nxtPl");
	}
	
	/* (non-Javadoc)
	 * @see game.GameModelI#getCurrentPlayer()
	 */
	@Override
	public PlayerI getCurrentPlayer() {
		return  players.get(order.get(currentPlayer));
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#getNextPlayer()
	 */
	@Override
	public PlayerI getNextPlayer() {
		return(players.get(order.get((currentPlayer+1) % order.size())));
	}
	
	/* (non-Javadoc)
	 * @see game.GameModelI#removePlayer(int)
	 */
	@Override
	public void removePlayer(int playerId) {
		players.get(playerId).killPlayer();
		order.remove((Integer)playerId);
		currentPlayer--;
		// skips if all players are killed
		// so currentPlayer = -1
		if(order.size() != 0){
			currentPlayer%=order.size();
		}
	}
	

	/* (non-Javadoc)
	 * @see game.GameModelI#getPlayer(int)
	 */
	@Override
	public PlayerI getPlayer(int playerId) {
		return players.get(playerId);
	}
	
	/* (non-Javadoc)
	 * @see game.GameModelI#getLastUsedCard()
	 */
	@Override
	public ArrayList<Card> getLastUsedCard() {
		return lastUsedCard;
	}
	
	/* (non-Javadoc)
	 * @see game.GameModelI#addLastCard(cards.Card)
	 */
	@Override
	public void addLastCard(Card card) {
		lastUsedCard.add(card);
	}
	
	/* (non-Javadoc)
	 * @see game.GameModelI#removeLastCard(int)
	 */
	@Override
	public void removeLastCard(int id) {
		lastUsedCard.remove(id);
	}
	
	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#clearLastUsedCards()
	 */
	@Override
	public void clearLastUsedCards() {
		lastUsedCard.clear();
	}

	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#addObserverX(java.util.Observer)
	 */
	@Override
	public void addObserverX(Observer observer) {
		addObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#hasUntoaster()
	 */
	@Override
	public boolean hasUntoaster() {
		return untoaster;
	}

	/*
	 * (non-Javadoc)
	 * @see game.GameModelI#notifyEndAction()
	 */
	@Override
	public void notifyEndAction() {
		setChanged();
		notifyObservers("decLA");
	}

	/*
	 * @see game.GameModelI#removeRandomPlayer(game.board.PlanetModelI)
	 */
	@Override
	public void removeRandomPlayer(PlanetModelI planet) {
		if(planet.hasPlayers()){
			ArrayList<Integer> pl = new ArrayList<>();
			for (int i = 0; i < 4; i++) {
				if (planet.hasPlayer(i) == true) {
					pl.add(i);
				}
			}
			int rand = pl.get(ThreadLocalRandom.current().nextInt(0, pl.size()));
			planet.setPlayer(rand, false);
			removePlayer(rand);
		}
	}


	/* (non-Javadoc)
	 * @see game.GameModelI#getNPlayers(boolean)
	 */
	public int getNPlayers(boolean excludeUntoaster) {
		if(untoaster && excludeUntoaster){
			return order.size()-1;
		}
		return order.size();
	}
}
