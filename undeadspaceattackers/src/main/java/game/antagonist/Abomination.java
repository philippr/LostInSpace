package game.antagonist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import game.board.BoardControllerI;

/**
 * This class represents an abomination appearing if the feature is selected and
 * if a planet is conquered.
 * 
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class Abomination implements Serializable {

	/**
	 * BoardController model contains information about the field and can add or
	 * remove abominations.
	 */
	private BoardControllerI boardController;

	/**
	 * List of all abominations in the game.
	 */
	ArrayList<Integer> abominations;
	
	/**
	 * List of all new abominations of this round. 
	 */
	ArrayList<Integer> newAbominations;

	/**
	 * Creates new abominations.
	 * 
	 * @param abominations
	 *            List of all abominations of the game.
	 * @param boardController
	 *            Controller for field which can add, remove and move
	 *            abominations.
	 */
	public Abomination(BoardControllerI boardController) {
		this.abominations = new ArrayList<>();
		this.newAbominations = new ArrayList<>();
		this.boardController = boardController;
	}

	public Abomination(ArrayList<Integer> abominations, BoardControllerI boardController) {
		this.abominations = abominations;
		this.newAbominations = new ArrayList<>();
		this.boardController = boardController;
	}

	/**
	 * Moves every abdomination to a new planet. Searches the shortest way to
	 * nearest player.
	 */
	public void moveAllAbomination() {

		// Breadth-first search
		// for all abominations:
		// - calculate for all neighbours the distance to the next player
		// - then go to the neighbour planet with the shortest distance
		// - but don't let two abominations go to the same planet

		Collections.shuffle(abominations, new Random(System.nanoTime()));
		ArrayList<Integer> plannedPath = new ArrayList<Integer>(abominations.size());
		ArrayList<Integer> old_abominations = new ArrayList<Integer>(abominations.size());
		ArrayList<Integer> toAdd_abominations = new ArrayList<Integer>(abominations.size());

		for (Integer abomination : abominations) {
			int initialPlanet = abomination;
			// if the planet has a player, then stay (if possible)
			if (boardController.getPlanet(initialPlanet).hasPlayers() && !plannedPath.contains(initialPlanet)) {
				plannedPath.add(initialPlanet);
				boardController.getPlanet(abomination).setAbomination(false);
				continue;
			}
			@SuppressWarnings("unchecked")
			ArrayList<Integer> startPlanets = (ArrayList<Integer>) boardController.getNeighbours(initialPlanet).clone();
			Collections.shuffle(startPlanets, new Random(System.nanoTime()));
			ArrayList<Integer> visited = new ArrayList<>();
			ArrayList<Integer> next = new ArrayList<>();
			ArrayList<Integer> current = new ArrayList<>();
			ArrayList<Integer> neighbours;
			int[] distances = new int[startPlanets.size()]; // distance to the
															// next player
			int j = 0;

			for (int i = 0; i < distances.length; i++) {
				distances[i] = Integer.MAX_VALUE;
			}
			for (int start : startPlanets) {

				if (boardController.getPlanet(start).hasPlayers()) {
					j++;
					continue;
				}
				current.clear();
				visited.clear();
				next.clear();
				current.add(start);
				distances[j]--;
				start: while (!current.isEmpty()) {
					while (!current.isEmpty()) {
						visited.add(current.get(0));
						neighbours = boardController.getNeighbours(current.remove(0));
						Collections.shuffle(neighbours, new Random(System.nanoTime()));

						for (int neighbour : neighbours) {

							if (boardController.getPlanet(neighbour).hasPlayers()) {
								current.clear();
								break start;
							}
							if (!visited.contains(neighbour) && !next.contains(neighbour)) {
								next.add(neighbour);
							}
						}
						distances[j]--;
					}
					current = next;
					Collections.shuffle(current, new Random(System.nanoTime()));
				}
				j++;
			}

			int distance = Integer.MIN_VALUE;
			int index = -1;

			// search for the largest distance, because distance[i] begins with
			// Integer.MAX_VALUE and becomes decremented
			for (int i = 0; i < distances.length; i++) {
				if (distances[i] > distance && !plannedPath.contains(startPlanets.get(i))) {
					distance = distances[i];
					index = i;
				}
			}

			// remove abominations, set their next planet
			if (index != -1) { // move abomination
				boardController.getPlanet(abomination).setAbomination(false);

				old_abominations.add(abomination);
				toAdd_abominations.add(startPlanets.get(index));
				plannedPath.add(startPlanets.get(index));

			} else { // this abomination has no neighours: so reset the
						// abomination on its planet.
				boardController.getPlanet(abomination).setAbomination(false);
			}
		} // end For

		abominations.removeAll(old_abominations);
		old_abominations.clear();
		abominations.addAll(toAdd_abominations);
		toAdd_abominations.clear();

		for (int planet : plannedPath) {
			boardController.getPlanet(planet).setAbomination(true);
		}
	}

	/**
	 * Add an Abomination
	 * 
	 * @param planetId the Id of the abominations planet
	 */
	public void addAbomination(int planetId) {
		newAbominations.add(planetId);
	}

	/**
	 * Removes the Abomination from the given planet.
	 * Doesn't remove new Abominations 
	 * 
	 * @param planetId
	 */
	public void removeAbomination(Integer planetId) {
		abominations.remove(planetId);
	}

	/**
	 * Getter for the Abominations.
	 * Doesn't return the new Abominations.
	 * 
	 * @return ArrayList<Integer> with the Abominations planetIDs
	 */
	public ArrayList<Integer> getAbominations() {
		return abominations;
	}
	
	/**
	 * Moves the new Abominations to the old ones
	 */
	public void moveNew() {
		abominations.addAll(newAbominations);
		newAbominations.clear();
	}

}
