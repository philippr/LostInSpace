package game.antagonist;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import game.board.BoardControllerI;
import game.board.PlanetModelI;
import player.PlayerI;

public class Plague {
	BoardControllerI boardController;
	int nPlanets;
	
	public Plague(BoardControllerI boardController, int nPlanets) {
		this.boardController = boardController;
		this.nPlanets = nPlanets;
		
		ArrayList<Integer> randPlanets = new ArrayList<>();

		//make resources
		for (int i = 0; i < 8; i++) {
			int randInt = ThreadLocalRandom.current().nextInt(0, nPlanets);
			if (!randPlanets.contains(randInt)) {
				randPlanets.add(randInt);
				boardController.getPlanet(randInt).setResource(i % 4); // from each color2<=>imod 4
			} else {
				i--;
			}
		}
	}
	
	
	/**
	 * sets depending on the number of undeads on a planet plague to a player
	 * @param player who gets maybe the plague
	 * @param planet on which the player is
	 */
	public void makePlague(PlayerI player, PlanetModelI planet) {
		int maxUd = 0;
		double prob = -0.1; // probability 1undead=10%, 2=30%, 3=50%
		int colorId = 0;

		for (int i = 0; i < 4; i++) {
			if (planet.getUndeads(i) > maxUd) {
				maxUd = planet.getUndeads(i);
				prob += 0.2 * maxUd; // for each undead, add 20% probability
				colorId = i;
			}
		}
		double x = Math.random();
		if (x < prob) {
			player.setPlague(colorId);
		}
	}
}
