package game.antagonist;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import game.GameStatusModelI;
import game.board.BoardControllerI;

/**
 * 
 * Dark columns can be activated in the main menu. They can add an undead to a
 * planet they are located on or on a neighbor planet.
 * 
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class DarkColumn {

	/**
	 * BoardController to handle actions where dark columns are involved.
	 */
	private BoardControllerI boardController;

	/**
	 * GameStatusModel contains information about dark columns. It states where
	 * dark columns are located.
	 */
	private GameStatusModelI status;

	/**
	 * Sets dark columnns on four random planets.
	 * 
	 * @param boardController
	 *            Sets dark columns on planets.
	 * @param status
	 *            Saves information about dark columns location (PlanetId).
	 * @param nPlanets
	 *            Number of existing planets on the board.
	 */
	public DarkColumn(BoardControllerI boardController, GameStatusModelI status, int nPlanets) {
		this.boardController = boardController;
		this.status = status;
		ArrayList<Integer> rand = new ArrayList<Integer>(4);
		for (int i = 0; i < 4; i++) {
			int randInt = ThreadLocalRandom.current().nextInt(0, nPlanets);
			if (!rand.contains(randInt)) {
				rand.add(randInt);
			} else {
				i--;
			}
			boardController.getPlanet(rand.get(i)).setDarkColumn(true);
		}
		status.setDarkColumnPlanets(rand);
	}

	/**
	 * Creates a list of undeads to be added at the end of a turn.
	 * 
	 * @return List of undeads to add.
	 */
	public ArrayList<Integer> spawn() {
		ArrayList<Integer> addUndead = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			ArrayList<Integer> ids = new ArrayList<Integer>();
			ids.add(status.getDarkColumnPlanets().get(i));
			ids.addAll(boardController.getNeighbours(status.getDarkColumnPlanets().get(i)));
			addUndead.add(ThreadLocalRandom.current().nextInt(0, ids.size()));
		}

		return addUndead;
	}

	/**
	 * Removes a dark column if a player destroys a column with an action.
	 * 
	 * @param planetId
	 *            Planet where dark column is located.
	 */
	public void remove(int planetId) {
		boardController.getPlanet(planetId).setDarkColumn(false);
		status.removeDarkColumnPlanet(planetId);
	}
}
