package game.antagonist;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import cards.Card;
import cards.PlanetCard;
import game.GameStatusModelI;
import game.board.BoardControllerI;
import game.board.PlanetModelI;
import player.PlayerI;
import player.Weapontrader;

/**
 * EvilAi can be activated in main menu. It is supposed to block planets for players.
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class EvilAi {
	
	/**
	 * BoardController to handle events where evil AI is involved.
	 */
	BoardControllerI boardController;
	
	/**
	 * GameStatusModel contains information about planets that are currently blocked by the evil AI.
	 */
	GameStatusModelI status;
	
	/**
	 * Number of existing planets.
	 */
	int nPlanets;
	
	/**
	 * Blocks initially two planets and instantiates attributes.
	 * @param boardController
	 * 	Controller handling all board events.
	 * @param status
	 *  StatusModel contains all information about changing information (e.g. blocked planets).
	 * @param nPlanets
	 *  Total number of planets from chosen map.
	 */
	public EvilAi(BoardControllerI boardController, GameStatusModelI status, int nPlanets) {
		this.boardController = boardController;
		this.status = status;
		this.nPlanets = nPlanets;
	}

	/**
	 * Moves a blockade to a new planet. Blocks neighbor planets if possible.
	 * @param nextPlayer Player which's turn is next.
	 */
	public void move(PlayerI nextPlayer) {
		int[] blockedPlanets = {0,0};

		LinkedList<Integer> extreme = new LinkedList<>();
		LinkedList<Integer> very = new LinkedList<>();
		LinkedList<Integer> mid = new LinkedList<>();
		LinkedList<Integer> less = new LinkedList<>();
		LinkedList<Integer> lesser = new LinkedList<>();
		LinkedList<Integer> worst = new LinkedList<>();
		
		PlanetModelI tPlanet;
		
		nxtCard: for(Card card: nextPlayer.getHand()) {
			if(!(card instanceof PlanetCard)) {
				continue;
			}
			int neighbor = ((PlanetCard)card).getPlanetId();
			
			tPlanet = boardController.getPlanet(neighbor);
			if(tPlanet.isBlocked()) {
				continue;
			}
			
			if(tPlanet.hasBase() || nextPlayer.getHand().hasNumber(tPlanet.getId())) {
				for(int color = 0; color<4; color++) {
					if(nextPlayer.getHand().hasColor(color, (nextPlayer instanceof Weapontrader?4:5))) {
						extreme.add(neighbor);
						continue nxtCard;
					}
				}
			}
			
			
			for(int plague =0; plague<4; plague++) {
				if(nextPlayer.hasPlague(plague)) {
					if(tPlanet.hasHealer(plague) || (tPlanet.hasBase() && nextPlayer.getHand().hasColor(plague))) {
						very.add(neighbor);
					} else if (tPlanet.hasResource(plague)) {
						mid.add(neighbor);
					}
					continue nxtCard;
				}
			}
			
			if(tPlanet.hasDarkColumn() && nextPlayer.getHand().hasColor(tPlanet.getColor(), 2)) {
				very.add(neighbor);
				break;
			}
			
			
			for(int color =0; color<4; color++) {
				if(tPlanet.getUndeads(color) == 3) {
					mid.add(neighbor);
					continue nxtCard;
				}
			}
			for(int color =0; color<4; color++) {
				if(tPlanet.getUndeads(color) == 2) {
					less.add(neighbor);
					continue nxtCard;
				}
			}
			worst.add(neighbor);	
		}
		
		
		nxtPlanet: for(int neighbor: boardController.getNeighbours(nextPlayer.getPlanetId())) {
			tPlanet = boardController.getPlanet(neighbor);
			if(tPlanet.isBlocked()) {
				continue;
			}
			
			if(tPlanet.hasBase() || nextPlayer.getHand().hasNumber(tPlanet.getId()))
			for(int color = 0; color<4; color++) {
				if(nextPlayer.getHand().hasColor(color, (nextPlayer instanceof Weapontrader?4:5))) {
					extreme.add(neighbor);
					continue nxtPlanet;
				}
			}
			
			
			for(int plague =0; plague<4; plague++) {
				if(nextPlayer.hasPlague(plague) && (tPlanet.hasResource(plague)||tPlanet.hasHealer(plague) || (tPlanet.hasBase() && nextPlayer.getHand().hasColor(plague)))) {
					very.add(neighbor);
					continue nxtPlanet;
				}
			}
			
			if(tPlanet.hasDarkColumn() && nextPlayer.getHand().hasColor(tPlanet.getColor(), 2)) {
				very.add(neighbor);
				break;
			}
			for(int color =0; color<4; color++) {
				if(tPlanet.getUndeads(color) == 3) {
					very.add(neighbor);
					continue nxtPlanet;
				}
			}
			for(int color =0; color<4; color++) {
				if(tPlanet.getUndeads(color) == 2) {
					mid.add(neighbor);
					continue nxtPlanet;
				}
			}
			for(int color =0; color<4; color++) {
				if(tPlanet.getUndeads(color) == 1) {
					less.add(neighbor);
					continue nxtPlanet;
				}
			}
			
			
			if(nextPlayer.getLastPlanetId() == neighbor) {
				worst.add(neighbor);
			} else {
				lesser.add(neighbor);
			}
		}

		Collections.shuffle(extreme, new Random(System.nanoTime()));
		Collections.shuffle(very, new Random(System.nanoTime()));
		Collections.shuffle(mid, new Random(System.nanoTime()));
		Collections.shuffle(less, new Random(System.nanoTime()));
		Collections.shuffle(lesser, new Random(System.nanoTime()));
		Collections.shuffle(worst, new Random(System.nanoTime()));
		

		for (int i = 0; i < 2; i++) {
			if(!extreme.isEmpty()) {
				blockedPlanets[i] = extreme.pop();
			} else if(!very.isEmpty()) {
				blockedPlanets[i] = very.pop();
			} else if(!mid.isEmpty()) {
				blockedPlanets[i] = mid.pop();
			} else if(!less.isEmpty()) {
				blockedPlanets[i] = less.pop();
			} else if(!lesser.isEmpty()) {
				blockedPlanets[i] = lesser.pop();
			} else if(!worst.isEmpty()) {
				blockedPlanets[i] = worst.pop();
			} else  {
				while(true) {
					tPlanet =  boardController.getPlanet(ThreadLocalRandom.current().nextInt(0, nPlanets));
					if(tPlanet.isBlocked() || (i==1 && blockedPlanets[0] == tPlanet.getId())) {
						continue;
					}
					blockedPlanets[i] = tPlanet.getId();
					break;
				}
			}
			tPlanet =  boardController.getPlanet(blockedPlanets[i]);
			tPlanet.setBlocked(true);
		}
		boardController.getPlanet(status.getBlockedPlanets()[0]).setBlocked(false);
		boardController.getPlanet(status.getBlockedPlanets()[1]).setBlocked(false);
		status.setBlockedPlanets(blockedPlanets);
	}

	
	
}
