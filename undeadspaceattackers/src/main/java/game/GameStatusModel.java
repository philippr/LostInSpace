package game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("serial")
public class GameStatusModel extends Observable implements GameStatusModelI, Serializable {

	/**
	 * List that states which planets are conquered in one turn. This list is
	 * needed because a planet can only be conquered once in a turn.
	 */
	private final ArrayList<Integer> conqueredPlanets = new ArrayList<>();

	/**
	 * Array contains information about changing numbers in game, sequence of
	 * numbers: left allowed planet conquests, undeadStrength,
	 * undeadStrengthMarker, playerCardCounter, undeadCardCounter.
	 */
	private int[] numbers = new int[5];

	/**
	 * Array of Undeads that are not on Planets, sequence of species: yellow
	 * (skeletons), green (zombies), blue (mummies), red (ghosts)
	 */
	int[] leftUndead = { main.Constants.totalUndeads, main.Constants.totalUndeads, main.Constants.totalUndeads,
			main.Constants.totalUndeads };

	/**
	 * Boolean array that states whether a weapon for a certain species is
	 * developed. Sequence of weapons: dog, schnetler, fire, vacuum
	 */
	private boolean[] developedWeapons = { false, false, false, false };

	/**
	 * Boolean array that states whether a weapon for a certain species is
	 * extinct. Sequence of species: skeletons, zombies, mummies, ghosts
	 */
	private boolean[] extinctSpecies = { false, false, false, false };

	/**
	 * Two planets that are blocked by evil AI, if AI is selected. Planets are
	 * represented by their planetId.
	 */
	int[] blockedPlanets = new int[2];

	/**
	 * List that contains all planets with a dark column. Planets are
	 * represented by their planetId.
	 */
	ArrayList<Integer> darkColumnPlanets = new ArrayList<Integer>(4);

	/**
	 * List that contains all abdominations.
	 */
	ArrayList<Integer> abominations = new ArrayList<Integer>();

	/**
	 * Flags for the game. Sequence of flags: 0 - protectionShield, states
	 * whether the action card protection shield is used. True if it is used. 1
	 * - discard flag, states whether a player is allowed to throw cards away.
	 * Needs to be true if a player has more than seven cards on his or her
	 * hands. True if player is allowed to throw cards away. 2 - states whether
	 * the game is ended. True if game is ended.
	 */
	private boolean flags[] = { false, false, false };

	/**
	 * Constructor for GameStatusModel. Sets undeadStrength and
	 * leftConqueredPlanets.
	 * 
	 * @param undeadStrength
	 *            number of how many cards need to be picked from undead card
	 *            pile.
	 * @param leftConqueredPlanets
	 *            number of initial allowed planet conquests
	 */
	public GameStatusModel(int undeadStrength, int leftConqueredPlanets) {
		this.numbers[1] = undeadStrength;
		this.numbers[0] = leftConqueredPlanets;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#getConqueredPlanets()
	 */
	@Override
	public ArrayList<Integer> getConqueredPlanets() {
		return conqueredPlanets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#setConqueredPlanets(java.util.ArrayList)
	 */
	@Override
	public void addConqueredPlanets(int conqueredPlanet) {
		this.conqueredPlanets.add(conqueredPlanet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#getNumbers(int)
	 */
	@Override
	public int getNumbers(int index) {
		return numbers[index];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#setNumbers(int, int)
	 */
	@Override
	public void setNumbers(int value, int index) {
		numbers[index] = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#decreaseLeftConqueredPlanets()
	 */
	public void decreaseLeftConqueredPlanets() {
		numbers[0]--;
		setChanged();
		notifyObservers("leftCPlanets");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#getDevelopedWeapons()
	 */
	@Override
	public boolean[] getDevelopedWeapons() {
		return developedWeapons;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#setDevelopedWeapons(boolean[])
	 */
	@Override
	public void developWeapon(int weaponColor) {
		this.developedWeapons[weaponColor] = true;
		setChanged();
		notifyObservers("developedWeapons");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#getExtinctSpecies()
	 */
	@Override
	public boolean[] getExtinctSpecies() {
		return extinctSpecies;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#setExtinctSpecies(boolean[])
	 */
	@Override
	public void setExtinctSpecies(int extinctSpecies) {
		this.extinctSpecies[extinctSpecies] = true;

		setChanged();
		notifyObservers("extinctSpecies");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#getOccupiedPlanets()
	 */
	@Override
	public int[] getBlockedPlanets() {
		return blockedPlanets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#setOccupiedPlanets(int[])
	 */
	@Override
	public void setBlockedPlanets(int[] blockedPlanets) {
		this.blockedPlanets = blockedPlanets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#getDarkColumnPlanets()
	 */
	@Override
	public ArrayList<Integer> getDarkColumnPlanets() {
		return darkColumnPlanets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#setDarkColumnPlanets(int[])
	 */
	@Override
	public void setDarkColumnPlanets(ArrayList<Integer> darkColumnPlanets) {
		this.darkColumnPlanets = darkColumnPlanets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#increaseUndeadStrengthMarker()
	 */
	@Override
	public void increaseUndeadStrengthMarker() {
		this.numbers[2]++;
		setChanged();
		notifyObservers("strengthMarker");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#inceaseUndeadStrength()
	 */
	@Override
	public void inceaseUndeadStrength() {
		this.numbers[1]++;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#removeDarkColumnPlanet(java.lang.Integer)
	 */
	@Override
	public void removeDarkColumnPlanet(Integer planetId) {
		this.darkColumnPlanets.remove(planetId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#addObserverX(java.util.Observer)
	 */
	@Override
	public void addObserverX(Observer infoView) {
		addObserver(infoView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#getLeftUndeads()
	 */
	public int[] getLeftUndead() {
		return leftUndead;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#decreaseLeftUndeads(int)
	 */
	@Override
	public void decreaseLeftUndead(int colorId) {
		leftUndead[colorId]--;
		setChanged();
		notifyObservers("leftUndead");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#increaseLeftUndeads(int)
	 */
	@Override
	public void increaseLeftUndead(int colorId) {
		leftUndead[colorId]++;
		setChanged();
		notifyObservers("leftUndead");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#incPlayerCardCounter()
	 */
	@Override
	public void incPlayerCardCounter() {
		numbers[3]++;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#incUndeadCardCounter()
	 */
	@Override
	public void incUndeadCardCounter() {
		numbers[4]++;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see game.GameStatusModelI#resetCardCounters()
	 */
	@Override
	public void resetCardCounters() {
		numbers[3] = 0;
		numbers[4] = 0;
	}


	/*
	 * (non-Javadoc)
	 * @see game.GameStatusModelI#getFlag(int)
	 */
	@Override
	public boolean getFlag(int index) {
		return flags[index];
	}

	/*
	 * (non-Javadoc)
	 * @see game.GameStatusModelI#setFlag(boolean, int)
	 */
	@Override
	public void setFlag(boolean value, int index) {
		flags[index] = value;
		
	}
}
