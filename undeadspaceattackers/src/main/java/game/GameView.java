package game;

import cards.CardController;
import cards.CardControllerI;
import game.board.BoardController;
import game.board.BoardControllerI;
import game.board.BoardView;
import game.board.PlanetConnecter;
import game.cardinfo.CardBoxView;
import game.cardinfo.CardInfoController;
import game.cardinfo.InfoView;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import start.ConfigModelI;
import utils.MessageBoxController;

public class GameView extends AnchorPane{
	private final GameModelI model;
	private final InfoView infoBox;
	private final GameControllerI controller; 
	private final DragContainer dragContainer;
	private final GameStatusModelI gameStatusModel;
	private BoardView board;
	
	/**
	 * @return the board
	 */
	public BoardView getBoard() {
		return board;
	}


	private CardControllerI cardController;
	private Pane cardBox;
	private Pane movePane;


	
	public GameView(GameModelI gameModel, GameStatusModelI gameStatusModel, ConfigModelI configModel, Pane root, MessageBoxController msgBoxController) {
		movePane = new Pane();
		dragContainer = new DragContainer();
		this.model = gameModel;
		this.gameStatusModel = gameStatusModel;
		cardController = new CardController(configModel.getCardModel());		
		
		this.controller = new GameController(configModel, model, gameStatusModel, cardController);

		BoardControllerI boardController = new BoardController(controller, dragContainer, movePane, msgBoxController);

		board = new BoardView(configModel.getBoardModel(), configModel.getPlayers(),
				new PlanetConnecter(controller, boardController),
				new double[] { root.getWidth() - main.Constants.INFOBOX_WIDTH,
						root.getHeight() - main.Constants.CARDBOX_HEIGHT });
		boardController.setBoardView(board);
		boardController.setBoardModel(board.getModel());
		boardController.connectAll();
		this.controller.setBoardController(boardController);

		infoBox =new InfoView(configModel.getGameStatusModel(), configModel.isImprovedWeapons(), root);
		
		initiateGameView(root);
	}
	
	
	public GameView(ConfigModelI configModel, Pane parentPane, MessageBoxController msgBoxController){
		movePane = new Pane();
		dragContainer = new DragContainer();
		model =  new GameModel(configModel.getAllPlayers());
		gameStatusModel = configModel.getGameStatusModel();
		
		cardController = new CardController(configModel.getCardModel());		
		
		this.controller = new GameController(configModel, model, gameStatusModel, cardController);
		BoardControllerI boardController = new BoardController(controller,dragContainer, movePane, msgBoxController); 

		board = new BoardView(configModel.getBoardModel(), configModel.getPlayers(), new PlanetConnecter(controller, boardController),  
				new double[] {parentPane.getWidth() - main.Constants.INFOBOX_WIDTH, parentPane.getHeight()-main.Constants.CARDBOX_HEIGHT});
		boardController.setBoardView(board);
		boardController.setBoardModel(board.getModel());
		boardController.connectAll();
		this.controller.setBoardController(boardController);

		infoBox =new InfoView(configModel.getGameStatusModel(), configModel.isImprovedWeapons(), parentPane);
		this.controller.initiateGame();
		
		initiateGameView(parentPane);
	}
	
	public void initiateGameView(Pane parentPane) {
		movePane.setMouseTransparent(true);
	
		cardBox = new CardBoxView(cardController.getModel(), model,dragContainer, new CardInfoController(this, dragContainer));
 
        Stop[] stops = new Stop[] {new Stop(1, Color.rgb(0, 0, 0, 0.3)), new Stop(0, Color.rgb(50, 0, 0, 0.9))};

        infoBox.setPrefWidth(main.Constants.INFOBOX_WIDTH);
        infoBox.setMinWidth(main.Constants.INFOBOX_WIDTH);
        infoBox.setMaxWidth(main.Constants.INFOBOX_WIDTH);
        
        infoBox.setBackground(new Background(new BackgroundFill(new LinearGradient(1,0,0,0,true, CycleMethod.NO_CYCLE, stops), CornerRadii.EMPTY, Insets.EMPTY)));

		cardBox.setMinHeight(main.Constants.CARDBOX_HEIGHT);
		cardBox.setPrefHeight(main.Constants.CARDBOX_HEIGHT);
		cardBox.setMaxHeight(main.Constants.CARDBOX_HEIGHT);
		cardBox.setBackground(new Background(new BackgroundFill(new LinearGradient(0,1,0,0,true, CycleMethod.NO_CYCLE, stops), CornerRadii.EMPTY, Insets.EMPTY)));
		cardBox.prefWidthProperty().bind(this.widthProperty().subtract(main.Constants.INFOBOX_WIDTH));
		
		this.getChildren().addAll(infoBox, board, cardBox, movePane);

		AnchorPane.setTopAnchor(board, 0d);
		AnchorPane.setTopAnchor(infoBox, 0d);
		AnchorPane.setTopAnchor(movePane, 0d);

		AnchorPane.setRightAnchor(board, main.Constants.INFOBOX_WIDTH);
		GameView.setRightAnchor(infoBox, 0d);
		AnchorPane.setRightAnchor(cardBox, main.Constants.INFOBOX_WIDTH);
		GameView.setRightAnchor(movePane, 0d);

		AnchorPane.setBottomAnchor(board, main.Constants.CARDBOX_HEIGHT);
		AnchorPane.setBottomAnchor(infoBox, 0d);
		AnchorPane.setBottomAnchor(cardBox, 0d);
		AnchorPane.setBottomAnchor(movePane, 0d);

		AnchorPane.setLeftAnchor(board, 0d);
		AnchorPane.setLeftAnchor(cardBox, 0d);
		AnchorPane.setLeftAnchor(movePane, 0d);
		
		board.toBack();

		
		this.widthProperty().addListener((obs, oldVal, newVal) -> {
		     resize(obs, oldVal, newVal, true);
		});
		this.heightProperty().addListener((obs, oldVal, newVal) -> {
		     resize(obs, oldVal, newVal, false);
		});

	}

	public Pane getMovePane() {
		return movePane;
	}


	private void resize(ObservableValue<? extends Number> obs, Number oldVal, Number newVal, boolean horizontal) {
		board.changePrefSize(this.getWidth()-main.Constants.INFOBOX_WIDTH, this.getHeight()-main.Constants.CARDBOX_HEIGHT);
	}

	public void releaseCard() {

	}

	
	public void grabCard() {

	}
	
	
	public void setViews(BoardView boradView, Pane cardView){
		this.board = boradView;
		this.cardBox = cardView;
	}



	/**
	 * @return the controller
	 */
	public GameControllerI getController() {
		return controller;
	}


	/**
	 * @return the infoBox
	 */
	public InfoView getInfoBox() {
		return infoBox;
	}
	
	
	
	

}
