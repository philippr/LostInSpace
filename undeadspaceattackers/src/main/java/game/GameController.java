package game;

import java.util.ArrayList;

import cards.ActionCard;
import cards.Card;
import cards.CardControllerI;
import cards.MassAttackCard;
import cards.ProtectionShield;
import cards.UndeadCard;
import game.antagonist.Abomination;
import game.antagonist.DarkColumn;
import game.antagonist.EvilAi;
import game.antagonist.Plague;
import game.board.BoardControllerI;
import game.board.PlanetModelI;
import game.board.UndeadView;
import player.CrazyWeaponArchitect;
import player.Exorcist;
import player.PlayerI;
import player.Untoaster;
import player.Warpmaster;
import start.ConfigModelI;
import start.MenuController;
import utils.SaveLoader;

/**
 * Main controller for the game logic.
 *  @author Philipp, Fabian, Julia, Ronja
 */
public class GameController implements GameControllerI {

	/**
	 * GameModel contains information about players and last used card.
	 */
	private final GameModelI model;

	/**
	 * GameStatusModel contains information of changing information.
	 */
	private final GameStatusModelI status;

	/**
	 * ConfigModel contains information about chosen map, players, settings and features.
	 */
	private final ConfigModelI configModel;

	/**
	 * The BoardController
	 */
	private BoardControllerI boardController;

	/**
	 * Card controller handels events with cards.
	 */
	private CardControllerI cardController;

	/**
	 * Feature Evil AI. Instantiated if chosen in main menu.
	 */
	private EvilAi evilAi;

	/**
	 * Feature plagues. Instantiated if chosen in main menu.
	 */
	private Plague plague;

	/**
	 * Feature dark columns. Instantiated if chosen in main menu.
	 */
	private DarkColumn darkColumn;

	/**
	 * Feature abominations. Instantiated if chosen in main menu.
	 */
	private Abomination abominations;



	/**
	 * Instantiates all important attributes.
	 * @param configModel
	 *            contains all the settings, map, etc
	 */

	GameController(ConfigModelI configModel, GameModelI gameModel, GameStatusModelI gameStatusModel,
			CardControllerI cardController) {
		this.configModel = configModel;
		model = gameModel;
		status = gameStatusModel;
		this.cardController = cardController;

	}


	/* (non-Javadoc)
	 * @see game.GameControllerI#setCardController(cards.CardControllerI)
	 */
	@Override
	public void setCardController(CardControllerI cardController) {
		this.cardController = cardController;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#getCardController()
	 */
	@Override
	public CardControllerI getCardController() {
		return cardController;
	}


	/* (non-Javadoc)
	 * @see game.GameControllerI#setBoardController(game.board.BoardControllerI)
	 */
	@Override
	public void setBoardController(BoardControllerI boardControllerI) {
		this.boardController = boardControllerI;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#initiateGame()
	 */
	@Override
	public void initiateGame() {
		// set random 4 planets with dark columns if activated
		if (configModel.isDarkColumns()) {
			darkColumn = new DarkColumn(boardController, status, configModel.getBoardModel().getnPlanets());
		}

		// set random 2 planets to be blocked of evil ai if activated
		if (configModel.isEvilAI()) {
			evilAi = new EvilAi(boardController, status, configModel.getBoardModel().getnPlanets());
			evilAi.move(model.getNextPlayer());
		}

		// set resources for the Plagues
		if (configModel.isPlague()) {
			plague = new Plague(boardController, configModel.getBoardModel().getnPlanets());
		}

		if(configModel.isAbomination()) {
			abominations = new Abomination(boardController);

		}

		// take 3 * 3 Cards from UndeadPile and add each 3, 2, 1 Undeads to the
		// planets
		for (int i = 3; i > 0; i--) {
			for (int j = 0; j < 3; j++) {
				UndeadCard c = cardController.takeUndeadCard();
				for(int k = 0; k < i; k++) {
					addUndead(c.getPlanetId(), c.getPlanetColor(), true);
				}
			}
		}


		// set StartPlanet
		int startPlanet = boardController.getStartPlanet();

		for(int i =0; i<model.getNPlayers(true); i++){
			model.getPlayer(i).setPlanetId(startPlanet);
			boardController.getPlanet(startPlanet).setPlayer(model.getPlayer(i).getId(), true);
		}

		boardController.getPlanet(startPlanet).setBase(true);	

	}

	// ------------------------------------------------CARDS----------------------------------------------------------------//


	/* (non-Javadoc)
	 * @see game.GameControllerI#cardThrown(cards.Card, int)
	 */
	@Override
	public boolean cardThrown(Card card, int playerId) { 
		if(model.getCurrentPlayerId() == playerId || card instanceof ActionCard) {
			if(!status.getFlag(1)) { //discard flag
				if (card instanceof ProtectionShield) {
					status.setFlag(true,0); //protection shield
					model.getPlayer(playerId).removeCard(card);
				} else {
					model.addLastCard(card);
					model.getPlayer(playerId).removeCard(card);
				}
			} else {
				model.getPlayer(playerId).removeCard(card);		
				status.setFlag(false, 1);  //discard flag
			}
			return true;	
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#flipPlayerCard()
	 */
	@Override
	public void flipPlayerCard() {
		// actions left && cards thrown ==> return cards to their owner
		if (model.getLastUsedCard().size() > 0) {
			for (Card card : model.getLastUsedCard()) {
				if(card instanceof ActionCard) {
					handleException(new Exception("GCTAC")); //#GCTAC actioncard was thrown but cannot be retaken
					break;
				}
				model.getCurrentPlayer().addCard(card);
			}
			model.getLastUsedCard().clear();
		}else if (model.getCurrentPlayer().getLeftActions() > 0 && model.getLastUsedCard().isEmpty()) {  // left actions, but no cards thrown --> endAction
			endAction(true);
		}else if (model.getCurrentPlayer().getLeftActions() == 0  // no acions left, flip the top player card
				&& !(model.getCurrentPlayer().getRole() instanceof Untoaster)) {
			if (status.getNumbers(3) < 2) { //playercard counter
				if(cardController.getPlayerPile().isEmpty()) {
					endGame(false);
				} else {
					cardController.flipTopPlayerCard();
				}
			}
		}
	}


	/* (non-Javadoc)
	 * @see game.GameControllerI#takePlayerCard()

	 */
	@Override
	public void takePlayerCard() {
		Card card = cardController.getTopPlayerCard();
		if (status.getNumbers(3) < 2) { //playercard counter
			model.clearLastUsedCards(); // clear last used card, so the player can't cheat

			if (card != null) {
				if (card instanceof MassAttackCard) {
					undeadMassAttack();
					cardController.removeTopPlayerCard();
					status.incPlayerCardCounter();
					flipPlayerCard();	
					return;
				}

				if (model.getCurrentPlayer().getHand().size() < 7) {
					model.getCurrentPlayer().addCard(card);
					cardController.removeTopPlayerCard();
					status.incPlayerCardCounter();
					flipPlayerCard();
				} else {
					if(status.getFlag(1)) {  //discard flag
						cardController.removeTopPlayerCard();
						status.incPlayerCardCounter();
						status.setFlag(false,1);  //discard flag
						flipPlayerCard();
					} else {
						status.setFlag(true,1);
						handleException(new Exception("GCTNC")); // #GCTNC GameController number of Cards
					}
				}

			} else {
				endGame(false);
				return;
			}
		}
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#flipUndeadCard()
	 */
	@Override
	public void flipUndeadCard() {
		if(model.getCurrentPlayer().getRole() instanceof Untoaster) {
			endTurn();
		}

		if (status.getNumbers(3)>1) { //playercard counter
			if (status.getFlag(0)) { // action Card protects against undead attack
				endTurn();
			}else if (status.getNumbers(4) < status.getNumbers(1)) { //undeadcardcounter < undeadstrength
				cardController.flipTopUndeadCard();

			}
		}
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#takeUndeadCard()
	 */
	@Override
	public void takeUndeadCard() {
		if (status.getNumbers(3) < 2) { //player card counter
			handleException(new Exception("GCTPC")); //#GCTPC not enough player cards taken
		}
		if (status.getNumbers(4) >= status.getNumbers(1)) { //undeadCardCounter > UndeadStrength
			handleException(new Exception("GCTUC")); //#GCTUC not enough undead cards taken
		}
		UndeadCard card = cardController.getTopUndeadCard();
		if (card == null) {
			handleException(new Exception("GCTUC")); //GCTUC no undeadscard left
		}

		int pId = card.getPlanetId();
		int pColor = card.getPlanetColor();

		if (!status.getExtinctSpecies()[pColor]) { // if not species is extinct, add undead
			addUndead(pId, pColor, true);
		}
		cardController.removeTopUndeadCard();


		if(status.getNumbers(4) == status.getNumbers(1)-1) { //undeadCardCounter == UndeadStrength
			endTurn();
		} else {
			status.incUndeadCardCounter();
			flipUndeadCard();

		}
	}


	/* (non-Javadoc)
	 * @see game.GameControllerI#undeadMassAttack()
	 */
	@Override
	public void undeadMassAttack() {
		status.increaseUndeadStrengthMarker();

		if (status.getNumbers(2) == 3 || status.getNumbers(2) == 5) { //strength marker
			status.inceaseUndeadStrength();
		}

		UndeadCard bottomCard = cardController.takeBottomUndeadCard();
		for(int i=0; i<3;i++) {
			addUndead(bottomCard.getPlanetId(), bottomCard.getPlanetColor(),  true);
		}
		cardController.mixDecks();




	}

	// -------------------------------------------------------------------------//

	/* (non-Javadoc)
	 * @see game.GameControllerI#endGame(boolean)
	 */
	@Override
	public void endGame(boolean isWon) {
		if (status.getFlag(2) == false) {
			status.setFlag(true,2); //end game flag
			MenuController.switchContent(new EndView(isWon, model.hasUntoaster())); 
		} 

	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#endTurn()
	 */
	@Override
	public void endTurn() {

		status.resetCardCounters();
		status.setFlag(false, 0);  //protection shield


		// add undeads to darkColumn planets
		if (configModel.isDarkColumns()) {
			for(int i : darkColumn.spawn()) {
				addUndead(i, boardController.getPlanet(i).getColor(),  true);
			}
		}

		// Evil AI aka blocked Planets
		if (configModel.isEvilAI()) {
			evilAi.move(model.getNextPlayer());
		}

		if (configModel.isAbomination()) {
			abominations.moveAllAbomination();

			for (int i = 0;  i< abominations.getAbominations().size(); i++) {
				Integer a = abominations.getAbominations().get(i);
				PlanetModelI planet = boardController.getPlanet(a);
				if (planet.hasPlayers()) {
					model.removeRandomPlayer(planet);
					if(model.getNPlayers(true)==0){
						endGame(false);
						return;
					}
				} else if (planet.hasUndead()) {
					planet.removeRandomUndead();
				} else {
					planet.setAbomination(false);;
					abominations.removeAbomination(a);
				}
			}
			abominations.moveNew();
		}

		model.nextPlayer();

		model.getCurrentPlayer().resetLeftActions();
		// TODO check of untoaster call method in boardcontroller

	}

	/*
	 * (non-Javadoc)
	 * @see game.GameControllerI#exorcistKills()
	 */
	@Override
	public void exorcistKills(int playerId) {
		for(int weapon=0;weapon<4; weapon++) {
			if(status.getDevelopedWeapons()[weapon]) {
				int r = boardController.getPlanet(model.getPlayer(playerId).getPlanetId()).getUndeads(weapon); //number of undeads on planet
				boardController.getPlanet(model.getPlayer(playerId).getPlanetId()).removeAllUndead(weapon);
				for (int j = 0; j < r; j++) { // update as often as one undead is killed
					status.increaseLeftUndead(weapon);
				}

				if (status.getLeftUndead()[weapon] == (25 - boardController.getPlanet(model.getPlayer(playerId).getPlanetId()).getUndeads(weapon))) {
					status.setExtinctSpecies(weapon);
				}
			}
		}

	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#endAction(player.PlayerI, boolean)
	 */
	@Override
	public void endAction(boolean decreaseAction) {
		PlayerI cPlayer = model.getCurrentPlayer();
	
		for(int playerId=0; playerId < model.getNPlayers(true); playerId++) { //check for Exorcist
			if(model.getPlayer(playerId).getRole() instanceof Exorcist) {
				exorcistKills(playerId);
			}
		}

		// if decreaseAction true && (if its empty no actioncard was thrown ||
		// if not check of action card) - no null pointer this way ;)
		if (decreaseAction
				&& (model.getLastUsedCard().isEmpty() || !(model.getLastUsedCard().get(0) instanceof ActionCard))) { 
			cPlayer.decreaseLeftActions();
		}

		model.clearLastUsedCards();
		status.getConqueredPlanets().clear(); // clear the conquered planets so
		// they can be attacked the next
		// action
		if(cPlayer.getLeftActions() == 0) {
			flipPlayerCard();
		}
		model.notifyEndAction();
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#mayDragUndead()
	 */
	@Override
	public boolean mayDragUndead() {
		return model.getCurrentPlayer().getRole().mayDragUndead();
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#addUndead(int, int, boolean)
	 */
	@Override
	public void addUndead(int planetId, int colorId, boolean notify) {
		PlanetModelI p = boardController.getPlanet(planetId);
		if (p.getUndeads(colorId) > 2) { // if more than 2 undeads of the
			// same color on the planet, all
			// of his neighbours will get
			// undeads
			status.addConqueredPlanets(planetId);

			ArrayList<Integer> neighbours = boardController.getNeighbours(planetId);

			for (int j : neighbours) {
				if (!status.getConqueredPlanets().contains(j)) {
					addUndead(j, colorId, true);
				}
			}

			if (configModel.isAbomination()) {

				if(! p.hasAbomination()) {
					p.setAbomination(true);
					abominations.addAbomination(planetId);
				}

			}

			if (configModel.isPlague()) {
				for (int j = 0; j < 4; j++) {
					if (p.getPlayers()[j]) {
						model.getPlayer(j).setPlague(colorId);
					}
				}
			}

			status.decreaseLeftConqueredPlanets();
			if (status.getNumbers(0) < 0) { //leftConqueredPlanets
				endGame(false);
				return;
			}
		} else {
			p.addUndead(colorId, notify);
			status.decreaseLeftUndead(colorId);

			for(int playerId=0; playerId < model.getNPlayers(true); playerId++) { //check for Exorcist
				if(model.getPlayer(playerId).getRole() instanceof Exorcist) {
					exorcistKills(playerId);
				}
			}

			for (int j = 0; j<4; j++) {
				if (status.getLeftUndead()[j] <= 0) {
					endGame(false);
					return;
				}
			}


			if (configModel.isPlague()) {
				for (int k = 0; k < 4; k++) {
					if (p.getPlayers()[k]) {
						plague.makePlague(model.getPlayer(k), p);
					}
				}
			}
		}
	}

	// -------------ACTION-METHODS----------------------------


	/* (non-Javadoc)
	 * @see game.GameControllerI#buildBase(int)
	 */

	@Override
	public boolean buildBase(int planetId) {
		PlayerI cPlayer = model.getCurrentPlayer(); // currentPlayer
		try {
			// check if action is valid
			if (cPlayer.getRole().buildBase(cPlayer, boardController.getPlanet(planetId), model.getLastUsedCard())) {
				boardController.getPlanet(planetId).setBase(true); // build base
				endAction(true);
				return true;
			}
		} catch (Exception e) {
			handleException(e); 
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#moveSpaceship(int, int, int)
	 */
	@Override
	public boolean moveSpaceship(int actionPlayerId, int startPlanetId, int endPlanetId) {

		PlayerI cPlayer = model.getCurrentPlayer(); // currentPlayer
		PlayerI aPlayer = model.getPlayer(actionPlayerId); // actionPlayer
		PlanetModelI sP = boardController.getPlanet(startPlanetId); // startPlanet
		PlanetModelI eP = boardController.getPlanet(endPlanetId);// endPlanet
		try {
			if(startPlanetId == endPlanetId) return false;

			try {
				if(cPlayer.getRole() instanceof Warpmaster) {
					if(cPlayer.getRole().warpToPlanet(cPlayer, aPlayer, sP, eP, model.getLastUsedCard())) {
						movePlayer(aPlayer, sP, eP);
						endAction(true);
						return true;
					}
				}
			} catch(Exception e) {

			}
			if (!model.getLastUsedCard().isEmpty()) { //WarpToPlanet
				if (cPlayer.getRole().warpToPlanet(cPlayer, aPlayer, sP, eP, model.getLastUsedCard())) {
					movePlayer(aPlayer, sP, eP);
					endAction(true);
					return true;
				}
			} else if (sP.hasBase() && eP.hasBase()) { //WarpToBase
				if (cPlayer.getRole().warpToBase(cPlayer, aPlayer, sP, eP)) {

					movePlayer(aPlayer, sP, eP);
					endAction(true);
					return true;
				}
			} else {	//MoveBySpaceship
				if (cPlayer.getRole().moveBySpaceShip(cPlayer, aPlayer, sP, eP,
						boardController.getNeighbours(startPlanetId))) {
					movePlayer(aPlayer, sP, eP);
					endAction(true);
					return true;
				}
			}

		} catch (Exception e) {
			handleException(e);
		}

		return false;
	}

	/**
	 * Moves the Player from StartPlanet to EndPlanet. Should only be used in
	 * GameController. Checks for Plague.
	 * 
	 * @param aPlayer
	 *            Player that is moving.
	 * @param startPlanet
	 *            Planet where player starts moving.
	 * @param endPlanet
	 *            Planet where player stops moving.
	 * @return true if action was valid
	 */
	private void movePlayer(PlayerI aPlayer, PlanetModelI startPlanet, PlanetModelI endPlanet) {
		startPlanet.setPlayer(aPlayer.getId(), false);
		endPlanet.setPlayer(aPlayer.getId(), true);
		aPlayer.setPlanetId(endPlanet.getId());


		if (configModel.isPlague()) {
			for (int i = 0; i < 4; i++) { // player has Plague and enters planet
				if (aPlayer.hasPlague(i)) {
					if (Math.random() < 0.2) { // 20% chance each
						addUndead(endPlanet.getId(), i, true);
					}
				}
			}

			if (endPlanet.hasUndead()) { // player enters planet with undead
				plague.makePlague(aPlayer, endPlanet);
			}
		}
	}


	/* (non-Javadoc)
	 * @see game.GameControllerI#shareKnowledge(cards.Card, int, int)
	 */
	@Override
	public boolean shareKnowledge(Card card, int givePlayerId, int getPlayerId) {

		PlayerI cPlayer = model.getCurrentPlayer();
		PlayerI getPlayer = model.getPlayer(getPlayerId);
		PlayerI givePlayer = model.getPlayer(givePlayerId);
		boardController.getPlanet(givePlayer.getPlanetId());
		boardController.getPlanet(getPlayer.getPlanetId());

		PlayerI aPlayer;

		try {
			if (givePlayer.getRole() instanceof CrazyWeaponArchitect) { //check for weaponarchitect
				aPlayer = givePlayer;
			} else if (getPlayer.getRole() instanceof CrazyWeaponArchitect) {
				aPlayer = getPlayer;
			} else {
				aPlayer = cPlayer;
			}
			if (aPlayer.getRole().shareKnowledge(cPlayer, givePlayer, getPlayer,
					model.getLastUsedCard(), card)) {
				givePlayer.removeCard(card);
				getPlayer.addCard(card);
				endAction(true);
				return true; 
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}


	/* (non-Javadoc)
	 * @see game.GameControllerI#killUndead(int, int)
	 */
	@Override

	public boolean killUndead(int planetId, int undeadColor) {
		PlayerI cPlayer = model.getCurrentPlayer();
		PlanetModelI planet = boardController.getPlanet(planetId);
		try {
			if (cPlayer.getRole().killUndead(cPlayer, planet, undeadColor)) {
				if (status.getDevelopedWeapons()[undeadColor]) {
					if (status.getLeftUndead()[undeadColor] == (25 - planet.getUndeads(undeadColor))) {
						status.setExtinctSpecies(undeadColor);
					}
					int r = planet.getUndeads(undeadColor);
					planet.removeAllUndead(undeadColor);
					for (int i = 0; i < r; i++) { // update as often as undead is killed
						status.increaseLeftUndead(undeadColor);
					}
				} else if (cPlayer.getRole() instanceof Exorcist) { // exorcist  kills them all
					int r = planet.getUndeads(undeadColor);
					planet.removeAllUndead(undeadColor);
					for (int i = 0; i < r; i++) { // update as often as one undead is killed
						status.increaseLeftUndead(undeadColor);
					}
				} else if (cPlayer.getImprovedWeaponLeft() > 0) { // improved weapon kills 2
					planet.removeUndead(undeadColor, true);
					planet.removeUndead(undeadColor, true);
					cPlayer.useImprovedWeapon();

				} else {
					planet.removeUndead(undeadColor, true); // boring normal killing
					status.increaseLeftUndead(undeadColor);
				}

				endAction(true);
				return true;
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}


	/* (non-Javadoc)
	 * @see game.GameControllerI#removeDarkColumn(int)
	 */
	@Override
	public boolean removeDarkColumn(int planetId) {
		if (configModel.isDarkColumns()) {
			PlayerI cPlayer = model.getCurrentPlayer();

			try {
				if (cPlayer.getRole().removeDarkColumn(cPlayer, boardController.getPlanet(planetId),
						model.getLastUsedCard())) {
					darkColumn.remove(planetId);
					endAction(true);
					return true;

				}
			} catch (Exception e) {
				handleException(e);
			}
		} else {
			handleException(new Exception("GCTDK")); // #GTCDK Controller DarkColumns not enabled
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#developWeapon(int)
	 */
	@Override
	public boolean developWeapon(int weaponColor) {
		PlayerI cPlayer = model.getCurrentPlayer();

		try {
			if (cPlayer.getRole().developWeapon(cPlayer, boardController.getPlanet(cPlayer.getPlanetId()), weaponColor,
					model.getLastUsedCard())) {
				if(! status.getDevelopedWeapons()[weaponColor]) {
					status.developWeapon(weaponColor);


					if (status.getLeftUndead()[weaponColor] == (25 -  boardController.getPlanet(cPlayer.getPlanetId()).getUndeads(weaponColor))) {
						status.setExtinctSpecies(weaponColor);
					}

					if (status.getDevelopedWeapons()[0] && status.getDevelopedWeapons()[1] && status.getDevelopedWeapons()[2] 
							&& status.getDevelopedWeapons()[3]) {
						endGame(true);
						return false;
					}


					endAction(true);
					return true;
				} else {
					throw new Exception("GCTWD");
				}
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#buyImprovedWeapon()
	 */
	@Override
	public boolean buyImprovedWeapon() {

		PlayerI cPlayer = model.getCurrentPlayer();
		int planetId = cPlayer.getPlanetId();
		PlanetModelI planet = boardController.getPlanet(planetId);

		try {
			if (cPlayer.getRole().buyImprovedWeapon(cPlayer, planet, model.getLastUsedCard())) {
				cPlayer.setImprovedWeapon();
				endAction(false);
				return true;
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#mineResource(int, int)
	 */
	@Override
	public boolean mineResource(int colorId, int planetId) { 
		PlayerI cPlayer = model.getCurrentPlayer();
		PlanetModelI planet = boardController.getPlanet(planetId);

		try {
			if (cPlayer.getRole().mineResource(cPlayer, planet, colorId)) {
				cPlayer.setResource(colorId);
				endAction(true);
				return true;
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#recruiteHealer(int)
	 */
	@Override
	public boolean recruiteHealer(int colorId) {
		PlayerI cPlayer = model.getCurrentPlayer();
		PlanetModelI planet = boardController.getPlanet(model.getCurrentPlayer().getPlanetId());

		try {
			if (cPlayer.getRole().recruiteHealer(cPlayer, planet, colorId)) {
				planet.setHealer(colorId);
				cPlayer.removeResource(colorId);
				endAction(false);
				return true;
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#healPlague(int)
	 */
	@Override
	public boolean healPlague(int colorId) {
		PlayerI cPlayer = model.getCurrentPlayer();
		PlanetModelI planet = boardController.getPlanet(cPlayer.getPlanetId());
		try {
			if(cPlayer.getRole().healPlague(cPlayer, planet, colorId)) {
				cPlayer.removePlague(colorId);
				endAction(true);
				return true;
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#moveUndead(int, int, int)
	 */
	@Override
	public boolean moveUndead(int colorId, int sPlanetId, int ePlanetId) {
		//TODO one undead might me moved just once
		PlayerI cPlayer = model.getCurrentPlayer();
		PlanetModelI sPlanet = boardController.getPlanet(sPlanetId);
		PlanetModelI ePlanet = boardController.getPlanet(ePlanetId);
		try {
			if (cPlayer.getRole().moveUndead(sPlanet, ePlanet, colorId, boardController.getNeighbours(sPlanetId))) {
				sPlanet.removeUndead(colorId, false);
				addUndead(ePlanetId, colorId, false);
				return true;
			}
		} catch (Exception e) {
			handleException(e);
		}
		return false;
	}

	// ---------------------------------------------------------------------------



	/* (non-Javadoc)
	 * @see game.GameControllerI#saveGame(java.lang.String)

	 */
	@Override
	public void saveGame(String path) {
		try {
			SaveLoader s = new SaveLoader();
			s.save(path, model, status, configModel);
		} catch (Exception e) {
			handleException(e);
		}
	}

	/* (non-Javadoc)
	 * @see game.GameControllerI#handleException(java.lang.Exception)
	 */
	@Override
	public void handleException(Exception e) {
		//	e.printStackTrace();
		if (e.getMessage().length() == 5) { //check for correct length, undefindet exceptions would throw a excpetion in substrings
			boardController.showMessage(e.getMessage().substring(0, 5));		
		} else {
			e.printStackTrace();
		}
	}


	/*
	 * (non-Javadoc)
	 * @see game.GameControllerI#connectUndead(game.board.UndeadView)
	 */
	@Override
	public void connectUndead(UndeadView uV) {
		model.addObserverX(uV);
	}

}
