package game.cardinfo;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ThreadLocalRandom;

import cards.Card;
import game.DragContainer;
import game.GameModelI;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import language.Language;
import language.TrText;
import player.Player;
import player.PlayerI;
import utils.ImageLoader;

/**
 * Player panel shows information about players. It contains a player's name,
 * cards, resources, plagues, improved weapons.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class PlayerPanel extends HBox implements Observer {

	/**
	 * Id of player.
	 */
	private final int playerId;

	/**
	 * Color of player's spaceship.
	 */
	private final int colorId;

	/**
	 * Box containing player's hand cards.
	 */
	private final HBox cards;

	/**
	 * Scale factor for resizing.
	 */
	private final DoublePropertyBase scale;

	/**
	 * Image views for all plagues. Size of four because four different plagues
	 * possible.
	 */
	private final ImageView[] plagues = new ImageView[4];

	/**
	 * Image views for all plagues. Size of four because four different plagues
	 * possible.
	 */
	private final ImageView[] resources = new ImageView[4];

	/**
	 * View for bought improved weapon.
	 */
	private final ImageView weapon;

	/**
	 * Counter stating how many improved weapons a player has left.
	 */
	private final Label weaponCount;

	/**
	 * Controller for cardbox handels events on CardBoxView.
	 */
	private final CardInfoController cardBoxController;

	/**
	 * Name of player.
	 */
	private Label name;

	/**
	 * Player that is displayed.
	 */
	private PlayerI player;

	/**
	 * Pane that displays information about improved weapons, plagues and
	 * resources.
	 */
	private GridPane states;

	/**
	 * Creates a player panel with name, role, cards and all other information.
	 * 
	 * @param player
	 *            Player the panel is created for.
	 * @param drag
	 *            Container of dragable objects.
	 * @param cardInfoController
	 *            Controller handeling events.
	 * @param startPlayerId
	 *            Id of player who starts.
	 */
	public PlayerPanel(PlayerI player, DragContainer drag, CardInfoController cardInfoController, int startPlayerId) {
		this.playerId = player.getId();
		this.cardBoxController = cardInfoController;
		this.colorId = player.getColorId();
		this.player = player;

		player.addObserverX(this);

		scale = new SimpleDoubleProperty(1);

		this.setHeight(main.Constants.CARDBOX_HEIGHT * 0.4925);
		this.setPrefHeight(main.Constants.CARDBOX_HEIGHT * 0.4925);
		this.setMinHeight(main.Constants.CARDBOX_HEIGHT * 0.4925);

		VBox infos = new VBox();
		infos.setPadding(new Insets(0, 0, 0, 3));

		infos.prefWidthProperty().bind(this.widthProperty().multiply(0.3));
		infos.setMaxHeight(this.getHeight());

		cards = new HBox();
		cards.prefWidthProperty().bind(this.prefWidthProperty().multiply(0.7));
		cards.setMaxHeight(this.getHeight());

		name = new Label(player.getName());
		name.setMaxHeight(5);
		name.setPrefHeight(5);

		name.prefWidthProperty().bind(infos.widthProperty());
		infos.setSpacing(0);

		TrText role = Language.getInstance().createText(player.getRole().getTextId());

		role.setStyle("-fx-position-shape: false");

		role.setStyle("-fx-background-color: green; -fx-pref-width: 5; -fx-max-width: 5;");
		role.setStyle("-fx-text-fill: green;");
		role.setStyle("-fx-fill: #" + main.Constants.PLAYER_COLORS[player.getColorId()].toString().substring(2) + ";");
		// role.setId("text");

		states = new GridPane();
		states.setMaxHeight(main.Constants.CARDBOX_HEIGHT * 0.4925 * 0.25);
		states.setGridLinesVisible(true);
		states.setStyle("-fx-border-color: black");
		states.setStyle("-fx-background-color: #c2c4c6");

		ColumnConstraints column;
		RowConstraints row;

		for (int i = 0; i < 5; i++) {
			column = new ColumnConstraints();
			column.setPercentWidth(100.0 / 5);
			states.getColumnConstraints().add(column);
		}

		for (int i = 0; i < 2; i++) {
			row = new RowConstraints();
			row.setPercentHeight(50.0);
			states.getRowConstraints().add(row);
		}

		for (int i = 0; i < 4; i++) {
			plagues[i] = new ImageView(ImageLoader.getInstance().get("/undeads/undead" + i
					+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png"));
			plagues[i].setPreserveRatio(true);
			plagues[i].fitWidthProperty().bind(infos.widthProperty().divide(5));
			plagues[i].setFitHeight(main.Constants.CARDBOX_HEIGHT * 0.4925 / 4);
			plagues[i].setVisible(player.hasPlague(i));
			states.add(plagues[i], i + 1, 0);
			cardInfoController.connectPlague(plagues[i], i);
			plagues[i].setStyle("-fx-border-color: white");

		}
		for (int i = 0; i < 4; i++) {
			resources[i] = new ImageView(ImageLoader.getInstance().get("/planetTiles/resource" + i + ".png"));
			resources[i].setPreserveRatio(true);
			resources[i].fitWidthProperty().bind(infos.widthProperty().divide(5));
			resources[i].setFitHeight(main.Constants.CARDBOX_HEIGHT * 0.4925 / 4);
			resources[i].xProperty()
					.bind(((states.widthProperty().divide(5)).subtract(resources[i].fitWidthProperty())).divide(2)); // TODO
																														// CNTER
			resources[i].setVisible(player.getResource(i));
			states.add(resources[i], i + 1, 1);
			cardInfoController.ConnectResource(resources[i], i, this);
		}

		for (Node child : states.getChildren()) {
			GridPane.setHalignment(child, HPos.CENTER);
			GridPane.setValignment(child, VPos.CENTER);
		}

		weapon = new ImageView(ImageLoader.getInstance().get("/planetTiles/imprweapon.png"));
		weapon.setPreserveRatio(true);
		weapon.fitWidthProperty().bind(infos.widthProperty().divide(5));
		weapon.setFitHeight(main.Constants.CARDBOX_HEIGHT * 0.4925 / 4);
		weapon.setVisible(player.getImprovedWeaponLeft() != 0);

		states.add(weapon, 0, 0);

		weaponCount = new Label(Integer.toString(player.getImprovedWeaponLeft()));
		weaponCount.setTextFill(Color.BLACK);
		weaponCount.prefWidthProperty().bind(resources[0].fitWidthProperty());
		weaponCount.prefHeightProperty().bind(resources[0].fitHeightProperty());
		weaponCount.translateXProperty()
				.bind((resources[0].fitWidthProperty().subtract(weaponCount.widthProperty()).divide(2)));

		weaponCount.setVisible(player.getImprovedWeaponLeft() != 0);
		weaponCount.setAlignment(Pos.CENTER);

		states.add(weaponCount, 0, 1);
		states.setAlignment(Pos.TOP_RIGHT);

		infos.getChildren().addAll(name, role, states);

		this.widthProperty().addListener((obs, oldVal, newVal) -> {
			this.resize();
		});
		this.heightProperty().addListener((obs, oldVal, newVal) -> {
			this.resize();

		});

		for (Card c : player.getHand()) {
			addCard(c);
		}
		this.getChildren().addAll(infos, cards);
		HBox.setHgrow(cards, Priority.ALWAYS);

		changeColor(startPlayerId);
	}

	/**
	 * Resize cards.
	 */
	private void resize() {
		scale.set(Math.min(this.getHeight() / 3, (this.getWidth() / 7 - 6) / 2));
	}

	/**
	 * Adds a card to player's hand cards.
	 * 
	 * @param c
	 *            Card which is added.
	 */
	private void addCard(Card c) {

		Pane card = c.toPane();
		card.prefWidthProperty().bind(scale.multiply(2));
		card.prefHeightProperty().bind(scale.multiply(3));

		cardBoxController.ConnectCard(card, c, this);

		this.cards.getChildren().add(card);
	}

	/**
	 * Getter for player's id.
	 * 
	 * @return playerId
	 */
	public int getPlayerId() {
		return playerId;
	}

	/**
	 * Move Card of a panel.
	 * 
	 * @param card
	 *            Card which is moved.
	 */
	public void moveCard(Pane card) {
		card.setTranslateX(0);
		card.setTranslateY(0);
		this.cards.getChildren().add(card);
	}

	/**
	 * Updates view if it is called.
	 */
	@Override
	public void update(Observable model, Object msg) {
		switch (msg.toString().substring(0, 5)) {
		case "nxtPl": // next playr
			changeColor(((GameModelI) model).getCurrentPlayer().getId());
			return;
		case "setPl": // set plague
			setPlague(Character.getNumericValue(msg.toString().charAt(5)));
			return;
		case "getRs": // get resource
			setResource(Character.getNumericValue(msg.toString().charAt(5)));
			return;
		case "impWp": // improved weapon
			setWeapon(((Player) model).getImprovedWeaponLeft());
			return;
		case "addCa": // ad card
			addCard(((Player) model).getHand().get(Character.getNumericValue(msg.toString().charAt(5))));
			return;
		case "killP": // kill player
			removePile();
		}
	}

	/**
	 * Removes cards from view, e.g. if player is killed.
	 */
	private void removePile() {
		Pane parent = (Pane) this.getParent();
		parent.getChildren().remove(this);
	}

	/**
	 * If player has bought an improved weapon, counter for left weapons is set
	 * visible. If player has used an improved weapon the counter will be
	 * decreased. If there are no weapons left counter gets set invisible again.
	 * 
	 * @param ammo
	 */
	private void setWeapon(int ammo) {
		weaponCount.setText(Integer.toString(ammo));
		weaponCount.setVisible(true);
		weapon.setVisible(true);
		if (ammo == 0) {
			weaponCount.setVisible(false);
			weapon.setVisible(false);
		}
	}

	/**
	 * Resource gets set visible if player mines a resource.
	 * 
	 * @param colorId
	 *            Color of resourced mine.
	 */
	private void setResource(int colorId) {
		resources[colorId].setVisible(player.getResource(colorId));
	}

	/**
	 * Plague gets set visible if player gets a plague.
	 * 
	 * @param colorId
	 *            Color of plague.
	 */
	private void setPlague(int colorId) {
		plagues[colorId].setVisible(player.hasPlague(colorId));
	}

	/**
	 * Active player's name is colored. If it is next player's turn the next
	 * player's name gets colored.
	 * 
	 * @param playerId Id of player who's turn it is.
	 */
	private void changeColor(int playerId) {
		if (this.playerId == playerId) {
			name.setTextFill(main.Constants.PLAYER_COLORS[colorId]);
		} else {
			name.setTextFill(Color.WHITE);
		}
	}


}
