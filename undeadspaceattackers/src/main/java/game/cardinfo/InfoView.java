package game.cardinfo;

import java.util.Observable;
import java.util.Observer;

import game.GameStatusModelI;
import game.board.BaseView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import language.Language;
import language.TrText;
import start.Title;
/**
 * this class represents the infoBox on the right side of the gameView
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class InfoView extends AnchorPane implements Observer {

	/**
	 * model that contains status information displayed by the infobox
	 */
	private GameStatusModelI gsm;
	
	/**
	 * pane array that displays the attack strength marker
	 */
	private StackPane[] strengthPane = new StackPane[6];
	
	/**
	 * text that displays the left allowed planet conquests
	 */
	private Text cPlanetsNo;
	
	/**
	 * text array that displays how many undead of every species are left
	 */
	private Text[] undeadNo = new Text[4];
	
	/**
	 * rectangle array that displays which species is extinct 
	 */
	private Rectangle[] speciesRectangle = new Rectangle[4];
	
	/**
	 * rectangle array that contains strength and may have a
	 * red background if it is the current strength
	 */
	private Rectangle[] strengthRectangle = new Rectangle[6];

	/**
	 * BaseView to show a base. Base symbol can be draged in game to a planet to
	 * build a base.
	 */
	private BaseView baseButton;
	
	/**
	 * Button for improved buttons. Button is only shown if improved weapons are activated.
	 */
	private Button iWeaponsBtn;
	
	/**
	 * Button to develop weapon against ghosts. Weapon is a vaccum.
	 */
	private Button vacuumBtn;
	
	/**
	 *  Button to develop weapon against zombies. Weapon is a schnetler.
	 */
	private Button schnetlerBtn;
	
	/**
	 *  Button to develop weapon against mummies. Weapon is fire.
	 */
	private Button fireBtn;
	
	/**
	 *   Button to develop weapon against skeletons. Weapon is a dog.
	 */
	private Button dogBtn;	
	
	/**
	 * Button to quit the game. Before quitting an option to save the game is given.
	 */
	private Button quitBtn = Language.getInstance().createButton("QUT");;
	
	/**
	 * Constructor for InfoBox, creates InfoView without improved Weapons
	 * 
	 * @param gsm
	 *            GameStatusModel
	 * @param activateIWeapons
	 *            true if improved weapons are activated, chosen in GameSettings
	 */
	public InfoView(GameStatusModelI gsm, boolean activateIWeapons, Pane parent) {
		
		this.gsm = gsm;
		this.gsm.addObserverX(this);
		quitBtn.setId("quitBtn");
		Title title = new Title(quitBtn);	
		
		Text texts[] = new TrText[10];		
		texts[0] = Language.getInstance().createText("SKE");
		texts[1] = Language.getInstance().createText("ZOM");
		texts[2] = Language.getInstance().createText("MUM");
		texts[3] = Language.getInstance().createText("GHO");	
		texts[4] = Language.getInstance().createText("ES");
		texts[5] = Language.getInstance().createText("NCP");	
		texts[6] = Language.getInstance().createText("ASU");
		texts[7] = Language.getInstance().createText("NRU");
		texts[8] = Language.getInstance().createText("IMW");
		texts[9] = Language.getInstance().createText("WEA");			
		for (int i=0; i<10; i++) {
			texts[i].setId("info");
		}
		texts[0].setStyle("-fx-fill: yellow;");
		texts[1].setStyle("-fx-fill: green;");
		texts[2].setStyle("-fx-fill: blue;");
		texts[3].setStyle("-fx-fill: red;");
		
	

		//extinct species
		HBox species = createExtinctSpecies();	
		
		//conquered Planets
		cPlanetsNo = new Text(String.valueOf(gsm.getNumbers(0)));
		cPlanetsNo.setId("info");
		HBox hbox1 = new HBox(texts[5], cPlanetsNo);

		//attackStrength
		HBox attackStrength = createAttackStrength();
		
		
		
		//left undead
		for (int i = 0; i<4; i++) {
			int leftundead = gsm.getLeftUndead()[i];
			undeadNo[i] = new Text(String.valueOf(leftundead));
			undeadNo[i].setId("info");
		}	
		GridPane undead = new GridPane();
		for (int i = 0; i<4; i++) {
			undead.add(texts[i], 0, i);	
		}
		for (int i = 0; i<4; i++) {
			undead.add(undeadNo[i], 1, i);	
		}
		undead.setHgap(5);	

		GridPane weapons = createButtons();	

		
		// improved weapons and build base	
		StackPane basePane = createBaseRectangle(activateIWeapons);
		GridPane gridBase = createGridBase(activateIWeapons);	

		//develop weapons
		StackPane weaponsPane = new StackPane();
		Rectangle weaponsR = new Rectangle(main.Constants.INFOBOX_WIDTH - 10, 30);
		weaponsPane.getChildren().addAll(weaponsR, texts[9]);

		//arrange elements
		setTopAnchor(title, 0.0);
		setLeftAnchor(title, 0.0);
		setRightAnchor(title, 0.0);
	
		VBox vbox = new VBox(texts[4], species, hbox1, texts[6], attackStrength, texts[7], undead, basePane,
				gridBase, weaponsPane, weapons);
		arrangeVBox(vbox, parent);		
		this.getChildren().addAll(title, vbox);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {

		
		switch ((String) arg) {
		case "leftCPlanets":
			updateCPlanets();
			return;
		case "strengthMarker":
			updateStrengthMarker();
			return;
		case "developedWeapons":
			updateDevelopedWeapons();
			return;
		case "extinctSpecies":
			updateExtinctSpecies();
			return;
		case "leftUndead":
			updateLeftundead();
			return;
		}
	}
	
	/**
	 * Creates rectangles for extinct species. In the beginning rectangles are white,
	 * if a species is extinct the rectangle is filled with the extinct species.
	 * @return
	 */
	private HBox createExtinctSpecies(){
		for (int i = 0; i<4; i++) {
			speciesRectangle[i] = new Rectangle(32,32,Color.WHITE);
		}
		HBox species = new HBox(speciesRectangle[0], speciesRectangle[1], speciesRectangle[2], speciesRectangle[3]);
		species.setSpacing(5);
		species.setPrefWidth(main.Constants.INFOBOX_WIDTH - 10);
		return species;
	}
	
	/**
	 * Creates a bar displaying the attack strength of undead.
	 * @return
	 */
	private HBox createAttackStrength(){

		for (int i = 0; i<6; i++) {
			strengthRectangle[i] = new Rectangle(18,30,Color.WHITE);
		}
		Label[] no = new Label[6];
		no[0] = new Label(String.valueOf(gsm.getNumbers(1)));
		no[1] = new Label(String.valueOf(gsm.getNumbers(1)));
		no[2] = new Label(String.valueOf(gsm.getNumbers(1)));
		no[3] = new Label(String.valueOf(gsm.getNumbers(1) + 1));
		no[4] = new Label(String.valueOf(gsm.getNumbers(1) + 1));
		no[5] = new Label(String.valueOf(gsm.getNumbers(1) + 2));	
		for (int i = 0; i<6; i++) {
			no[i].setId("numbers");
			strengthPane[i] = new StackPane();
			strengthPane[i].getChildren().addAll(strengthRectangle[i], no[i]);
		}		
		strengthRectangle[0].getStyleClass().add("my-rect"); //initial current strength
		HBox attackStrength = new HBox(strengthPane[0], strengthPane[1], strengthPane[2], strengthPane[3], strengthPane[4], strengthPane[5]);
		attackStrength.setSpacing(5);	
		return attackStrength;
	}
	
	/**
	 * Creates Pane with a Rectangle and a text. Text above button depends on improved weapons.
	 * 
	 * @param activateIWeapons
	 *            Boolean that states whether improved weapons are activated.
	 *            True if they are.
	 * @return StackPane with Rectangle and Text.
	 */
	private StackPane createBaseRectangle(boolean activateIWeapons){
		Text baseT;
		Rectangle baseR = new Rectangle(main.Constants.INFOBOX_WIDTH - 10, 30);	
		if (activateIWeapons == false) {
			 baseT = Language.getInstance().createText("MIB");

		} else {
			 baseT = Language.getInstance().createText("BAI");

		}
		baseT.setId("info");
		StackPane basePane = new StackPane();
		basePane.getChildren().addAll(baseR, baseT);
		return basePane;
		
	}
	
	/**
	 * Creates a GridPane with baseView and button for improved weapons (if activated).
	 * @param activateIWeapons True if improved weapons are activated.
	 * @return GridPane with icons(base and weapon).
	 */
	private GridPane createGridBase(boolean activateIWeapons){
		baseButton = new BaseView();
		baseButton.setPrefSize(main.Constants.INFOBOX_WIDTH/2-15, main.Constants.INFOBOX_WIDTH/2-15);
		GridPane gridBase = new GridPane();
		
		if (activateIWeapons == false) {
			gridBase.add(baseButton, 0, 0);
		} else {
			gridBase.add(baseButton, 0, 0);
			gridBase.add(iWeaponsBtn, 1, 0);
			gridBase.setVgap(20);
			gridBase.setHgap(20);
		}
		return gridBase;
	}
	

	/**
	 * Instantiates all buttons for normal weapons. Sets a size for each.
	 * Arranges all buttons on a gridPane
	 * @return GridPane with all buttons.
	 */
	private GridPane createButtons(){
		iWeaponsBtn = new Button("     ");
		iWeaponsBtn.setId("weapon");	
		iWeaponsBtn.setMaxWidth(65);
		iWeaponsBtn.setMinWidth(65);
		iWeaponsBtn.setPrefWidth(65);
		
		vacuumBtn = new Button("     ");
		vacuumBtn.setId("vacuum");
		vacuumBtn.getStyleClass().add("my-btn");
		vacuumBtn.setMaxWidth(65);
		vacuumBtn.setMinWidth(65);
		vacuumBtn.setPrefWidth(65);

		schnetlerBtn = new Button("     ");
		schnetlerBtn.setId("schnetler");
		schnetlerBtn.setMaxWidth(65);
		schnetlerBtn.setMinWidth(65);
		schnetlerBtn.setPrefWidth(65);

		fireBtn = new Button("     ");
		fireBtn.setId("fire");
		fireBtn.setMaxWidth(65);
		fireBtn.setMinWidth(65);
		fireBtn.setPrefWidth(65);

		dogBtn = new Button("     ");
		dogBtn.setId("dog");
		dogBtn.setMaxWidth(65);
		dogBtn.setMinWidth(65);
		dogBtn.setPrefWidth(65);

		GridPane weapons = new GridPane();
		weapons.add(vacuumBtn, 0, 0);
		weapons.add(dogBtn, 1, 0);
		weapons.add(fireBtn, 0, 1);
		weapons.add(schnetlerBtn, 1, 1);
		weapons.setStyle("-fx-background-color: grey");
		weapons.setPadding(new Insets(5, 5, 5, 5));
		weapons.setHgap(5);
		weapons.setVgap(5);
		return weapons;
	}
	
	/**
	 * Arranges vBox in infobox. Sets a fixed size.
	 * @param vbox
	 * @param parent
	 */
	private void arrangeVBox(VBox vbox, Pane parent){
		vbox.spacingProperty().bind((parent.heightProperty().subtract(525.0)).divide(10)); 
		vbox.setPrefWidth(main.Constants.INFOBOX_WIDTH - 10);
		vbox.setMaxWidth(main.Constants.INFOBOX_WIDTH - 10);
		vbox.setMinWidth(main.Constants.INFOBOX_WIDTH - 10);
		vbox.setPadding(new Insets(3));
		vbox.setAlignment(Pos.CENTER);

		setTopAnchor(vbox, 55.0);
		setLeftAnchor(vbox, 0.0);
		setRightAnchor(vbox, 0.0);
		setBottomAnchor(vbox, 0.0);
	}

	/**
	 * Updates number of allowed left conquered planets.
	 */
	public void updateCPlanets() {
		cPlanetsNo.setText(String.valueOf(gsm.getNumbers(0)));
		// cPlanetsNo.setText(value);
	}

	/**
	 * Updates Strength marker by switching the red background to the next
	 * rectangle.
	 */
	public void updateStrengthMarker() {
		int marker = gsm.getNumbers(2);
		switch (marker) {
		case 1: 
			strengthRectangle[1].getStyleClass().add("my-rect");
			strengthRectangle[0].getStyleClass().remove("my-rect");
			break;
		case 2:
			strengthRectangle[2].getStyleClass().add("my-rect");
			strengthRectangle[1].getStyleClass().remove("my-rect");
			break;
		case 3:
			strengthRectangle[3].getStyleClass().add("my-rect");
			strengthRectangle[2].getStyleClass().remove("my-rect");
			break;
		case 4:
			strengthRectangle[4].getStyleClass().add("my-rect");
			strengthRectangle[3].getStyleClass().remove("my-rect");
			break;
		case 5:
			strengthRectangle[5].getStyleClass().add("my-rect");
			strengthRectangle[4].getStyleClass().remove("my-rect");
			break;
		}

	}

	/**
	 * Adds frames around developed Weapons if they are developed.
	 */
	public void updateDevelopedWeapons() {

		for (int i = 0; i < 4; i++) {
			if (gsm.getDevelopedWeapons()[i] == true) {
				switch (i) {
				case 0:
					dogBtn.getStyleClass().add("developedWeapon");
					break;
				case 1:
					schnetlerBtn.getStyleClass().add("developedWeapon");
					break;
				case 2:
					fireBtn.getStyleClass().add("developedWeapon");
					break;
				case 3:
					vacuumBtn.getStyleClass().add("developedWeapon");
					break;
				}
			}
		}
	}

	/**
	 * Updates rectangles of extinct species. Fills them if a species is extinct with 
	 * picture of corresponding species.
	 */
	public void updateExtinctSpecies() {
		for (int i = 0; i < 4; i++) {
			if (gsm.getExtinctSpecies()[i] == true) {
				switch (i) {
				case 0:
					speciesRectangle[0].getStyleClass().add("extinct-Dog");
					
					break;
				case 1:
					speciesRectangle[1].getStyleClass().add("extinct-Schnetler");
					
					break;
				case 2:
					speciesRectangle[2].getStyleClass().add("extinct-Fire");
					
					break;
				case 3:
					speciesRectangle[3].getStyleClass().add("extinct-Vacuum");
					
					break;
				}

			}
		}
	}

	/**
	 * Updates number of left undead for all undead.
	 */
	public void updateLeftundead() {

		undeadNo[0].setText(String.valueOf(gsm.getLeftUndead()[0]));
		undeadNo[1].setText(String.valueOf(gsm.getLeftUndead()[1]));
		undeadNo[2].setText(String.valueOf(gsm.getLeftUndead()[2]));
		undeadNo[3].setText(String.valueOf(gsm.getLeftUndead()[3]));
	}


	/**
	 * Getter for improved weapons button.
	 * @return the iWeaponsBtn
	 */
	public Button getiWeaponsBtn() {
		return iWeaponsBtn;
	}

	/**
	 * Getter for vaccumBtn.
	 * @return the vacuumBtn
	 */
	public Button getVacuumBtn() {
		return vacuumBtn;
	}

	/**
	 * Getter for schnetlerBtn.
	 * @return the schnetlerBtn
	 */
	public Button getSchnetlerBtn() {
		return schnetlerBtn;
	}

	/**
	 * Getter for fireBtn.
	 * @return the fireBtn
	 */
	public Button getFireBtn() {
		return fireBtn;
	}

	/**
	 * getter for dogBtn
	 * @return the dogBtn
	 */
	public Button getDogBtn() {
		return dogBtn;
	}


	
	/**
	 * Getter for dogBtn
	 * @return the quitBtn
	 */
	public Button getQuitBtn() {
		return quitBtn;
	}
	
	/**
	 * Getter for BaseView.
	 * @return baseView
	 */
	public BaseView getBaseButton() {
		return baseButton;
	}
	
}
