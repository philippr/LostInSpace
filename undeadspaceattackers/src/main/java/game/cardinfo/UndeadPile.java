package game.cardinfo;

import java.util.Observable;
import java.util.concurrent.ThreadLocalRandom;

import cards.CardModelI;
import javafx.scene.image.ImageView;
import utils.ImageLoader;

/**
* Undead pile with player cards. In undead card pile only cards with undeads
* 
* @author Philipp, Fabian, Julia, Ronja
*
*/
public class UndeadPile extends Pile {
	
	/**
	 * Create pile with given width and height.
	 * @param width Width of pile.
	 * @param height Height of pile.
	 */
	UndeadPile(double width, double height){
		super(width, height);

	}
	
	/**
	 * Loads images of four undeads and creates an image view for each color.
	 */
	@Override
	protected void makeColors() {
		yellow = new ImageView(ImageLoader.getInstance().get("/undeads/undead0"+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) +".png"));
		green = new ImageView(ImageLoader.getInstance().get("/undeads/undead1"+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) +".png"));
		blue = new ImageView(ImageLoader.getInstance().get("/undeads/undead2"+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) +".png"));
		red = new ImageView(ImageLoader.getInstance().get("/undeads/undead3"+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) +".png"));
	}

	@Override
	public void update(Observable model, Object msg) {
		switch (msg.toString().charAt(0)) {
		case 'u':
			setLabel(((CardModelI) model).getUndeadPile().size());
		case 't':
			turnCard(((CardModelI) model).getTopUndeadCard());
			return;
		}
	}
}
