package game.cardinfo;

import java.util.Observable;
import java.util.Observer;

import cards.CardModelI;
import game.DragContainer;
import game.GameModelI;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * CardBox View shows cards for players, actionbar, player card pale and undead
 * card pile.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class CardBoxView extends HBox implements Observer {

	/**
	 * Undead card pile contains all undead cards that can be taken by players
	 * at the end of their turn.
	 */
	private final UndeadPile undeadPile;
	
	/**
	 * Player card pile with all player cards containing planet cards, action cards and mass
	 * attack cards.
	 */
	private final PlayerPile playerPile;
	
	/**
	 * Pane for Untoaster, if it is untoasters turn. 
	 */
	private final StackPane untoasterPane;
	
	/**
	 * Information for players. Contains information about how many improved a
	 * player has left, which plagues and which resources a player has.
	 */
	private final GridPane playerGrid;

	/**
	 * Creates PlayerPanels for every player and creates piles.
	 * 
	 * @param cardModel
	 *            Cards of the game.
	 * @param gameModel
	 *            GameModel that contains all players.
	 * @param dragContainer
	 *            Make objects dragable.
	 * @param controller
	 *            Controller handling events on card box
	 */
	public CardBoxView(CardModelI cardModel, GameModelI gameModel, DragContainer dragContainer,
			CardInfoController controller) {

		gameModel.addObserverX(this);
		
		playerGrid = new GridPane();
		
		ColumnConstraints column;
		RowConstraints row;

		PlayerPanel p0 = new PlayerPanel(gameModel.getPlayer(0), dragContainer, controller, gameModel.getCurrentPlayerId());
		PlayerPanel p1 = new PlayerPanel(gameModel.getPlayer(1), dragContainer, controller, gameModel.getCurrentPlayerId());
		PlayerPanel p2 = null;
		PlayerPanel p3 = null;

		p0.prefWidthProperty().bind(this.widthProperty().multiply(0.425));
		p0.setPrefHeight(this.getHeight() * 0.4925);
		p1.prefWidthProperty().bind(this.widthProperty().multiply(0.425));
		p1.setPrefHeight(this.getHeight() * 0.4925);

		gameModel.addObserverX(p0);
		gameModel.getPlayer(0).addObserverX(p0);
		gameModel.addObserverX(p1);
		gameModel.getPlayer(1).addObserverX(p1);

		column = new ColumnConstraints();
		column.setPercentWidth(50);
		playerGrid.getColumnConstraints().add(column);
		column = new ColumnConstraints();
		column.setPercentWidth(50);
		playerGrid.getColumnConstraints().add(column);

		playerGrid.add(p0, 0, 0);
		playerGrid.add(p1, 1, 0);
		controller.connectPanel(p0);
		controller.connectPanel(p1);

		if (gameModel.getNPlayers(true)> 2) {
			row = new RowConstraints();
			row.setPercentHeight(50);
			playerGrid.getRowConstraints().add(row);
			row = new RowConstraints();
			row.setPercentHeight(50);
			playerGrid.getRowConstraints().add(row);
			p2 = new PlayerPanel(gameModel.getPlayer(2), dragContainer, controller, gameModel.getCurrentPlayerId());

			playerGrid.add(p2, 0, 1);
			p2.prefWidthProperty().bind(this.widthProperty().multiply(0.425));
			p2.setPrefHeight(this.getHeight() * 0.4925);
			controller.connectPanel(p2);
			gameModel.addObserverX(p2);
			gameModel.getPlayer(2).addObserverX(p2);
		}
		if (gameModel.getNPlayers(true) > 3) {
			p3 = new PlayerPanel(gameModel.getPlayer(3), dragContainer, controller, gameModel.getCurrentPlayerId());
			playerGrid.add(p3, 1, 1);
			p3.prefWidthProperty().bind(this.widthProperty().multiply(0.425));
			p3.setPrefHeight(this.getHeight() * 0.4925);
			controller.connectPanel(p3);
			gameModel.addObserverX(p3);
			gameModel.getPlayer(3).addObserverX(p3);
		}
		
		if(gameModel.hasUntoaster()){
			untoasterPane = new StackPane();
			Label name = new Label(gameModel.getPlayer(-1).getName());
			name.setTextFill(Color.RED);
			name.setStyle("-fx-font-size: 24");
			
			Label role = new Label("Untoaster");
			role.setTextFill(Color.RED);
			VBox textBox = new VBox(name, role);
			untoasterPane.getChildren().add(textBox);

			textBox.setAlignment(Pos.CENTER);
			CardBoxView.setHgrow(untoasterPane, Priority.ALWAYS);
		} else {
			untoasterPane = null;
		}

		undeadPile = new UndeadPile(main.Constants.CARDBOX_CARD_WIDTH, main.Constants.CARDBOX_CONTENT_HEIGHT);
		playerPile = new PlayerPile(main.Constants.CARDBOX_CARD_WIDTH, main.Constants.CARDBOX_CONTENT_HEIGHT);
		
		cardModel.addObserverX(undeadPile);
		cardModel.addObserverX(playerPile);
		
		controller.connectPlayerPile(playerPile);
		controller.connectUndeadPile(undeadPile);

		undeadPile.setLabel(cardModel.getUndeadPile().size());
		playerPile.setLabel(cardModel.getPlayerPile().size());
		
		HBox deckBox = new HBox(playerPile, undeadPile);
		deckBox.setAlignment(Pos.BOTTOM_RIGHT);
		deckBox.setSpacing(4);

		deckBox.prefWidthProperty().bind((this.widthProperty().subtract(10)).multiply(0.15));
		deckBox.setMaxHeight(main.Constants.DECKBOX_HEIGHT);
		deckBox.maxWidthProperty().bind(deckBox.heightProperty().divide(3.0).multiply(2 * 2));

		ActionBar actions = new ActionBar(gameModel.getCurrentPlayer());
		gameModel.addObserverX(actions);
		actions.prefWidthProperty().bind(deckBox.widthProperty());
		actions.setPrefHeight(main.Constants.CARDBOX_HEIGHT - deckBox.getPrefHeight());
		actions.prefHeightProperty().bind(deckBox.heightProperty().subtract(main.Constants.CARDBOX_HEIGHT).multiply(-0.8));
		
		

		VBox rightBox = new VBox(actions, deckBox);
		rightBox.setAlignment(Pos.BOTTOM_RIGHT);
		rightBox.setSpacing(4);

		this.getChildren().addAll(playerGrid, rightBox);
		CardBoxView.setHgrow(playerGrid, Priority.ALWAYS);

	}
	
	/**
	 * Switches View if untoaster's turn starts or ends.
	 * @param currentPlayerId
	 */
	private void switchUntoaster(int currentPlayerId) {
		if(currentPlayerId == -1) {
			this.getChildren().remove(playerGrid);
			this.getChildren().add(untoasterPane);
			untoasterPane.toBack();
		} else if(this.getChildren().contains(untoasterPane)) {
			this.getChildren().remove(untoasterPane);
			this.getChildren().add(playerGrid);
			playerGrid.toBack();
		}
	}

	/**
	 * updates observers if its untoasters turn
	 */
	@Override
	public void update(Observable model, Object msg) {
		if(msg.toString().equals("nxtPl")){
			switchUntoaster(((GameModelI)model).getCurrentPlayerId());
		}
		
	}

}
