package game.cardinfo;

import java.util.Observable;
import java.util.concurrent.ThreadLocalRandom;

import cards.CardModelI;
import javafx.scene.image.ImageView;
import utils.ImageLoader;

/**
 * Player pile with player cards. In player card pile are planet cards, mass
 * attack cards and action cards.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class PlayerPile extends Pile {

	/**
	 * Create pile with given width and height.
	 * @param width Width of pile.
	 * @param height Height of pile.
	 */
	PlayerPile(double width, double height) {
		super(width, height);
	}

	/**
	 * Loads images of four planets and creates an image view for each color.
	 */
	@Override
	protected void makeColors() {
		yellow = new ImageView(ImageLoader.getInstance().get("/planets/planet0"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png"));
		green = new ImageView(ImageLoader.getInstance().get("/planets/planet1"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png"));
		blue = new ImageView(ImageLoader.getInstance().get("/planets/planet2"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png"));
		red = new ImageView(ImageLoader.getInstance().get("/planets/planet3"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png"));

	}

	/**
	 * Update pile, turn card or remove card.
	 */
	@Override
	public void update(Observable model, Object msg) {
		switch (msg.toString().charAt(0)) {
		case 'p':
			setLabel(((CardModelI) model).getPlayerPile().size() - 1);
			return;
		case 't':
			turnCard(((CardModelI) model).getTopPlayerCard());
			return;
		}
	}
}
