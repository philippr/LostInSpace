
package game.cardinfo;

import java.io.File;
import java.util.Optional;

import cards.Card;
import game.DragContainer;
import game.GameControllerI;
import game.GameView;
import game.board.BaseView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import language.Language;
import start.MenuController;

/**
 * Handels events on cardbox or infobox.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class CardInfoController implements EventHandler<ActionEvent> {

	/**
	 * GameController handels events on gameView.
	 */
	private final GameControllerI gameController;

	/**
	 * GameView shows all parts of the game.
	 */
	private final GameView gameView;

	/**
	 * Container for draged objects.
	 */
	private final DragContainer draged;

	/**
	 * Mouse x.
	 */
	private double mousex;

	/**
	 * Mouse y.
	 */
	private double mousey;

	/**
	 * Infoview contains buttons to develop weapons, buy improved weapons and
	 * build a base.
	 */
	private final InfoView infoView;

	/**
	 * Instantiates attributes, sets buttons on actions.
	 * 
	 * @param gameView
	 *            View showing all parts of the game.
	 * @param draged
	 *            DragContainer.
	 */
	public CardInfoController(GameView gameView, DragContainer draged) {
		this.gameView = gameView;
		this.draged = draged;
		this.gameController = gameView.getController();

		infoView = gameView.getInfoBox();
		infoView.getiWeaponsBtn().setOnAction(this);
		infoView.getDogBtn().setOnAction(this);
		infoView.getFireBtn().setOnAction(this);
		infoView.getSchnetlerBtn().setOnAction(this);
		infoView.getVacuumBtn().setOnAction(this);
		infoView.getQuitBtn().setOnAction(this);

		infoView.getBaseButton().setOnDragDetected(event -> {
			infoView.getBaseButton().startFullDrag();
			event.consume();
		});

		infoView.getBaseButton().setOnMousePressed(event -> {
			// just listen to the primary button
			if (event.getButton() != MouseButton.PRIMARY) {
				return;
			}

			BaseView b = new BaseView();
			b.setPrefSize(main.Constants.INFOBOX_WIDTH / 3 - 15, main.Constants.INFOBOX_WIDTH / 3 - 15);
			draged.set((Pane) b, "base", (Pane) infoView.getBaseButton().getParent(), -1);

			draged.getParentPane().getChildren().remove(draged.getDragPane());
			gameView.getMovePane().getChildren().add(draged.getDragPane());

			draged.getDragPane().setLayoutX(event.getSceneX());
			draged.getDragPane().setLayoutY(event.getSceneY());
		});

		infoView.getBaseButton().setOnMouseDragged(event -> {
			draged.getDragPane().setLayoutX(event.getSceneX());
			draged.getDragPane().setLayoutY(event.getSceneY());
		});

		infoView.getBaseButton().setOnMouseReleased(event -> {
			gameView.getMovePane().getChildren().clear();
			draged.clear();
			event.consume();
		});

		this.gameView.setOnMouseDragReleased(event -> {
			if (draged.getMsg() == "card") {
				event.consume();
				((PlayerPanel) draged.getParentPane()).moveCard(draged.getDragPane());
				draged.clear();
			}
			if (draged.getMsg() == "base") {
				event.consume();
				gameView.getMovePane().getChildren().clear();
				draged.clear();
			}
		});

		this.gameView.getMovePane().setOnMouseClicked(event -> {
			event.consume();

		});

		this.gameView.getMovePane().setOnMouseReleased(event -> {
			if (draged.getMsg() == "card") {
				((PlayerPanel) draged.getParentPane()).moveCard(draged.getDragPane());
				event.consume();

			}
			draged.clear();

		});
		this.gameView.getMovePane().setOnMouseExited(event -> {
			event.consume();

		});

	}

	/**
	 * Connects cards to activate actions with cards
	 * 
	 * @param cardPane
	 *            Pane displaying card.
	 * @param card
	 *            Card which is used
	 * @param playerPanel
	 *            player's panel with cards
	 */
	public void ConnectCard(Pane cardPane, Card card, PlayerPanel playerPanel) {

		cardPane.setOnDragDetected(event -> {
			cardPane.startFullDrag();
			event.consume();
		});

		cardPane.setOnMouseClicked(event -> {
			// just listen to the primary button
			if (event.getButton() != MouseButton.PRIMARY) {
				return;
			}
			playerPanel.moveCard(cardPane);
			event.consume();

		});

		cardPane.setOnMousePressed(event -> {

			// just listen to the primary button
			if (event.getButton() != MouseButton.PRIMARY) {
				return;
			}

			Bounds before = cardPane.localToScene(cardPane.getBoundsInLocal());

			draged.set(cardPane, "card", playerPanel, -1);
			draged.setBoxedObj(card);
			draged.setParentId(playerPanel.getPlayerId());

			mousex = event.getScreenX();
			mousey = event.getScreenY();

			draged.getParentPane().getChildren().remove(draged.getDragPane());
			gameView.getMovePane().getChildren().add(draged.getDragPane());

			cardPane.setLayoutX(before.getMinX());
			cardPane.setLayoutY(before.getMinY());

		});

		cardPane.setOnMouseDragged(event -> {
			cardPane.setTranslateX(cardPane.getTranslateX() + event.getScreenX() - mousex);
			cardPane.setTranslateY(cardPane.getTranslateY() + event.getScreenY() - mousey);
			mousex = event.getScreenX();
			mousey = event.getScreenY();
		});

	}

	/**
	 * Connects plague to activate actions with plagues.
	 * 
	 * @param plague
	 *            image with plague
	 * @param colorId
	 *            Color of plague
	 */
	public void connectPlague(ImageView plague, int colorId) {
		plague.setOnMouseClicked(event -> {
			gameController.healPlague(colorId);
		});
	}

	public void connectPanel(PlayerPanel target) {
		target.setOnMouseClicked(event -> {
			event.consume();

		});
		target.setOnMouseReleased(event -> {
			event.consume();

		});

		target.setOnMouseDragReleased(event -> {
			// just listen to cards
			if (draged.getMsg() != "card") {
				return;
			}

			if (gameController.shareKnowledge((Card) draged.getBoxedObj(), draged.getParentId(),
					target.getPlayerId())) {
				gameView.getMovePane().getChildren().remove(draged.getDragPane());
			} else {
				((PlayerPanel) draged.getParentPane()).moveCard(draged.getDragPane());
			}
			draged.clear();
		});
	}

	/**
	 * Connects resource with healer.
	 * 
	 * @param imageView
	 *            view with a base
	 * @param i
	 *            type of healer that gets recruited
	 * @param playerPanel
	 *            player's panel with all cards
	 */
	public void ConnectResource(ImageView imageView, int i, PlayerPanel playerPanel) {

		imageView.setOnMouseClicked(event -> {
			gameController.recruiteHealer(i);

		});
	}

	/**
	 * Connect player pile and activate actions for it.
	 * 
	 * @param pile
	 *            Pile with player cards
	 */
	public void connectPlayerPile(Pile pile) {
		pile.getUnturned().setOnMouseClicked(event -> {
			event.consume();
			gameController.flipPlayerCard();
		});

		pile.setOnMouseClicked(event -> {
			event.consume();
			gameController.takePlayerCard();
		});

	}

	/**
	 * Connect undead card pile and activates actions for it.
	 * 
	 * @param pile
	 */
	public void connectUndeadPile(Pile pile) {
		pile.getUnturned().setOnMouseClicked(event -> {
			event.consume();
			gameController.flipUndeadCard();
		});

		pile.setOnMouseClicked(event -> {
			event.consume();
			gameController.takeUndeadCard();
		});

	}

	/**
	 * Handels action events in the infoview. Handels actions for clicked
	 * buttons in the infobox.
	 */
	public void handle(ActionEvent event) {

		if (event.getSource() == infoView.getiWeaponsBtn()) {
			gameController.buyImprovedWeapon();
		}
		if (event.getSource() == infoView.getDogBtn()) {
			gameController.developWeapon(0);
		}
		if (event.getSource() == infoView.getFireBtn()) {
			gameController.developWeapon(2);
		}
		if (event.getSource() == infoView.getSchnetlerBtn()) {
			gameController.developWeapon(1);
		}
		if (event.getSource() == infoView.getVacuumBtn()) {
			gameController.developWeapon(3);
		}
		if (event.getSource() == infoView.getQuitBtn()) {

			Dialog<ButtonType> dialog = new Dialog<ButtonType>();
			dialog.setTitle(Language.getInstance().translateText("DITIT"));
			dialog.setContentText(Language.getInstance().translateText("DITXT"));

			ButtonType yesButtonType = new ButtonType(Language.getInstance().translateText("DIYES"), ButtonData.YES);
			ButtonType noButtonType = new ButtonType(Language.getInstance().translateText("DINO"),
					ButtonData.BACK_PREVIOUS);
			dialog.getDialogPane().getButtonTypes().addAll(yesButtonType, noButtonType);

			Optional<ButtonType> result = dialog.showAndWait();

			if (result.get() == yesButtonType) {

				// saveGame and change to main menu
				FileChooser fileChooser = new FileChooser();
				fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("USA-save (*.ser)", "*.ser"));
				
				// Dialogs are bugged in our Java-Version.
				// After binding the the the Dialog to our Stage, after closing, 
				// It will be impossible to resize.
				File file = fileChooser.showSaveDialog(null);
			

				// if the file is valid
				if (file != null) {
					String path;
					// check if .ser ending, add if missing
					if (file.getPath().endsWith(".ser")) {
						path = file.getPath();
					} else {
						path = file.getPath() + ".ser";
					}

					// save to file
					gameController.saveGame(path);
					MenuController.switchContent();
				}

			} else if (result.get() == noButtonType) {
				// change to Main Menu
				MenuController.switchContent();
			} // else closed with X -> do nothing
		}
	}
}
