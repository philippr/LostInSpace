package game.cardinfo;

import java.util.Observable;
import java.util.Observer;

import game.GameModel;
import game.GameModelI;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import player.PlayerI;

/**
 * 
 * ActionBar shows how many actions a player has left. Only left actions of
 * current player are shown. A player can recognize his or her actions from the
 * color the left actions at the action bar.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class ActionBar extends StackPane implements Observer {

	/**
	 * Circles which are displaying the left actions. Four circles are shown if
	 * it is a normal player's turn. Five circles for lieutenant.
	 */
	private final Ellipse[] circs;

	/**
	 * Box to arrange the circles.
	 */
	private HBox layout;

	/**
	 * Creates circles.
	 * 
	 * @param startPlayer
	 */
	public ActionBar(PlayerI startPlayer){

		layout = new HBox();
		layout.setAlignment(Pos.CENTER);

		circs = new Ellipse[5];
		for (int i = 0; i < 5; i++) {
			circs[i] = new Ellipse();
			circs[i].radiusXProperty().bind(this.prefWidthProperty().divide(14));
			circs[i].radiusYProperty().bind(circs[i].radiusXProperty());
			circs[i].setStroke(Color.BLACK);
			circs[i].setStrokeWidth(2);
			layout.getChildren().add(circs[i]);
		}

		layout.spacingProperty()
				.bind(this.prefWidthProperty().subtract(circs[0].radiusXProperty().multiply(10)).divide(5));
		this.getChildren().add(layout);

		reset(startPlayer);
	}

	/**
	 * Updates action bar, either fills an ellipse black (player has made an action) or changes
	 * color to next player's colors (if it is next players turn).
	 */
	@Override
	public void update(Observable model, Object msg) {
		switch (msg.toString().substring(0, 5)) {
		case "decLA":
			decrement(((GameModel) model).getCurrentPlayer().getLeftActions());
			return;
		case "nxtPl":
			reset(((GameModelI) model).getCurrentPlayer());
			return;
		}
	}

	/**
	 * If an action is made left actions are decreased. This is displayed by
	 * changing the color of an ellipse from the players color to black.
	 * 
	 * @param nActions
	 */
	private void decrement(int nActions) {
		if (nActions < circs.length) {
			circs[nActions].setFill(Color.BLACK);
		}
	}

	/**
	 * Shows as many ellipses as player has actions. Fills ellipses with color
	 * of current player.
	 * 
	 * @param player
	 */
	private void reset(PlayerI player) {

		setVisible(player.getRole().hasNActions());

		layout.getChildren().remove(circs[4]);
		
		for(Ellipse e: circs){
			e.setFill(main.Constants.PLAYER_COLORS[player.getColorId()]);
		}
		if (player.getRole().getnActions() == 5) {
			layout.getChildren().add(circs[4]);
		}

	}
}
