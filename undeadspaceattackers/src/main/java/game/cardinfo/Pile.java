package game.cardinfo;

import java.util.Observer;

import cards.Card;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * Represents a pile of cards.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
abstract class Pile extends Pane implements Observer {

	/**
	 * Red image that is either a ghost or a red planet.
	 */
	protected ImageView red;

	/**
	 * Blue image that is either a mummy or a blue planet.
	 */
	protected ImageView blue;

	/**
	 * Green image that is either a zombie or a green planet.
	 */
	protected ImageView green;

	/**
	 * Yellow image that is either a skeleton or a yellow planet.
	 */
	protected ImageView yellow;

	/**
	 * Pane that shows an unturned card. Pane displays number of left cards and
	 * symbols to identify the pile (undeads or planets).
	 */
	private final Pane unturned;

	/**
	 * Turned pane shows a turned card, it represents the card on top of the
	 * pile and a player can see which content the top card has.
	 */
	private Pane turned;

	/**
	 * Counter for left cards in the pile.
	 */
	protected final Label count;

	/**
	 * Creates pile view with top card which shows content if turned and size of
	 * pile if unturned.
	 * 
	 * @param width
	 *            width of pile
	 * @param height
	 *            height of pile
	 */
	protected Pile(double width, double height) {
		unturned = new Pane();
		unturned.setId("cardLargeBack");

		setPrefSize(width, height);
		DoubleProperty fontSize;
		fontSize = new SimpleDoubleProperty(10);

		makeColors();

		yellow.fitWidthProperty().bind(this.widthProperty().multiply(0.4));
		yellow.setPreserveRatio(true);
		green.fitWidthProperty().bind(this.widthProperty().multiply(0.4));
		green.setPreserveRatio(true);
		blue.fitWidthProperty().bind(this.widthProperty().multiply(0.4));
		blue.setPreserveRatio(true);
		red.fitWidthProperty().bind(this.widthProperty().multiply(0.4));
		red.setPreserveRatio(true);

		yellow.setX(2);
		yellow.setY(5);
		green.xProperty().bind(this.widthProperty().multiply(0.6).subtract(5));
		green.setY(5);
		blue.setX(2);
		blue.yProperty().bind((this.heightProperty().subtract(this.widthProperty().multiply(0.4))).subtract(5));
		red.xProperty().bind(this.widthProperty().multiply(0.6).subtract(5));
		red.yProperty().bind((this.heightProperty().subtract(this.widthProperty().multiply(0.4))).subtract(5));

		count = new Label("42");

		count.layoutXProperty().bind((this.widthProperty().subtract(count.widthProperty())).divide(2));
		count.layoutYProperty().bind((this.heightProperty().subtract(count.heightProperty())).divide(2));

		fontSize.bind(this.heightProperty().divide(6));
		count.styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSize.asString(), ";"));

		unturned.getChildren().addAll(yellow, green, blue, red, count);
		this.getChildren().add(unturned);
	}

	/**
	 * Loads images to identify the pile (undeads or planets).
	 */
	protected abstract void makeColors();

	/**
	 * Turn the card on top of the pile.
	 * 
	 * @param card
	 *            Card that is turned.
	 */
	public void turnCard(Card card) {
		if (card == null) {
			this.getChildren().remove(turned);
		} else {
			turned = card.toPane();
			turned.setPrefWidth(unturned.getWidth());
			turned.setPrefHeight(unturned.getHeight());
			turned.setId("cardLargeFront");
			this.getChildren().add(turned);
		}

	}

	/**
	 * Get top card on pile that is unturned.
	 * 
	 * @return Unturned card on pile.
	 */
	public Pane getUnturned() {
		return unturned;
	}

	/**
	 * Setter for label which displays the size of the pile.
	 * 
	 * @param size
	 *            Size of pile.
	 */
	public void setLabel(int size) {
		count.setText(Integer.toString(size));
	}

}
