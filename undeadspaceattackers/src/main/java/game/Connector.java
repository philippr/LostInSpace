package game;

import game.board.PlanetView;
import game.board.UndeadGrid;
import game.board.UndeadView;
import javafx.scene.image.ImageView;
import utils.RemovingLine;

public interface Connector {
	public void connectResource(ImageView resource, int color, int planetId);
	public void connectDarkColumn(ImageView darkColumn, int planetId);
	public void connectUndead(UndeadView iV, int planetId, int colorId, UndeadGrid parent);
	public void connectPlanet(PlanetView planetView);
	public void connectLine(RemovingLine line);
}
