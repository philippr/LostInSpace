package game;

import java.util.ArrayList;
import java.util.Observer;

import cards.Card;
import game.board.PlanetModelI;
import player.PlayerI;

public interface GameModelI {

	/**
	 * Getter for current player's playerID.
	 * 
	 * @return currentPlayerId
	 */
	int getCurrentPlayerId();

	/**
	 * Setter for current player.
	 * 
	 * @param id
	 *            ID of Player.
	 */
	void setCurrentPlayer(int id);

	/**
	 * Sets next player.
	 */
	void nextPlayer();

	/**
	 * Removes the current player and sets the next current player.
	 * 
	 * @param playerId
	 *            ID of Player that is killed.
	 */
	void removePlayer(int playerId);

	/**
	 * Getter for current player.
	 * 
	 * @return Current player.
	 */
	PlayerI getCurrentPlayer();

	/**
	 * Getter for a certain player.
	 * 
	 * @param playerId
	 *            ID of Player that is wanted.
	 * @return Player with corresponding player ID.
	 */
	PlayerI getPlayer(int playerId);

	/**
	 * Getter for next player.
	 * 
	 * @return Player who's turn is next.
	 */
	PlayerI getNextPlayer();

	/**
	 * Getter for last used card(s).
	 * 
	 * @return List of used cards.
	 */
	ArrayList<Card> getLastUsedCard();

	/**
	 * Adds a card to last used cards.
	 * 
	 * @param card
	 *            Card that is thrown away by a player.
	 */
	void addLastCard(Card card);

	/**
	 * If player stops an action not using his/her card the player will get his
	 * cards back.
	 * 
	 * @param id
	 *            ID of Card that is given back to the player.
	 */
	void removeLastCard(int id);

	/**
	 * Removes all last used cards from list.
	 */
	void clearLastUsedCards();

	/**
	 * Add an Observer.
	 * 
	 * @param Observer
	 */
	void addObserverX(Observer Observer);

	/**
	 * Getter for boolean untoaster.
	 * 
	 * @return True if untoaster is activated.
	 */
	boolean hasUntoaster();

	/**
	 * Notifies Observers.
	 */
	void notifyEndAction();

	/**
	 * Removes a random player from a planet if an abomination gets on a planet
	 * with players.
	 * 
	 * @param planet
	 *            Planet where abomination is.
	 */
	public void removeRandomPlayer(PlanetModelI planet);

	/**
	 * Returns the number of living players, in-/ or excluding the untoaster.
	 * 
	 * @param excludeUntoaster
	 *            True to exclude the untoaster, false to include.
	 * @return The number of players living.
	 */
	int getNPlayers(boolean excludeUntoaster);
}