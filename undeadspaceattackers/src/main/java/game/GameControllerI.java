package game;

import cards.Card;
import cards.CardControllerI;
import game.board.BoardControllerI;
import game.board.UndeadView;

/**
 * Main controller for the game logic.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public interface GameControllerI {

	/**
	 * Getter for the CardControler.
	 * 
	 * @return the CardController
	 */
	CardControllerI getCardController();

	/**
	 * Setter for CardController.
	 * 
	 * @param cardController
	 */
	void setCardController(CardControllerI cardController);

	/**
	 * Setter for BoardController.
	 * @param boardControllerI
	 */
	void setBoardController(BoardControllerI boardControllerI);

	/**
	 * Initiates Game. Sets initial undead on planets. Sets players on start
	 * planet. Check which features are activated and sets them on planets.
	 */
	void initiateGame();

	// ------------------------------------------------CARDS----------------------------------------------------------------//

	/**
	 * Checks if a card might be thrown and adds it to lastUsedCard.
	 * 
	 * @param card
	 *            Card which is thrown.
	 * @param playerId
	 *            Player who throws the card.
	 * @return true when its the CurrentPlayer or an ActionCard.
	 */
	boolean cardThrown(Card card, int playerId);

	/**
	 * Checks for the different Uses of PlayerCardPile. 1. Player has left
	 * Actions and card is thrown. Method returns all cards from lastUsedCard to
	 * currentPlayer. 2. Player has left Actions and no cards are thrown. An
	 * action is skipped. 3.Player has no left actions. Top player card is
	 * flipped.
	 * 
	 */
	void flipPlayerCard();

	/**
	 * When a card was flipped, it can be added to playerhand. Handles mass
	 * attack card. Ends Game if no playerCards are left (card is null).
	 */
	void takePlayerCard();

	/**
	 * After taking 2 player cards, an undead card can be flipped.
	 */
	void flipUndeadCard();

	/**
	 * When undeadcard is flipped, it can be taken and undead will be added
	 */
	void takeUndeadCard();

	/**
	 * Is called if a undead mass attack has been taken. Increases undead
	 * strength marker / undeadtrength. Adds 3 undead to bottom card. Adds
	 * undead to all cards from discard pile. Mix discard pile and put on top of
	 * undead pile.
	 */
	void undeadMassAttack();

	/**
	 * Ends game and changes GameView to EndView.
	 * 
	 * @param isWon
	 */
	void endGame(boolean isWon);

	/**
	 * EndTurn is called if there are no more left actions or the player presses
	 * on the player card pile.
	 */
	void endTurn();

	/**
	 * Exorcist is called at the end of an action. If the player's role is
	 * exorcist undead are treated diffrent if a weapon is developed. If the
	 * exorcist is located on a planet with undead with a color of the
	 * corresponding developed weapon, the undead of that colour will be killed
	 * without loosing an action.
	 */
	public void exorcistKills(int playerId);


	/**
	 * @param decreaseAction
	 *            set True if you want the player to lose a leftAction
	 */
	void endAction( boolean decreaseAction);

	/**
	 * Checks whethe the current player is allowed to drag undead. Only
	 * untoaster may drag undead.
	 * 
	 * @return True if current player may drag undead.
	 */
	boolean mayDragUndead();


	/**
	 * Adds Undeads <br>
	 * handles planet conquers <br>
	 * checks for Abomination and Plagues <br>
	 * 
	 * @param planetId
	 *            ID of the Planet to add n undead.
	 * @param colorId
	 *            Color of the Undead species.
	 */
	void addUndead(int planetId, int colorId, boolean silent);

	/**
	 * Builds a base on the given planet if the action is valid.
	 * 
	 * @param planetId
	 *            Planet where a base wanted to be build.
	 * @return True if action was valid.
	 */

	boolean buildBase(int planetId);

	/**
	 * Moves a spaceship and checks if it's valid.
	 * 
	 * @param actionPlayerId
	 *            The spaceship which has been moved.
	 * @param startPlanetId
	 *            PlanetId where the spaceship started.
	 * @param endPlanetId
	 *            PlanetId where the spaceship ended.
	 * @return True if action was valid.
	 */
	boolean moveSpaceship(int actionPlayerId, int startPlanetId, int endPlanetId);

	/**
	 * Shares a card between two players. CrazyWeaponArchitect can share every
	 * card when involved.
	 * 
	 * @param card
	 *            Card that is shared.
	 * @param givePlayerId
	 *            Player who gives the card to another.
	 * @param getPlayerId
	 *            Player the card is given to.
	 * @return True if action is valid.
	 */
	boolean shareKnowledge(Card card, int givePlayerId, int getPlayerId);

	/**
	 * Kills an undead and validates the action. If a wespon is developed all
	 * undead of a given color are killed. If no undead of that color are left
	 * on planets and weapon is developed the species is extinct.
	 * 
	 * Exorcist always kills every undead of the given color on a planet.
	 * Improved weapons kill two undead. Normal players kill one undead in an
	 * action.
	 * 
	 * @param planetId
	 *            Planet where undead is/are killed.
	 * @param undeadColor
	 *            Color of killed undead.
	 * @return True if action was valid.
	 */
	boolean killUndead(int planetId, int undeadColor);

	/**
	 * Removes dark column from planet if its valid. Throws Exception: GCTDK:
	 * GameController DarkColumns not enabled.
	 * 
	 * @param planetId
	 *            Planet where dark column is removed.
	 * @return True if action was valid.
	 */
	boolean removeDarkColumn(int planetId);

	/**
	 * Develops a weapon if it's valid . Ends Game if all Weapons are developed.
	 * Throws Exception: GCTWD: GameController Weapon already developed
	 * 
	 * @param weaponColor
	 *            Color of developed weapon.
	 * @return True if action was valid.
	 */
	boolean developWeapon(int weaponColor);

	/**
	 * Buys an improved weapon for the current player. Costs no action.
	 * 
	 * @return True if action was valid.
	 */
	boolean buyImprovedWeapon();

	/**
	 * Gives the player a resource if it's valid.
	 * 
	 * @param colorId
	 *            Color of resource.
	 * 
	 * @param planetId
	 *            Planet where the resource is mined.
	 * @return True if action was valid.
	 */
	boolean mineResource(int colorId, int planetId);

	/**
	 * Recruits a Healer against the speciesColor.
	 * 
	 * @param colorId
	 *            Species' color.
	 * 
	 * @return True if actions was valid.
	 */
	boolean recruiteHealer(int colorId);

	/**
	 * Heals a player from the plague.
	 * 
	 * @param colorId
	 *            Color of plague.
	 * @return True if action was valid.
	 */
	boolean healPlague(int colorId);

	/**
	 * Moves undead of colorId from StartPlanet to EndPlanet.
	 * 
	 * @param colorId
	 * @param sPlanetId
	 * @param ePlanetId
	 * @return true if action was valid
	 */
	boolean moveUndead(int colorId, int sPlanetId, int ePlanetId);

	/**
	 * Save the current Game in given path.
	 * 
	 * @param path
	 *            Path where game is saved. Must end with .ser .
	 */
	void saveGame(String path);

	/**
	 * Handles exceptions calls view.showMessage
	 * 
	 * @param e
	 *            Exception
	 */
	void handleException(Exception e);

	/**
	 * Connect the undead view to the model to get a reset notification at the end of a turn.
	 * 
	 * @param uV the undead view
	 */
	void connectUndead(UndeadView uV);

}