package game;

import java.util.ArrayList;
import java.util.Observer;

public interface GameStatusModelI  {

	ArrayList<Integer> getConqueredPlanets();

	void addConqueredPlanets(int conqueredPlanet);

	public void decreaseLeftConqueredPlanets();

	
	void increaseUndeadStrengthMarker();



	int getNumbers(int index);
	void setNumbers(int value, int index);


	boolean[] getDevelopedWeapons();

	void developWeapon(int weapon);

	boolean[] getExtinctSpecies();

	void setExtinctSpecies(int extinctSpecies);

	int[] getBlockedPlanets();

	void setBlockedPlanets(int[] blockedPlanets);

	ArrayList<Integer> getDarkColumnPlanets();

	void setDarkColumnPlanets(ArrayList<Integer> darkColumnPlanets);
	
	void removeDarkColumnPlanet(Integer planetId);

	void inceaseUndeadStrength();
	
	public void addObserverX(Observer infoView);
	
	public int[] getLeftUndead();

	void decreaseLeftUndead(int colorId);
	void increaseLeftUndead(int colorId); 
	void incPlayerCardCounter();
	void incUndeadCardCounter();
	void resetCardCounters();
	
	public boolean getFlag(int index);
	public void setFlag(boolean value, int index);
	
}