package language;

import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;


/**
 * TrButton is a button that is able to display texts in two languages (English and German). 
 * @author Philipp, Fabian, Julia, Ronja
 */
class TrButton extends Button{
	
	/**
	 * Array of texts in both languages.
	 */
	private final String[] texts;
	
	/**
	 * Sets texts with given Strings. Adds a listener (needed if language is changed during the game).
	 * @param strings
	 * 	String with texts in English and German.
	 * @param language Instance of Language.
	 */
	public TrButton(String[] strings, IntegerProperty language){
		super(strings[language.get()]);
		texts = strings;
		language.addListener(this::changed);
	}
	
	/**
	 * Changes displayed texts if language is changed.
	 * @param observable Observed object.
	 * @param oldLanguage Language that has been selected before.
	 * @param newLanguage New language that is switched to.
	 */
	public void changed(ObservableValue<? extends Number> observable, Number oldLanguage, Number newLanguage) {
		setText(texts[newLanguage.intValue()]);
	}

}
