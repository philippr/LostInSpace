package language;

import java.util.HashMap;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Language represents the two languages (English and German) in the game.
 *
 */
public class Language {

	private static Language instance;
	private final HashMap<String, String[]> dictionary = new HashMap<String, String[]>();;
	private final IntegerProperty language = new SimpleIntegerProperty(0);

	public synchronized static Language getInstance() {
		if (instance == null) {
			instance = new Language();
			instance.loadDict();
		}
		return instance;
	}

	/**
	 * loads every entry 
	 */
	private void loadDict() {

		loadDictMainMenu();
		loadDictSelection();
		loadDictSettings();
		loadDictInfoView();
		loadDictEndView();
		loadDictExceptions();
		loadDictExceptionsSaveGame();
		loadDictGenerator();

		dictionary.put("", new String[] { "", "" });

	}

	/**
	 * loads entries that are needed in Generator
	 */
	private void loadDictGenerator() {
		dictionary.put("GENSV", new String[] { "Save", "Speichern" });
		dictionary.put("GENLD", new String[] { "Load", "Laden" });
	}

	/**
	 * loads entries that are needed in Main Menu
	 */
	public void loadDictMainMenu() {
		dictionary.put("USA", new String[] { "Undead Space Attakers : Make Space Great Again",
				"Undead Space Attakers : Make Space Great Again" });
		dictionary.put("SRT", new String[] { "Start Game", "Spiel Starten" });
		dictionary.put("LOD", new String[] { "Load Game", "Spiel Laden" });
		dictionary.put("MAG", new String[] { "Map-Generator", "Map-Generator" });
		dictionary.put("QUG", new String[] { "Quit", "Beenden" });
		dictionary.put("QUT", new String[] { "Quit", "Ende" });
		dictionary.put("GST", new String[] { "Game Settings", "Spieleinstellungen" });
		dictionary.put("GO", new String[] { "Go!", "Los!" });
	}

	/**
	 * loads entries that are needed in SelectionView
	 */
	public void loadDictSelection() {
		dictionary.put("PS", new String[] { "Player Selection", "Spielerwahl" });
		dictionary.put("PSY", new String[] { "Player Symbol", "Spielfigur" });
		dictionary.put("PR", new String[] { "Player Role", "Rolle" });
		dictionary.put("PN", new String[] { "Player Name", "Spielername" });
		dictionary.put("WM", new String[] { "Warpmaster", "Warpmeister" });
		dictionary.put("EX", new String[] { "Exorcist", "Exorzist" });
		dictionary.put("CK", new String[] { "Constructor", "Konstrukteur" });
		dictionary.put("LO", new String[] { "Lieutenant", "Oberleutnant" });
		dictionary.put("WT", new String[] { "Weapontreader", "Waffenh\u00E4ndler" });
		dictionary.put("WA", new String[] { "Weaponarchitect", "Waffenarchitekt" });
		dictionary.put("UN", new String[] { "Untoaster", "Untoaster" });
	}

	/**
	 * loads entries that are needed in SettingsView
	 */
	public void loadDictSettings() {
		dictionary.put("PL5", new String[] { "Play with untoaster", "mit Untoaster spielen" });
		dictionary.put("APC", new String[] { "allowed planetary conquests", "erlaubte Planeteneroberungen" });
		dictionary.put("ASU", new String[] { "attack strength of undead", "Angriffsst\u00E4rke der Untoten" });
		dictionary.put("UMA", new String[] { "undead mass attack cards", "Massenangriffskarten" });
		dictionary.put("AI", new String[] { "activate AI", "KI aktivieren" });
		dictionary.put("DC", new String[] { "activate dark columns", "dunkle S\u00E4ulen aktivieren" });
		dictionary.put("AAB", new String[] { "activate abomination", "Fleischkoloss aktivieren" });
		dictionary.put("AIW", new String[] { "activate improved weapons", "verbesserte Waffen aktivieren" });
		dictionary.put("PQ", new String[] { "activate plaque", "Heimsuchungen aktivieren" });
		dictionary.put("MN", new String[] { "Map-Name: ", "Karten Name" });
		dictionary.put("DS", new String[] { "default", "standard" });
		dictionary.put("LM", new String[] { "Load Custom Map", "Eigene Karte laden" });
	}

	/**
	 * loads entries that are needed in InfoView
	 */
	public void loadDictInfoView() {
		dictionary.put("ES", new String[] { "Extinct species", "Ausgerottete Spezien" });
		dictionary.put("NCP", new String[] { "allowed conquests: ", "erlaubte Eroberungen: " });
		dictionary.put("ASU", new String[] { "undead strength", "St\u00E4rke der Untoten" });
		dictionary.put("NRU", new String[] { "remaining undeads", "verbleibende Untote" });
		dictionary.put("GHO", new String[] { "ghosts", "Geister" });
		dictionary.put("MUM", new String[] { "mummies", "Mumien" });
		dictionary.put("SKE", new String[] { "skeletons", "Skelette" });
		dictionary.put("ZOM", new String[] { "zombies", "Zombies" });
		dictionary.put("MIB", new String[] { "Military base", "Milit\u00E4rbasis" });
		dictionary.put("BAI", new String[] { "base & \n improved Weapons", "Basis & \n verbesserte Waffen" });
		dictionary.put("IMW", new String[] { "Improved Weapons", "Verbesserte Waffen" });
		dictionary.put("WEA", new String[] { "Weapons", "Waffen" });
	}

	/**
	 * loads entries that are needed in EndView
	 */
	public void loadDictEndView() {
		dictionary.put("ENDLT",
				new String[] {
						"Oh no, you have lost! \n The Undead have conquered the " + "\n Universe. \n Untoaster has won.",
						"Oh nein, die Untoten haben das \n Universum erobert. \n Der Untoaster" + " hat gewonnen" });
		dictionary.put("ENDLF",
				new String[] { "Oh no, you have lost! \n The Undead have conquered the " + "\n Universe.",
						"Oh nein, die Untoten haben das \n Universum erobert." });
		dictionary.put("ENDW", new String[] { "You are the winners.\n You saved the Universe!",
				"Ihr seid die Gewinner!\n" + "Ihr habt das Universum gerettet!" });
		dictionary.put("MM", new String[] { "Main Menu", "Hauptmenu" });
	}
	
	/**
	 * loads entries that are needed in Exceptions
	 */
	public void loadDictExceptions() {
		//MAPLOADER
		dictionary.put("MAPFP", new String[] { "Wrong file path", "Falscher Dateipfad" });
		dictionary.put("MAPMD", new String[] { "Multiple planet id ", "Mehrfache Planeten id" });
		dictionary.put("MAPWC", new String[] { "Wrong planet color", "Falsche planeten Farbe" });
		dictionary.put("MAPXD", new String[] { "Wrong X dimension", "Falsche X Dimension" });
		dictionary.put("MAPYD", new String[] { "Wrong Y dimension", "Falsche Y Dimension" });
		dictionary.put("MAPMS", new String[] { "to many StartPlanets", "Zu viele Startplaneten" });
		dictionary.put("MAPNS", new String[] { "no StartPlanet", "kein Startplanet" });
		dictionary.put("MAPLP", new String[] { "not enough planets from each color", "nicht genug Planeten von jeder Farbe" });
		dictionary.put("MAPFT", new String[] { "one of the connected Planets does not exist", "Einer der verbundenen Planeten existiert nicht" });
		dictionary.put("MAPPI", new String[] { "parse Integer ERROR", "Fehler beim Integer Parsen" });
		dictionary.put("MAPIO", new String[] { "INPUT/OUTPUT ERROR", "Eingabe/Ausgabe Fehler" });
		dictionary.put("MAPPO", new String[] { "A position is assigned twice", "Eine Position ist durch Planeten mehrmals belegt" });
		dictionary.put("MAPFF", new String[] { "Fileformat corruptet", "Korrumpiertes Dateiformat" });
		dictionary.put("MAPNE", new String[] { "A refferensed planet does not exist", "Ein Planet der Untotenliste existiert nicht" });

		//MAINMENU
		dictionary.put("FILAB", new String[] { "Abort load game.", "Laden des Spiels abgebrochen." });
		
		//GAMESETTIGN
		dictionary.put("SELWL",new String[] { "Name doesn't fit 3 to 15 characters.", "Der Name muss 3 bis 15\n Zeichen lang sein." });
		dictionary.put("SELNU", new String[] { "Names must be different.", "Namen d\u00FCrfen nicht gleich sein." });
		dictionary.put("SELRO", new String[] { "Select a role once.", "W\u00E4hlt keine Rolle doppelt." });
		dictionary.put("SELNP", new String[] { "Select 2 to 4 roles.", "W\u00E4hlt 2 bis 4 Rollen." });
		dictionary.put("SELUN", new String[] { "Untoaster only with 4 Players", "Untoaster nur mit 4 Spielern." });
		
		//GAME
		dictionary.put("GCTPC", new String[] { "Not enough Player Cards taken.", "Noch nicht genug Player Karten gezogen." });
		dictionary.put("GCTUC", new String[] { "Not enough Undead Cards taken.", "Noch nicht genug Untoten Karten gezogen." });
		dictionary.put("GCTNC", new String[] { "To many handcards. Discard one of yours.", "Zu viele Handkarten. Wirf eine Ab." });
		dictionary.put("GCTAC", new String[] { "Actioncards cannot be taken back", "Aktionskarten k\u00D6nnen nicht zur\u00FCckgenommen werden."});
		dictionary.put("GCTDK", new String[] { "DarkColumns not activated", "DarkColumns sind nicht aktiviert" });
		dictionary.put("ROLLA", new String[] { "Not enough left actions or it's not your turn.", "Nicht genug Aktionen übrig oder du bist nicht am Zug." });
		dictionary.put("ROLNC", new String[] { "Wrong number of cards.", "Falsche Anzahl an Karten." });
		dictionary.put("ROLHB", new String[] { "Planet already has a base.", "Planet hat bereits eine Basis." });
		dictionary.put("ROLNB", new String[] { "Planet has no base.", "Planet hat keine Basis." });
		dictionary.put("ROLPL", new String[] { "Wrong planet!", "Falscher Planet!" });
		dictionary.put("ROLCT", new String[] { "Wrong cardtype.", "Falscher Kartentyp." });
		dictionary.put("ROLNP", new String[] { "Not a neighbour planet.", "Kein Nachbarplanet." });
		dictionary.put("ROLUC", new String[] { "No undead of this color.", "Kein Untoter dieser Farbe." });
		dictionary.put("ROLCC", new String[] { "Wrong card color.", "Falsche Kartenfarbe." });
		dictionary.put("ROLBL", new String[] { "Planet is blocked by the evil AI.", "Planet ist durch b\u00D6se KI blockiert." });
		dictionary.put("ROLRS", new String[] { "No resource on this planet.", "Keine Resorcea auf diesem Planeten." });
		dictionary.put("ROLHL", new String[] { "No Healer on this planet.", "Kein Heiler auf diesem Planeten." });
		dictionary.put("ROLTT", new String[] { "One of the trading players needs to be at turn.", "Einer der Handelnden muss am Zug sein." });
		dictionary.put("ROLTC", new String[] { "Cards were thrown: finish that action or \n take them back by pressing the player pile.", "Es wurden Karten abgewurfen: beende die Aktion oder\nclicke auf den Planetenkarten-Stapel, um deine Karten wieder zu nehmen." });
		dictionary.put("ROLHC", new String[] { "Too many handcards", "Zu viele Handkarten" });
		dictionary.put("GCTWD", new String[] { "Weapon already developed.", "Waffe wurde schon entwickelt." });

		dictionary.put("DIYES", new String[] { "Yes", "Ja" });
		dictionary.put("DINO", new String[] { "No", "Nein" });
		dictionary.put("DICAN", new String[] { "Cancel", "Abbrechen" });
		dictionary.put("DITXT", new String[] { "Would you like to save the game?", "M\u00F6chtst du das Spiel speichern?" });
		dictionary.put("DITIT", new String[] { "Quit Game", "Spiel Beenden" });
	}

	
	public void loadDictExceptionsSaveGame(){
		dictionary.put("SLGFP", new String[] { "Wrong FilePath.", "Falscher Pfad." });
		dictionary.put("SLGNL", new String[] { "Object is null.", "Objekt ist null." });
		dictionary.put("SLGCF", new String[] { "Corrupted File / StreamCoruppted.", "Beschaedigte Datei / Beschaedigter Stream" });
		dictionary.put("SLGNF", new String[] { "Class not found", "Klasse nicht gefunden." });
		dictionary.put("SLGIO", new String[] { "Input/Output", "Eingabe/Ausgabe Fehler" });
		dictionary.put("SLGEX", new String[] { "Unknown exception", "Unbekannter Fehler" });
	}
	

	public TrButton createButton(String lgID) {
		return new TrButton(dictionary.get(lgID), language);

	}

	public TrText createText(String lgID) {
		return new TrText(dictionary.get(lgID), language);

	}

	public void translate(int lang) {
		language.set(lang);

	}

	public String[] getText(String string) {
		return dictionary.get(string);
	}

	public String translateText(String string) {
		return dictionary.get(string)[language.intValue()];
	}

	public int getLanguage() {
		return language.intValue();
	}

}
