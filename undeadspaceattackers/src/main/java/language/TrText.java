package language;

import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.text.Text;

/**
 * TrText is able to display texts in two languages (English and German).
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class TrText extends Text {

	/**
	 * Array of texts in both languages.
	 */
	private String[] texts;

	/**
	 * Sets texts with given Strings. Adds a listener (needed if language is
	 * changed during the game).
	 * 
	 * @param strings
	 *            String with texts in English and German.
	 * @param language
	 *            Instance of Language.
	 */
	public TrText(String[] strings, IntegerProperty language) {
		super(strings[language.get()]);
		texts = strings;
		language.addListener(this::changed);
	}

	void translate(int lang) {
		setText(texts[lang]);
	}

	/**
	 * Setter for TrText.
	 * 
	 * @param text
	 *            Text
	 */
	public void setTrText(String text) {
		texts = Language.getInstance().getText(text);
		setText(texts[Language.getInstance().getLanguage()]);
	}

	/**
	 * Changes displayed texts if language is changed.
	 * 
	 * @param observable
	 *            Observed object.
	 * @param oldLanguage
	 *            Language that has been selected before.
	 * @param newLanguage
	 *            New language that is switched to.
	 */
	public void changed(ObservableValue<? extends Number> observable, Number oldLanguage, Number newLanguage) {
		setText(texts[newLanguage.intValue()]);
	}

}
