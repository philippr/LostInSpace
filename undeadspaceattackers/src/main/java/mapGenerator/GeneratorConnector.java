/**
 * 
 */
package mapGenerator;

import game.Connector;
import game.board.PlanetView;
import game.board.UndeadGrid;
import game.board.UndeadView;
import javafx.scene.image.ImageView;
import utils.RemovingLine;

/**
 * @author Administrator
 *
 */
public class GeneratorConnector implements Connector {

	private GeneratorController controller;
	
	GeneratorConnector(GeneratorController controller){
		this.controller = controller;
	}
	
	/* (non-Javadoc)
	 * @see board.Connector#connectDarkColumn(javafx.scene.image.ImageView, int)
	 */
	@Override
	public void connectDarkColumn(ImageView darkColumn, int planetId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see board.Connector#connectUndead(board.UndeadView, int, int, board.UndeadGrid)
	 */
	@Override
	public void connectUndead(UndeadView iV, int planetId, int colorId, UndeadGrid parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectResource(ImageView resource, int color, int planetId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectPlanet(PlanetView planetView) {
		controller.registerPlanet(planetView);
	}

	@Override
	public void connectLine(RemovingLine line) {
		controller.registerLine(line);
	}
}
