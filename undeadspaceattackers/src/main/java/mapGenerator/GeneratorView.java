package mapGenerator;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ThreadLocalRandom;

import game.Connector;
import game.board.BoardView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Line;
import language.Language;
import utils.ImageLoader;
import utils.MessageBoxController;

public class GeneratorView extends AnchorPane implements Observer {

	private Connector connector;
	private final GeneratorController controller;
	private final BoardView mapView;

	private final GridPane field;
	private GridPane colorChoose;
	private final GeneratorModel model;

	private ImageView planetButtonImg;
	private ImageView undeadImage;
	private final Pane movePane = new Pane();
	

	/**
	 * The Constructor
	 * 
	 * @param parentPane The Parent
	 */
	public GeneratorView(Pane parentPane, MessageBoxController msgBController) {
		model = new GeneratorModel();
		
		controller = new GeneratorController(this, model, msgBController);
		connector = new GeneratorConnector(controller);
		model.addObserver(this);
		
		VBox toolBar = new VBox();
		toolBar.setAlignment(Pos.CENTER);
		toolBar.setPrefWidth(main.Constants.GENERATOR_TOOLBAR_WIDTH);
		toolBar.setStyle("-fx-padding: 1;");
		toolBar.setBackground(new Background(new BackgroundFill(
				new LinearGradient(1,0,0,0,true, CycleMethod.NO_CYCLE, new Stop[] {new Stop(1, Color.rgb(0, 0, 0, 0.3)), 
						new Stop(0, Color.rgb(50, 0, 0, 0.9))}), CornerRadii.EMPTY, Insets.EMPTY)));
		
		mapView = new BoardView(model, connector, new double[] { 
													parentPane.getWidth() - main.Constants.GENERATOR_TOOLBAR_WIDTH - 52 * 2,
													parentPane.getHeight() - 52 * 2 });
		model.addObserver(mapView);
		field = mapView.getField();
		controller.connectAll();
		
		createColorChooser();
	
		addCildren(toolBar);
		
		movePane.setMouseTransparent(true);
		field.setGridLinesVisible(true);
		mapView.setFieldBackground("grey");


		
		mapView.getConnections().setMouseTransparent(false);
		mapView.getConnections().setPickOnBounds(false);

		toolBar.getChildren().addAll(colorChoose, createUndeadButton(), createPlanetButton(), createConnectorButton(), createStartButton());
		toolBar.setAlignment(Pos.CENTER);
	}

	/**
	 * Add all children to the GeneratorView
	 * 
	 * @param the toolBar
	 */
	private void addCildren(VBox toolBar) {

		BorderPane buttonHolder = createButtonHolder();
		
		Button back = new Button("<");
		back.setId("smlLgrButton");	
	
		Button loadButton = Language.getInstance().createButton("GENLD");
		back.setId("smlLgrButton");	
		
		Button saveButton = Language.getInstance().createButton("GENSV");
		back.setId("smlLgrButton");
		
		controller.registerBackButton(back);
		controller.registerLoadButton(loadButton);
		controller.registerSaveButton(saveButton);
		
		GeneratorView.setTopAnchor(buttonHolder, 0.0);
		GeneratorView.setTopAnchor(back, 0.0);
		GeneratorView.setTopAnchor(movePane, 0.0);
		GeneratorView.setTopAnchor(toolBar, 0.0);
		
		GeneratorView.setLeftAnchor(buttonHolder, 0.0);
		GeneratorView.setLeftAnchor(back, 0.0);
		GeneratorView.setLeftAnchor(saveButton, 0.0);
		GeneratorView.setLeftAnchor(movePane, 0.0);
		
		GeneratorView.setBottomAnchor(buttonHolder, 0.0);
		GeneratorView.setBottomAnchor(toolBar, 0.0);
		GeneratorView.setBottomAnchor(saveButton, 0.0);
		GeneratorView.setBottomAnchor(loadButton, 0.0);
		GeneratorView.setBottomAnchor(movePane, 0.0);
		
		GeneratorView.setRightAnchor(buttonHolder, main.Constants.GENERATOR_TOOLBAR_WIDTH);
		GeneratorView.setRightAnchor(toolBar, 0.0);
		GeneratorView.setRightAnchor(loadButton, main.Constants.GENERATOR_TOOLBAR_WIDTH);
		GeneratorView.setRightAnchor(movePane, 0.0);

		this.getChildren().addAll(buttonHolder,movePane ,toolBar, back, saveButton, loadButton);
		
	}

	/**
	 * Create the ButtonHolder/Container
	 * 
	 * @return the holder/container
	 */
	private BorderPane createButtonHolder() {
		BorderPane buttonHolder = new BorderPane();
		Button smlTop = new Button("-");
		Button smlLeft = new Button("-");
		Button smlRight = new Button("-");
		Button smlBot = new Button("-");

		Button lgrTop = new Button("+");
		Button lgrLeft = new Button("+");
		Button lgrRight = new Button("+");
		Button lgrBot = new Button("+");

		controller.connectButton(true, 'N', lgrTop);
		controller.connectButton(true, 'E', lgrRight);
		controller.connectButton(true, 'S', lgrBot);
		controller.connectButton(true, 'W', lgrLeft);
		controller.connectButton(false, 'N', smlTop);
		controller.connectButton(false, 'E', smlRight);
		controller.connectButton(false, 'S', smlBot);
		controller.connectButton(false, 'W', smlLeft);

		smlBot.setId("smlLgrButton");
		smlLeft.setId("smlLgrButton");
		smlRight.setId("smlLgrButton");
		smlTop.setId("smlLgrButton");

		lgrBot.setId("smlLgrButton");
		lgrLeft.setId("smlLgrButton");
		lgrRight.setId("smlLgrButton");
		lgrTop.setId("smlLgrButton");
		
		HBox topBox = new HBox(smlTop, lgrTop);
		VBox leftBox = new VBox(smlLeft, lgrLeft);
		VBox rightBox = new VBox(smlRight, lgrRight);
		HBox botBox = new HBox(smlBot, lgrBot);

		topBox.setAlignment(Pos.CENTER);
		leftBox.setAlignment(Pos.CENTER);
		rightBox.setAlignment(Pos.CENTER);
		botBox.setAlignment(Pos.CENTER);

		buttonHolder.setTop(topBox);
		buttonHolder.setLeft(leftBox);
		buttonHolder.setRight(rightBox);
		buttonHolder.setBottom(botBox);
		buttonHolder.setCenter(mapView);
		buttonHolder.setPadding(new Insets(2, 2, 2, 2));

		return buttonHolder;
	}

	/**
	 * Create the UndeadButton
	 * 
	 * @return the Button
	 */
	private StackPane createUndeadButton() {
		StackPane undeadButton = new StackPane();
		undeadButton.setId("toolBox");
		
		undeadImage = new ImageView(ImageLoader.getInstance().get(
				"/undeads/undead" + model.getCurColorId()
						+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
		undeadImage.setFitHeight(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.8);
		undeadImage.setFitWidth(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.8);
		
		undeadButton.getChildren().add(undeadImage);
		controller.registerUndeadButton(undeadButton);
		return undeadButton;
	}

	/**
	 * Create the STartButton
	 * 
	 * @return the Button
	 */
	private StackPane createStartButton() {
		StackPane startButton = new StackPane();
		startButton.setId("toolBox");
		
		ImageView startPlanetImage = new ImageView(ImageLoader.getInstance().get(
				"/planets/planet" + ThreadLocalRandom.current().nextInt(0, 4)+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
		startPlanetImage.setFitHeight(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.6);
		startPlanetImage.setFitWidth(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.6);
		
		ImageView startPlanetBase = new ImageView(ImageLoader.getInstance().get("/planetTiles/base.png"));
		startPlanetBase.setFitHeight(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.35);
		startPlanetBase.setFitWidth(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.35);
		startPlanetBase.setTranslateY(-30);
		
		Label startPlanetText = new Label("START");
		startPlanetText.setWrapText(true);
		startPlanetText.prefWidthProperty().bind(startPlanetImage.fitWidthProperty());
		startPlanetText.setStyle("-fx-font-size: 14; -fx-text-fill: black;");
		startPlanetText.setAlignment(Pos.CENTER);
		
		startButton.getChildren().addAll(startPlanetImage, startPlanetBase, startPlanetText);
		controller.registerStartButton(startButton);
		return startButton;
	}

	/**
	 * Create the PlanetButton
	 * 
	 * @return Button
	 */
	private StackPane createPlanetButton() {
		StackPane planetButton = new StackPane();
		planetButton.setId("toolBox");
		planetButtonImg = new ImageView(ImageLoader.getInstance().get(
				"/planets/planet" + model.getCurColorId()
						+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
		planetButtonImg.setFitHeight(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.8);
		planetButtonImg.setFitWidth(main.Constants.GENERATOR_TOOLBAR_BOX_WIDTH*0.8);
		
		planetButton.getChildren().add(planetButtonImg);
		controller.registerPlanetButton(planetButton);
		return planetButton;
	}

	/**
	 * Create the ConnectorButton
	 * 
	 * @return the utton
	 */
	private Pane createConnectorButton() {
		Pane connectorButton = new Pane();
		ImageView planetTL = new ImageView(ImageLoader.getInstance().get(
				"/planets/planet" + ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS)
						+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
		ImageView planetBR = new ImageView(ImageLoader.getInstance().get(
				"/planets/planet" + ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS)
						+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));

		planetBR.setFitHeight(main.Constants.GENERATOR_TOOLBAR_WIDTH / 3);
		planetTL.setFitHeight(main.Constants.GENERATOR_TOOLBAR_WIDTH / 3);
		planetBR.setFitWidth(main.Constants.GENERATOR_TOOLBAR_WIDTH / 3);
		planetTL.setFitWidth(main.Constants.GENERATOR_TOOLBAR_WIDTH / 3);

		planetBR.setTranslateX(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.8 - planetBR.getFitWidth());
		planetBR.setTranslateY(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.8 - planetBR.getFitWidth());

		Line line = new Line();
		line.setStartX(planetTL.getFitWidth() / 2);
		line.setStartY(planetTL.getFitHeight() / 2);
		line.setEndX(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.8 - planetBR.getFitWidth() / 2);
		line.setEndY(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.8 - planetBR.getFitHeight() / 2);
		line.setStroke(Color.WHITE);
		line.setStrokeWidth(8);

		connectorButton.setId("toolBox"); 
		connectorButton.getChildren().addAll(line, planetBR, planetTL);
		controller.registerConnectorButton(connectorButton);
		
		return connectorButton;
		
	}

	/**
	 * Create the ColorCHooser
	 */
	private void createColorChooser() {
		colorChoose = new GridPane();
		colorChoose.setStyle("-fx-padding: 1;" + "-fx-border-style: solid inside;" + "-fx-border-width: 2;"
				+ "-fx-border-insets: 5;" + "-fx-border-radius: 0;" + "-fx-border-color: grey;");

		colorChoose.setVgap(2);
		colorChoose.setHgap(2);

		Button red = new Button();
		Button yellow = new Button();
		Button green = new Button();
		Button blue = new Button();

		red.setStyle("-fx-background-color: red");
		yellow.setStyle("-fx-background-color: yellow");
		green.setStyle("-fx-background-color: green");
		blue.setStyle("-fx-background-color: blue");

		red.setPrefSize(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2,
				main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2);
		yellow.setPrefSize(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2,
				main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2);
		green.setMaxSize(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2,
				main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2);
		blue.setPrefSize(main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2,
				main.Constants.GENERATOR_TOOLBAR_WIDTH * 0.1 / 2);

		red.setId("colorButton");
		yellow.setId("colorButton");
		green.setId("colorButton");
		blue.setId("colorButton");

		colorChoose.add(red, 0, 0);
		colorChoose.add(yellow, 1, 0);
		colorChoose.add(green, 0, 1);
		colorChoose.add(blue, 1, 1);

		colorChoose.setId("toolBox");
		controller.registerColorButtons(yellow, green, blue, red);
		
	}


	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable modelObj, Object msg) {
		switch (msg.toString().substring(0, 5)) {
		case "genCo": //change planet color
			planetButtonImg.setImage(ImageLoader.getInstance().get(
				"/planets/planet" + model.getCurColorId()
						+ ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
			
			undeadImage.setImage(ImageLoader.getInstance().get(
				"/undeads/undead" + model.getCurColorId()
						+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png",
				main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
		}
	}
	
	/**
	 * Getter for the Field
	 * 
	 * @return the GridPane
	 */
	public GridPane getField() {
		return field;
	}
	
	/**
	 * Getter for the MapView
	 * 
	 * @return the MapView
	 */
	public BoardView getMapView() {
		return mapView;
	}
	

	/**
	 * Getter for the MovePane
	 * 
	 * @return THe MovePane
	 */
	public Pane getMovePane() {
		return movePane;
	}


}
