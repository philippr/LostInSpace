package mapGenerator;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import game.DragContainer;
import game.board.PlanetView;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.FileChooser;
import start.MenuController;
import utils.MessageBoxController;
import utils.RemovingLine;

public class GeneratorController {


	private DragContainer draged;
	private GeneratorView view;
	private GeneratorModel model;
	private StringProperty curTool = new SimpleStringProperty();
	private final ObservableList<String> colors =FXCollections.observableArrayList (
		    "yellow", "green", "blue", "red");
	private MessageBoxController msgBoxController;

	GeneratorController(GeneratorView view, GeneratorModel model, MessageBoxController msgBoxController) {
		this.model = model;
		this.view = view;
		curTool.set("planet");
		draged = new DragContainer();
		this.msgBoxController = msgBoxController;
	}

	void setGeneratView(GeneratorView view) {
		this.view = view;
	}

	public void connectAll() {
		
		view.setOnDragDetected(event -> {
			view.startFullDrag();
			event.consume();
		});

		view.setOnMouseReleased(event -> {
			if(draged.getMsg().equals("line")){
				draged.getParentPane().getChildren().remove((Line)draged.getBoxedObj());
				draged.clear();
				event.consume();
			}
		});

		
		view.setOnScroll(event -> {
			if (event.getDeltaY() > 0) {
				model.setCurColor((model.getCurColor().get()+1)%4);
			} else {
				model.setCurColor(Math.floorMod(model.getCurColor().get()-1,4));
			}
		});
		

		view.getField().setOnMouseClicked(event -> {
			int x = (int) (event.getX() / view.getField().getWidth() * model.getWidth());
			int y = (int) (event.getY() / view.getField().getHeight() * model.getHeight());

			switch (event.getButton()) {
			case PRIMARY:
				if (model.hasPlanet(x, y)) {
					switch (curTool.get()) {
					case "planet":
						model.getPlanet(x, y).setColor(model.getCurColorId());
						return;
					case "undead":
						model.getPlanet(x, y).addUndead(model.getCurColorId(), true);
						return;
					case "start":
						model.getPlanet(x, y).setBase(true);
						return;
					}
				} else if (curTool.get().equals("planet")) {
					model.addPlanet(x, y);
				}
				return;
			case SECONDARY:
				if (model.hasPlanet(x, y)) {

					switch (curTool.get()) {
					case "planet":
						model.removePlanet(x, y);
						return;
					case "undead":
						model.getPlanet(x, y).removeUndead(model.getCurColorId(), true);
						return;
					case "start":
						model.getPlanet(x, y).setBase(false);
						return;
					}
				}
			default:
				return;
			}
		});
	}

	
	public void registerPlanetButton(Pane button){
		bindButtonStyle(button, "planet");
		button.setOnMouseClicked(event -> {
			curTool.set("planet");
			view.getMapView().getField().setPickOnBounds(true);
		});	
	}
	
	
	public void registerUndeadButton(Pane button) {
		bindButtonStyle(button, "undead");
		button.setOnMouseClicked(event -> {
			curTool.set("undead");
			view.getMapView().getField().setPickOnBounds(true);
		});
	}
	
	public void registerConnectorButton(Pane button){
		bindButtonStyle(button, "connect");
		button.setOnMouseClicked(event -> {
			curTool.set("connect");
			view.getMapView().getField().setPickOnBounds(false);
		});	
	}
	
	public void registerStartButton(Pane button){
		bindButtonStyle(button, "start");
		button.setOnMouseClicked(event -> {
			curTool.set("start");
			view.getMapView().getField().setPickOnBounds(true);
		});	
	}
	
	private void bindButtonStyle(Pane button, String name){
		button.styleProperty().bind(Bindings.concat("-fx-border-color:",
				Bindings.when(Bindings.equal(name, curTool))
						.then(Bindings.valueAt(colors, model.getCurColor()))
						.otherwise("lightgrey") ) );
	}

	public void registerColorButtons(Button yellow, Button green, Button blue, Button red) {
		yellow.setOnMousePressed(event -> {
			model.setCurColor(0);
		});
		green.setOnMousePressed(event -> {
			model.setCurColor(1);
		});
		blue.setOnMousePressed(event -> {
			model.setCurColor(2);
		});
		red.setOnMousePressed(event -> {
			model.setCurColor(3);
		});

	}


	public void registerPlanet(PlanetView pV) {
		pV.setOnDragDetected(event -> {
			pV.startFullDrag();
			event.consume();
		});

		pV.setOnMousePressed(event -> {
			if(!curTool.get().equals("connect") || event.getButton() != MouseButton.PRIMARY){
				return;
			}
			
			draged.set(pV, "line", view.getMovePane(), model.getCurColorId());
			draged.setId(((PlanetView) pV).getPlanetId());
			
			Line line = new Line();
			Bounds before = pV.localToScene(pV.getBoundsInLocal());

			line.setStartX(before.getMinX()+before.getWidth()/2);
			line.setStartY(before.getMinY()+before.getHeight()/2);

			line.setEndX(event.getSceneX());
			line.setEndY(event.getSceneY());

			
		    line.setStrokeLineCap(StrokeLineCap.ROUND);

			
			line.setStrokeWidth(pV.getHeight()/15.0*view.getMapView().getScale());

			line.styleProperty().bind(Bindings.concat("-fx-stroke: ",
					Bindings.valueAt(colors, model.getCurColor()))
				 );
			line.setMouseTransparent(true);
			view.getMovePane().getChildren().add(line);
			draged.setBoxedObj(line);
			event.consume();
		});

		pV.setOnMouseDragged(event -> {
			if (!curTool.get().equals("connect") || event.getButton() != MouseButton.PRIMARY) {
				return;
			}
			Line line = (Line) draged.getBoxedObj();
			line.setEndX(event.getSceneX());
			line.setEndY(event.getSceneY());
			event.consume();

		});
		
		pV.setOnMouseDragReleased(event -> {			
			if (!curTool.get().equals("connect") || !draged.getMsg().equals("line")) {
				return;
			}
			model.addConnection(draged.getId(), pV.getPlanetId());

			draged.getParentPane().getChildren().remove((Line)draged.getBoxedObj());
			draged.clear();
			event.consume();
		});
	

	}
	
	private void extendMap(char orient){
		double width = view.getMapView().getWidth();
		double height = view.getMapView().getHeight();

		if (orient == 'N' || orient == 'S') {
			if (model.getHeight() == 7) {
				return;
			}
			model.setHeight(1, (orient == 'N' ? true : false));

			for (RowConstraints row : view.getField().getRowConstraints()) {
				row.setPercentHeight(100.0 / model.getHeight());
			}
			RowConstraints row = new RowConstraints();
			row.setPercentHeight(100.0 / model.getHeight());
			view.getField().getRowConstraints().add(row);

		} else {
			if (model.getWidth() == 14) {
				return;
			}
			model.setWidth(1, (orient == 'W' ? true : false));

			for (ColumnConstraints column : view.getField().getColumnConstraints()) {
				column.setPercentWidth(100.0 / model.getWidth());
			}
			ColumnConstraints column = new ColumnConstraints();
			column.setPercentWidth(100.0 / model.getWidth());
			view.getField().getColumnConstraints().add(column);

		}
		view.getMapView().changeMapRatio(width, height, false);

		if (orient == 'E' || orient == 'S') {
			return;
		} else if (orient == 'N') {
			for (Node child : view.getField().getChildren()) {
				if (child instanceof PlanetView) {
					view.getField();
					GridPane.setRowIndex(child, GridPane.getRowIndex(child) + 1);
				}
			}
		} else {
			for (Node child : view.getField().getChildren()) {
				if (child instanceof PlanetView) {
					view.getField();
					GridPane.setColumnIndex(child, GridPane.getColumnIndex(child) + 1);
				}
			}
		}
	}
	
	@SuppressWarnings("static-access")
	private void shrinkMap(char orient) {
		double width = view.getMapView().getWidth();
		double height = view.getMapView().getHeight();

		if (orient == 'N' || orient == 'S') {
			if (model.getHeight() == 1) {
				return;
			}
			model.setHeight(-1, (orient == 'N' ? true : false));
			view.getField().getRowConstraints().remove(0);

			for (RowConstraints row : view.getField().getRowConstraints()) {
				row.setPercentHeight(100.0 / model.getHeight());
			}
		} else {
			if (model.getWidth() == 1) {
				return;
			}
			model.setWidth(-1, (orient == 'W' ? true : false));
			view.getField().getColumnConstraints().remove(0);

			for (ColumnConstraints column : view.getField().getColumnConstraints()) {
				column.setPercentWidth(100.0 / model.getWidth());
			}
		}
		
		view.getMapView().changeMapRatio(width, height, false);

		Node child;
		Iterator<Node> iT = view.getField().getChildren().iterator();

		while (iT.hasNext()) {
			child = iT.next();

			if (child instanceof PlanetView) {
				switch(orient) {
				case 'N':
					if (view.getField().getRowIndex(child) == 0) {
						iT.remove();
						model.removePlanet(((PlanetView) child).getPlanetId());
						continue;
					}
					view.getField().setRowIndex(child, view.getField().getRowIndex(child) - 1);
					continue;
				case 'S':
					if (view.getField().getRowIndex(child) == model.getHeight()) {
						iT.remove();
						model.removePlanet(((PlanetView) child).getPlanetId());
					}
						continue;
				case 'W':
					if (view.getField().getColumnIndex(child) == 0) {
						iT.remove();
						model.removePlanet(((PlanetView) child).getPlanetId());
						continue;
					}
					view.getField().setColumnIndex(child, view.getField().getColumnIndex(child) - 1);
					continue;
				
				case 'E':
					if (child instanceof PlanetView && view.getField().getColumnIndex(child) == model.getWidth()) {
						iT.remove();
						model.removePlanet(((PlanetView) child).getPlanetId());
					}
					continue;
				}
			}
		}
	}
	
	
	public void connectButton(boolean larger, char orient, Button button) {
		if(larger){
			button.setOnMouseClicked(event -> {
				extendMap(orient);
			});
		} else {
			button.setOnMouseClicked(event -> {
				shrinkMap(orient);
			});
		}
	}
	

	public void registerLine(RemovingLine line) {
		line.setOnMouseClicked(event -> {
			if(event.getButton() != MouseButton.SECONDARY || !curTool.get().equals("connect")){
				return;
			}
			model.removeConnection(line.getFromPlanet().getId(), line.getToPlanet().getId());
			view.getMapView().getConnections().getChildren().remove(line);
		});
	}

	public void registerBackButton(Button button) {
		button.setOnMouseClicked(event -> {


			MenuController.switchContent();

		});
		
	}


	public void registerLoadButton(Button button) {
		button.setOnMouseClicked(event -> {
			FileChooser fileChooser = new FileChooser();

			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("USA-Map (*.txt)", "*.txt");
			fileChooser.getExtensionFilters().add(extFilter);
			
			// Dialogs are bugged in our Java-Version.
			// After binding the the the Dialog to our Stage, after closing, 
			// It will be impossible to resize.
			File file = fileChooser.showOpenDialog(null);
			
			if (file != null) {
				try{
					model.loadMap(new FileInputStream(file));
					view.getMapView().setModel(model, null);
				} catch(Exception e){
					msgBoxController.addMsg(e.getMessage());
				}
			}
		});
	}


	public void registerSaveButton(Button button) {
		button.setOnMouseClicked(event -> {
			FileChooser fileChooser = new FileChooser();

			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("USA-Map (*.txt)", "*.txt");
			fileChooser.getExtensionFilters().add(extFilter);
			
			// Dialogs are bugged in our Java-Version.
			// After binding the the the Dialog to our Stage, after closing, 
			// It will be impossible to resize.
			File file = fileChooser.showSaveDialog(null);
			
			if (file != null) {
				try{
					if (file.getPath().endsWith(".txt")) {
						model.writeMap(file.getPath());
					} else {
						model.writeMap(file.getPath() + ".txt");
					}
				} catch(Exception e){
					msgBoxController.addMsg(e.getMessage());
				}
			}
		});
	}
}
