package mapGenerator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;

import game.board.BoardModelI;
import game.board.PlanetModel;
import game.board.PlanetModelI;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * @author 650748
 *
 *         Notifications:
 *
 *         genCo: change current color of the generator adPlnX: add Planet with
 *         planetId X conctX#Y: connect the planets width IDs X and Y.
 */
public class GeneratorModel extends Observable implements BoardModelI {

	/**
	 * The current color as IntegerProperty, so it can be bound
	 */
	private final IntegerProperty curColor = new SimpleIntegerProperty();


	/**
	 * This HashMap maps the planetId of a planet to an ArrayList with all neighbors
	 */
	private HashMap<Integer, ArrayList<Integer>> neightbors;

	// "posX#posY to planetId"
	/**
	 * This HashMap maps the position of a planet [x+"#"+y]
	 * to the planetId of these planet.
	 */
	private HashMap<String, Integer> map;


	/**
	 * This HashMap maps the planetId to the PlanetModelI.
	 */
	private HashMap<Integer, PlanetModelI> planets;
	
	/**
	 * The width of the map
	 */
	private int width;
	
	/**
	 * The height of the map
	 */
	private int height;
	
	/**
	 * The current PlanetId, increases with every added planet
	 */
	private int curPlanetId;
	
	/**
	 * The horizontal offset of the map.
	 * Needed to  calculate the position after shrinking or extending on the top or left.
	 */
	private int offsetX;
	
	/**
	 * The vertical offset of the map.
	 * Needed to  calculate the position after shrinking or extending on the top or left.
	 */
	private int offsetY;

	/**
	 * Constructor of the GeneratorModel
	 * Initialized as 3x3 Map with a blue planet.
	 */
	public GeneratorModel() {
		neightbors = new HashMap<>();
		planets = new HashMap<>();
		map = new HashMap<>();
		curPlanetId = 0;
		offsetX = 0;
		offsetY = 0;
		width = 3;
		height = 3;
		curColor.set(2);
		addPlanet(1, 1);
	}

	/**
	 * Add a planet to the Model and notify
	 * 
	 * @param x the x position
	 * @param y the y position
	 */
	public void addPlanet(int x, int y) {
		planets.put(curPlanetId, new PlanetModel(curPlanetId, curColor.get(), x + offsetX, y + offsetY, false));
		map.put(x + offsetX + "#" + (y + offsetY), curPlanetId);
		neightbors.put(curPlanetId, new ArrayList<>());
		
		setChanged();
		notifyObservers("adPln" + curPlanetId);

		curPlanetId++;
	}

	/**
	 * Remove a planet with a given planetId
	 * 
	 * @param planetId the iD of the planet
	 */
	public void removePlanet(int planetId) {
		removePlanet(planets.get(planetId).getPos()[0] - offsetX, planets.get(planetId).getPos()[1] - offsetY);
	}

	/**
	 * Remove a planet by given x and y coordinates.
	 * 
	 * @param x the x position
	 * @param y the y position
	 */
	public void removePlanet(int x, int y) {
		Integer planetId = map.remove(x + offsetX + "#" + (y + offsetY));
		PlanetModelI planet = planets.remove(planetId);
		planet.close();

		for (int neighborId : neightbors.remove(planetId)) {
			neightbors.get(neighborId).remove(planetId);
		}
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getPlanet(int)
	 */
	@Override
	public PlanetModelI getPlanet(int planetId) {
		return planets.get(planetId);
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getNeighbors(int)
	 */
	@Override
	public ArrayList<Integer> getNeighbors(int planetId) {
		return neightbors.get(planetId);
	}

	/**
	 * Adds a connection between to neighbors
	 * 
	 * @param planetA One of theses planets
	 * @param planetB the other planet
	 */
	public void addConnection(int planetA, int planetB) {
		neightbors.get(planetA).add(planetB);
		neightbors.get(planetB).add(planetA);
		setChanged();
		notifyObservers("conct" + planetA + "#" + planetB);
	}

	/**
	 * Remove a connection between two neighbors
	 * 
	 * @param planetA One of theses planets
	 * @param planetB the other planet
	 */
	public void removeConnection(Integer planetA, Integer planetB) {
		neightbors.get(planetA).remove(planetB);
		neightbors.get(planetB).remove(planetA);
	}
	
	
	/**
	 * Load a Map by the given path
	 * 
	 * @param path the path as an Inputstream
	 * @throws Exception
	 */
	public void loadMap(InputStream path) throws Exception {
		int counter = 0;
		int[] size = new int[] { -1, -1 };
		

		// ID to neighborId
		HashMap<Integer, ArrayList<Integer>> loadNeightbors = new HashMap<>();

		// "posX#posY to planetId"
		HashMap<String, Integer> loadMap = new HashMap<>();

		// planetId to Planet
		HashMap<Integer, PlanetModelI> loadPlanets = new HashMap<>();

		HashMap<String, Integer>  planetIdDic = new HashMap<>();

		BufferedReader br;
		
		if (path == null) { // Check if path exists
			throw new Exception("MAPFP"); // # Error wrong File Path
		}
		
		br = new BufferedReader(new InputStreamReader(path));
		try {
			counter = loadPlanets(br, loadPlanets, planetIdDic, size, loadMap);
			loadNeighbors(br, loadNeightbors, planetIdDic, counter);
			loadUndead(br, loadPlanets, planetIdDic);

			// size = lastIndex +1
			size[0] +=1;
			size[1] +=1;
			br.close();

		} catch (NumberFormatException e) {
			throw new Exception("MAPPI"); // #Error parseint
		} catch(FileNotFoundException e) {
			throw new Exception("MAPFP");
		} catch (IOException e) {
			throw new Exception("MAPIO"); // #Error inputoutput

		} finally {
			br.close();
		}
		neightbors = loadNeightbors;
		map = loadMap;
		planets = loadPlanets;
		curPlanetId = counter;
		width = size[1];
		height = size[0];
		offsetX = 0;
		offsetY = 0;
	}

	/**
	 * Load planets from a file to the temporary HashMap loadPlanets 
	 * and also set the loadMap and the size.
	 * 
	 * @param br the BufferedReader
	 * @param loadPlanets a temporary HashMap of the Planets which will be filled
	 * @param planetIdDic the dictionary from read planetId to planetId in the game
	 * @param size an array with the size of the map which will be filled
	 * @param loadMap the temporary HashMap with the map which will be filled 
	 * @return the total amount of planets as an integer
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws Exception
	 */
	private int loadPlanets(BufferedReader br, HashMap<Integer, PlanetModelI> loadPlanets, HashMap<String, Integer> planetIdDic, int[] size, HashMap<String, Integer> loadMap) throws NumberFormatException, IOException, Exception {
		int colorId, x, y;
		String id;
		String line;
		
		int counter = 0;
		
		while ((line = br.readLine().trim()) != null && !line.isEmpty()) {  // detect empty line for seperation

			String[] p = line.split(",");
			id = p[0];

			if (planetIdDic.containsKey(id)) {
				throw new Exception("MAPMD" + id); // #Error multiple ID
			}

			planetIdDic.put(id, counter++);

			switch (p[1]) {
			case "y":
				colorId = 0;
				break;
			case "g":
				colorId = 1;
				break;
			case "b":
				colorId = 2;
				break;
			case "r":
				colorId = 3;
				break;
			default:
				throw new Exception("MAPWC" + id); // #Error wrong color
			}

			x = Integer.parseInt(p[2]);
			y = Integer.parseInt(p[3]);

			if (x < 0 || x > 6) {
				throw new Exception("MAPXD" + id); // #Error x dimension
			}
			if (y < 0 || y > 13) {
				throw new Exception("MAPYD" + id); // #Error y dimension
			}
			
			// shift y by 3 bits(its max size) and write x next to it.
			if(loadMap.containsKey(y+"#"+x)) {
				throw new Exception("MAPPO" + id); // #Error position occupied another planet
			} else {
				loadMap.put(y+"#"+x, planetIdDic.get(id));
			}

			// Update the MaxSize of the map
			size[0] = Math.max(size[0], x);
			size[1] = Math.max(size[1], y);

			loadPlanets.put(planetIdDic.get(id),new PlanetModel(planetIdDic.get(id), colorId, y, x, false));
			
			if (p.length > 4 && p[4].trim().equals("1")) {
				loadPlanets.get(planetIdDic.get(id)).setBase(true);
			}
		}
		return counter;
	}

	/**
	 * Load the neighbors to the temporary HashMap
	 * 
	 * @param br the BufferedReader
	 * @param loadNeightbors the temporary HashMap which will be filled witch all connections
	 * @param planetIdDic the dictionary from read planetId to planetId in the game
	 * @param counter the number of planets
	 * @throws IOException
	 * @throws Exception
	 */
	private void loadNeighbors(BufferedReader br, HashMap<Integer, ArrayList<Integer>> loadNeightbors, HashMap<String, Integer> planetIdDic, int counter) throws IOException, Exception {
		String line;
		String[] fromTo;
		
		for(int i=0; i<counter; i++){
			loadNeightbors.put(i, new ArrayList<>());
		}

		while ((line = br.readLine()).trim() != null && !line.isEmpty()) { // read planets

			fromTo = line.split(","); // split by , and add String array to list
			if (!planetIdDic.containsKey(fromTo[0]) || !planetIdDic.containsKey(fromTo[1])) {
				throw new Exception("MAPFT" + fromTo[0] + "," + fromTo[1]); // #Error from  to
			}
			
			if (planetIdDic.get(fromTo[0])<planetIdDic.get(fromTo[1]) &&
					!loadNeightbors.get(planetIdDic.get(fromTo[0])).contains(planetIdDic.get(fromTo[1]))) {
				loadNeightbors.get(planetIdDic.get(fromTo[0])).add(planetIdDic.get(fromTo[1]));
				loadNeightbors.get(planetIdDic.get(fromTo[1])).add(planetIdDic.get(fromTo[0]));
			}
		}
	}

	/**
	 * Load the undeads and add them to the planets
	 * 
	 * @param br the BufferedReader
	 * @param loadPlanets  a temporary HashMap of the Planets
	 * @param planetIdDic the dictionary from read planetId to planetId in the game
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws Exception
	 */
	private void loadUndead(BufferedReader br, HashMap<Integer, PlanetModelI> loadPlanets, HashMap<String, Integer> planetIdDic) throws NumberFormatException, IOException, Exception {
	
		String line;	
		for(int i=0; i<12; i++){
			line = br.readLine().trim();
			if(line == null){
				throw new Exception("MAPIO"); // #Error inputoutput
			}
			if(line.isEmpty()){
				continue;
			}
			
			for (String str : line.split(",")) {
				if(!planetIdDic.containsKey(str)){
					throw new Exception("MAPNE");
				}
				
				if(!planetIdDic.containsKey(str)){
					throw new Exception("MAPNE");
				}
				
				PlanetModelI planet = loadPlanets.get(planetIdDic.get(str));
				if(planet.getUndeads(i /3) != 0){
					throw new Exception("MAPDP"); // #MAPDP dublicate planets in  same Color
				}
				for(int undeadCount = 0; undeadCount<i % 3+1; undeadCount++){
					planet.addUndead(i /3, true);
				}
			}
		}
	}
	

	/**
	 * Write the current map to a file given by athe path
	 * 
	 * @param path the path
	 * @throws Exception
	 */
	public void writeMap(String path) throws Exception {

		HashMap<Integer, Integer> translater = new HashMap<>();
		ArrayList<ArrayList<ArrayList<Integer>>> undeads = new ArrayList<>(4); // [colorId][numberUndeads]:planetId
		FileWriter fW;
		String newLine;
		String startPlanetString;
		String colorString = "";
		String undeadString = "";
		int curId = 0;

		for (int i = 0; i < 4; i++)		
			 {
			undeads.add(new ArrayList<>(3));
			for(int j = 0; j<3;j++){
				undeads.get(i).add(new ArrayList<>(3));
			}
		}

		
		if (System.getProperty("os.name").equals("Windows 7")) {
			// Windows needs this for a new line. it's better for debugging at home =)
			newLine = "\r\n"; 
		} else {
			newLine = "\n";
		}

		try {
			fW = new FileWriter(path);
			for (PlanetModelI planet : planets.values()) {
				translater.put(planet.getId(), curId);

				switch (planet.getColor()) {
				case 0:
					colorString = "y";
					break;
				case 1:
					colorString = "g";
					break;
				case 2:
					colorString = "b";
					break;
				case 3:
					colorString = "r";
					break;
				}
				if (planet.hasBase()) {
					startPlanetString = ",1";
				} else {
					startPlanetString = "";
				}

				fW.write(curId + "," + colorString + "," + (planet.getPos()[1] - offsetY) + ","
						+ (planet.getPos()[0] - offsetX) + startPlanetString + newLine);
				for(int colorId=0; colorId<4; colorId++){
					if(planet.getUndeads(colorId)>0){
						undeads.get(colorId).get(planet.getUndeads(colorId)-1).add(curId);
					}
				}
				curId++;
			}

			// new line
			fW.write(newLine);
			
			// write connections
			for(int iD: map.values()){
				for(int neighbor: neightbors.get(iD)){
					if(translater.get(iD)<translater.get(neighbor)){
						fW.write(translater.get(iD) + "," +translater.get(neighbor) + newLine);
					}
				}
			}

			// new line
			fW.write(newLine);
			
			//write undeads
			for(int colorIndex = 0; colorIndex<12; colorIndex++){
				undeadString = "";
				for(int iD: undeads.get(colorIndex/3).get(colorIndex%3)){
					undeadString += iD + ",";
				}
				if (undeadString.length() == 0) {
					fW.write(newLine);
				} else {
					fW.write(undeadString.substring(0, undeadString.length() - 1) + newLine);
				}
			}
			fW.close();
			return;
		} catch (IOException e) {
			throw new Exception("MAPIO");
		}
	}

	/**
	 * Getter for the color.
	 * 
	 * @return the IntegerProperty of the color
	 */
	public IntegerProperty getCurColor() {
		return curColor;
	}

	/**
	 * Set the current color by the colorId
	 * 
	 * @param colorId
	 */
	public void setCurColor(int colorId) {
		this.curColor.set(colorId);
		setChanged();
		notifyObservers("genCo");
	}

	/**
	 * Getter for the current colorId
	 * 
	 * @return the colorId as an int
	 */
	public int getCurColorId() {
		return curColor.get();
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#setNeighbors(java.util.ArrayList)
	 */
	@Override
	public void setNeighbors(ArrayList<ArrayList<Integer>> map) {
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#setStartPlanet(int)
	 */
	@Override
	public void setStartPlanet(int startPlanet) {
	}

	/**
	 * Manipulator for the width.
	 * 
	 * @param diff adds the diff to the width
	 * @param offset if true, set the offset
	 */
	public void setWidth(int diff, boolean offset) {
		this.width += diff;
		if (offset) {
			offsetX -= diff;
		}
	}

	/**
	 * Manipulator for the height.
	 * 
	 * @param diff adds the diff to the height
	 * @param offset if true, set the offset
	 */
	public void setHeight(int diff, boolean offset) {
		this.height += diff;
		if (offset) {
			offsetY -= diff;
		}
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getHeight()
	 */
	@Override
	public int getHeight() {
		return height;
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getWidth()
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/**
	 * Returns true, is a planet is on the given coordinates.
	 * 
	 * @param x the x position
	 * @param y the y position
	 * @return answer as a boolean true/false
	 */
	public boolean hasPlanet(int x, int y) {
		return map.containsKey(x + offsetX + "#" + (y + offsetY));
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getPlanets()
	 */
	@Override
	public Iterator<PlanetModelI> getPlanets() {
		return planets.values().iterator();
	}

	/**
	 * Getter for a planet by given coordinates
	 * 
	 * @param x the x position
	 * @param y the y position
	 * @return the planet on that position
	 */
	public PlanetModelI getPlanet(int x, int y) {
		return planets.get(map.get(x + offsetX + "#" + (y + offsetY)));
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#setPlanets(java.util.ArrayList)
	 */
	@Override
	public void setPlanets(ArrayList<PlanetModelI> planets) {
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getStartPlanet()
	 */
	@Override
	public int getStartPlanet() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getOffsetY()
	 */
	public int getOffsetY() {
		return offsetY;
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getOffsetX()
	 */
	public int getOffsetX() {
		return offsetX;
	}

	/* (non-Javadoc)
	 * @see game.board.BoardModelI#getnPlanets()
	 */
	@Override
	public int getnPlanets() {
		return planets.size();
	}
}
