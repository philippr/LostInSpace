package utils;

import java.util.Observable;

public class MessageBoxModel extends Observable {
	
	private int nMsg;
	
	public void addMsg() {
		nMsg +=1;
	}	
	public void clrMsg() {
		nMsg = 0;
		setChanged();
		notifyObservers();
	}	
	public void remMsg() {
		nMsg -=1;
		setChanged();
		notifyObservers();
	}
	
	public int nMsg() {
		return nMsg;
	}
}
