package utils;

import java.util.LinkedList;

import cards.Card;
import cards.PlanetCard;

/**
 * This class represents the playerHand.
 * Its a LinkedList with the extra methods hasColor.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class Hand extends LinkedList<Card> {
	
	
	
	/**
	 * Returns true if the Player has a card of the given color.
	 * 
	 * @param colorId of the card
	 * @return true/false
	 */
	public boolean hasColor(int colorId) {
		for (Card card : this) {
			if (card instanceof PlanetCard && ((PlanetCard) card).getPlanetColor() == colorId) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns true if the Player has as lest  N cards of the given color.
	 * 
	 * @param colorId of the card
	 * @param n total amount of these cards
	 * @return true/false
	 */
	public boolean hasColor(int colorId, int n) {
		int count = 0;
		if(n < 1 ) {
			return true;
		}
		
		for (Card card : this) {
			if (card instanceof PlanetCard && ((PlanetCard) card).getPlanetColor() == colorId) {
				count++;
				if(count == n) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean hasNumber(int id) {
		for (Card card : this) {
			if (card instanceof PlanetCard && ((PlanetCard) card).getPlanetId() == id) {
				return true;
			}
		}
		return false;
	}
}
