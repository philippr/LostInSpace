package utils;

import java.util.HashMap;

import javafx.scene.image.Image;

/**
 * Class to load images. Stores the Images in a cache.
 * 
 * !Impossible to test with junit! :(
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class ImageLoader {
	private final static HashMap<String, Image> images = new HashMap<>();
	private static ImageLoader instance;

	
	
	/**
	 * Returns the actual instance of the ImageLoader
	 * 
	 * @return an instance of the ImageLoader
	 */
	public synchronized static ImageLoader getInstance(){
		if(instance == null){
			instance = new ImageLoader();
		}
		return instance;
	}
	
	
	/**
	 * Load the image from the cache. Adds the Image to the cache if necessary.
	 * 
	 * @param path
	 *            the path to the image, eg. /images/planets/planet.png
	 * @return the Image
	 */
	public Image get(String path) {
		if (!images.containsKey(path)) {
			images.put(path, new Image(System.class.getResource(path).toExternalForm()));
		}
		return images.get(path);
	}

	/**
	 * Load the scaled image from the cache. Adds the Image to the cache if
	 * necessary.
	 * 
	 * @param path
	 *            the path to the image, eg. /images/planets/planet.png
	 * @param width
	 *            prescale the Image to with
	 * @param height
	 *            prescale the Image to height
	 * @return the Image
	 */
	public Image get(String path, double width, double height) {
		if (!images.containsKey(path + "#" + width + "#" + height)) {
			images.put(path + "#" + width + "#" + height,
					new Image(System.class.getResource(path).toExternalForm(), width, height, true, true, false));
		}
		return images.get(path + "#" + width + "#" + height);

	}
}
