package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import game.GameModelI;
import game.GameStatusModelI;
import start.ConfigModelI;


/**
 * Saves a game or loads a saved game.
 *@author Philipp, Fabian, Julia, Ronja
 *
 */
public class SaveLoader {
	
	/**
	 * GameModel contains information about players.
	 */
	private GameModelI gameM;
	
	/**
	 * Status model contains information about changing informations (e.g.
	 * informations displayed in infobox).
	 */
	private GameStatusModelI statusM;
	
	/**
	 * ConfigModel contains information about chosen settings.
	 */
	private ConfigModelI configM;

	/**
	 * Saves a game.
	 * 
	 * @param path
	 *            Path where game is saved.
	 * @param gameModel
	 *            GameModel contains information about players.
	 * @param gameStatusModel
	 *            GameStatusModel
	 * @param configModel
	 *            ConfigModel
	 * @throws Exception
	 */
	public void save(String path, GameModelI gameModel, GameStatusModelI gameStatusModel, ConfigModelI configModel)
			throws Exception {
	
		try {
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
			objOut.writeObject(gameModel);
			objOut.writeObject(gameStatusModel);
			objOut.writeObject(configModel);
			objOut.writeObject(null);
			objOut.close();
			fileOut.close();
		} catch (IOException e) {
			throw new Exception("SLGIO"); // #SLGIO Input Output
		} catch (Exception e) {
			throw new Exception("SLGEX"); // #SLGEX unknown exception
		}
	}

	/**
	 * Loads a game from a file.
	 * @param savedGame File with saved game.
	 * @throws Exception 
	 */
	public void load(File savedGame) throws Exception {
		try {
			if (!savedGame.exists() || savedGame.isDirectory()) {
				throw new Exception("SLGFP"); // #SLIFP FilePath
			}

			FileInputStream fileIn = new FileInputStream(savedGame.getPath());
			ObjectInputStream objIn = new ObjectInputStream(fileIn);

			gameM = (GameModelI) objIn.readObject();
			statusM = (GameStatusModelI) objIn.readObject();
			configM = (ConfigModelI) objIn.readObject();

			objIn.close();
			fileIn.close();

			if (gameM == null || statusM == null || configM == null) {
				throw new Exception("SLGNL"); // #SLGNL object is null
			}
		} catch (StreamCorruptedException e) {
			throw new Exception("SLGCF"); // #SLGCF Corrupted File / StreamCoruppted
		} catch (ClassNotFoundException e) {
			throw new Exception("SLGNF"); // #SLGCF Class not found
		} catch (IOException e) {
			throw new Exception("SLGIO"); // #SLFIO Input/Output
		} 

	}

	/**
	 * Getter for ConfigModel.
	 * @return configM
	 */
	public ConfigModelI getConfigModel() {
		return configM;
	}

	/**
	 * Getter for GameModel
	 * @return gameM
	 */
	public GameModelI getGameModel() {
		return gameM;
	}

	/**
	 * Getter for GameStatusModel
	 * @return statusM
	 */
	public GameStatusModelI getGameStatusModel() {
		return statusM;
	}
}
