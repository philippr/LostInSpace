package utils;

import java.util.*;

import game.board.PlanetModel;
import game.board.PlanetModelI;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Loads a Map from a File an swaps x and y.
 * 
 * <p>
 * MapLoader interprets given text file map which is utf8 and looks like this:
 * <br>
 * id, color, x, y (, isStartPlanet) <br>
 * emptyline <br>
 * id,id <br>
 * EOF <br>
 * <br>
 * colors will be coded as integer like this: <br>
 * y = 0 <br>
 * g = 1 <br>
 * b = 2 <br>
 * r = 3<br>
 * <br>
 * Error-Codes:<br> 
 * MAPFP = ERROR wrong file path<br> 
 * MAPMD = ERROR multiple id <br>
 * MAPWC = ERROR wrong color <br>
 * MAPXD = ERROR X dimension<br> 
 * MAPYD = ERROR Y dimension <br>
 * MAPMS = ERROR to many StartPlanets<br> 
 * MAPNS = ERROR no StartPlanet<br> 
 * MAPLP = ERROR not enough planets from each color<br> 
 * MAPFT = ERROR FromTo, one of the connected Planets does not exist<br> 
 * MAPPI = ERROR parse int<br>
 * MAPIO = ERROR INPUT OUTPUT<br>
 * MAPPO = ERROR position occupied by an other Planet MAPFF = ERROR Fileformat
 * MAPNE = ERROR planet does not exist
 *
* @author Philipp, Fabian, Julia, Ronja
 */

public class MapLoader {

	/**
	 * This counter counts the current planetId.
	 * So every new planet gets a new Id inm ascending order.
	 */
	private int curPlanetId;
	
	/**
	 * This HashMap maps the read iD to the new iD.
	 */
	private HashMap<String, Integer> planetIdDic;
	
	/**
	 * This ArrayList provides the neighborIds of every planet.
	 * The mapped planetId is the index. 
	 */
	private ArrayList<ArrayList<Integer>> neighbors;
	
	/**
	 * If a planet is read from the file, its Id will be written into this Set.
	 * So, no planet can be hashed twice at in planetIdDic.
	 */
	private HashSet<Integer> occupiedPos;
	
	/**
	 * This is a ArrayList of all read planet models.
	 */
	private ArrayList<PlanetModelI> planets;
	
	/**
	 * This is the startPlanetId of the map.
	 */
	private int startPlanetId;
	
	/**
	 * The BufferedReader to read the files
	 */
	private BufferedReader br;
	
	/**
	 * This Array is needed to count how much planets of a color are present on the map.
	 */
	private int[] planetColors;
	
	/**
	 * The map size is stored here.
	 */
	private int[] size;

	/**
	 * loads map from given path sets private variables map, planets,
	 * planetIdDic
	 * 
	 * @param in InputStream
	 *            StringPath to the map file
	 * @throws Exception
	 */
	public void loadMap(InputStream in) throws Exception {
		curPlanetId = 0;
		startPlanetId = -1;
		size = new int[] { -1, -1 };
		planetColors = new int[4];
		planetIdDic = new HashMap<>();
		planets = new ArrayList<PlanetModelI>();
		occupiedPos = new HashSet<>();
		
		if (in == null) { // Check if path exists
			throw new Exception("MAPFP"); // # Error wrong File Path
		}

		try {
			br = new BufferedReader(new InputStreamReader(in));

			loadPlanets(br);
			loadNeighbors(br);
			
			// size = lastIndex +1
			size[0] +=1; 
			size[1] +=1;

		} catch (NumberFormatException e) {
			throw new Exception("MAPPI"); // #Error parseint
		} catch(FileNotFoundException e) {
			throw new Exception("MAPFP");
		} catch (IOException e) {
			throw new Exception("MAPIO"); // #Error inputoutput
		} finally {
			br.close();
		}
	}


	/**
	 * Load the Planets.
	 * 
	 * @param br the BufferedReader
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws Exception
	 */
	/**
	 * @param br
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws Exception
	 */
	private void loadPlanets(BufferedReader br) throws NumberFormatException, IOException, Exception {
		int colorId, x, y;
		String id, line;
		
		while ((line = br.readLine()) != null && !line.isEmpty()) {  // detect empty line for seperation
			line = line.trim();
			String[] p = line.split(",");
			id = p[0];

			if (planetIdDic.containsKey(id)) {
				throw new Exception("MAPMD" + id); // #Error multiple ID
			}

			planetIdDic.put(id, curPlanetId++);
			
			colorId = parseColor(p[1]);
			
			x = Integer.parseInt(p[2]);
			y = Integer.parseInt(p[3]);

			test_size(x,y, id);
	
			// shift y by 3 bits(its max size) and write x next to it.
			if(occupiedPos.contains((y<<3) + x)) {
				throw new Exception("MAPPO" + id); // #Error position occupied another planet
			} else {
				occupiedPos.add((y<<3) + x);
			}

			// x and y are switched, so our map is widther then high.
			
			// Update the MaxSize of the map
			size[0] = Math.max(size[0], y);
			size[1] = Math.max(size[1], x);

			planets.add(new PlanetModel(planetIdDic.get(id), colorId, y, x, true));
			planetColors[colorId]++;

			if (p.length > 4) {
				if (startPlanetId != -1) {
					throw new Exception("MAPMS" + id); // #Error to many StartPlanets
				}
				startPlanetId = planetIdDic.get(id);
			}
		}		
		testStart_Colors();
	}
	
	
	/**
	 * Load the neighbors (Adjacency list).		trimmed = false;
	 * 
	 * @param br the BufferedReader
	 * @throws IOException
	 * @throws Exception
	 */
	private void loadNeighbors(BufferedReader br) throws IOException, Exception {
		String line;
		String[] fromTo;

		neighbors = new ArrayList<>(curPlanetId);
		for (int i = 0; i < curPlanetId; i++) {
			neighbors.add(new ArrayList<Integer>());
		}
		while ((line = br.readLine()) != null && !line.isEmpty()) { // read  planets
			line = line.trim();
			fromTo = line.split(","); // split by , and add String array to list
			if (!planetIdDic.containsKey(fromTo[0]) || !planetIdDic.containsKey(fromTo[1])) {
				throw new Exception("MAPFT" + fromTo[0] + "," + fromTo[1]); // #Error from to
			}
			if (!neighbors.get(planetIdDic.get(fromTo[0])).contains(planetIdDic.get(fromTo[1]))) {
				neighbors.get(planetIdDic.get(fromTo[0])).add(planetIdDic.get(fromTo[1]));
				neighbors.get(planetIdDic.get(fromTo[1])).add(planetIdDic.get(fromTo[0]));
			}
		}
		
		if(line != null){
			loadUndead(br);
		}
	}


	/**
	 * Loads the undeads from the file and adds them to the planets.
	 * 
	 * @param br the BufferedReader
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws Exception
	 */
	private void loadUndead(BufferedReader br) throws NumberFormatException, IOException, Exception {
		String line;	
		for(int i=0; i<12; i++){
			line = br.readLine().trim();
			if(line == null){
				throw new Exception("MAPIO"); // #Error inputoutput
			}
			if(line.isEmpty()){
				continue;
			}
			
			for (String str : line.split(",")) {
				if(!planetIdDic.containsKey(str)){
					throw new Exception("MAPNE");
				}
				
				if(!planetIdDic.containsKey(str)){
					throw new Exception("MAPNE");
				}
				
				PlanetModelI planet = planets.get(planetIdDic.get(str));
				if(planet.getUndeads(i /3) != 0){
					throw new Exception("MAPDP"); // #MAPDP dublicate planets in  same Color
				}
				for(int undeadCount = 0; undeadCount<i % 3+1; undeadCount++){
					planet.addUndead(i /3, true);
				}
			}
		}
	}

	/**
	 * Parse the color string to the related colorId.
	 * y(ellow):0, g(reen):1, b(lue):2, r(ed):3
	 * 
	 * @param string the first letter of the color.
	 * @return the parsed colorId
	 * @throws Exception
	 */
	private int parseColor(String string) throws Exception {
		int colorId;
		switch (string) {
		case "y":
			colorId = 0;
			break;
		case "g":
			colorId = 1;
			break;
		case "b":
			colorId = 2;
			break;
		case "r":
			colorId = 3;
			break;
		default:
			throw new Exception("MAPWC" + string); // #Error wrong color
		}

		return colorId;
	}


	/**
	 * Tests whether the planet position is allowed.
	 * 
	 * @param x the x position of the planet
	 * @param y the y position of the planet
	 * @param id the id of the current planet
	 * @throws Exception
	 */
	private void test_size(int x, int y, String id) throws Exception {
		if (x < 0 || x > 6) {
			throw new Exception("MAPXD" + id); // #Error x dimension
		} else if (y < 0 || y > 13) {
			throw new Exception("MAPYD" + id); // #Error y dimension
		}
	}


	/**
	 * Tests whether the Map has just one start planet 
	 * and enough planets of each color.
	 * 
	 * @throws Exception
	 */
	private void testStart_Colors() throws Exception{
		if (startPlanetId == -1) {
			throw new Exception("MAPNS"); // #Error no StartPlanet
		}
		for (int i : planetColors) {
			if (i < 5) {
				throw new Exception("MAPLP" + i); // #Error not enough  planets from each color
			}
		}
	}
	

	/**
	 * getter for the MapSize
	 * 
	 * @return the size of the map as int[]: {maxX, maxY}
	 */
	public int[] getSize() {
		return size;
	}


	/**
	 * Getter for the start planet
	 * 
	 * @return the id of the startplanet
	 */
	public int getStartPlanetId() {
		return startPlanetId;
	}


	/**
	 * Getter for list with all players.
	 * @return planets
	 */
	public ArrayList<PlanetModelI> getPlanets() {
		return planets;
	
	}


	/**
	 * Getter for list with map.
	 * @return map
	 */
	public ArrayList<ArrayList<Integer>> getNeighbors() {
		return neighbors;
	}
}
