package utils;

import java.util.Observable;
import java.util.Observer;

import game.board.PlanetModelI;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;

/**
 *
 * This is a self-removing Line.
 * If one of its planets propagates the notification to remove,
 * this Line will also remove itself from its parent.
 * @author Philipp, Fabian, Julia, Ronja
 */
public class RemovingLine extends Line implements Observer {

	/**
	 * Planet from which the line starts.
	 */
	private final PlanetModelI fromPlanet;
	
	/**
	 * Planet where line ends.
	 */
	private final PlanetModelI toPlanet;
	
	/**
	 * Adds Observers and instantiates attributes.
	 * @param fromPlanet
	 * @param toPlanet
	 */
	public RemovingLine(PlanetModelI fromPlanet, PlanetModelI toPlanet){
		super();
		fromPlanet.addObserverX(this);
		toPlanet.addObserverX(this);
		this.fromPlanet = fromPlanet;
		this.toPlanet = toPlanet;
	}

	/**
	 * Let the line remove itself on a "suicide"-notification 
	 * from one of its parents.
	 */
	@Override
	public void update(Observable model, Object msg) {
		if(msg.toString().equals("sucid")){
			Pane parent = (Pane) this.getParent();
			if(parent != null){
				parent.getChildren().remove(this);
			}
		}
	}
	
	/**
	 * Getter for planet where line starts.
	 * @return fromPlanet
	 */
	public PlanetModelI getFromPlanet() {
		return fromPlanet;
	}

	/**
	 * Getter for planet where line starts.
	 * @return toPlanet
	 */
	public PlanetModelI getToPlanet() {
		return toPlanet;
	}
}
