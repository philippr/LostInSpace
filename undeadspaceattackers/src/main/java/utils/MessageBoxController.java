package utils;

public class MessageBoxController {
	
	MessageBoxView view;
	MessageBoxModel model;
	

	public MessageBoxController(MessageBoxView view, MessageBoxModel model) {
		this.view = view;
		this.model = model;
		
		view.setOnMouseClicked(event -> {
			model.clrMsg();
		});
	}
	
	public void addMsg(String msgID) {
		System.out.println(model.nMsg());
		if(model.nMsg() == 3) {
			System.out.println("REM");
			model.remMsg();
		}
		model.addMsg();
		view.addMsg(msgID);
	}
	
	



}
