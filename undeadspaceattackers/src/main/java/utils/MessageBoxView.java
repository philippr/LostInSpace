package utils;


import java.util.Observable;
import java.util.Observer;

import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import language.Language;

/**
 * This class represents a message box with error messages if something went wrong.
 * It displays an corresponding message to the error.
 *  @author Philipp, Fabian, Julia, Ronja
 *
 */
public class MessageBoxView extends VBox implements Observer {
	
	MessageBoxModel model;
	MessageBoxController controller;
	
	/**
	 * Constructor for MessageBox. Creates a pane with a certain size and background.
	 */
	public MessageBoxView() {
		
		this.setPrefWidth(500);
		this.setMinWidth(500);
		this.setMaxWidth(500);
		this.setPrefHeight(50);
		this.setMinHeight(50);
		this.setMaxHeight(50);
		this.setStyle("-fx-background-color: rgba(255, 255, 255, 0.8);");
		
		
		model = new MessageBoxModel();
		model.addObserver(this);
		
		controller = new MessageBoxController(this, model);
		
		this.setAlignment(Pos.TOP_CENTER);
		this.setVisible(false);
	}
	
	public MessageBoxController getController() {
		return controller;
	}
	
	/**
	 * Setter for error message text. Adds an error message to the message box.
	 * Only three messages will be displayed.
	 * @param msgID Error Message Text
	 */
	void addMsg(String msgID){
		if(!this.isVisible()) {
			this.setVisible(true);
		}
		StackPane message = new StackPane(Language.getInstance().createText(msgID));
		this.getChildren().add(message);	
	}

	private void removeLast() {
		System.out.println("REMOVE");
		System.out.println(this.getChildren());
		this.getChildren().remove(0);
	}
	
	/**
	 * Clear the box - Remove all texts.
	 */
	private void clear() {
		this.getChildren().clear();
		this.setVisible(false);
	}

	@Override
	public void update(Observable o, Object arg) {
		if(model.nMsg() == 0) {
			clear();
			return;
		} else {
			removeLast();
		}
	}
}
