package player;

/**
 * Lieutenant has 5 instead of 4 actions.
 *@author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public class Lieutenant extends Role {

	/*
	 * (non-Javadoc)
	 * @see player.Role#getnActions()
	 */
	@Override
	public int getnActions() {
		return 5;
	}	
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#getTextId()
	 */
	@Override
	public String getTextId() {
		return "LO";
	}

}
