package player;

import java.util.ArrayList;

import cards.Card;
import cards.FindBase;
import game.board.PlanetModelI;

/**
 * Constructor does not need to throw a card to build a base.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public class Constructor extends Role {

	/* (non-Javadoc)
	 * @see player.Role#buildBase(player.PlayerI, game.board.PlanetModelI, java.util.ArrayList)
	 */
	public boolean buildBase(PlayerI cPlayer, PlanetModelI planet,  ArrayList<Card> lastUsedCard) throws Exception {
		if(planet.hasBase()) {			
			throw new Exception("ROLHB");	//#ROLHB has already base 
		}

		if (isOneCard(lastUsedCard) && (lastUsedCard.get(0) instanceof FindBase)) {
			return true;
		}

		if (((cPlayer.getLeftActions() < 1))) {
			throw new Exception("ROLLA"); // #ROLLA: not enough Left Actions
		}

		if ((cPlayer.getPlanetId() == planet.getId())) {
			return true;
		} else {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see player.Role#getTextId()
	 */
	@Override
	public String getTextId() {
		return "CK";
	}
}
