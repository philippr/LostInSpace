package player;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import cards.Card;
import utils.Hand;


/**
 *	contains all the Player Informations
 *
 * Notifications:
 * decLA: decrement left actions
 * impWp: improved weapon
 * setPl: set plague
 * remPl: remove plague
 * getRs: player gets a resource
 * addCa: add card
 * killP: kill player
 * 
 * @author Philipp, Fabian, Julia, Ronja
 */

@SuppressWarnings("serial")
public class Player extends Observable implements PlayerI, Serializable {

	/**
	 * Role a player can choose.
	 */
	private final Role role;
	
	/**
	 * Every player gets an unique ID.
	 */
	private int playerId;
	
	/**
	 * Color ID of player's chosen spaceship.
	 */
	private final int colorId;
	
	/**
	 * Name a player has chosen.
	 */
	private final String name;
	
	/**
	 * Planet a player is located.
	 */
	private int planetId;
	
	/**
	 * Number of left actions a player has.
	 */
	private int leftActions;
	
	/**
	 * Number of improved weapons a player has left.
	 */
	private int improvedWeaponLeft = 0;
	
	/**
	 * Resources give information about resources a player possesses.
	 * True if player possesses a resource.
	 * Sequence of resources: yellow, green, blue, red
	 */
	private boolean[] resources = {false,false,false,false};
	
	/**
	 * Plagues a player has. True if a player has a certain plague.
	 * Sequence of plagues: skeletons, zombies, mummies, ghosts
	 */
	private boolean[] plagues = {false,false,false,false};
	
	/**
	 * Player cards a player possesses.
	 */
	private Hand playerHand;

	/**
	 * The ID of the last visited planet. Used by the Evil AI.
	 */
	private int lastPlanetId;


	/**
	 * Instantiates attributes.
	 * @param id Player ID
	 * @param role Role a player has chosen.
	 * @param color Color of player's spaceship.
	 * @param name Name player has chosen.
	 * @param startPlanet planet where all players start at the beginning
	 */
	public Player(int id, Role role, int color, String name, int startPlanet) {

		this.playerId = id;
		this.role = role;
		this.colorId = color;
		this.name = name;
		playerHand = new Hand();
		planetId = startPlanet;
		lastPlanetId = startPlanet;

	}

	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see player.IPlayer#getRole()
	 */
	@Override
	public Role getRole() {
		return role;
	}

	/* (non-Javadoc)
	 * @see player.IPlayer#getPlanetId()
	 */
	@Override
	public int getPlanetId() {
		return planetId;
	}
	
	/* (non-Javadoc)
	 * @see player.IPlayer#getLeftActions()
	 */
	@Override
	public int getLeftActions(){
		return leftActions;
	}
	
	/* (non-Javadoc)
	 * @see player.IPlayer#decreaseLeftActions()
	 */
	@Override
	public void decreaseLeftActions() {
		leftActions--;
		setChanged();
		notifyObservers("decLA" + leftActions);
	}
	
	/* (non-Javadoc)
	 * @see player.IPlayer#resetLeftActions()
	 */
	@Override
	public void resetLeftActions() {
		leftActions = role.getnActions();
	}
	
	/* (non-Javadoc)
	 * @see player.IPlayer#setPlanetId(int)
	 */
	@Override
	public void setPlanetId(int id) {
		lastPlanetId = planetId;
		this.planetId = id;
	}
	

	/**
	 * Getter for the ID of the last visited planet.
	 * 
	 * @return The ID as an Integer.
	 */
	@Override
	public int getLastPlanetId() {
		return lastPlanetId;
	}

	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#setImprovedWeapon()
	 */
	@Override
	public void setImprovedWeapon() {
		this.improvedWeaponLeft  = 3;
		this.setChanged();
		notifyObservers("impWp");
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#getImprovedWeaponLeft()
	 */
	@Override
	public int getImprovedWeaponLeft() {
		return improvedWeaponLeft;
	}	
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#useImprovedWeapon()
	 */
	@Override
	public void useImprovedWeapon() {
		this.improvedWeaponLeft--;
		this.setChanged();
		notifyObservers("impWp");
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#getId()
	 */
	@Override
	public int getId() {
		return playerId;
	}

	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#getColorId()
	 */
	@Override
	public int getColorId() {
		return colorId;
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#getPlague(int)
	 */
	@Override
	public boolean hasPlague(int colorId) {
		return plagues[colorId];
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#setPlague(int)
	 */
	@Override
	public void setPlague(int colorId) {
		this.plagues[colorId] = true;
		setChanged();
		notifyObservers("setPl"+colorId);
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#removePlague(int)
	 */
	@Override
	public void removePlague(int colorId) {
		this.plagues[colorId] = false;
		setChanged();
		notifyObservers("setPl"+colorId);
		
	}
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#getHand()
	 */
	@Override
	public Hand getHand() {
		return playerHand;
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#setHand(java.util.LinkedList)
	 */
	@Override
	public void setHand(Hand hand) {
		this.playerHand = hand;
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#addCard(cards.Card)
	 */
	@Override
	public void addCard(Card card) {
		this.playerHand.add(card);
		setChanged();
		notifyObservers("addCa"+(playerHand.size()-1));
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#addObserverX(java.util.Observer)
	 */
	@Override
	public void addObserverX(Observer playerPanel) {
		addObserver(playerPanel);
	}

	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#getResource(int)
	 */
	@Override
	public boolean getResource(int colorId) {
		return resources[colorId];
	}

	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#setResource(int)
	 */
	@Override
	public void setResource(int colorId) {
		resources[colorId] = true;
		setChanged();
		notifyObservers("getRs"+colorId);
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#removeResource(int)
	 */
	@Override
	public void removeResource(int colorId) {
		resources[colorId] = false;
		setChanged();
		notifyObservers("getRs"+colorId);
	}

	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#removeCard(cards.Card)
	 */
	@Override
	public void removeCard(Card card) {
		playerHand.remove(card);
	}


	/*
	 * (non-Javadoc)
	 * @see player.PlayerI#killPlayer()
	 */
	public void killPlayer(){
		setChanged();
		notifyObservers("killP");
	}

	/* (non-Javadoc)
	 * @see player.PlayerI#setId(int)
	 */
	@Override
	public void setId(int id) {
		this.playerId = id;
		
	}

}
