package player;

import java.io.Serializable;
import java.util.ArrayList;

import cards.Card;
import cards.FindBase;
import cards.FindWarptor;
import cards.PlayerCard;
import game.board.PlanetModelI;

/**
 * Role Contains the ruleset for each action might be overriden by a playerrole.
 * <br>
 * 
 * ERROR CODES: <br>
 * ROLLA: not enough Left Actions <br>
 * ROLNC: wrong number of thrown cards <br>
 * ROLHB: has already base <br>
 * ROLNB: planet doesn't have a base <br>
 * ROLPL: wrong planet <br>
 * ROLCT: wrong card type <br>
 * ROLNP: not a neighbor planet <br>
 * ROLUC: no undeads of this color <br>
 * ROLCC: wrong card color <br>
 * ROLBL: Planet is blocked by KI <br>
 * ROLRS: no Resource<br>
 * ROLHL: no Healer<br>
 * ROLTC: Cards were thrown <br>
 * ROLTT: need Turn to trade <br>
 * ROLHC getplayer has too many handcards <br>
 * 
 * @author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public abstract class Role implements Serializable {

	/**
	 * Getter for number of actions a player has at the beginning of a turn.
	 * 
	 * @return Number of actions. Normal players have 4 actions.
	 */
	public int getnActions() {
		return 4;
	}

	/**
	 * Checks if number of cards in lastusedcard is 1.
	 * 
	 * @param lastUsedCard
	 *            List of last used cards. (Cards that have been thrown away by
	 *            a player).
	 * @return True if only one card is used.
	 * @throws Exception
	 */
	public boolean isOneCard(ArrayList<Card> lastUsedCard) { // TODO:RENAME
		if (lastUsedCard.size() == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks whether a role has a fixed number of actions.
	 *
	 * 
	 * @return True if the role has a fixed number of actions or false if not
	 *         (untoaster).
	 */
	public boolean hasNActions() {
		return true;
	}

	/**
	 * Checks whether role is allowed to drag undead.
	 * @return False if it is a normal role, true if role is Untoaster. 
	 */
	public boolean mayDragUndead() {
		return false;
	}

	/**
	 *  Checks whether role is allowed to drag a spaceship.
	 * @return False  a true if role Untoaster ,  if it is is normal role. 
	 */
	public boolean mayDragSpaceship() {
		return true;
	}

	/**
	 * Checks whether a moved undead is moved correctly. Undead can only be moved to a neighbor planet and only by
	 * Untoaster.
	 * @param sPlanet Planet an undead is moved from.
	 * @param ePlanet Planet an undead is moved to.
	 * @param colorId Color of undead being moved.
	 * @param neighbors List of planet's neighbors.
	 * @return True if movement is allowed. 
	 * @throws Exception
	 */
	public boolean moveUndead(PlanetModelI sPlanet, PlanetModelI ePlanet, int colorId, ArrayList<Integer> neighbors)
			throws Exception {
		return false;
	}

	/**
	 * buildBase ruleset. Checks whether base is build correctly. Player needs
	 * to be on planet where base is build. Players need to use card of the
	 * planet where they are or use the action card "FindBase" to build a base.
	 * 
	 * @param cPlayer
	 *            Player who build base.
	 * @param planet
	 *            Planet where base is build.
	 * @param lastUsedCard
	 *            Card(s) that have been thrown away.
	 * @return True if base is build correctly.
	 * @throws Exception
	 */
	public boolean buildBase(PlayerI cPlayer, PlanetModelI planet, ArrayList<Card> lastUsedCard) throws Exception {

		if (((cPlayer.getLeftActions() < 1)) && !(lastUsedCard.get(0) instanceof FindBase)) {
			throw new Exception("ROLLA"); // #ROLLA: not enough Left Actions
		}

		if (!isOneCard(lastUsedCard)) {
			throw new Exception("ROLNC"); // #ROLNC wrong number of cards
		}

		if (planet.hasBase()) {
				throw new Exception("ROLHB"); // #ROLHB has already base
		} 
		
		if(lastUsedCard.get(0) instanceof FindBase) {
			return true;
		}

		if (!(lastUsedCard.get(0) instanceof PlayerCard)) {
			throw new Exception("ROLCT"); // #ROLCT wrong card type
		}

		PlayerCard pCard = (PlayerCard) lastUsedCard.get(0); // cast last card
																// to playercard

		if ((cPlayer.getPlanetId() != planet.getId()) || (pCard.getPlanetId() != planet.getId())) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}

		return true;
	}

	/**
	 * moveBySpaceShip ruleset. Checks whether a spaceship is moved correctly.
	 * Spaceships can only be moved to neighor planets.
	 * @param cPlayer Player who's turn it is.
	 * @param aPlayer
	 *            Player who's spaceship is dragged.
	 * @param startP
	 *            Planet a spaceship is moved from.
	 * @param endP
	 *            Planet a spaceship is moved to.
	 * @param neighbors
	 *            Neighbors of start planet.
	 * @return True if spaceship is moved correctly.
	 * @throws Exception
	 */
	public boolean moveBySpaceShip(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP,
			ArrayList<Integer> neighbors) throws Exception {
		if (((cPlayer != aPlayer) || (cPlayer.getLeftActions() < 1))) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}
		if (endP.isBlocked()) {
			throw new Exception("ROLBL"); // #ROLBL Planet is blocked
		}

		for (int i : neighbors) {
			if (i == endP.getId()) {
				return true;
			}
		}
		throw new Exception("ROLNP"); // #ROLNP not a neighbor planet
	}


	/**
	 * warpToBase ruleset. Checks whether a warp to a base is correct.
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param aPlayer
	 *            action player, player who is moved.
	 * @param startP
	 *            Planet action player is moved from.
	 * @param endP
	 *            Planet where action player is moved to.
	 * @return True if action is correct.
	 * @throws Exception
	 */
	public boolean warpToBase(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP)
			throws Exception {
		if (((cPlayer != aPlayer) || (cPlayer.getLeftActions() < 1))) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}

		if (!(startP.hasBase() && endP.hasBase())) {
			throw new Exception("ROLNB"); // #ROLNB planet doesnt have a
		}

		if (endP.isBlocked()) {
			throw new Exception("ROLBL"); // #ROLBL Planet is blocked
		}

		if (aPlayer.getPlanetId() != startP.getId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}
		return true;
	}

	/**
	 * warpToPlanet ruleset. Checks whether a warp to a planet is correct.
	 * Players can warp to planets if they use a planet card and warp to that
	 * specific planet. Players can use card they are currently and move to any
	 * other planet. Warpmaster can move player to player.
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param aPlayer
	 *            action player, player who is moved.
	 * @param startP
	 *            Planet a spaceship is moved from.
	 * @param endP
	 *            Planet where action player is moved to.
	 * @param lastUsedCard
	 *            List of last used cards. (Cards that have been thrown away by
	 *            a player).
	 * @return true if action is valid
	 * @throws Exception
	 */
	public boolean warpToPlanet(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP,
			ArrayList<Card> lastUsedCard) throws Exception {
		if (endP.isBlocked()) {
			throw new Exception("ROLBL"); // #ROLBL Planet is blocked
		}

		if (isOneCard(lastUsedCard) && lastUsedCard.get(0) instanceof FindWarptor) {
			return true;
		}

		if ((cPlayer != aPlayer) || (cPlayer.getLeftActions() < 1)) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}

		if (!isOneCard(lastUsedCard)) {
			throw new Exception("ROLNC"); // #ROLNC wrong number of cards
		}

		if (!(lastUsedCard.get(0) instanceof PlayerCard)) {
			throw new Exception("ROLCT"); // #ROLCT card type
		}

		PlayerCard card = (PlayerCard) lastUsedCard.get(0);
		if ((startP.getId() != card.getPlanetId()) && (endP.getId() != card.getPlanetId())) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}

		return true;
	}

	/**
	 * shareKnowledge ruleset. Checks whether a card is swapped correctly.
	 * ShareKnowledge is only allowed if players are on the planet of the
	 * planet's card the are swapping.
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param givePlayer
	 *            Player who give the card to another player.
	 * @param getPlayer
	 *            Player the card is given to.
	 * @param lastUsedCard
	 *            List of last used cards. (Cards that have been thrown away by
	 *            a player).
	 * @return True if shareKnowledge is correct.
	 * @throws Exception
	 */
	public boolean shareKnowledge(PlayerI cPlayer, PlayerI givePlayer, PlayerI getPlayer, ArrayList<Card> lastUsedCard,
			Card card) throws Exception {
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}
		if (((cPlayer != givePlayer) && (cPlayer != getPlayer))) {
			throw new Exception("ROLTT"); // #ROLTT traders turn needed
		}
		if (lastUsedCard.size() != 0) {
			throw new Exception("ROLNC"); // #ROLNC wrong number of cards
		}
		if (givePlayer.getPlanetId() != getPlayer.getPlanetId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}
		if (!(card instanceof PlayerCard)) {
			throw new Exception("ROLCT"); // #ROLCT wrong card type
		}

		if (getPlayer.getHand().size() >= 7) {
			throw new Exception("ROLHC"); // #ROLHC getplayer has too many
											// handcards
		}

		PlayerCard pCard = (PlayerCard) card;
		if ((pCard.getPlanetId() != givePlayer.getPlanetId()) || (pCard.getPlanetId() != getPlayer.getPlanetId())) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}
		return true;
	}

	/**
	 * Checks whether a undead is saved correctly.
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param cPlanet
	 *            current player's planet
	 * @param undeadColor
	 *            Color of undead that is killed.
	 * @return True if kill undead is correct.
	 * @throws Exception
	 */
	public boolean killUndead(PlayerI cPlayer, PlanetModelI cPlanet, int undeadColor) throws Exception {
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}

		if (cPlayer.getPlanetId() != cPlanet.getId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}

		if (cPlanet.getUndeads(undeadColor) < 1) {
			throw new Exception("ROLUC"); // #ROLUC no undeads of this color
		}

		return true;
	}

	/**
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param planet
	 *            Planet where dark column is removed.
	 * @param lastUsedCard
	 *            List of last used cards. (Cards that have been thrown away by
	 *            a player).
	 * @return True if dark column is removed correctly.
	 * @throws Exception
	 */
	public boolean removeDarkColumn(PlayerI cPlayer, PlanetModelI planet, ArrayList<Card> lastUsedCard)
			throws Exception {
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}
		if (cPlayer.getPlanetId() != planet.getId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong Planet
		}

		if (lastUsedCard.size() != 2) {
			throw new Exception("ROLNC"); // #ROLNC wrong number of thrown cards
		}

		if (lastUsedCard.get(0) instanceof PlayerCard && lastUsedCard.get(1) instanceof PlayerCard) {
			PlayerCard c1 = (PlayerCard) lastUsedCard.get(0);
			PlayerCard c2 = (PlayerCard) lastUsedCard.get(1);
			if (c1.getPlanetColor() == c2.getPlanetColor() && c1.getPlanetColor() == planet.getColor()) {
				return true;
			} else {
				throw new Exception("ROLCC"); // #ROLCC wrong card color
			}
		} else {
			throw new Exception("ROLCT"); // #ROLCT wrong card type
		}
	}

	/**
	 * developWeapon ruleset
	 * 
	 * TODO: check for already developed weapon?
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param cPlanet
	 *            current player's planet
	 * @param weaponColor
	 *            Color weapon that is developed
	 * @param lastUsedCard
	 *            List of last used cards. (Cards that have been thrown away by
	 *            a player).
	 * @return True if weapon is developed correctly.
	 * @throws Exception
	 */
	public boolean developWeapon(PlayerI cPlayer, PlanetModelI cPlanet, int weaponColor, ArrayList<Card> lastUsedCard)
			throws Exception {
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}

		if (!cPlanet.hasBase()) {
			throw new Exception("ROLNB"); // #ROLNB: planet doesn't have a base
		}

		if (lastUsedCard.size() != 5) {
			throw new Exception("ROLNC"); // #ROLNC: wrong number of cards
		}

		for (Card i : lastUsedCard) {
			if (i instanceof PlayerCard) {
				if (((PlayerCard) i).getPlanetColor() != weaponColor) {
					throw new Exception("ROLCC"); // #ROLCC wrong card color
				}
			} else {
				throw new Exception("ROLCT"); // #ROLCT: wrong card type
			}
		}
		return true;
	}

	/**
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param planet
	 *            current player's planet
	 * @param lastUsedCard
	 *            List of last used cards. (Cards that have been thrown away by
	 *            a player).
	 * @return True if buying improved weapon is correct.
	 * @throws Exception
	 */
	public boolean buyImprovedWeapon(PlayerI cPlayer, PlanetModelI planet, ArrayList<Card> lastUsedCard)
			throws Exception {
		if (cPlayer.getPlanetId() != planet.getId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong Planet
		}
		if (!planet.hasBase()) {
			throw new Exception("ROLNB"); // #ROLNB: no base
		}
		if (!(isOneCard(lastUsedCard))) {
			throw new Exception("ROLNC"); // #ROLNC: wrong number of cards
		}

		if (!(lastUsedCard.get(0) instanceof PlayerCard)) {
			throw new Exception("ROLCT"); // #ROLCT: wrong card type
		}

		PlayerCard c1 = (PlayerCard) lastUsedCard.get(0);
		if (planet.getColor() != c1.getPlanetColor()) {
			throw new Exception("ROLCC"); // #ROLCC wrong card color
		}

		return true;
	}

	/**
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param planet
	 *            current player's planet
	 * @param colorId
	 *            Color of mined resource
	 * @return True if mining resource is correctly.
	 * @throws Exception
	 */
	public boolean mineResource(PlayerI cPlayer, PlanetModelI planet, int colorId) throws Exception {
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA no left action
		}

		if (cPlayer.getPlanetId() != planet.getId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}

		if (!planet.hasResource(colorId)) {
			throw new Exception("ROLRS"); // #ROLRS no Resource
		}

		return true;
	}

	/**
	 * 
	 * @param cPlayer
	 *            current player, player which's turn it is.
	 * @param planet
	 *            current player's planet
	 * @param colorId
	 *            resource's color
	 * @return True if recruiting healer is correct.
	 * @throws Exception
	 */
	public boolean recruiteHealer(PlayerI cPlayer, PlanetModelI planet, int colorId) throws Exception {
		if (!planet.hasBase()) {
			throw new Exception("ROLNB"); // #ROLNB: no base
		}

		if (cPlayer.getPlanetId() != planet.getId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}

		if (!cPlayer.getResource(colorId)) {
			throw new Exception("ROLRS"); // #ROLRS no resource
		}

		return true;
	}

	/**
	 * 
	 * @param cPlayer current player, player which's turn it is.
	 * @param planet current player's planet
	 * @param colorId healer's color
	 * @return True if healing happened correctly.
	 * @throws Exception
	 */
	public boolean healPlague(PlayerI cPlayer, PlanetModelI planet, int colorId) throws Exception {
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA no left action
		}

		if (cPlayer.getPlanetId() != planet.getId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}

		
		if (!planet.hasBase()) {
			throw new Exception("ROLNB"); // #ROLNB: no base
		}

		if (!planet.hasHealer(colorId)) {
			throw new Exception("ROLHL"); // #ROLHL: no Healer
		}

		return true;
	}

	/**
	 * Return String of text ID for a role.
	 * @return String with ID.
	 */
	public abstract String getTextId();

}
