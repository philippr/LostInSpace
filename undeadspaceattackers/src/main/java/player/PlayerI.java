package player;

import java.util.Observer;

import cards.Card;
import utils.Hand;

/**
 *
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public interface PlayerI {

	/**
	 * Getter for Role.
	 * 
	 * @return Role of player.
	 */
	Role getRole();

	/**
	 * Getter for planet ID.
	 * 
	 * @return Planet where player is.
	 */
	int getPlanetId();

	/**
	 * Getter for actions a player has left.
	 * 
	 * @return Player's left actions.
	 */
	int getLeftActions();

	/**
	 * Decreases left actions minus 1.
	 */
	void decreaseLeftActions();

	/**
	 * Sets left actions to 4.
	 */
	void resetLeftActions();

	/**
	 * Sets new planet ID if a player has moved to another planet.
	 * 
	 * @param id
	 */
	void setPlanetId(int id);

	/**
	 * Sets number of improved weapons a player has left.
	 */
	void setImprovedWeapon();

	/**
	 * Number of improved weapons a player has left.
	 * 
	 * @return left improved weapons
	 */
	int getImprovedWeaponLeft();

	/**
	 * Getter for players spaceship color.
	 * 
	 * @return spaceship color.
	 */
	int getColorId();

	/**
	 * Getter for resource of a given color a player possesses. Sequence of
	 * resources: yellow, green, blue, red
	 * 
	 * @param colorId
	 *            Color of resource.
	 * @return True if player possesses resource.
	 */
	public boolean getResource(int colorId);

	/**
	 * Removes resource of given color. Sets corresponding boolean false.
	 * 
	 * @param colorId
	 *            Color of resource. Sequence of resources: yellow, green, blue,
	 *            red.
	 */
	public void removeResource(int colorId);

	/**
	 * Sets resource of given color true.
	 * 
	 * @param resource
	 *            Color of resource. Sequence of resources: yellow, green, blue,
	 *            red.
	 */
	public void setResource(int resource);

	/**
	 * Getter for plague of a given color.
	 * 
	 * @param colorId
	 *            Color of plague. Sequence of resources: yellow, green, blue,
	 *            red.
	 * @return True if player has a plague.
	 */
	public boolean hasPlague(int colorId);

	/**
	 * Setter for plague of a given color.
	 * @param colorId Color of plague. Sequence of resources: yellow, green, blue,
	 *            red.
	 */
	public void setPlague(int colorId);

	/**
	 * Adds an Observer.
	 * @param playerPanel Observer that's added.
	 */
	void addObserverX(Observer playerPanel);

	/**
	 * Removes plague. Sets corresponding boolean in plague array false.
	 * @param colorId Color of plague. Sequence of resources: yellow, green, blue,
	 *            red.
	 */
	public void removePlague(int colorId);

	/**
	 * Getter for playerID.
	 * @return playerId
	 */
	public int getId();

	/**
	 * Getter for cards player processes.
	 * @return List of all cards a player possesses.
	 */
	Hand getHand();

	/**
	 * Sets player cards.
	 * @param cards Cards given to a player.
	 */
	void setHand(Hand hand);

	/**
	 * Adds a card to hand cards a player possesses.
	 * @param card Card given to player.
	 */
	void addCard(Card card);

	/**
	 * Getter for player's name.
	 * @return Player's name.
	 */
	String getName();

	/**
	 * Decreases left improved weapons. 
	 */
	void useImprovedWeapon();

	/**
	 * Removes card from players hand.
	 * @param card Card that is removed.
	 */
	void removeCard(Card card);

	/**
	 * Notifies observer if player is killed.
	 */
	public void killPlayer();

	/**
	 * Setter for the playerId
	 * 
	 * @param playerId
	 */
	void setId(int playerId);

	int getLastPlanetId();


}