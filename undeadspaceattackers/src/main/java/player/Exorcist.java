package player;

import game.board.PlanetModelI;

/**
 * Exorcist kills all of one species at once.
 * If a weapon is developed all undead are removed where the exorcist is located.
 * @author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public class Exorcist extends Role {
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#killUndead(player.PlayerI, game.board.PlanetModelI, int)
	 */
	public boolean killUndead(PlayerI cPlayer, PlanetModelI cPlanet, int undeadColor) throws Exception{
		if(cPlayer.getLeftActions() > 0 ) {
			if(cPlayer.getPlanetId() == cPlanet.getId()) {
				if(cPlanet.getUndeads(undeadColor) > 0) {
					return true;
				} else {
					throw new Exception("ROLUC"); //#ROLUC no undeads of this color
				}
			} else {
				throw new Exception("ROLPL"); //#ROLPL wrong planet
			}
		} else {
			throw new Exception("ROLLA"); //#ROLLA not enough Left Actions
		}
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#getTextId()
	 */
	@Override
	public String getTextId() {
		return "EX";
	}
}
