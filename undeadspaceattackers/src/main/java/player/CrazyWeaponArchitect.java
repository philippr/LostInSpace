package player;

import java.util.ArrayList;

import cards.Card;
import cards.PlayerCard;

/**
 * CrazyWeaponArchitect can share any card with someone on a planet, even if its not his turn, as long he is part of the trade.
 * @author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public class CrazyWeaponArchitect extends Role { 
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#shareKnowledge(player.PlayerI, player.PlayerI, player.PlayerI, java.util.ArrayList, cards.Card)
	 */
	@Override
	public boolean shareKnowledge(PlayerI cPlayer, PlayerI givePlayer, PlayerI getPlayer, ArrayList<Card> lastUsedCard, Card card) throws Exception {
		
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}
		if ((cPlayer != givePlayer) && (cPlayer != getPlayer)) {
			throw new Exception("ROLTT"); // #ROLLA not enough Left Actions
		}
		if (lastUsedCard.size() != 0) {
			throw new Exception("ROLNC"); // #ROLNC wrong number of cards
		}
		if (!(card instanceof PlayerCard)) {
			throw new Exception("ROLCT"); // #ROLCT wrong card type
		}
		if (givePlayer.getPlanetId() != getPlayer.getPlanetId()) {
			throw new Exception("ROLPL"); // #ROLPL wrong planet
		}
		if(getPlayer.getHand().size() >= 7) {
			throw new Exception("ROLHC"); //#ROLHC getplayer has too many handcards
		}
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#getTextId()
	 */
	@Override
	public String getTextId() {
		return "WA";
	}
}
