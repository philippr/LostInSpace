package player;

import java.util.ArrayList;

import cards.Card;
import cards.PlayerCard;
import game.board.PlanetModelI;

/**
 * Weapontrader needs only 4 cards to discover a new weapon.
 * @author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public class Weapontrader extends Role {
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#developWeapon(player.PlayerI, game.board.PlanetModelI, int, java.util.ArrayList)
	 */
	@Override
	public boolean developWeapon(PlayerI cPlayer, PlanetModelI cPlanet, int weaponColor, ArrayList<Card> lastUsedCard)
			throws Exception {
		if (cPlayer.getLeftActions() < 1) {
			throw new Exception("ROLLA"); // #ROLLA not enough Left Actions
		}

		if (!cPlanet.hasBase()) {
			throw new Exception("ROLNB"); // #ROLNB: planet doesn't have a base
		}

		if (lastUsedCard.size() != 4) {
			throw new Exception("ROLNC"); // #ROLNC: wrong number of cards
		}

		for (Card i : lastUsedCard) {
			if (i instanceof PlayerCard) {
				if (((PlayerCard) i).getPlanetColor() != weaponColor) {
					throw new Exception("ROLCC"); // #ROLCC wrong card color
				}
			} else {
				throw new Exception("ROLCT"); // #ROLCT: wrong card type
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#getTextId()
	 */
	@Override
	public String getTextId() {
		return "WT";
	}
}