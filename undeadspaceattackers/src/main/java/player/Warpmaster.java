package player;

import java.util.ArrayList;

import cards.Card;
import cards.FindWarptor;
import cards.PlayerCard;
import game.board.PlanetModelI;

/**
 * Warpmaster is capable to do all the moving actions for someone else in one of his own actions
 * @author Philipp, Fabian, Julia, Ronja
 */
@SuppressWarnings("serial")
public class Warpmaster extends Role{

	/* (non-Javadoc)
	 * @see player.Role#moveBySpaceShip(player.PlayerI, player.PlayerI, board.PlanetModelI, board.PlanetModelI, java.util.ArrayList)
	 */
	@Override
	public boolean moveBySpaceShip(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP, ArrayList<Integer> neighbours) throws Exception {
		if (endP.isBlocked()) {
			throw new Exception("ROLBL"); // #ROLBL Planet is blocked
		}
		
		if(((cPlayer.getLeftActions() > 0))) {
			for(int i : neighbours) {
				if(i == endP.getId()) {
					return true;
				}
			}
			throw new Exception("ROLNP"); //#ROLNP not a neighbor planet
		} else {
			throw new Exception("ROLLA"); //#ROLLA not enough Left Actions
		}
	}


	/* (non-Javadoc)
	 * @see player.Role#warpToBase(player.PlayerI, player.PlayerI, board.PlanetModelI, board.PlanetModelI)
	 */
	@Override
	public boolean warpToBase(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP) throws Exception {
		if (endP.isBlocked()) {
			throw new Exception("ROLBL"); // #ROLBL Planet is blocked
		}
		
		if(cPlayer.getLeftActions() > 0) {
			if(startP.hasBase() && endP.hasBase()) {
				if(aPlayer.getPlanetId() == startP.getId()) {
					return true;
				} else {
					throw new Exception("ROLPL"); //#ROLPL wrong planet
				}
			} else {
				throw new Exception("ROLNB"); //#ROLNB planet doesnt have a base
			}
		} else {
			throw new Exception("ROLLA"); //#ROLLA not enough Left Actions
		}
	}



	/* (non-Javadoc)
	 * @see player.Role#warpToPlanet(player.PlayerI, player.PlayerI, board.PlanetModelI, board.PlanetModelI, java.util.ArrayList)
	 */
	@Override
	public boolean warpToPlanet(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP, ArrayList<Card> lastUsedCard) throws Exception { 
		if (endP.isBlocked()) {
			throw new Exception("ROLBL"); // #ROLBL Planet is blocked
		}
		
		if(isOneCard(lastUsedCard) && lastUsedCard.get(0) instanceof FindWarptor) {
			return true;
		}
		
		if(cPlayer.getLeftActions() > 0) {
			if (aPlayer.getPlanetId() == startP.getId() && endP.hasPlayers()) {
				return true;
			}
			else if(isOneCard(lastUsedCard) && lastUsedCard.get(0) instanceof PlayerCard) {
				PlayerCard card = (PlayerCard) lastUsedCard.get(0);
				if((startP.getId() == card.getPlanetId()) || (endP.getId() == card.getPlanetId())) {
					return true;
				} else {
					throw new Exception("ROLPL"); //#ROLPL wrong planet
				}
			} else {
				throw new Exception("ROLPL"); //#ROLPL wrong planet
			}
		} else {
			throw new Exception("ROLLA"); //#ROLLA not enough Left Actions
		}
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#getTextId()
	 */
	@Override
	public String getTextId() {
		return "WM";
	}
	
	
}
