package player;

import java.util.ArrayList;

import cards.Card;

import game.board.PlanetModelI;

/**
 * Untoaster is a fifth player. It is an antagonist playing against the normal players.
 * Untoaster has no actions and no hand cards. Untoaster does not take cards from pile.
 * Only Untoster is allowed to drag undeads.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class Untoaster extends Role {
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#mayDragUndead()
	 */
	@Override
	public boolean mayDragUndead() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#mayDragSpaceship()
	 */
	@Override
	public boolean mayDragSpaceship() {
		return false;
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#getnActions()
	 */
	@Override
	public int getnActions() {
		return 0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see player.Role#hasNActions()
	 */
	@Override
	public boolean hasNActions() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#moveUndead(game.board.PlanetModelI, game.board.PlanetModelI, int, java.util.ArrayList)
	 */
	@Override
	public boolean moveUndead(PlanetModelI sPlanet, PlanetModelI ePlanet, int colorId, ArrayList<Integer> neighbours) throws Exception {
		if(sPlanet.getUndeads(colorId) < 1) {
			throw new Exception("ROLUC"); //#ROLUC no undead of this color
		}
		for(int i : neighbours) {
			if(i == ePlanet.getId()) {
				return true;
			}
		}

		throw new Exception("ROLNP"); //#ROLNP not a neighbourplanet		
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#buildBase(player.PlayerI, game.board.PlanetModelI, java.util.ArrayList)
	 */
	@Override
	public boolean buildBase(PlayerI cPlayer, PlanetModelI planet, ArrayList<Card> lastUsedCard) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#moveBySpaceShip(player.PlayerI, player.PlayerI, game.board.PlanetModelI, game.board.PlanetModelI, java.util.ArrayList)
	 */
	@Override
	public boolean moveBySpaceShip(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP,
			ArrayList<Integer> neighbours) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#warpToBase(player.PlayerI, player.PlayerI, game.board.PlanetModelI, game.board.PlanetModelI)
	 */
	@Override
	public boolean warpToBase(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#warpToPlanet(player.PlayerI, player.PlayerI, game.board.PlanetModelI, game.board.PlanetModelI, java.util.ArrayList)
	 */
	@Override
	public boolean warpToPlanet(PlayerI cPlayer, PlayerI aPlayer, PlanetModelI startP, PlanetModelI endP,
			ArrayList<Card> lastUsedCard) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#shareKnowledge(player.PlayerI, player.PlayerI, player.PlayerI, java.util.ArrayList, cards.Card)
	 */
	@Override
	public boolean shareKnowledge(PlayerI cPlayer, PlayerI givePlayer, PlayerI getPlayer, ArrayList<Card> lastUsedCard, Card card) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#killUndead(player.PlayerI, game.board.PlanetModelI, int)
	 */
	@Override
	public boolean killUndead(PlayerI cPlayer, PlanetModelI cPlanet, int undeadColor) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#removeDarkColumn(player.PlayerI, game.board.PlanetModelI, java.util.ArrayList)
	 */
	@Override
	public boolean removeDarkColumn(PlayerI cPlayer, PlanetModelI planet, ArrayList<Card> lastUsedCard) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#developWeapon(player.PlayerI, game.board.PlanetModelI, int, java.util.ArrayList)
	 */
	@Override
	public boolean developWeapon(PlayerI cPlayer, PlanetModelI cPlanet, int weaponColor, ArrayList<Card> lastUsedCard)
			throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#buyImprovedWeapon(player.PlayerI, game.board.PlanetModelI, java.util.ArrayList)
	 */
	@Override
	public boolean buyImprovedWeapon(PlayerI cPlayer, PlanetModelI planet, ArrayList<Card> lastUsedCard) throws Exception {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see player.Role#getTextId()
	 */
	@Override
	public String getTextId() {
		return "UN";
	}
}
