package start;

import game.board.PlayerView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import language.Language;
import language.TrText;
import utils.ImageLoader;

/**
 * SelectionView represents a View where Players can choose a role by drag and drop their
 * spaceship on a role. Furthermore they can also choose a name for their role.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class SelectionView extends StackPane {
	
	/**
	 * Checkbox for untoaster. Untoaster can be selected or not selected.
	 */
	private CheckBox fifthPlayer;
	
	/**
	 * GridPane for roles.
	 */
	private GridPane roles;

	/**
	 * Textfields for all players where players can choose a name.
	 */
	private TextField[] players;
	
	/**
	 * Button to start the game.
	 */
	private Button go;
	
	/**
	 * Button to change the view from selection to settings.
	 */
	private Button settings;
	
	/**
	 * Button to go back to main menu.
	 */
	private Button back;
	
	/**
	 * PlayerView for all spaceships.
	 */
	private PlayerView[] spaceships;
	
	/**
	 * StackPanes for roleBox
	 */
	private StackPane[] roleBox;
	
	/**
	 * GridPane for selection
	 */
	private GridPane selection;
	
	/**
	 * Pane for move
	 */
	private Pane movePane;
	
	/**
	 * VBox that contains rockets
	 */
	private VBox rocketContainer;
	
	/**
	 * VBox that contains names.
	 */
	private VBox names;

	/**
	 * Constructor creates instantiates all class variables and creates the View for Selection.
	 */
	public SelectionView() {
		
		instantiateAttributes();
		BorderPane outerBorder = new BorderPane();
		GridPane field = new GridPane();
		BorderPane innerBorder = new BorderPane();
		Title titleView = new Title(Language.getInstance().createText("PS"), back);
		TrText symbol = Language.getInstance().createText("PSY");	
		symbol.setId("text");
		TrText[] roleTexts = createRoleText();
			
		outerBorder.setTop(titleView);
		VBox vB = new VBox(field);
		vB.setAlignment(Pos.CENTER);
		outerBorder.setCenter(vB);
		// BorderPane for title, language and button
		this.getChildren().addAll(outerBorder);
	
		createPlayerViews(symbol); 	//PlayerViews
		BorderPane symbolContainer = createBorderPane(symbol);
		VBox centerSymbols = new VBox(symbolContainer);
		centerSymbols.setAlignment(Pos.CENTER);
		centerSymbols.setPrefHeight(350);
		
		// PlayerTextFields, Roles, arrange inner three boxes, Buttons, Image
		createTextFields(symbolContainer);
		createRoles(roleTexts);	
		arrangeSelection(centerSymbols, innerBorder);
		HBox buttons = createButtons(roleTexts[7]);	
		outerBorder.setBottom(buttons);
		ImageView imageView = createImage();
		
		// GridPane to arrange the Picture and the inner Boxes
		createColumnConstraints(field, innerBorder, imageView);

		this.getChildren().add(movePane);
		rocketContainer.toFront();
		movePane.toFront();
	}
	
	public void addMsgBox(Pane msgBox) {
		this.getChildren().add(msgBox);
		msgBox.toFront();
		StackPane.setAlignment(msgBox, Pos.TOP_CENTER);
	}
	
	/***
	 * creates an array of TrTexts displaying the roles that can be chosen
	 * @return roleTexts names of roles that can be chosen
	 */
	private TrText[] createRoleText() {
		TrText[] roleTexts = new TrText[8];
		roleTexts[0] = Language.getInstance().createText("PR"); //Rolle
		roleTexts[1] = Language.getInstance().createText("WM"); //Warpmaster
		roleTexts[2] = Language.getInstance().createText("EX"); //Exorcist
		roleTexts[3] = Language.getInstance().createText("CK"); //Constructor
		roleTexts[4] = Language.getInstance().createText("LO"); //Lieutenant
		roleTexts[5] = Language.getInstance().createText("WT"); //Weapontrader
		roleTexts[6] = Language.getInstance().createText("WA"); //Weaponarchitect
		roleTexts[7] = Language.getInstance().createText("PL5"); //Player5
		return roleTexts;
	}
	
	/**
	 * instantiates class attributes
	 */
	private void instantiateAttributes(){
		movePane =  new Pane();
		movePane.setMouseTransparent(true);
		movePane.setPickOnBounds(false);
		back = new Button("<");
		players = new TextField[5];
		players[0] = new TextField("Player1");
		players[1] = new TextField("Player2");
		players[2] = new TextField("Player3");
		players[3] = new TextField("Player4");
		players[4] = new TextField("Untoaster");
		
		for(TextField player : players) {
			player.setMaxWidth(350);
			player.setPrefHeight(70);
		}
		
		go = Language.getInstance().createButton("GO");
		settings = Language.getInstance().createButton("GST");
	}
	
	/**
	 * creates PlayerViews (spaceships) and arranges them
	 * @param symbol heading of box in which spaceships are
	 */
	private void createPlayerViews(Text symbol) {
		PlayerView orange = new PlayerView(0, 0);
		PlayerView magenta = new PlayerView(1, 1);
		PlayerView pink = new PlayerView(2, 2);
		PlayerView turquoise = new PlayerView(3, 3);
		

		orange.setImage(rotatePlayer(orange.getImage(), 90));
		magenta.setImage(rotatePlayer(magenta.getImage(), 90));
		pink.setImage(rotatePlayer(pink.getImage(), 90));
		turquoise.setImage(rotatePlayer(turquoise.getImage(), 90));
		
		magenta.preserveRatioProperty().set(true);
		rocketContainer = new VBox(orange, magenta, pink, turquoise);
		rocketContainer.setAlignment(Pos.TOP_CENTER);
		
		VBox.setVgrow(orange, Priority.ALWAYS);
		VBox.setVgrow(magenta, Priority.ALWAYS);
		VBox.setVgrow(pink, Priority.ALWAYS);
		VBox.setVgrow(turquoise, Priority.ALWAYS);
		rocketContainer.setAlignment(Pos.CENTER);
		VBox.setVgrow(symbol, Priority.ALWAYS);
		spaceships = new PlayerView[] {orange, magenta, pink, turquoise};
	}
	
	/**
	 * creates TextFields where players can choose a name
	 * @param symbolContainer 
	 */
	private void createTextFields(BorderPane symbolContainer) {
		TrText name = Language.getInstance().createText("PN");
		name.setId("text"); 
		
		for(int i=0; i<players.length;i++){
			players[i].setBackground(new Background(new BackgroundFill(main.Constants.PLAYER_COLORS[i], CornerRadii.EMPTY, Insets.EMPTY)));
		}

		names = new VBox(name, players[0],players[1],players[2],players[3]);
		names.setSpacing(5);
		names.setId("greybox");
		names.prefHeightProperty().bind(symbolContainer.heightProperty());
		names.maxHeightProperty().bind(symbolContainer.heightProperty());
		names.setPadding(new Insets(5));

	}
	
	/**
	 * creates Boxes for given role names
	 * @param roleTexts names of Roles that can be chosen
	 */
	private void createRoles(TrText[] roleTexts) {
		for(Text role : roleTexts) {
			role.setId("text");
			role.setMouseTransparent(true);
		}
		
		roleBox = new StackPane[6];		
		for(int i=0; i<6;i++){
			roleBox[i] = new StackPane(roleTexts[i+1]);
		}

		roles = new GridPane();
		roles.setVgap(4);

		RowConstraints  row;
		row = new RowConstraints();
		row.setPercentHeight(10);
		roles.getRowConstraints().add(row);
		
		for(int i = 0; i<6; i++){
			row = new RowConstraints();
			row.setPercentHeight(15);
			roles.getRowConstraints().add(row);
		}
		
		roles.add(roleTexts[0], 0, 0);
		
		for(int i=0; i<roleBox.length; i++) {
			roles.add(roleBox[i], 0, i+1);
		}

		for(Node n: roles.getChildren().filtered(p->p instanceof Pane)){
			n.setId("greybox");
			((Pane)n).setPrefHeight(90);
			((Pane)n).setMaxHeight(90);
		}

	}
	
	/**
	 * creates a BorderPane where spaceships are arranged
	 * @param symbol heading for BorderPane
	 * @return symbolcontainer BorderPane with spaceships and heading
	 */
	private BorderPane createBorderPane(TrText symbol) {
		BorderPane symbolContainer = new BorderPane();
		symbolContainer.setPadding(new Insets(5));
		symbolContainer.setTop(new VBox(symbol));
		symbolContainer.setPrefHeight(350);
		rocketContainer.setPrefHeight(350);
		symbolContainer.setCenter(new VBox(rocketContainer));
		BorderPane.setAlignment(rocketContainer, Pos.CENTER);
		symbolContainer.setId("greybox");
		return symbolContainer;
	}
	
	/**
	 * arranges spaceships, roles and names
	 * @param centerSymbols box with spaceships
	 * @param innerBorder 
	 */
	private void arrangeSelection(VBox centerSymbols, BorderPane innerBorder) {
		selection = new GridPane();
		selection.add(centerSymbols, 0, 0);
		selection.add(roles, 1, 0);
		selection.add(names, 2, 0);
		selection.setHgap(30);
		selection.setAlignment(Pos.CENTER);
		innerBorder.setCenter(selection);
		BorderPane.setAlignment(selection, Pos.CENTER_RIGHT);
	}
	
	/**
	 * creates HBox in which are buttons to start the game, go to settings or 
	 * choose untoaster
	 * @param text Text that asks whether if untoaster is wanted
	 * @return Hbox in which buttons are
	 */
	private HBox createButtons( Text text) {
		GridPane extraPlayer = new GridPane();
		fifthPlayer = new CheckBox();
		extraPlayer.add(fifthPlayer, 0, 0);
		extraPlayer.add(text, 1, 0);
		extraPlayer.setHgap(5);
		
		HBox buttons = new HBox(extraPlayer, settings, go);
		HBox.setHgrow(settings, Priority.ALWAYS);
		HBox.setHgrow(go, Priority.ALWAYS);
		buttons.spacingProperty().bind(this.widthProperty().divide(40));
		buttons.setAlignment(Pos.BOTTOM_CENTER);
		buttons.setPadding(new Insets(0,0,3,0));
		
		go.maxWidthProperty().bind(selection.widthProperty().divide(3));
		settings.maxWidthProperty().bind(selection.widthProperty().divide(3));
		return buttons;
	}
	
	/**
	 * creates an imageView where the logo is contained
	 * @return ImageView View with logo
	 */
	private ImageView createImage() {
		ImageView imageView = new ImageView(ImageLoader.getInstance().get("/start/logo.png"));
		imageView.preserveRatioProperty().set(true);
		imageView.fitHeightProperty().bind(this.heightProperty().multiply(0.8));
		imageView.fitWidthProperty().bind(this.widthProperty().multiply(0.25));
		return imageView;
	}
	
	/**
	 * create column constraints
	 * @param field
	 * @param innerBorder
	 * @param imageView
	 */
	private void createColumnConstraints(GridPane field, BorderPane innerBorder, ImageView imageView) {
		ColumnConstraints column;
		column = new ColumnConstraints();
		column.setPercentWidth(72);
		field.getColumnConstraints().add(column);
		
		column = new ColumnConstraints();
		column.setPercentWidth(28);
		field.getColumnConstraints().add(column);
		
		field.add(innerBorder,0,1);
		field.add(imageView,1,1);
		
	}
	
	/**
	 * If untoaster is selected a text field will be added. Due to that the
	 * Untoaster can choose a player name.
	 */
	public void addUntoaser(){
		names.getChildren().add(players[4]);
	}
	
	/**
	 * If untoaster was selected and gets deselected the text field for the 
	 * Untoaster will be removed.
	 */
	public void removeUntoaster(){
		names.getChildren().remove(players[4]);
	}
	
	/**
	 * Getter for names
	 * @return names VBox 
	 */
	public VBox getNames() {
		return names;
	}

	/**
	 * Getter for rocketContainer
	 * @return rocketContainer
	 */
	public VBox getShipContainer() {
		return rocketContainer;
	}

	/**
	 * Getter for move
	 * @return move Pane
	 */
	public Pane getMovePane() {
		return movePane;
	}

	/**
	 * Getter for selection
	 * @return selection GridPane
	 */
	public GridPane getSelection() {
		return selection;
	}

	/**
	 * Getter for roleBox
	 * @return roleBox StackPane
	 */
	public StackPane[] getRoleBox() {
		return roleBox;
	}
	
	/**
	 * Getter for spaceships
	 * @return spaceships all PlayerViews
	 */
	public PlayerView[] getSpaceships() {
		return spaceships;
	}

	/**
	 * Getter for back
	 * @return back Button 
	 */
	public Button getBtnBack() {
		return back;
	}

	/**
	 * Getter for settings
	 * @return settings 
	 */
	public Button getBtnSettings() {
		return settings;
	}

	/**
	 * Getter for go
	 * @return go 
	 */
	public Button getBtnGo() {
		return go;
	}

	/**
	 * Getter for players
	 * @return players Textfields for all players
	 */
	public TextField[] getPlayers() {
		return players;
	}
	

	/**
	 * Getter for roles
	 * @return roles 
	 */
	public GridPane getRoles() {
		return roles;
	}
	
	/**
	 * Getter for fifthPlayer
	 * @return fifthPlayer Checkbox for untoaster
	 */
	public CheckBox getCheckPlayer() {
		return fifthPlayer;
	}
	
	/**
	 * @return the fifthPlayer
	 */
	public CheckBox getFifthPlayer() {
		return fifthPlayer;
	}
	
	/**
	 * Rotate an image by an given angle.
	 * 
	 * @param image the image 
	 * @param angle the angle in degree
	 * @return the rotated image
	 */
	public Image rotatePlayer(Image image, int angle) {
	    ImageView iv = new ImageView(image);
	    SnapshotParameters params = new SnapshotParameters();
	    params.setFill(Color.TRANSPARENT);
	    params.setTransform(new Rotate(angle, image.getHeight() / 2, image.getWidth() / 2+10));
	    params.setViewport(new Rectangle2D(0, 0, image.getHeight(), image.getWidth()));
	    return iv.snapshot(params, null);
	}
}