package start;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import language.Language;
import utils.ImageLoader;

/**
 * View to change settings, activating features or load a different map.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 */
public class SettingsView extends StackPane {

	/**
	 * Box to activate plagues.
	 */
	private CheckBox plaguesBox;

	/**
	 * Box to activate Weapons.
	 */
	private CheckBox weaponsBox;

	/**
	 * Box to activate Abominations.
	 */
	private CheckBox abdBox;

	/**
	 * Box to activate dark columns.
	 */
	private CheckBox darkCBox;

	/**
	 * Box to activate evil AI.
	 */
	private CheckBox aiBox;

	/**
	 * Box to choose number of mass attack cards.
	 */
	private ComboBox<Integer> attackBox;

	/**
	 * Box to choose the attack strength of undeads.
	 */
	private ComboBox<Integer> strengthBox;

	/**
	 * Box to choose the allowed planet conquests.
	 */
	private ComboBox<Integer> conquestsBox;

	/**
	 * Texts for all boxes and choices.
	 */
	private Text[] settingsTexts;

	/**
	 * Constructor for SettingsView. It creates all boxes, buttons and texts and
	 * arranges them.
	 */
	public SettingsView(MenuController controller) {
		
		Button loadMap = Language.getInstance().createButton("LM");
		Button go = Language.getInstance().createButton("GO");
		Button back = new Button("<");

		controller.connectLoadButton(loadMap);
		controller.connectGoButton(go);
		controller.connectBackButton(back);
		
		
		BorderPane settings = new BorderPane();
		
		GridPane grid = new GridPane();
		instantiateAttributes(grid, loadMap, go, back);
		// BorderPane for title, language and button
		Title titleView = new Title(Language.getInstance().createText("GST"), back);
		settings.setTop(titleView);

		for (int i = 0; i < settingsTexts.length - 1; i++) {
			grid.add(settingsTexts[i], 1, i);
		}

		grid.setHgap(30);
		grid.setVgap(20);
		settings.setCenter(grid);

		// image
		Image image = ImageLoader.getInstance().get("/start/logo.png");
		ImageView imageView = new ImageView(image);
		imageView.preserveRatioProperty().set(true);
		imageView.fitHeightProperty().bind(this.heightProperty().multiply(0.7));
		imageView.fitWidthProperty().bind(this.widthProperty().multiply(0.4));

		VBox right = new VBox(imageView);
		right.setAlignment(Pos.CENTER_RIGHT);
		right.getStyleClass().add("vbox");
		settings.setRight(right);

		this.getChildren().add(settings);
	}

	/**
	 * All class attributes are instantiated.
	 * @param grid 
	 * @param back2 
	 * @param go2 
	 * @param loadMap2 
	 */
	private void instantiateAttributes(GridPane grid, Button loadMap, Button go, Button back) {
		settingsTexts = createTexts();

		conquestsBox = new ComboBox<Integer>();
		strengthBox = new ComboBox<Integer>();
		attackBox = new ComboBox<Integer>();
		aiBox = new CheckBox();
		darkCBox = new CheckBox();
		abdBox = new CheckBox();
		weaponsBox = new CheckBox();
		plaguesBox = new CheckBox();

		conquestsBox.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		conquestsBox.setValue(7); // default value
		strengthBox.getItems().addAll(1, 2, 3, 4, 5);
		strengthBox.setValue(2); // default value
		attackBox.getItems().addAll(3, 4, 5, 6);
		attackBox.setValue(4); // default value

		for (Text setting : settingsTexts) {
			setting.setId("text");
		}
		
		// GridPane for Texts and Selections
		grid.add(conquestsBox, 2, 0);
		grid.add(strengthBox, 2, 1);
		grid.add(attackBox, 2, 2);
		grid.add(aiBox, 2, 3);
		grid.add(darkCBox, 2, 4);
		grid.add(abdBox, 2, 5);
		grid.add(weaponsBox, 2, 6);
		grid.add(plaguesBox, 2, 7);
		grid.add(settingsTexts[9], 2, 8); // chosen map
		grid.add(loadMap, 2, 9);
		grid.add(go, 2, 10);

	}

	/**
	 * Creates the texts of the view.
	 * 
	 * @return the Array of the texts
	 */
	private Text[] createTexts() {
		Text[] texts = new Text[10];
		texts[0] = Language.getInstance().createText("APC"); // conquests
		texts[1] = Language.getInstance().createText("ASU"); // strength
		texts[2] = Language.getInstance().createText("UMA"); // attackCards
		texts[3] = Language.getInstance().createText("AI"); // ai
		texts[4] = Language.getInstance().createText("DC"); // darkC
		texts[5] = Language.getInstance().createText("AAB"); // abomination
		texts[6] = Language.getInstance().createText("AIW"); // weapons
		texts[7] = Language.getInstance().createText("PQ"); // Plaque
		texts[8] = Language.getInstance().createText("MN"); // mapName
		texts[9] = Language.getInstance().createText("DS"); // chosenMap
		return texts;
	}

	/**
	 * Getter for settings texts.
	 * 
	 * @return settingsTexts
	 */
	public Text[] getSettingsTexts() {
		return settingsTexts;
	}

	/**
	 * Getter for allowed conquests box.
	 * 
	 * @return conquestsBox, box where allowed conquests can be chosen.
	 */
	public ComboBox<Integer> getConquestsBox() {
		return conquestsBox;
	}

	/**
	 * Getter for strength of undead box.
	 * 
	 * @return strengthBox, box where strength of undead can be chosen.
	 */
	public ComboBox<Integer> getStrengthBox() {
		return strengthBox;
	}

	/**
	 * Getter for mass attack card box.
	 * 
	 * @return attackBox, box where number of mass attack card can be chosen.
	 */
	public ComboBox<Integer> getAttackBox() {
		return attackBox;
	}

	/**
	 * Getter for evil AI checkbox.
	 * 
	 * @return aiBox, checkboy where evil AI can be activated.
	 */
	public CheckBox getAiBox() {
		return aiBox;
	}

	/**
	 * Getter for dark column checkbox.
	 * 
	 * @return darkCBox, checkbos where dark columns can be activated.
	 */
	public CheckBox getDarkCBox() {
		return darkCBox;
	}

	/**
	 * Getter for abomination checkbox.
	 * 
	 * @return abdBox, checkbox where abominations can be activated.
	 */
	public CheckBox getAbdBox() {
		return abdBox;
	}

	/**
	 * Getter for weapons checkbox.
	 * 
	 * @return weaponsBox, checkbox where improved weapons can be activated.
	 */
	public CheckBox getWeaponsBox() {
		return weaponsBox;
	}

	/**
	 * Getter for plagues checkbox.
	 * 
	 * @return plaguesBox, checkbox where plagues can be activated.
	 */
	public CheckBox getPlaguesBox() {
		return plaguesBox;
	}
	
	/**
	 * Adds the messageBox to the view
	 * 
	 * @param msgBox the MessageBox
	 */
	public void addMsgBox(Pane msgBox) {
		this.getChildren().add(msgBox);
		msgBox.toFront();
		StackPane.setAlignment(msgBox, Pos.TOP_CENTER);
	}
}
