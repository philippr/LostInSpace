package start;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import cards.CardModel;
import game.DragContainer;
import game.GameView;
import game.board.BoardModel;
import game.board.PlayerView;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import mapGenerator.GeneratorView;
import player.Constructor;
import player.CrazyWeaponArchitect;
import player.Exorcist;
import player.Lieutenant;
import player.Player;
import player.PlayerI;
import player.Untoaster;
import player.Warpmaster;
import player.Weapontrader;
import utils.MapLoader;
import utils.MessageBoxController;
import utils.SaveLoader;

/**
 * MenuController handels action events in the main menu.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("restriction")
public class MenuController implements EventHandler<ActionEvent> {

	/**
	 * Config model contains informations about players, map and settings.
	 */
	private final ConfigModel config;
	
	/**
	 * Maploader can load a chosen map.
	 */
	private final MapLoader ml;
	
	/**
	 * Path to chosen map that is loaded.
	 */
	InputStream mapInput;

	
	/**
	 * The Generator View, generated if needed in getGeneratorView
	 */
	private GeneratorView gView;
	
	/**
	 * First menu that is shown.
	 */
	private final static MenuView view = new MenuView();
	
	/**
	 * View shown so players can choose their roles and names.
	 */
	private SelectionView selView;
	
	/**
	 * View shown so players can change settings.
	 */
	private final SettingsView setView;
	
	/**
	 * Message box Controller to add Messages.
	 */
	MessageBoxController msgBoxController;
	
	/**
	 * Root all views added to.
	 */

	private static StackPane root;

	private double mousex;
	private double mousey;
	private DragContainer draged;

	/**
	 * Constructor for MenuController, initializes config model and views
	 * activates and connect Buttons, Boxes, Drag and Drop events
	 * @param messageBoxController 
	 */
	public MenuController(MessageBoxController messageBoxController) {

		
		
		draged = new DragContainer();
		config = new ConfigModel();
		ml = new MapLoader();
		mapInput = System.class.getResourceAsStream("/maps/MapStandard.txt");
		// initiate Views
		selView = new SelectionView();
		setView = new SettingsView(this);
		msgBoxController = messageBoxController;

		// activate Buttons, Boxes, TexFields
		activateBtns();
		for (TextField player : selView.getPlayers()) {
			player.setOnAction(this);
		}

		// connect drag and drop
		connectShips();
		connectRoleBoxes();
		connectShipBox();
		connectSelView();

		// set the initial with of the left spacer
		selView.getNames().widthProperty().addListener(new ChangeListener<Number>() {
			private boolean firstResize = true;

			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth,
					Number newSceneWidth) {
				if (firstResize) {
					selView.getScene().getWindow().setWidth(selView.getScene().getWindow().getWidth() + 1);
					firstResize = false;
				}
			}
		});

	}

	/**
	 * activates Buttons from all views in the main Menu
	 */
	private void activateBtns() {
		// activate Buttons
		view.getBtnLoad().setOnAction(this);
		view.getBtnStart().setOnAction(this);
		view.getBtnQuit().setOnAction(this);
		view.getBtnMap().setOnAction(this);

		selView.getBtnBack().setOnAction(this);
		selView.getBtnSettings().setOnAction(this);
		selView.getBtnGo().setOnAction(this);
		selView.getFifthPlayer().setOnAction(this);

		// activate ColumnBoxes
		setView.getConquestsBox().setOnAction(this);
		setView.getStrengthBox().setOnAction(this);
		setView.getAttackBox().setOnAction(this);
		setView.getAiBox().setOnAction(this);
		setView.getDarkCBox().setOnAction(this);
		setView.getAbdBox().setOnAction(this);
		setView.getWeaponsBox().setOnAction(this);
		setView.getPlaguesBox().setOnAction(this);
	}

	/**
	 * Connects SelView to the controller.
	 */
	private void connectSelView() {
		
		// prevent getting stuck by using Tabulator to leave the window and focus another.
		selView.setOnMouseExited(event -> {
			if (draged.getMsg().equals("ship")) {
				draged.getDragImage().setTranslateX(0);
				draged.getDragImage().setTranslateY(0);
				draged.getParentPane().getChildren().add(draged.getDragImage());
				draged.clear();
			}
		});
		
		
		selView.setOnMouseDragged(event -> {
			if (draged.getMsg().equals("ship")) {
				draged.getDragImage().setTranslateX(draged.getDragImage().getTranslateX() + event.getSceneX() - mousex);
				draged.getDragImage().setTranslateY(draged.getDragImage().getTranslateY() + event.getSceneY() - mousey);
				mousex = event.getSceneX();
				mousey = event.getSceneY();
			}
		});

		selView.setOnMouseReleased(event -> {
			if (draged.getMsg().equals("ship")) {
				draged.getDragImage().setTranslateX(0);
				draged.getDragImage().setTranslateY(0);
				draged.getParentPane().getChildren().add(draged.getDragImage());
				draged.clear();
			}
		});
	}

	/**
	 * connect the spaceShips to the controller: activates Drag and Drop for
	 * Spaceships
	 */
	private void connectShips() {
		for (PlayerView pV : selView.getSpaceships()) {
			pV.setOnDragDetected(event -> {
				event.consume();
				pV.startFullDrag();
			});

			pV.setOnMousePressed(event -> {
				// just listen to the primary button -> reset with a right-click
				if (event.getButton() != MouseButton.PRIMARY) {
					return;
				}
				draged.set(pV, "ship", (Pane) pV.getParent(), pV.getColorId());
				mousex = event.getSceneX();
				mousey = event.getSceneY();
				
				Bounds before = pV.localToScene(pV.getBoundsInLocal());
				
				draged.getParentPane().getChildren().remove(pV);
				selView.getMovePane().getChildren().add(draged.getDragImage());

				pV.setLayoutX(0);
				pV.setLayoutY(0);
				pV.setTranslateX(before.getMinX());
				pV.setTranslateY(before.getMinY());
			});
		}
	}

	/**
	 * connect role boxes to choose a role
	 */
	private void connectRoleBoxes() {
		for (StackPane target : selView.getRoleBox()) {
			target.setOnMouseDragReleased(event -> {

				// the roleBox as a Child -> another player has this role already
				if (target.getChildren().size() == 1) {
					draged.getDragImage().setTranslateX(0);
					draged.getDragImage().setTranslateY(0);
					target.getChildren().add(draged.getDragImage());
					selView.getRoles();
					config.getRoles()[draged.getColorId()] = GridPane.getRowIndex(target) - 1;
					draged.getDragImage().toFront();
					event.consume();
					draged.clear();
				} else {
					handleException(new Exception("SELRO"), selView);
				}
			});
		}

	}

	/**
	 * connect shipBox where the spaceships are at the beginning
	 */
	private void connectShipBox() {
		selView.getShipContainer().setOnMouseDragReleased(event -> {
			draged.getDragImage().setLayoutX(0);
			draged.getDragImage().setLayoutY(0);
			draged.getDragImage().setTranslateX(0);
			draged.getDragImage().setTranslateY(0);
			selView.getShipContainer().getChildren().add(draged.getDragImage());
			config.getRoles()[draged.getColorId()] = - 1;
			draged.clear();
			event.consume();
		});
	}

	/**
	 * handels an event on MenuView, SelectionView, SettingsView
	 * 
	 * @param event ActionEvent
	 */
	public void handle(ActionEvent event) {
		loadSavedGame(event); // outsource just because McCabe
		if (event.getSource() == view.getBtnStart()) {
			switchContent(selView);
		} else if (event.getSource() == view.getBtnQuit()) {
			Platform.exit();
			System.exit(0);
		}  else if (event.getSource() == view.getBtnMap()) {
			switchContent(getGeneratorView());
		} else if (event.getSource() == selView.getBtnBack()) {
			switchContent(view);
		} else if (event.getSource() == selView.getBtnSettings()) {
			selToSet();
		} else if (event.getSource() == selView.getBtnGo()) {
			startGame(selView);
		} else if (event.getSource() == selView.getFifthPlayer()) {
			if (selView.getFifthPlayer().isSelected()) {
				selView.addUntoaser();
			}
			if (!selView.getFifthPlayer().isSelected()) {
				selView.removeUntoaster();
			}
		}
	}

	/**
	 * loads a saved Game which can be chosen from a FileChoser
	 */
	private void loadSavedGame(ActionEvent event) {
		 // outsourced just because McCabe
		if (event.getSource() != view.getBtnLoad()) {
			return;
		}
		try {
			File s = openFile("ser", view);
			SaveLoader g = new SaveLoader();
			g.load(s);
			GameView gameView = new GameView(g.getGameModel(), g.getGameStatusModel(), g.getConfigModel(), root, msgBoxController);
			switchContent(gameView);

		} catch (Exception e) {
			handleException(e, view);
		}
	}

	/**
	 * handels chosen Settings to prepare the Game
	 */
	private void handleBoxes() {

		//undeads strength
		config.getGameStatusModel().setNumbers(setView.getStrengthBox().getValue(),1);
		config.setSettingsNo(setView.getAttackBox().getValue(), 2);
		
		//leftConqueredPlanets
		config.getGameStatusModel().setNumbers((setView.getConquestsBox().getValue()),0);

		// handle activate Boxes
		config.setFlags(0,setView.getAiBox().isSelected());
		config.setFlags(1,setView.getDarkCBox().isSelected());
		config.setFlags(2,setView.getAbdBox().isSelected());
		config.setFlags(3,setView.getWeaponsBox().isSelected());
		config.setFlags(4, setView.getPlaguesBox().isSelected());
	}

	/**
	 * This Method checks whether all names are unique and fit the specified
	 * length
	 * 
	 * @throws Exception
	 */
	private void checkTextFields() throws Exception {
		String[] playerNames = new String[5];

		for (int i = 0; i < 5; i++) {
			playerNames[i] = selView.getPlayers()[i].getText().trim();

			if (!(playerNames[i].length() >= 3 && playerNames[i].length() <= 15)) {
				throw new Exception("SELWL" + i); // #Error wrong length
			}
		}

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (i != j && playerNames[i].equals(playerNames[j])) {
					throw new Exception("SELNU"); // #Error names are not unique
				}
			}
		}
	}

	/**
	 * Counts how many Roles are chosen and correctness of chosen Roles
	 * 
	 * @return
	 */
	private void checkRoles() throws Exception {

		int player = 0;
		// check which Role is chosen
		for (int i = 0; i < 4; i++) {
			if(config.getRoles()[i] != -1) {
				player++;
			}
		}

		if (player < 2) {
			throw new Exception("SELNP");
		}

		if (player < 4 && selView.getFifthPlayer().isSelected() == true) {
			throw new Exception("SELUN");
		}
	}

	/**
	 * Creates the Players based on which Spaceship is moved to a Role
	 * 
	 * @param startPlanet
	 */
	private ArrayList<PlayerI> createPlayers(int startPlanet) {
		ArrayList<PlayerI> players = new ArrayList<>(4); // get normal Players
		for (int i = 0; i < 4; i++) {
			if (config.getRoles()[i] > -1) {

				String playerName = getNames(i); // get PlayerName of Player i
				switch (config.getRoles()[i]) { // create Player with selected
												// Role
				case 0:
					players.add(new Player(players.size(), new Warpmaster(), i, playerName, startPlanet));
					break;
				case 1:
					players.add(new Player(players.size(), new Exorcist(), i, playerName, startPlanet));
					break;
				case 2:
					players.add(new Player(players.size(), new Constructor(), i, playerName, startPlanet));
					break;
				case 3:
					players.add(new Player(players.size(), new Lieutenant(), i, playerName, startPlanet));
					break;
				case 4:
					players.add(new Player(players.size(), new Weapontrader(), i, playerName, startPlanet));
					break;
				case 5:
					players.add(new Player(players.size(), new CrazyWeaponArchitect(), i, playerName, startPlanet));
					break;
				}// switch
			} // if
		} // for

		// let the players start in a random order
		Collections.shuffle(players, new Random(System.nanoTime()));
		for(int i=0; i<players.size(); i++){
			players.get(i).setId(i);
		}
		players.get(0).resetLeftActions();

		config.setnPlayers(players.size()); // Untoaster is not considered,  because of reasons
		if (selView.getCheckPlayer().isSelected() == true) { // check if 5th Player is  chosen
			players.add(new Player(-1, new Untoaster(), 4, "Untoaster", -1));
		}
		return players;
	}

	/**
	 * looks at a given Player and returns name that this player has chosen
	 * 
	 * @param i
	 *            Integer which represents number of player that is considered
	 * 
	 * @return playerName Name that considered player has chosen
	 */
	private String getNames(int i) {
		String playerName = "";
		switch (i) {
		case 0:
			playerName = selView.getPlayers()[0].getText();
			break;
		case 1:
			playerName = selView.getPlayers()[1].getText();
			break;
		case 2:
			playerName = selView.getPlayers()[2].getText();
			break;
		case 3:
			playerName = selView.getPlayers()[3].getText();
			break;
		default:
			break;
		}
		return playerName;
	}

	/**
	 * Checks Selections and starts Game, change to GameView
	 */
	private void startGame(Pane pane) {
		// int i = 0;
		try {
			checkRoles();
		} catch (Exception e) {
			handleException(e, pane);
			return;
		}
		try {
			checkTextFields();
		} catch (Exception e) {
			handleException(e, pane);
			return;
		}

		try {
			// load Map

			ml.loadMap(mapInput);

			// fill configmodel
			config.setBoardModel(new BoardModel(ml.getNeighbors(), ml.getPlanets(), ml.getStartPlanetId(), ml.getSize()[0],
					ml.getSize()[1]));
			config.setPlayers(createPlayers(ml.getStartPlanetId()));
			config.setCardModel(new CardModel(ml.getPlanets(), config.getnMassAttackCards(), config.getPlayers()));
		} catch (Exception e) {
			handleException(e, pane);
			return;
		}

		handleBoxes();

		switchContent(new GameView(config, root, msgBoxController));

	}

	/**
	 * @param fileType only Files that fit the filetype can be selected
	 * @param currView the current view
	 * @return return an File
	 * @throws Exception
	 */
	public File openFile(String fileType, Pane currView) throws Exception  {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("USA-"+(fileType=="txt"? "Map": "Save") +"(*."+ fileType + ")", "*."+fileType));
		
		// Dialogs are bugged in our Java-Version.
		// After binding the the the Dialog to our Stage, after closing, 
		// It will be impossible to resize.
		File file = fileChooser.showOpenDialog(null);		
		
		if (file != null) {
			return file;
		} else {
			throw new Exception("FILAB"); // FILAB no file chosen
		}

		

	}

	/**
	 * Checks Selections, creates Players, change to SettingsView
	 */
	private void selToSet() {
		try {
			checkRoles();

		} catch (Exception e) {
			handleException(e, selView);
			return;
		}
		try {
			checkTextFields();

		} catch (Exception e) {
			handleException(e, selView);
			return;
		}

		switchContent(setView);

	}

	/**
	 * getter for MenuView
	 * 
	 * @return MenuView
	 */
	public MenuView getMenuView() {
		return view;
	}

	public void handleException(Exception e, Pane pane) {
		if (e.getMessage().length() > 4) {
			msgBoxController.addMsg(e.getMessage().substring(0, 5));
		} else {
			e.printStackTrace();
		}

	}

	/**
	 * Change from current View to new View
	 * 
	 * @param switchTo
	 *            new view
	 */
	public static void switchContent(Pane switchTo) {
		root.getChildren().clear();
		root.getChildren().add(switchTo);

	}

	/**
	 * Change from given View to MenuView
	 *            current pane which will be removed
	 */
	public static void switchContent() {

		root.getChildren().clear();
		root.getChildren().add(view);
	}

	/**
	 * Connect the load button of the Sel-View
	 * 
	 * @param loadMap the Button
	 */
	public void connectLoadButton(Button loadMap) {
		loadMap.setOnMouseClicked(event -> {
			try {
				File map = openFile("txt", setView);
				mapInput = new FileInputStream(map);
				setView.getSettingsTexts()[9].setText(map.getName());
			} catch (Exception e) {
				handleException(e, setView);
			}
		});
	}

	/**
	 * Connect the Back button of the Sel-View
	 * 
	 * @param back the Button
	 */
	public void connectBackButton(Button back) {
		back.setOnMouseClicked(event -> {
			switchContent(selView);			
		});
	}

	/**
	 * Connect the start button of the Sel-View
	 * 
	 * @param go the Button
	 */
	public void connectGoButton(Button go) {
		go.setOnMouseClicked(event -> {
			handleBoxes();
			startGame(setView);
		});
	}

	/**
	 * setter for root
	 * 
	 * @param root
	 */
	public void setRoot(StackPane root) {
		MenuController.root = root;
	}

	public GeneratorView getGeneratorView() {
		if(gView == null){
			gView = new GeneratorView(root, msgBoxController);
		}
		return gView;
	}

	/**
	 * setter for ConfigView
	 * 
	 * @param sView
	 */
	public void setSelectionView(SelectionView sView) {
		this.selView = sView;
	}
}
