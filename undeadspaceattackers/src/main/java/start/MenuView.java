package start;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import language.Language;
import utils.ImageLoader;

/**
 * MenuView shows the main menu where a game can be started.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public class MenuView extends StackPane {

	/**
	 * Button to start the game.
	 */
	private final Button btnStart;
	
	/**
	 * Button to load a saved game.
	 */
	private final Button btnLoad;
	
	/**
	 * Button to change to map generator.
	 */
	private final Button btnMap;
	
	/**
	 * Button to quit the game.
	 */
	private final Button btnQuit;
	
	/**
	 * Title containing buttons to change Language and heading
	 */
	private final Title titleView;


	/**
	 * create the menuView for the game
	 */
	public MenuView() {
		
		BorderPane pane = new BorderPane();
		
		btnStart = Language.getInstance().createButton("SRT");
		btnLoad = Language.getInstance().createButton("LOD");
		btnMap = Language.getInstance().createButton("MAG");
		btnQuit = Language.getInstance().createButton("QUG");

		// title
		titleView = new Title(Language.getInstance().createText("USA"));
		pane.setTop(titleView);

		// buttons

		VBox left = new VBox(btnStart, btnLoad, btnMap, btnQuit);
		left.getStyleClass().add("vbox");

		left.setAlignment(Pos.CENTER);
		left.setSpacing(20);
		btnQuit.setMinWidth(250);
		btnStart.setMinWidth(250);
		btnLoad.setMinWidth(250);
		btnMap.setMinWidth(250);

		// image
		Image image = ImageLoader.getInstance().get("/start/logo.png");
		ImageView imageView = new ImageView(image);
		imageView.preserveRatioProperty().set(true);
		
		imageView.fitHeightProperty().bind(this.heightProperty().multiply(0.7));
		imageView.fitWidthProperty().bind(this.widthProperty().multiply(0.4));

		VBox right = new VBox(imageView);
		right.setAlignment(Pos.CENTER_RIGHT);
		right.getStyleClass().add("vbox");

		
		// arange boxes
		pane.setRight(right);
		pane.setCenter(left);

		this.getChildren().add(pane);

	}

	/**
	 * Getter for the btnStart for the game
	 * @return btnStart
	 */
	public Button getBtnStart() {
		return btnStart;
	}

	/**
	 * Getter for the btnLoad for the game
	 * @return btnLoad
	 */
	public Button getBtnLoad() {
		return btnLoad;
	}

	/**
	 * Getter for the btnMap for the game
	 * @return btnMap
	 */
	public Button getBtnMap() {
		return btnMap;
	}

	/**
	 * Getter for the btnQuit for the game
	 * @return btnQuit
	 */
	public Button getBtnQuit() {
		return btnQuit;
	}
	
	/**
	 * Getter for Title
	 * @return titleView
	 */
	public Title getTitle() {
		return titleView;
	}

	public void addMsgBox(Pane msgBox) {
		this.getChildren().add(msgBox);
		msgBox.toFront();
		StackPane.setAlignment(msgBox, Pos.TOP_CENTER);
	}

}
