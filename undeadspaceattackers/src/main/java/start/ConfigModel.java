package start;

import java.io.Serializable;
import java.util.ArrayList;

import cards.CardModelI;
import game.GameStatusModel;
import game.GameStatusModelI;
import game.board.BoardModelI;
import player.PlayerI;
import player.Untoaster;

/**
 * This class represents configurations that can be made in the main menu. It
 * contains information about players, the chosen map (and it's planets) and
 * settings.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class ConfigModel implements ConfigModelI, Serializable {


	/**
	 * Settings with numbers that can be changed in settings menu. Default
	 * values are used if numbers are not changed in settings menu. Sequence of
	 * values that can be changed: undeadStrength, leftConqueredPlanets,
	 * nMassAttackCards,
	 */
	private int[] settingsNo;

	/**
	 * Flags for activated features. States whether features are activated.
	 * Sequence of features: evilAI 0, darkColumns 1, abomination 2, improvedWeapons 3,
	 * plague 4 (ger: Heimsuchungen)
	 */
	private boolean[] flags = { false, false, false, false, false };

	/**
	 * List of all players including untoaster if untoaster is chosen.
	 */
	private ArrayList<PlayerI> players;

	/**
	 * List of all normal players meaning all players without the untoaster.
	 */
	private ArrayList<PlayerI> normalPlayers;

	/**
	 * Number of normal players.
	 */
	private int nPlayers;

	/**
	 * Array of chosen roles. States which roles players have chosen. Default
	 * value -1 states that a player has not chosen a role. Meaning of numbers a
	 * player can chose are: 0 Warpmaster, 1 Exorcist, 2 Constructor, 3
	 * Lieutnant, 4 Weapontrader, 5 Weaponarchitect.
	 */
	private int[] roles = new int[] { -1, -1, -1, -1 };

	/**
	 * BoardModel contains information about the map.
	 */
	private BoardModelI boardModel;

	/**
	 * GameStatusModel contains information about attributes that can be changed
	 * in settings. Information about number of mass attack cards and initial
	 * strength goes in here.
	 */
	private GameStatusModelI gameStatusModel;
	
	/**
	 * CardModel contains information about card piles.
	 */
	private CardModelI cardModel;

	/**
	 * Constructor for ConfigModel. Sets default values for undead strength, allowed conquests and number of mass attack cards.
	 * Creates GameStatusModel with default values.
	 */
	public ConfigModel() {
		settingsNo = new int[] { main.Constants.DEFAULT_UNDEADSTRENGTH, main.Constants.DEFAULT_CONQUERED_PLANETS,
				main.Constants.DEFAULT_NMASSATTACK_CARDS };
		gameStatusModel = new GameStatusModel(settingsNo[0], settingsNo[1]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getnMassAttackCards()
	 */
	public int getnMassAttackCards() {
		return settingsNo[2];
	}
	
	@Override
	public void setSettingsNo(int newNumber, int index) {
		// TODO Auto-generated method stub
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#isEvilAI()
	 */
	public boolean isEvilAI() {
		return flags[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#isDarkColumns()
	 */
	public boolean isDarkColumns() {
		return flags[1];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#isAbomination()
	 */
	public boolean isAbomination() {
		return flags[2];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#isImprovedWeapons()
	 */
	public boolean isImprovedWeapons() {
		return flags[3];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#isVisitations()
	 */
	public boolean isPlague() {
		return flags[4];
	}

	/*
	 * (non-Javadoc)
	 * @see start.ConfigModelI#setFlags(boolean, int)
	 */
	public void setFlags(int index, boolean flags) {
		this.flags[index] = flags;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getPlayers()
	 */
	public ArrayList<PlayerI> getPlayers() {
		return normalPlayers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getAllPlayers()
	 */
	public ArrayList<PlayerI> getAllPlayers() {
		return players;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#setPlayers(java.util.ArrayList)
	 */
	public void setPlayers(ArrayList<PlayerI> players) {
		normalPlayers = new ArrayList<>(4);
		for (PlayerI p : players) {
			if (!(p.getRole() instanceof Untoaster)) {
				normalPlayers.add(p);
			}
		}
		this.players = players;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getnPlayers()
	 */
	public int getnPlayers() {
		return nPlayers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#setnPlayers(int)
	 */
	public void setnPlayers(int nPlayers) {
		this.nPlayers = nPlayers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getBoardModel()
	 */
	public BoardModelI getBoardModel() {
		return boardModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#setBoardModel(board.BoardModelI)
	 */
	public void setBoardModel(BoardModelI boardModel) {
		this.boardModel = boardModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getGameStatusModel()
	 */
	public GameStatusModelI getGameStatusModel() {
		return gameStatusModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#setGameStatusModel(game.GameStatusModelI)
	 */
	public void setGameStatusModel(GameStatusModelI gameStatusModel) {
		this.gameStatusModel = gameStatusModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getRoles()
	 */
	public int[] getRoles() {
		return roles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#setRoles(int[])
	 */
	public void setRoles(int[] roles) {
		this.roles = roles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#getCardModel()
	 */
	public CardModelI getCardModel() {
		return cardModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see start.ConfigModelI#setCardModel(cards.CardModel)
	 */
	public void setCardModel(CardModelI cardModel) {
		this.cardModel = cardModel;
	}

}