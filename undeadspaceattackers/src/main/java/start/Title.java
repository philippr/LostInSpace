package start;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import language.Language;

/**
 * Title represents a view showing a text, buttons and flags to change the language.
 * @author Philipp, Fabian, Julia, Ronja
 */
public class Title extends BorderPane {

	/**
	 * Texts that represents the heading. 
	 */
	private Text text;
	
	/**
	 * Button displaying the German flag, if clicked language changes to German.
	 */
	private final Button btnFlagG;
	
	/**
	 * Button displaying the American flag, if clicked language changes to English.
	 */
	private Button btnFlagE;
	
	/**
	 * Button which can be pressend and instructions on how to play the game will appear.
	 */
	private final Button btnInstructions;

	/**
	 * VBox that contains the flag an will be disposed on the right side.
	 */
	HBox topRight;

	/**
	 * Constructor for Title with text, help and change language
	 * 
	 * @param text
	 *            Title for view
	 */
	public Title(Text text) {
		this();
		this.text = text;
		text.setId("title");

		HBox topTitle = new HBox(text);
		topTitle.setAlignment(Pos.TOP_CENTER);
		topTitle.setSpacing(20);
		setCenter(topTitle);
	}

	/**
	 * Constructor for Title with text, help, change language and button to go
	 * back
	 * 
	 * @param text
	 *            Title for the view
	 * @param btn
	 *            Button for action
	 */
	public Title(Text text, Button btn) {
		this(text);

		HBox topLeft = new HBox(btn);
		topLeft.setAlignment(Pos.TOP_LEFT);
		topLeft.setSpacing(20);
		setLeft(topLeft);

	}

	/**
	 * Constructor for Title with a Button, help and change language
	 * @param btn
	 */
	public Title(Button btn) {
		this();

		HBox topLeft = new HBox(btn);
		btn.setMaxWidth(50);
		btn.setMinWidth(50);
		btn.setPrefWidth(50);
		btn.setMaxHeight(30);
		btn.setMinHeight(30);
		btn.setPrefHeight(30);
		btn.setAlignment(Pos.TOP_LEFT);
		setLeft(topLeft);
	}

	/**
	 * Constructor for Title with help and change language, without a title
	 */
	public Title() {
		btnFlagG = new Button("     ");
		btnFlagE = new Button("     ");
		btnInstructions = new Button("?");
		
		btnFlagG.setId("germanFlag");
		btnFlagE.setId("americanFlag");
		btnInstructions.setId("helpBtn");
		
		setButtonSize();
		
		btnInstructions.setOnAction(new EventHandler<ActionEvent>() {
	
			public void handle(ActionEvent event) {
				WebView browser = new WebView();
				WebEngine webEngine = browser.getEngine();
				webEngine.load(Title.class.getResource("help.html").toExternalForm());
				Scene secondScene = new Scene(browser);
				Stage secondStage = new Stage();
				secondStage.setTitle("Instructions");
				secondStage.setScene(secondScene);
				secondStage.show();
			}
		});
	
		// handle ActionEvent, change Language
		setLanguageOnAction();
		
		topRight = new HBox(btnInstructions, btnFlagG);
		topRight.setAlignment(Pos.TOP_RIGHT);
		topRight.setSpacing(5);
	
		setRight(topRight);
		this.setPadding(new Insets(5));
	}

	/**
	 * Size of buttons (flags and instructions) will be set.
	 */
	private void setButtonSize() {
		btnFlagG.setMaxHeight(30);
		btnFlagG.setMinHeight(30);
		btnFlagG.setPrefHeight(30);
		btnFlagG.setMaxWidth(50);
		btnFlagG.setMinWidth(50);
		btnFlagG.setPrefWidth(50);

		btnFlagE.setMaxHeight(30);
		btnFlagE.setMinHeight(30);
		btnFlagE.setPrefHeight(30);
		btnFlagE.setMaxWidth(50);
		btnFlagE.setMinWidth(50);
		btnFlagE.setPrefWidth(50);
		
		btnInstructions.setMaxWidth(30);
		btnInstructions.setMinWidth(30);
		btnInstructions.setPrefWidth(30);
		btnInstructions.setMaxHeight(30);
		btnInstructions.setMinHeight(30);
		btnInstructions.setPrefHeight(30);
	}
	
	/**
	 * Sets Language on action. If the flag is clicked the language will change and the
	 * flag changes.
	 */
	private void setLanguageOnAction() {
		btnFlagG.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent e) {
				topRight.getChildren().add(btnFlagE);
				topRight.getChildren().remove(btnFlagG);
				Language.getInstance().translate(1);
			}
		});

		btnFlagE.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent e) {
				topRight.getChildren().add(btnFlagG);
				topRight.getChildren().remove(btnFlagE);
				Language.getInstance().translate(0);
			}
		});
	}
	
	/**
	 * Getter for text
	 * 
	 * @return text text of View
	 */
	public Text getText() {
		return text;
	}

	/**
	 * Getter for BtnFlagG
	 * 
	 * @return the btnFlagG button for German language
	 */
	public Button getBtnFlagG() {
		return btnFlagG;
	}

	/**
	 * Getter for BtnFlagE
	 * 
	 * @return btnFlagE button for English language
	 */
	public Button getBtnFlagE() {
		return btnFlagE;
	}

	/**
	 * Setter for btnFlagE
	 * 
	 * @param btnFlagE
	 *            button for English language
	 * 
	 */
	public void setBtnFlagE(Button btnFlagE) {
		this.btnFlagE = btnFlagE;
	}

	/**
	 * Getter for btnInstructions
	 * 
	 * @return btnInstructions button for instructions
	 */
	public Button getBtnInstructions() {
		return btnInstructions;
	}

}
