package start;

import java.util.ArrayList;

import cards.CardModelI;
import game.GameStatusModelI;
import game.board.BoardModelI;
import player.PlayerI;

/**
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public interface ConfigModelI {

	/**
	 * Getter for the nMassAttackCards in the game
	 * 
	 * @return nMassAttackCards
	 */
	int getnMassAttackCards();

	/**
	 * Setter for SettingsNo. Sets number of mass attack cards or allowed planet
	 * conquers.
	 * 
	 * @param newNumber
	 *            new value for attribute
	 * @param index
	 *            Indicates which attribute is updated. Index for planet
	 *            conquers is 1, Index for mass attack cards is 2.
	 */
	void setSettingsNo(int newNumber, int index);

	/**
	 * 
	 * @return evilAI
	 */
	boolean isEvilAI();

	

	/**
	 * 
	 * @return darkColumns
	 */
	boolean isDarkColumns();

	
	/**
	 * 
	 * @return abomination
	 */
	boolean isAbomination();


	/**
	 * 
	 * @return improvedWeapons
	 */
	boolean isImprovedWeapons();


	/**
	 * Setter for flags.
	 * Sequence of features: evilAI 0, darkColumns 1, abomination 2, improvedWeapons 3,
	 * plague 4 (ger: Heimsuchungen)
	 * @param flags the flags to set
	 */
	public void setFlags(int index, boolean flags);

	/**
	 * Getter for the Players except Untoaster
	 * 
	 * @return normalPlayers
	 */
	ArrayList<PlayerI> getPlayers();

	/**
	 * Getter for all players included Untoaster
	 * 
	 * @return players
	 */
	ArrayList<PlayerI> getAllPlayers();

	/**
	 * Setter of the players (all)
	 * 
	 * @param players
	 */
	void setPlayers(ArrayList<PlayerI> players);

	/**
	 * Getter for the number of player
	 * 
	 * @return nPlayers
	 */
	int getnPlayers();

	/**
	 * Setter for the number of player
	 * 
	 * @param nPlayers
	 */
	void setnPlayers(int nPlayers);

	/**
	 * Getter for the boardModel
	 * 
	 * @return boardModel
	 */
	BoardModelI getBoardModel();

	/**
	 * Setter for the boardModel
	 * 
	 * @param boardModel
	 */
	void setBoardModel(BoardModelI boardModel);

	/**
	 * Getter for the gameStatusModel
	 * 
	 * @return gameStatusModel
	 */
	GameStatusModelI getGameStatusModel();

	/**
	 * Setter for the gameStatusModel
	 * 
	 * @param gameStatusModel
	 */
	void setGameStatusModel(GameStatusModelI gameStatusModel);

	

	/**
	 * Getter for plague
	 * 
	 * @return plague
	 */
	public boolean isPlague();

	/**
	 * Getter for the roles of the game
	 * 
	 * @return roles
	 */
	public int[] getRoles();

	/**
	 * Setter for the roles of the game
	 * 
	 * @param roles
	 */
	public void setRoles(int[] roles);

	/**
	 * Getter for cardModel
	 * 
	 * @return cardModel
	 */
	public CardModelI getCardModel();

	/**
	 * Setter for the cardModel
	 * 
	 * @param cardModel
	 */
	public void setCardModel(CardModelI cardModel);

}