package main;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import start.MenuController;
import utils.MessageBoxView;

/**
 * Main starts the program.
 *   @author Philipp, Fabian, Julia, Ronja
 *
 */
public class Main extends Application {

	/**
	 * Starts game and shows first View.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Creates a scene and puts the main menu on it.
	 */
	public void start(Stage stage) throws Exception {
		
		
		stage.setTitle("Undead Space Attackers");
		
		MessageBoxView msgBox = new MessageBoxView();
		
		BorderPane msgBoxLayout = new BorderPane();
		msgBox.setAlignment(Pos.CENTER_RIGHT);
		msgBoxLayout.setTop(msgBox);
		BorderPane.setAlignment(msgBox,Pos.TOP_CENTER);
		msgBoxLayout.setPickOnBounds(false);
		
		MenuController menuController = new MenuController(msgBox.getController());
		
		StackPane ghostRoot = new StackPane(menuController.getMenuView());
		StackPane root = new StackPane(msgBoxLayout, ghostRoot);
		
		menuController.setRoot(ghostRoot);
	
		Scene scene = new Scene(root,1500,900);
		scene.getStylesheets().add(Main.class.getResource("stylesheet.css").toString());

		stage.setMinHeight(Constants.CARDBOX_HEIGHT * 4);
		stage.setMinWidth(Constants.INFOBOX_WIDTH * 4);
		msgBoxLayout.toFront();

		stage.setScene(scene);
		stage.show();
	}
}