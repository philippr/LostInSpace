package main;

import javafx.scene.paint.Color;

/**
 * Constants for the game. Contains values that do not change during a game.
 *   @author Philipp, Fabian, Julia, Ronja
 *
 */
public final class Constants {

	/**
	 * number of FindWarptor-ActionCards in the PlayerPile
	 */
	public final static int nFindWarptor = 1;

	/**
	 * number of FindBase-ActionCards in the PlayerPile
	 */
	public final static int nFindBase = 1;

	/**
	 * number of ProtectionShield-ActionCards in the PlayerPile
	 */
	public final static int nProtectionShield = 1;

	/**
	 * sum of ActionCards in the PlayerPile
	 */
	public final static int nActionCards = nFindWarptor + nFindBase + nProtectionShield;

	/**
	 * Colors for players.
	 */
	public static final Color[] PLAYER_COLORS = {Color.BLUEVIOLET, Color.ORANGE, Color.MAGENTA, Color.AQUAMARINE, Color.RED};
	
	/**
	 * Colors for players.
	 */
	public static final Color[] UNDEAD_COLORS = {Color.YELLOW, Color.GREEN, Color.BLUE, Color.RED};
	
	/**
	 * Total numbers of undead of a certain species. Every species has the same total number.
	 */
	public static final int totalUndeads = 25;

	/**
	 * Width of infobox.
	 */
	public static final double INFOBOX_WIDTH = 150;

	/**
	 * Numbers of planets types (not colors).
	 */
	public static final int NPLANET_VARIATIONS = 4;
	
	/**
	 * Number of diffrent undead species types (male or female).
	 */
	public static final int NUNDEAD_VARIATIONS = 2;

	/**
	 * Size of planet in px when planet is load.
	 */
	public static final double PLANET_LOAD_SIZE = 400.0;
	
	/**
	 *  Ratio from planet to cell
	 */
	public static final double PLANET_CELL_RATIO = 0.7; 
	
	/**
	 * ratio of base
	 */
	public static final double BASE_RATIO = 0.25; 
	
	/**
	 * ratio of player's height
	 */
	public static final double PLAYER_HRATIO = 0.27096774; 
	
	/**
	 * ratio of player's width
	 */
	public static final double PLAYER_VRATIO = 0.35;
	
	/**
	 * ratio of card to planet
	 */
	public static final double CARD_PLANET_RATIO = 0.75;

	/**
	 * number of default allowed planet conquests 
	 */
	public static final int DEFAULT_CONQUERED_PLANETS = 7;
	
	/**
	 * number of default  mass attack cards
	 */
	public static final int DEFAULT_NMASSATTACK_CARDS = 4;
	
	/**
	 * number of default undead strength
	 */
	public static final int DEFAULT_UNDEADSTRENGTH = 2;
	
	/**
	 * zoom factor
	 */
	public static final double ZOOM_FACTOR = 0.95;
	
	/**
	 * inverse zoom factor
	 */
	public static final double ZOOM_FACTOR_INVERSE = 1 / ZOOM_FACTOR;
	
	/**
	 * zoom factor rest
	 */
	public static final double ZOOM_FACTOR_REST = 1 - ZOOM_FACTOR;
	
	/**
	 *  zoom factor rest inverse
	 */
	public static final double ZOOM_FACTOR_REST_INVERSE = -1 * ZOOM_FACTOR_REST / ZOOM_FACTOR;
	
	/**
	 * maximum zoom factor
	 */
	public static final double ZOOM_MAX = 12;
	
	/**
	 * minimum zoom factor
	 */
	public static final double ZOOM_MIN = 0.3;

	/**
	 * width of tool bar in map generator
	 */
	public static final double GENERATOR_TOOLBAR_WIDTH = 120.0;
	
	/**
	 * height of tool bar in map generator
	 */
	public static final double GENERATOR_TOOLBAR_BOX_WIDTH = 120 * 0.8;

	/**
	 * height of cardbox in gameView
	 */
	public static final double CARDBOX_HEIGHT = 160;
	
	/**
	 * hight of cardbox with information for players
	 */
	public static final double CARDBOX_CONTENT_HEIGHT = CARDBOX_HEIGHT * 0.9;
	
	/**
	 * width of cardbox
	 */
	public static final double CARDBOX_CARD_WIDTH = CARDBOX_CONTENT_HEIGHT * 2 / 3;
	
	/**
	 * height of box for player card and undead card pile
	 */
	public static final double DECKBOX_HEIGHT = 0.7 * CARDBOX_HEIGHT;


}
