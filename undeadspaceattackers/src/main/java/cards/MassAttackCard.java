package cards;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import utils.ImageLoader;

/**
 * This class represents a mass attack card. Mass attack cards a part of the
 * player card pile.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class MassAttackCard implements Card, Serializable {

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.Card#toPane()
	 */
	@Override
	public Pane toPane() {
		Pane cardPane = new Pane();
		cardPane.setPrefWidth(200);
		cardPane.setPrefHeight(300);
		cardPane.setId("cardLargeFront");

		Rectangle clipPane = new Rectangle();
		clipPane.setId("cardLargeFront");
		clipPane.widthProperty().bind(cardPane.widthProperty());
		clipPane.heightProperty().bind(cardPane.heightProperty());

		Pane border = new Pane();
		border.setId("cardLargeFront");
		border.prefWidthProperty().bind(cardPane.widthProperty());
		border.prefHeightProperty().bind(cardPane.heightProperty());
		border.setStyle("-fx-background-color: null;");

		for (int i = 0; i < 30; i++) {
			ImageView undead = new ImageView(
					ImageLoader.getInstance().get(("/undeads/undead" + ThreadLocalRandom.current().nextInt(0, 4))
							+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png"));

			undead.fitWidthProperty()
					.bind(cardPane.widthProperty().multiply(ThreadLocalRandom.current().nextDouble(0.3, 0.5)));
			undead.fitHeightProperty().bind(undead.fitWidthProperty());

			undead.xProperty()
					.bind(cardPane.widthProperty().multiply(ThreadLocalRandom.current().nextDouble(-0.2, 0.9)));
			undead.yProperty()
					.bind(cardPane.heightProperty().multiply(ThreadLocalRandom.current().nextDouble(-0.2, 0.9)));

			cardPane.getChildren().add(undead);

		}

		cardPane.setClip(clipPane);

		cardPane.getChildren().add(border);
		return cardPane;

	}

}
