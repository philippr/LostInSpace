package cards;

import java.util.ArrayList;
import java.util.Observer;

/**
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public interface CardModelI {
	
	/**
	 * Shuffles the discard pile and adds it on top of the undeadDeck
	 */
	void mixDecks();
	
	ArrayList<UndeadCard> getUndeadPile();
		
	ArrayList<UndeadCard> getDiscardPile();

	public void addObserverX(Observer observer);
	
	public ArrayList<Card> getPlayerPile();

	Card removePlayerCard(int i);

	void setTopPlayerCard(Card object);

	UndeadCard removeUndeadCard(int i);

	UndeadCard getTopUndeadCard();

	void setTopUndeadCard(UndeadCard removeUndeadCard);

	Card getTopPlayerCard();
}