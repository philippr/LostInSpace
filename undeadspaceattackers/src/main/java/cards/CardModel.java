package cards;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import game.board.PlanetModelI;
import player.PlayerI;
import utils.Hand;

/**
 * This class represents the card piles of the game (player card pile and undead
 * card pile).
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class CardModel extends Observable implements CardModelI, Serializable {

	/**
	 * List of all player cards in the player pile.
	 */
	private final ArrayList<Card> playerPile;

	/**
	 * List of all undead cards in the undead pile.
	 */
	private final ArrayList<UndeadCard> undeadPile;

	/**
	 * List of all undead cards in the discard pile.
	 */
	private final ArrayList<UndeadCard> discardPile;

	/**
	 * Top undead card on undead card pile.
	 */
	private UndeadCard topUndeadCard;

	/**
	 * Top player card on player card pile.
	 */
	private Card topPlayerCard;

	/**
	 * Constructor for CardModel. Builds new decks.
	 * 
	 * @param planets
	 *            Planets of the game.
	 * @param nMassAttack
	 *            Number of mass attack cards.
	 * @param normalPlayers
	 *            List of all normal players without untoaster.
	 */
	public CardModel(ArrayList<PlanetModelI> planets, int nMassAttack, ArrayList<PlayerI> normalPlayers) {
		int nPlanets = planets.size();
		int lastBlockWidth;
		int blockWidth;

		playerPile = new ArrayList<>(nPlanets + main.Constants.nActionCards);
		discardPile = new ArrayList<>(nPlanets);
		undeadPile = new ArrayList<>(nPlanets);

		// add PlanetCards to both Decks
		for(PlanetModelI p : planets){
			playerPile.add(new PlayerCard(p.getId(), p.getColor(), p.getType()));
			undeadPile.add(new UndeadCard(p.getId(), p.getColor(), p.getType()));
		}

		// add actionCards to the PlayerDecks
		for (int i = 0; i < main.Constants.nFindBase; i++) {
			playerPile.add(new FindBase());
		}
		for (int i = 0; i < main.Constants.nFindWarptor; i++) {
			playerPile.add(new FindWarptor());
		}
		for (int i = 0; i < main.Constants.nProtectionShield; i++) {
			playerPile.add(new ProtectionShield());
		}

		// shuffle the Decks
		Collections.shuffle(playerPile, new Random(System.nanoTime()));
		Collections.shuffle(undeadPile, new Random(System.nanoTime()));

		// set the PlayerStartHands
		for (PlayerI p : normalPlayers) {
			Hand cards = new Hand();
			for (int i = 0; i < 6 - normalPlayers.size(); i++) {
				cards.add(playerPile.remove(0));

			}
			p.setHand(cards);
		}

		// calculate the size per pile for the MassAttackCards
		// for all |pile|%nMassAttack != 0, the last pile compensates the
		// "carry"
		blockWidth = (int) ((float) playerPile.size() / nMassAttack + 0.5);
		lastBlockWidth = playerPile.size() - (nMassAttack - 1) * (blockWidth);

		// insert MassAttackCards into all (nMassAttack-1)Blocks
		for (int i = 0; i < nMassAttack - 1; i++) {
			playerPile.add(
					ThreadLocalRandom.current().nextInt(i * (blockWidth + 1), i * blockWidth + blockWidth + i + 1),
					new MassAttackCard());
		}
		// insert the last MassAttackCard into the last Block
		playerPile.add(ThreadLocalRandom.current().nextInt(playerPile.size() - lastBlockWidth, playerPile.size() + 1),
				new MassAttackCard());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardModelI#mixDecks()
	 */
	@Override
	public void mixDecks() {
		Collections.shuffle(discardPile, new Random(System.nanoTime()));
		undeadPile.addAll(0, discardPile);
		discardPile.clear();
	}

	/*
	 * (non-Javadoc)
	 * @see cards.CardModelI#getDiscardPile()
	 */
	@Override
	public ArrayList<UndeadCard> getDiscardPile() {
		return discardPile;
	}

	/*
	 * (non-Javadoc)
	 * @see cards.CardModelI#getPlayerPile()
	 */
	@Override
	public ArrayList<Card> getPlayerPile() {
		return playerPile;
	}

	/*
	 * (non-Javadoc)
	 * @see cards.CardModelI#getUndeadPile()
	 */
	@Override
	public ArrayList<UndeadCard> getUndeadPile() {
		return undeadPile;
	}

	/*
	 * (non-Javadoc)
	 * @see cards.CardModelI#addObserverX(java.util.Observer)
	 */
	@Override
	public void addObserverX(Observer observer) {
		addObserver(observer);
	};

	
	public void setTopUndeadCard(UndeadCard card) {
		topUndeadCard = card; // todo take card from discard
		setChanged();
		notifyObservers("t");
	}

	public UndeadCard getTopUndeadCard() {
		return topUndeadCard;
	}

	public void setTopPlayerCard(Card card) {
		this.topPlayerCard = card;
		setChanged();
		notifyObservers("t");
	}

	public Card getTopPlayerCard() {
		return topPlayerCard;
	}

	public Card removePlayerCard(int i) {
		setChanged();
		notifyObservers('p');
		return playerPile.remove(i);
	}

	public UndeadCard removeUndeadCard(int i) {
		setChanged();
		notifyObservers('u');
		return undeadPile.remove(i);
	}
}
