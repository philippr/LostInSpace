package cards;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class handels events with player or undead card piles.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class CardController implements CardControllerI, Serializable {

	/**
	 * CardModel representing the cards of the game.
	 */
	private final CardModelI model;

	/**
	 * Constructor for CardController.
	 * 
	 * @param model
	 *            CardModel
	 */
	public CardController(CardModelI model) {
		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#mixDecks()
	 */
	@Override
	public void mixDecks() {
		model.mixDecks();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#getDiscardPile()
	 */
	@Override
	public ArrayList<UndeadCard> getDiscardPile() {
		return model.getDiscardPile();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#addDiscardPile(cards.UndeadCard)
	 */
	@Override
	public void addDiscardPile(UndeadCard c) {
		model.getDiscardPile().add(c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#flipTopPlayerCard()
	 */
	@Override
	public void flipTopPlayerCard() {
		if (!model.getPlayerPile().isEmpty()) {
			model.setTopPlayerCard(model.removePlayerCard(0));
		} else {
			model.setTopPlayerCard(null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#flipTopUndeadCard()
	 */
	@Override
	public void flipTopUndeadCard() {
		if (!model.getUndeadPile().isEmpty()) {
			addDiscardPile(model.getUndeadPile().get(0));
			model.setTopUndeadCard(model.removeUndeadCard(0));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#getTopPlayerCard()
	 */
	@Override
	public Card getTopPlayerCard() {
		return model.getTopPlayerCard();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#getTopUndeadCard()
	 */
	@Override
	public UndeadCard getTopUndeadCard() {
		return model.getTopUndeadCard();
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#removeTopPlayerCard()
	 */
	@Override
	public void removeTopPlayerCard() {
		model.setTopPlayerCard(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#takeUndeadCard()
	 */
	@Override
	public UndeadCard takeUndeadCard() {
		if (model.getUndeadPile().isEmpty()) {
			return null;
		}
		addDiscardPile(model.getUndeadPile().get(0));
		return model.getUndeadPile().remove(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#removeTopUndeadCard()
	 */
	public void removeTopUndeadCard() {
		model.setTopUndeadCard(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#takeBottomUndeadCard()
	 */
	@Override
	public UndeadCard takeBottomUndeadCard() {
		addDiscardPile(model.getUndeadPile().get(model.getUndeadPile().size() - 1));
		return model.getUndeadPile().remove(model.getUndeadPile().size() - 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#getModel()
	 */
	@Override
	public CardModelI getModel() {
		return model;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.CardControllerI#getPlayerPile()
	 */
	@Override
	public ArrayList<Card> getPlayerPile() {
		return model.getPlayerPile();
	}
}
