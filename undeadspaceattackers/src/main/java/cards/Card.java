package cards;

import java.io.Serializable;

import javafx.scene.layout.Pane;

/**
 * This class represents all cards of the game.
 * Cards can belong to players or to player or undead card pile.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public interface Card extends Serializable {

	/**
	 * This method draws a specific card of the game.
	 * 
	 * @return Pane that represents a card.
	 */
	Pane toPane();

}
