package cards;

import java.util.concurrent.ThreadLocalRandom;

import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import utils.ImageLoader;

/**
 * This class represents the protection shield action card. This card can be
 * used to skip the undead attack at the end of a turn.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class ProtectionShield extends ActionCard {

	@Override
	public Pane toPane() {
		Pane cardPane = new Pane();
		cardPane.setPrefWidth(200);
		cardPane.setPrefHeight(300);
		cardPane.setId("cardLargeFront");
		
		ImageView planet = new ImageView(ImageLoader.getInstance().get(("/planets/planet" + ThreadLocalRandom.current().nextInt(0, 4)) + ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png", main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
		ImageView base = new ImageView(ImageLoader.getInstance().get("/planetTiles/base.png"));
		
		planet.fitWidthProperty().bind(cardPane.widthProperty().multiply(main.Constants.CARD_PLANET_RATIO*0.7));
		planet.fitHeightProperty().bind(planet.fitWidthProperty());

		base.fitWidthProperty().bind(cardPane.widthProperty().multiply(main.Constants.BASE_RATIO * 0.7));
		base.fitHeightProperty().bind(base.fitWidthProperty());

		base.xProperty().bind((cardPane.widthProperty().subtract(base.fitWidthProperty())).divide(2));
		base.yProperty().bind(
				cardPane.heightProperty().subtract(planet.fitHeightProperty().add(base.fitHeightProperty())).divide(2));

		planet.xProperty().bind((cardPane.widthProperty().subtract(planet.fitWidthProperty())).divide(2));
		planet.yProperty()
				.bind(cardPane.heightProperty().subtract(planet.fitHeightProperty().add(base.fitHeightProperty()))
						.divide(2).add(base.fitHeightProperty()));

		Ellipse shield = createShield(cardPane);

		ImageView yellow = new ImageView(ImageLoader.getInstance().get("/undeads/undead0"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png"));
		ImageView green = new ImageView(ImageLoader.getInstance().get("/undeads/undead1"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png"));
		ImageView blue = new ImageView(ImageLoader.getInstance().get("/undeads/undead2"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png"));
		ImageView red = new ImageView(ImageLoader.getInstance().get("/undeads/undead3"
				+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png"));

		yellow.fitWidthProperty().bind(cardPane.widthProperty().multiply(0.3));
		yellow.setPreserveRatio(true);
		green.fitWidthProperty().bind(cardPane.widthProperty().multiply(0.3));
		green.setPreserveRatio(true);
		blue.fitWidthProperty().bind(cardPane.widthProperty().multiply(0.3));
		blue.setPreserveRatio(true);
		red.fitWidthProperty().bind(cardPane.widthProperty().multiply(0.3));
		red.setPreserveRatio(true);

		yellow.setX(2);
		yellow.setY(5);
		green.xProperty().bind(cardPane.widthProperty().multiply(0.7).subtract(5));
		green.setY(5);
		blue.setX(2);
		blue.yProperty().bind((cardPane.heightProperty().subtract(cardPane.widthProperty().multiply(0.3))).subtract(5));
		red.xProperty().bind(cardPane.widthProperty().multiply(0.7).subtract(5));
		red.yProperty().bind((cardPane.heightProperty().subtract(cardPane.widthProperty().multiply(0.3))).subtract(5));

		cardPane.getChildren().addAll(planet, base, shield, yellow, green, blue, red);

		return cardPane;
	}

	/**
	 * Creates an Ellipse displaying the shield.
	 * 
	 * @param cardPane
	 *            Pane representing the card.
	 * @return Ellipse displaying the shield.
	 */
	public Ellipse createShield(Pane cardPane) {
		Ellipse shield = new Ellipse();
		shield.centerXProperty().bind(cardPane.widthProperty().divide(2));
		shield.centerYProperty().bind(cardPane.heightProperty().divide(2));

		shield.radiusXProperty().bind(cardPane.widthProperty().multiply(0.4));
		shield.radiusYProperty().bind(shield.radiusXProperty());

		shield.setStrokeWidth(2);
		shield.setStroke(Color.DEEPSKYBLUE);
		shield.setFill(null);
		return shield;
	}

}
