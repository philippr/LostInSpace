package cards;

/**
 * This class represents an action card that can be used by a player and belongs
 * to the PlayerCard Pile.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public abstract class ActionCard implements Card {

}
