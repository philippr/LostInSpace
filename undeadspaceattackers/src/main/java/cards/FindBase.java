package cards;

import java.util.concurrent.ThreadLocalRandom;

import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import utils.ImageLoader;

/**
 * This class represents the action card FindBase. With this card a base can be build any time.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class FindBase extends ActionCard {

	/*
	 * (non-Javadoc)
	 * @see cards.Card#toPane()
	 */
	@Override
	public Pane toPane() {
		Pane cardPane = new Pane();
		cardPane.setPrefWidth(200);
		cardPane.setPrefHeight(300);
		cardPane.setId("cardLargeFront");
		
		ImageView planet = new ImageView(ImageLoader.getInstance().get(("/planets/planet" + ThreadLocalRandom.current().nextInt(0, 4)) + ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png", main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));
		ImageView base = new ImageView(ImageLoader.getInstance().get("/planetTiles/base.png"));
		base.setEffect(new DropShadow(5, Color.GOLD));
		
		
		planet.fitWidthProperty().bind(cardPane.widthProperty().multiply(main.Constants.CARD_PLANET_RATIO));
		planet.fitHeightProperty().bind(planet.fitWidthProperty());

		base.fitWidthProperty().bind(cardPane.widthProperty().multiply(main.Constants.BASE_RATIO));
		base.fitHeightProperty().bind(base.fitWidthProperty());

		base.xProperty().bind((cardPane.widthProperty().subtract(base.fitWidthProperty())).divide(2));
		base.yProperty().bind(cardPane.heightProperty().subtract(planet.fitHeightProperty().add(base.fitHeightProperty())).divide(2));

		planet.xProperty().bind(cardPane.widthProperty().multiply(0.125));
		planet.yProperty().bind(cardPane.heightProperty().subtract(planet.fitHeightProperty().add(base.fitHeightProperty())).divide(2).add(base.fitHeightProperty()));
		
		cardPane.getChildren().addAll(planet, base);
		return cardPane;
	}

}
