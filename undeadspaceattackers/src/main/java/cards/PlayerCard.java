package cards;

import java.io.Serializable;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import utils.ImageLoader;

/**
 * This class represents all player cards.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class PlayerCard extends PlanetCard implements Serializable {

	/**
	 * to assign the ID, color and type of the player card
	 * 
	 * @param id
	 *            ID of planet.
	 * @param color
	 *            Color of planet.
	 * @param type
	 *            Type of Planet.
	 */
	public PlayerCard(int id, int color, int type) {
		planetId = id;
		planetColor = color;
		planetType = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cards.Card#toPane()
	 */
	@Override
	public Pane toPane() {
		Pane cardPane = new Pane();
		cardPane.setPrefWidth(200);
		cardPane.setPrefHeight(300);
		cardPane.setId("cardLargeFront");

		Label name = new Label(Integer.toString(planetId));
		name.layoutXProperty().bind((cardPane.widthProperty().subtract(name.widthProperty()).divide(2)));
		name.layoutYProperty().bind(cardPane.heightProperty().multiply(2.0 / 3));

		ImageView planet = new ImageView(
				ImageLoader.getInstance().get(("/planets/planet" + planetColor) + planetType + ".png",
						main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));

		planet.fitWidthProperty().bind(cardPane.widthProperty().multiply(main.Constants.CARD_PLANET_RATIO));
		planet.setPreserveRatio(true);

		planet.xProperty().bind(cardPane.prefWidthProperty().multiply(0.125));
		planet.yProperty().bind((name.layoutYProperty().subtract(planet.fitWidthProperty())).divide(2));

		cardPane.getChildren().addAll(planet, name);

		return cardPane;
	}
}