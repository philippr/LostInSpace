package cards;

/**
 * This class represents a planet card. Planet cards belong to normal players
 * and the player card pile.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public abstract class PlanetCard implements Card {
	protected int planetId;
	protected int planetColor;
	protected int planetType;
	protected String name;

	/**
	 * Getter for the planet ID
	 * 
	 * @return the planet ID as an integer
	 */
	public int getPlanetId() {
		return this.planetId;
	}

	/**
	 * Getter for the color of the planet
	 * 
	 * @return the color of the planet as an integer
	 */
	public int getPlanetColor() {
		return this.planetColor;
	}	
	

}
