package cards;

import java.util.ArrayList;

/**
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
public interface CardControllerI {

	/**
	 * mix all decks for the game
	 */
	void mixDecks();

	/**
	 * List that contains all undead cards from discardPile. Is needed if a mass
	 * attack happens.
	 * 
	 * @return discardPile all cards from discard pile
	 */
	ArrayList<UndeadCard> getDiscardPile();

	/**
	 * Adds a card from undead card pile to discard pile.
	 * 
	 * @param c
	 *            UndeadCard
	 */
	void addDiscardPile(UndeadCard c);

	/**
	 * Take an UndeadCard from UndeadPile, set TopUndeadCard and throw it on
	 * DiscardPile
	 * 
	 * @return an UndeadCard oder null if UndeadPile is empty
	 */
	UndeadCard takeUndeadCard();

	/**
	 * take the bottom UndeadCard and throw it on DiscardPile
	 * 
	 * @return bottom Card from DiscardPile
	 */
	UndeadCard takeBottomUndeadCard();

	/**
	 * Getter for the CardModel
	 * 
	 * @return the CardModel
	 */
	public CardModelI getModel();

	/**
	 * Removes top undead card from undead card Pile.
	 */
	public void removeTopUndeadCard();

	/**
	 * Removes top player card from player card pile.
	 */
	public void removeTopPlayerCard();

	/**
	 * Flips top player card. Thereby player can see which card he can take.
	 */
	public void flipTopPlayerCard();

	/**
	 * Flips top undead card. Thereby player can see where the next undead will
	 * appear.
	 */
	public void flipTopUndeadCard();

	/**
	 * Getter for top player card
	 * 
	 * @return card top player card from player card pile.
	 */
	public Card getTopPlayerCard();

	/**
	 * Getter for top undead card
	 * 
	 * @return top undead card from undead card pile.
	 */
	public UndeadCard getTopUndeadCard();

	/**
	 * Getter for player card pile
	 * 
	 * @return Pile of all player cards.
	 */
	ArrayList<Card> getPlayerPile();
}