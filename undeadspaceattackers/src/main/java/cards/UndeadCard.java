package cards;

import java.util.concurrent.ThreadLocalRandom;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import utils.ImageLoader;

/**
 * This class represents the an undead card. Undead cards belong to undead card
 * pile. Undead cards give information about the planet that is attacked.
 * 
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class UndeadCard extends PlanetCard {

	/**
	 * Constructor for UndeadCard creates a card with information about a planet
	 * that can be attacked. To assign the ID, color and type of the UndeadCard.
	 * 
	 * @param id
	 *            ID of planet.
	 * @param color
	 *            Color of planet.
	 * @param type
	 *            Type of planet.
	 */
	public UndeadCard(int id, int color, int type) {
		planetId = id;
		planetColor = color;
		planetType = type;
	}

	/*
	 * (non-Javadoc)
	 * @see cards.Card#toPane()
	 */
	@Override
	public Pane toPane() {
		Pane cardPane = new Pane();
		cardPane.setPrefWidth(200);
		cardPane.setPrefHeight(300);
		cardPane.setId("cardLargeFront");
		ImageView undead = new ImageView(ImageLoader.getInstance()
				.get("/undeads/undead" + planetColor
						+ ThreadLocalRandom.current().nextInt(0, main.Constants.NUNDEAD_VARIATIONS) + ".png",
						main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE));

		undead.fitWidthProperty().bind(cardPane.prefWidthProperty().multiply(0.75));
		undead.setPreserveRatio(true);

		undead.xProperty().bind(cardPane.prefWidthProperty().multiply(0.125));
		undead.yProperty().bind(cardPane.prefWidthProperty().divide(12));

		Label name = new Label(Integer.toString(planetId));
		name.layoutXProperty().bind((cardPane.widthProperty().subtract(name.widthProperty()).divide(2)));
		name.layoutYProperty().bind(cardPane.heightProperty().multiply(2.0 / 3));

		cardPane.getChildren().addAll(undead, name);

		return cardPane;
	}
}