package cards;

import java.util.concurrent.ThreadLocalRandom;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import utils.ImageLoader;

/**
 * 
 * This class represents an action card FindWarptor. 
 * With this card any player can be warped to any planet any time.
 * @author Philipp, Fabian, Julia, Ronja
 *
 */
@SuppressWarnings("serial")
public class FindWarptor extends ActionCard {

	@Override
	public Pane toPane() {
		Pane cardPane = new Pane();
		cardPane.setPrefWidth(200);
		cardPane.setPrefHeight(300);
		cardPane.setId("cardLargeFront");
		
		Image imagea = ImageLoader.getInstance().get(("/planets/planet" + ThreadLocalRandom.current().nextInt(0, 4)) + ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png", main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE);
		Image imageb = ImageLoader.getInstance().get(("/planets/planet" + ThreadLocalRandom.current().nextInt(0, 4)) + ThreadLocalRandom.current().nextInt(0, main.Constants.NPLANET_VARIATIONS) + ".png", main.Constants.PLANET_LOAD_SIZE, main.Constants.PLANET_LOAD_SIZE);
		
		ImageView planeta = new ImageView(imagea);
		ImageView planetb = new ImageView(imageb);		
		
		ImageView planetaClip = new ImageView(imagea);
		ImageView planetbClip = new ImageView(imageb);

		
		planeta.fitWidthProperty().bind(cardPane.widthProperty().multiply(0.6));
		planeta.fitHeightProperty().bind(planeta.fitWidthProperty());
		planetb.fitWidthProperty().bind(planeta.fitWidthProperty());
		planetb.fitHeightProperty().bind(planeta.fitWidthProperty());

		planeta.xProperty().bind(cardPane.widthProperty().multiply(0.1));
		planeta.yProperty().bind(cardPane.widthProperty().multiply(0.1));
		
		planetb.xProperty().bind(cardPane.widthProperty().subtract(planeta.fitWidthProperty()).subtract(planeta.xProperty()));
		planetb.yProperty().bind(cardPane.heightProperty().subtract(planeta.fitHeightProperty()).subtract(planeta.yProperty()));

		
		planetbClip.fitWidthProperty().bind(planeta.fitWidthProperty());
		planetbClip.setPreserveRatio(true);
		planetbClip.xProperty().bind(planeta.xProperty());
		planetbClip.yProperty().bind(planeta.yProperty());

		planetaClip.fitWidthProperty().bind(planeta.fitWidthProperty());
		planetaClip.setPreserveRatio(true);
		planetaClip.xProperty().bind(planetb.xProperty());
		planetaClip.yProperty().bind(planetb.yProperty());

		
		
		Ellipse orange = createOrangeEllipse(planeta);
		Ellipse orangeClip = createOrangeClip(planeta, orange);
		Ellipse blue = createBlueEllipse(planeta, planetb);
		Ellipse blueClip = createBlueClip(planeta, planetb);
		

		planetbClip.setClip(orangeClip);
		planetaClip.setClip(blueClip);
		
		cardPane.getChildren().addAll(planeta,planetbClip,orange, planetb, planetaClip, blue);

		return cardPane;
	}
	
	public Ellipse createOrangeEllipse(ImageView planeta){
		Ellipse orange = new Ellipse();
		orange.centerXProperty().bind(planeta.fitWidthProperty().divide(2).add(planeta.xProperty()));
		orange.centerYProperty().bind(orange.centerXProperty());
		orange.radiusXProperty().bind(planeta.fitHeightProperty().divide(4));
		orange.radiusYProperty().bind(planeta.fitHeightProperty().divide(2.2));
		orange.setFill(null);
		orange.setStroke(Color.DARKORANGE);
		orange.setStrokeWidth(2);
		return orange;
	}
	
	public Ellipse createOrangeClip(ImageView planeta, Ellipse orange ) {
		Ellipse orangeClip = new Ellipse();
		orangeClip.centerXProperty().bind(planeta.fitWidthProperty().divide(2).add(planeta.xProperty()));
		orangeClip.centerYProperty().bind(orange.centerXProperty());
		orangeClip.radiusXProperty().bind(planeta.fitHeightProperty().divide(4));
		orangeClip.radiusYProperty().bind(planeta.fitHeightProperty().divide(2.2));
		orangeClip.setStrokeWidth(0);
		
		return orangeClip;
	}
	
	public Ellipse createBlueEllipse(ImageView planeta, ImageView planetb ) {
		Ellipse blue = new Ellipse();
		blue.centerXProperty().bind(planeta.fitWidthProperty().divide(2).add(planetb.xProperty()));
		blue.centerYProperty().bind(planeta.fitWidthProperty().divide(2).add(planetb.yProperty()));
		blue.radiusXProperty().bind(planeta.fitHeightProperty().divide(4));
		blue.radiusYProperty().bind(planeta.fitHeightProperty().divide(2.2));
		blue.setFill(null);
		blue.setStroke(Color.DODGERBLUE);
		blue.setStrokeWidth(2);
		return blue;
	}
	
	public Ellipse createBlueClip(ImageView planeta, ImageView planetb ) {
		Ellipse blueClip = new Ellipse();
		blueClip.centerXProperty().bind(planeta.fitWidthProperty().divide(2).add(planetb.xProperty()));
		blueClip.centerYProperty().bind(planeta.fitWidthProperty().divide(2).add(planetb.yProperty()));
		blueClip.radiusXProperty().bind(planeta.fitHeightProperty().divide(4));
		blueClip.radiusYProperty().bind(planeta.fitHeightProperty().divide(2.2));
		blueClip.setStrokeWidth(0);
		return blueClip;
	}

}
